//
//  LTFirstTimeBaseView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @typedef LTFTAnimationCompletion
 *  @brief Callback is called when show animation is finished.*/
typedef void(^LTFTAnimationCompletion)();

/*! @typedef LTFTHideAnimationDidFinish
 *  @brief Callback is called when hide animation is finished.*/
typedef void(^LTFTHideAnimationDidFinish)();



@class LTFTBackgroundView;
/*! @class LTFirstTimeBaseView
 *  @brief Base class for creating wizard. Use this class to create a new wizard page.*/
@interface LTFirstTimeBaseView : UIView

/*! Background view. @see LTFTBackgroundView*/
@property (weak, nonatomic) IBOutlet LTFTBackgroundView* animatedContentView;

/*! Title of page.*/
@property (weak, nonatomic) IBOutlet UILabel* titleLabel;

/*! Icon of page.*/
@property (weak, nonatomic) IBOutlet UIImageView* iconView;

/*! Description of page.*/
@property (weak, nonatomic) IBOutlet UILabel* descLabel;

/*! Hide animation callback.*/
@property (copy, nonatomic) LTFTHideAnimationDidFinish onHideAnimationFinished;

/*! Label for show page number of wizard.*/
@property (weak, nonatomic) IBOutlet UILabel *pageNumberLabel;

#pragma mark - Animation methods
/*! @brief Create aniomation for show page from bottom to top.
 *  @param view View show need to animate.*/
- (void)createAnimationViewToUp:(UIView*)view;

/*! @brief Create aniomation for show page from right to left.
 *  @param view View show need to animate.*/
- (void)createAnimationViewToLeft:(UIView*)view;

/*! @brief Create aniomation for show page from left to right.
 *  @param view View show need to animate.*/
- (void)createAnimationViewToRight:(UIView*)view;

/*! @brief Create aniomation for hide page.
 *  @param view View show need to animate.
 *  @param newPoint Coordinate for animation whhich using to move card when it needs to be hidden.*/
- (void)createHideAnimationForView:(UIView*)view toNewPoint:(CGPoint)newPoint;

/*! @brief Play all aniamtion for top.*/
- (void)animateAllViewsToUp;

/*! @brief Play all aniamtion for left.*/
- (void)animateAllViewsToLeft;

/*! @brief Play all aniamtion for right.*/
- (void)animateAllViewsToRight;

/*! @brief Play all aniamtion for hide to left.*/
- (void)animateAllViewForHideToLeft;

/*! @brief Show current page from down with aniamation.
 *  @param flag If YES card will show with animation.*/
- (void)animateShowFromDownWithSubviewsAnimation:(BOOL)flag;

/*! @brief Move card to left and hide it.*/
- (void)animateHideToLeft;


#pragma mark - setters
/*! @brief This methods will hide all subviews on current page.*/
- (void)needToHideAllSubviews;

@end
