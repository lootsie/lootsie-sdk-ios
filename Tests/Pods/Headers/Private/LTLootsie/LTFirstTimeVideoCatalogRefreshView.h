//
//  LTFirstTimeVideoCatalogRefreshView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/22/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @typedef LTFirstTimeVideoRefreshViewDidClose
 *  @brief Callback is called when video refresh view did close.*/
typedef void(^LTFirstTimeVideoRefreshViewDidClose)();


/*! @class LTFirstTimeVideoCatalogRefreshView
 *  @brief View for showing first time video refresh view.*/
@interface LTFirstTimeVideoCatalogRefreshView : UIView

/*! Close callback for view. @see LTFirstTimeVideoRefreshViewDidClose*/
@property (nonatomic, strong)   LTFirstTimeVideoRefreshViewDidClose onClose;

/*! Background image view.*/
@property (nonatomic, strong)   UIImage* backgroundImage;

@end
