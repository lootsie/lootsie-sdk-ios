//
//  LTUIAnimationDelegate.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/1/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

/*! @typedef LTUIAnimationCompleteBlock
 *  @brief Callback that is called when animation is finished.*/
typedef void(^LTUIAnimationCompleteBlock)();


/*! @protocol LTUIAnimationDelegate
 *  @brief Methods for add animation to controller.*/
@protocol LTUIAnimationDelegate <NSObject>

/*! @brief Play custom animation for show controller.
 *  @param startPoint Start point for start animate show child controller.
 *  @param block Called when animation was finished. @see LTUIAnimationCompleteBlock*/
- (void)animateFromStartPoint: (CGPoint)startPoint completion: (LTUIAnimationCompleteBlock)block;

@end
