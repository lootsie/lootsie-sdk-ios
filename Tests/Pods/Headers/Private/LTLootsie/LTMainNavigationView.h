//
//  LTMainNavigationView.h
//  LTLootsie iOS Example
//
//  Copyright © 2017 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTNibLoadingView.h"

@class LTMainNavigationView;             //define class, so protocol can see MyClass
@protocol LTMainNavigationViewDelegate <NSObject>   //define delegate protocol
- (void) navigateToPage: (LTMainNavigationView *) sender andPageSelected: (NSInteger) page;
@end //end protocol


IB_DESIGNABLE

/*! @brief The main navigation bar.
 */
@interface LTMainNavigationView : LTNibLoadingView


@property (nonatomic, weak) id <LTMainNavigationViewDelegate> delegate;

/*! Width of line.*/
@property (nonatomic)           IBInspectable NSUInteger lineWidth;

/*! Color for line.*/
@property (nonatomic)           IBInspectable UIColor* lineColor;

/*! Selected page index.*/
@property (nonatomic, assign)   NSInteger selectedIndex;

/*! Page index did update. @see LTHeaderNavigationBarDidChangeSelectionIndex*/
//@property (nonatomic, copy)     LTHeaderNavigationBarDidChangeSelectionIndex onSelectionIndexChenged;

/*! Count of buttons in navigation bar.*/
@property (nonatomic, assign, readonly)     NSInteger buttonsCount;

/*! Favorite icon for favorite button.*/
@property (nonatomic, weak) IBOutlet UIView *favoriteIcon;


@end
