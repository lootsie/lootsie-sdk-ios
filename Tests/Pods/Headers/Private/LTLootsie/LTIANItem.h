//
//  LTIANItem.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LTModelEntity.h"

/*! Types fo notifications.*/
typedef NS_ENUM(NSInteger, LTIANType) {
    LTIANUnknown,       /*! Unknown notification.*/
    LTIANGeneric,       /*! A generic IAN MAY include a point value. This will typically be used to display
                         *  general messages to the user such as when they earn points, earn an achievement,
                         *  or other generic messages which offer no action.*/
    LTIANCatalog,       /*! A catalog IAN is meant to prompt the user to open the catalog view and MUST include
                         *  a button or link that opens the marketplace view.*/
    LTIANReward,        /*! A reward IAN is meant to alert users of price drops, rewards that users can afford 
                         *  currently, or any other reason Lootsie may want to drive users to a specific reward card.*/
    LTIANVideo          /*! A video IAN is shown to the user to prompt them to watch a video to earn points. 
                         *  Applications MUST provide a button or link for the user to open a video.*/
};


/*! @class LTIANItem
 *  @brief Base class for saving notification object item.*/
@interface LTIANItem : LTModelEntity

/*! Unique id of notification.*/
@property (readonly, nonatomic, assign)     NSInteger ianId;

/*! Type of notification. @see LTIANType*/
@property (readonly, nonatomic, assign)     LTIANType type;

/*! Unique id of notification.*/
@property (readonly, nonatomic, strong)     NSString* title;

/*! Unique id of notification.*/
@property (readonly, nonatomic, strong)     NSString* descStr;


/*! @brief Create a new notification.
 *  @param uid Unique id of notification.
 *  @param type Type of notification.
 *  @param title Title of notification.
 *  @param desc Description of notification.*/
- (instancetype)initWithId: (NSInteger)uid
                      type: (LTIANType)type
                     title: (NSString*)title
               description: (NSString*)desc;

/*! @brief Get list of notifications list.
 *  @return List of test notifications list.*/
+ (NSArray *)getTestIANItems;

@end
