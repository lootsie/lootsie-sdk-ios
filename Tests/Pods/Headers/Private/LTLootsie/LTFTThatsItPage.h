//
//  LTFTThatsItPage.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/6/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFirstTimeBaseView.h"

/*! @typedef LTFTThatsItPageMaybeLaterButtonClicked
 *  @brief Callback is called when user click on "maybe later" button.*/
typedef void(^LTFTThatsItPageMaybeLaterButtonClicked)();

/*! @typedef LTFTThatsItPageSingUpNowButtonClicked
 *  @brief Callback is called when user click on "Ok, Im in" button.*/
typedef void(^LTFTThatsItPageSingUpNowButtonClicked)();


/*! @class LTFTThatsItPage
 *  @brief Page for show information about achievement.*/
@interface LTFTThatsItPage : LTFirstTimeBaseView

/*! Close wizard.*/
@property (nonatomic, copy) LTFTThatsItPageMaybeLaterButtonClicked onMaybeLaterButtonClicked;

/*! Close wizard and open account page for sign up.*/
@property (nonatomic, copy) LTFTThatsItPageSingUpNowButtonClicked onSignUpNowButtonClicked;

@end
