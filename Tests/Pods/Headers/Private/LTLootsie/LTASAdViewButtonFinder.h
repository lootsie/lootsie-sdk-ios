//
//  LTASAdViewButtonFinder.h
//  LTLootsie iOS Example
//
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AerServSDK/AerServSDK.h>


/*! @protocol LTDatabaseProtocol
 *  @brief Additional methods for work with data from cache.*/
@protocol LTASAdViewButtonFinderProtocol <NSObject>


/*! @brief Called when the close button on the ad view has been pressed. */
- (void)closeButtonPressed;

@end



@interface LTASAdViewButtonFinder : NSObject

@property (nonatomic, weak)     id<LTASAdViewButtonFinderProtocol> delegate;


-(void) attachToAdView: (ASAdView*) videoAdView;

- (void) findButtonsInSubviews:(NSArray *)subviews withTargetArray: (NSMutableArray*) target;

@end
