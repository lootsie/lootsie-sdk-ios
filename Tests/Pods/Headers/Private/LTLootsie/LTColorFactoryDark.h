//
//  LTColorFactoryDark.h
//  LTLootsie iOS Example
//
//  Created by neon on 10.08.16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTColorFactory.h"

/*! @class LTColorFactoryDark
 *  @brief Dark color style for UI elements.*/
@interface LTColorFactoryDark : LTColorFactory

@end
