//
//  LTEventManager+Private.h
//  LTLootsie iOS Example
//
//  Created by Macmini on 8/31/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTEventManager.h"

@interface LTEventManager (Private)

/*! @brief Add Lootsie SDK event to stack.
 *  @param eventID Event unique id.
 *  @param number Additional number.*/
- (void)addBaseEvent:(LTBaseUserEventID)eventID additionalNumber:(NSNumber *)number;

@end
