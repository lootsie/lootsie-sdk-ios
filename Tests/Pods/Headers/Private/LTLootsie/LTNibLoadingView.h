//
//  LTNibLoadingView.h
//  LTLootsie iOS Example
//

//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface LTNibLoadingView : UIView

@property (nonatomic, weak)   UIView* view; // Should be weak
- (void) nibSetup;

@end

