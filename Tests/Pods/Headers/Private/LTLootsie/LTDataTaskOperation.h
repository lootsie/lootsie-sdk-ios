//  LTURLSessionTaskOperation.h
//  Created by Fabio Teles on 7/14/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTOperation.h"

/*! @typedef LTRequestBuilder
 *  @brief Generate request object.*/
typedef NSURLRequest *(^LTRequestBuilder)(NSError *__autoreleasing *error);


@class LTNetworkManager;
/*! @class LTDataTaskOperation
 *  @brief Operation for sending requests to server and receiving responses. This operation parses response and generates correct data for using it as a result of request.*/
@interface LTDataTaskOperation : LTOperation

/*! Session task to send request.*/
@property (nonatomic, readonly, strong) NSURLSessionDataTask *task;

/*! Server response information object. This object will be use for analyze status code of request.*/
@property (nonatomic, readonly, strong) NSURLResponse *response;

/*! Response object with result of request to server.*/
@property (nonatomic, readonly, strong) id responseObject;

/*! Request object which will send to server.*/
@property (nonatomic, strong) NSURLRequest *request;


/*! @brief Creates a DataTask operation with a requestBuilder block. A block is used here to
 *  perform a lazy request build, allowing others operations to occur and retrieve data
 *  before it's used here. */
- (instancetype)initWithRequestBuilder:(LTRequestBuilder)requestBuilder;

/*! @brief Helper method that returns the HTTP method used by this data task operation if the
 *  request has been built, otherwise returns nil.*/
- (NSString *)HTTPMethod;

/*! @brief Called right after data task finishes. Gives a chance to subclasses to perform any
 *  further parsing on the responseObject and even generate a new error. Original
 *  implementation just returns nil.*/
- (NSError *)dataTaskDidFinishSuccessfully;

/*! @brief Gives subclasses a chance to format errors coming from the network. The basic
 *  implementation checkes for a common pattern returned from Lootsie's API and does a
 *  very basic formatting, so if you do any transformation there is no need to call super. */
- (NSError *)formatDataTaskError:(NSError *)error;

- (void) processDataTaskError:(NSError *)error;

/*! @brief Get global headers for request.*/
+ (NSDictionary *)header;

@end
