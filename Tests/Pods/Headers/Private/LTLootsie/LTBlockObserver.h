//  LTBlockObserver.h
//  Created by Fabio Teles on 7/15/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>
#import "LTOperation.h"

/*! @class LTBlockObserver
 *  @brief Observer that can be used if you need to call operation as a block of code.*/
@interface LTBlockObserver : NSObject <LTOperationObserver>

/*! @brief Create a new operation with block of code.
 *  @param startHandler Will be called whe operation will start to work.
 *  @param produceHandler Code of operation.
 *  @param finishHandler Will be called when operation will be finished.*/
- (instancetype)initWithStartHandler:(void (^)(LTOperation *))startHandler
                      produceHandler:(void (^)(LTOperation *, NSOperation *))produceHandler
                       finishHandler:(void (^)(LTOperation *, NSArray *))finishHandler;

@end
