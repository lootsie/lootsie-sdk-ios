//
//  LTIANViewController.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTIANItem.h"
#import "LTIANBannerView.h"

/*! @typedef LTIANControllerDidClose
 *  @brief Callback is called when controller did close.*/
typedef void(^LTIANControllerDidClose)();

/*! @typedef LTIANControllerDidAction
 *  @brief Callback is called when user click to action button in IAN view.
 *  @param item IAN info object.*/
typedef void(^LTIANControllerDidAction)(LTIANItem* item);



/*! @class LTIANViewController
 *  @brief Controller for showing IAN items.*/
@interface LTIANViewController : UIViewController

/*! Banner view.*/
@property (nonatomic, weak)     IBOutlet LTIANBannerView* bannerView;

/*! IAN items list.*/
@property (nonatomic, strong)   NSArray<LTIANItem *> *items;

/*! Banner text.*/
@property (nonatomic, strong)   NSString* bannerTitle;

/*! Delay in seconds for auto hide if user don't interact with banner.*/
@property (nonatomic, assign)   CGFloat delay;

/*! Called when need to close. @see LTIANControllerDidClose*/
@property (nonatomic, copy)     LTIANControllerDidClose onClose;

/*! Called when need to start some action. @see LTIANControllerDidAction*/
@property (nonatomic, copy)     LTIANControllerDidAction onAction;

@end
