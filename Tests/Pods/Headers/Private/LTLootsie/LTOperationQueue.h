//  LTOperationQueue.h
//  Created by Fabio Teles on 7/15/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>

@class LTOperationQueue;
/*! @protocol LTOperationQueueDelegate
 *  @brief Queue methods for working with operations.*/
@protocol LTOperationQueueDelegate <NSObject>

/*! @brief This method will call when operation will added for work.
 *  @param queue Queue instance object.
 *  @param operation Object instance object.*/
- (void)operationQueue:(LTOperationQueue *)queue willAddOperation:(NSOperation *)operation;

/*! @brief This method will call when operation did finish work.
 *  @param queue Queue instance object.
 *  @param operation Object instance object.
 *  @param errors List of errors for operations.*/
- (void)operationQueue:(LTOperationQueue *)queue operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors;

/*! @brief This method will call when all operations from queue did finishe work.
 *  @param queue Queue instance object.*/
- (void)operationQueueDidFinishAllCurrentTasks:(LTOperationQueue *)queue;

@end



/*! @class LTOperationQueue
 *  @brief Queue for operations running.*/
@interface LTOperationQueue : NSOperationQueue

/*! Additional methods for queue.*/
@property (nonatomic, weak) id<LTOperationQueueDelegate> delegate;

@end
