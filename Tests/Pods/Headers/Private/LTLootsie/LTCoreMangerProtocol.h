//  LTCoreManagerProtocol.h
//  Created by Alexander Dovbnya on 4/13/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>
#import "LTData.h"
#import "LTDatabaseProtocol.h"
#import "LTUserActivity.h"

/*! SDK status code.*/
typedef NS_ENUM(NSUInteger, LTSDKStatus) {
    LTSDKStatusNotInitialized = 0,      /*! SDK is not initialized.*/
    LTSDKStatusWaitingInitialization,   /*! SDK is waitnig initialization.*/
    LTSDKStatusInitializing,            /*! SDK is initializing. Load all needed data and send all needed request to server.*/
    LTSDKStatusReady,                   /*! SDK is initialized and ready for work.*/
    LTSDKStatusFailed,                  /*! SDK has failed. All data is clear. UI will not shown.*/
    LTSDKStatusNotAuthorized /* SDK is not authorized by user. UI will not be shown. */
};


/*! @protocol LTCoreManagerProtocol
 *  @brief Methods and properties for core manager.*/
@protocol LTCoreManagerProtocol <NSObject>

/*! SDK info object. @see LTData*/
@property (nonatomic, readonly) LTData *data;

/*! Lootsie SDK status.*/
@property (nonatomic, readonly, assign) LTSDKStatus status;


/*! @brief Create or return core manager instance.*/
+ (instancetype)sharedManager;

/*! @brief Set custom database delegate for save info to cache.
 *  @param database Object with database methods for save/load data.*/
- (void)setCustomDatabase:(id<LTDatabaseProtocol>)database;

/*! @brief Start init Lootsie SDK with app key.
 *  @param appKey Application key for init Lootsie SDK.
 *  @param success Callback is called when SDK will initialized correct.
 *  @param failure Callback is called if SDk was generate error while initializing.*/
- (void)startEngineWithAppKey:(NSString *)appKey
                      success:(void (^)(void))success
                      failure:(void (^)(NSError *error))failure;

/*! @brief Send user event to server. Use this method if you need post special event to Lootsie server.
 *  @param userActivity Application custom event.
 *  @param success Callback is called when SDK will initialized correct.
 *  @param failure Callback is called if SDk was generate error while initializing.*/
- (void)sendUserActivity:(NSArray<LTUserActivity *> *)userActivity
                 success:(void (^)(void))success
                 failure:(void (^)(NSError *error))failure;
@end
