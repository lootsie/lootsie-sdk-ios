//  LTManager.h
//  Created by Fabio Teles on 7/30/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>
#import "LTOperationQueue.h"
#import "LTCoreMangerProtocol.h"
#import "LTEventManager.h"

extern NSString * const LTInitializationCompleteNotification;
extern NSString * const LTUserAccountUpdatedNotification;
extern NSString * const LTIANNotification;
extern NSString * const LTChangeFavoriteRewardNotification;
extern NSString * const LTVideoInterstitialWathcedNotification;
extern NSString * const LTRewardRedeemedNotification;

extern NSString * const LTAchievementUserInfoKey;
extern NSString * const LTSavedInfoUserInfoKey;
extern NSString * const LTRewardUserInfoKey;
extern NSString * const LTUserUserInfoKey;
extern NSString * const LTIANKey;

extern NSString * const LTErrorDomain;
extern NSInteger const LTSDKFailedInitializingErrorCode;
extern NSString * const LTNotAuthorizedKey;

@class LTUserAccount, LTBlockOperation;
/*! @class LTManager
 *  @brief LTManager singleton class. This class simplifies server interaction and stores Lootsie loyalty data.
 *  All callbacks are implemented in blocks and are called async.*/
@interface LTManager : NSObject <LTOperationQueueDelegate, LTCoreManagerProtocol> {
    LTOperationQueue *_generalQueue;    /*! General operation queue.*/
}

/*! Return the data object in memory for easy access.*/
@property (nonatomic, readonly) LTData *data;

/*! Application key for initialize Lootsie SDK.*/
@property (nonatomic, copy) NSString *appKey;

/*! Manager for work with event. This method saves events to cache and send it to server.*/
@property (nonatomic, strong) LTEventManager *eventManager;

/*! Current status of the SDK. See LTSDKStatus */
@property (nonatomic, readonly, assign) LTSDKStatus status;


/*! @brief Returns the LTManager object.*/
+ (instancetype)sharedManager;

/*! @brief You can optionally define a custom database class that implements the LTDatabaseProtocol
 *  and set it here before initializing the SDK. If you try to call this method after
 *  initializing nothing will occur and an error message will be logged. If not set,
 *  by default an instance of LTDatabase class will be used as database.
 *  @param database Custom class with database methods.*/
- (void)setCustomDatabase:(id<LTDatabaseProtocol>)database;

/*! @brief installs Lootsie app identificator. Launches session start if lazy initialization is disabled or there are requests to server.
 *  @param appKey  - app identificator that is obtained in dashboard.
 *  @param success callback is called after successful session start.
 *  @param failure callback is called when server session failed to start.*/
- (void)startEngineWithAppKey:(NSString *)appKey
                      success:(void (^)(void))success
                      failure:(void (^)(NSError *error))failure;

/*! @brief Sends user activity to server for analytics.
 *  @param userActivity Object of user activity.
 *  @param success callback is called after successful send to server.
 *  @param failure callback is called if was received error.*/
- (void)sendUserActivity:(NSArray<LTUserActivity *> *)userActivity
                 success:(void (^)(void))success
                 failure:(void (^)(NSError *error))failure;

/*!
 *  @brief Adds to the event queue the amount / spend
 *
 *  @param cost    cost as micros
 */
- (void)registerSpend:(NSInteger)cost;

/*! @brief Checks if the devices has been locally deactivated by the user
 *  @return boolean indicating the account has been deactivated. */
- (BOOL) hasAccountBeenDeactivated;

/*! @brief Performs the start engine */
- (void) completeStartEngine;


@end
