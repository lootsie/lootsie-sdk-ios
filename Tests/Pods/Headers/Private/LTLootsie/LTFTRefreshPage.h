//
//  LTFTRefreshPage.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/7/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFirstTimeBaseView.h"

/*! @class LTFTRefreshPage
 *  @brief Page for show information about video refresh.*/
@interface LTFTRefreshPage : LTFirstTimeBaseView

@end
