//
//  LTCatalogRewardCell.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/17/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTBaseCatalogRewardCell.h"

/*! @class LTCatalogRewardCell
 *  @brief Standard reward cell.*/
@interface LTCatalogRewardCell : LTBaseCatalogRewardCell

/*! @brief Get show constrain constant.*/
- (CGFloat)showConstraintConstant;

/*! @brief Get hide constrain constant.*/
- (CGFloat)hideConstraintConstant;

@end
