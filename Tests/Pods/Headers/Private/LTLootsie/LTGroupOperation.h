//  LTGroupOperation.h
//  Created by Fabio Teles on 7/15/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTOperation.h"

/*! @class LTGroupOperation
 *  @brief A subclass of `Operation` that executes zero or more operations as part of its
 *  own execution. This class of operation is very useful for abstracting several
 *  smaller operations into a larger operation. As an example, the `GetEarthquakesOperation`
 *  is composed of both a `DownloadEarthquakesOperation` and a `ParseEarthquakesOperation`.
 *  Additionally, `GroupOperation`s are useful if you establish a chain of dependencies,
 *  but part of the chain may "loop". For example, if you have an operation that
 *  requires the user to be authenticated, you may consider putting the "register"
 *  operation inside a group operation. That way, the "register" operation may produce
 *  subsequent operations (still within the outer `GroupOperation`) that will all
 *  be executed before the rest of the operations in the initial chain of operations.*/
@interface LTGroupOperation : LTOperation

/*! @brief Create a new group with list of operations.
 *  @param operations List of operations.*/
- (instancetype)initWithOperations:(NSArray *)operations;

/*! @brief Create a new group with list of operations.
 *  @param operations List of operations.*/
+ (instancetype)groupWithOperations:(NSArray *)operations;

/*! @brief Create a new group with list of operations.
 *  @param operations List of operations.
 *  @param count Number of operation which will run as parallel operation.*/
+ (instancetype)groupWithOperations:(NSArray *)operations maxConcurrentOperationCount:(NSInteger)count;

/*! @brief Add new operation to current group.
 *  @param operation Operation object.*/
- (void)addOperation:(NSOperation *)operation;

/*! @brief Note that some part of execution has produced an error.
 *  Errors aggregated through this method will be included in the final array
 *  of errors reported to observers and to the `finished(_:)` method.
 *  @param error Error object.*/
- (void)aggregateError:(NSError *)error;

/*! @brief Call when operation did finish.
 *  @param operation Operation which was finished.
 *  @param errors List of errors for operation.*/
- (void)operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors;

@end