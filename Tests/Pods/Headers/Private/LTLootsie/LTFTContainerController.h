//
//  LTFTContainerController.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @typedef LTFTContainerControllerDidClose
 *  @brief Callback is called when user close wizard.*/
typedef void(^LTFTContainerControllerDidClose)();


/*! @class LTFTContainerController
 *  @brief Controller for show first time wizard.*/
@interface LTFTContainerController : UIViewController

/*! Close action callback.*/
@property (nonatomic, copy)     LTFTContainerControllerDidClose closeAction;

@end
