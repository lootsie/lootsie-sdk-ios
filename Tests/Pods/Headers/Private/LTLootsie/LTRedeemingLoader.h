//
//  LTRedeemingLoader.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/8/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTRedeemingLoader
 *  @brief Redeeming loader view. This view will load special step of redemption wizard.*/
@interface LTRedeemingLoader : UIView

@end
