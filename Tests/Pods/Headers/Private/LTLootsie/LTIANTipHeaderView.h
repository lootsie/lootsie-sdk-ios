//
//  LTIANTipHeaderView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/22/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTIANTipHeaderView
 *  @brief View show tooltip when user tries to see IAN for the first time.*/
@interface LTIANTipHeaderView : UICollectionReusableView

@end
