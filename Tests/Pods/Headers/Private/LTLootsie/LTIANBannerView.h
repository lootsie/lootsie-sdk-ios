//
//  LTIANBannerView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>


@class LTIANBannerView;
/*! @typedef LTIANBannerDidTouch
 *  @brief Callback is called when user tap to the banned view.*/
typedef void(^LTIANBannerDidTouch)(LTIANBannerView* bannerView);

/*! @typedef LTIANBannerDidViewButtonTouch
 *  @brief Called is called when user tap to "view" button.*/
typedef void(^LTIANBannerDidViewButtonTouch)(LTIANBannerView* bannerView);

/*! @typedef LTIANBannerDidAnimationComplete
 *  @brief Callback is called when animation did finishe.*/
typedef void(^LTIANBannerDidAnimationComplete)(void);



IB_DESIGNABLE
/*! @class LTIANBannerView
 *  @brief View show show infromation about new In-App Notification.*/
@interface LTIANBannerView : UIView

/*! Number of poinst for show on the left of banner.*/
@property (nonatomic, assign)   NSInteger leftPoints;

/*! Some of notification can has a icon. This property usage for show this icon.*/
@property (nonatomic, strong)   UIImage* leftImage;

/*! Banner text. Use this propertin form show some text.*/
@property (nonatomic, strong)   NSString* title;

/*! YES if banner was expanded. In this case use must see all notifications.*/
@property (nonatomic, assign)   BOOL expanded;

/*! Default text for right button. By Default it's 'VIEW'.*/
@property (nonatomic, strong)   NSString* viewButtonTitle;

/*! Content of call UI elements for show info.*/
@property (nonatomic, weak)     IBOutlet UIView* contantView;


/*! Called when user tap to banner. @see LTIANBannerDidTouch*/
@property (nonatomic, copy)     LTIANBannerDidTouch onBannerTouch;

/*! Called when user tap to right button. @see LTIANBannerDidViewButtonTouch*/
@property (nonatomic, copy)     LTIANBannerDidViewButtonTouch onViewButtonTap;


/*! @brief Play show animations for all UI components.
 *  @param completion Called when animation did finish. @see LTIANBannerDidAnimationComplete*/
- (void)playAnimationsWithCompletion:(LTIANBannerDidAnimationComplete)completion;

/*! @brief Play hide animations for all UI components.
 *  @param completion Called when animation did finish. @see LTIANBannerDidAnimationComplete*/
- (void)hideWithAnimationAndCompletion:(LTIANBannerDidAnimationComplete)completion;

@end
