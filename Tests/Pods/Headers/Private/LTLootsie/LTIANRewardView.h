//
//  LTIANRewardView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/21/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANGenericView.h"


@class LTCatalogReward;
/*! @class LTIANRewardView
 *  @brief Show reward IAN content. @see LTIANRewardItem*/
@interface LTIANRewardView : LTIANGenericView

/*! Reward object info.*/
@property (nonatomic, strong)   LTCatalogReward* reward;

@end
