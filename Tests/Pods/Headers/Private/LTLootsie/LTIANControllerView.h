//
//  LTIANControllerView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/17/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTIANControllerView
 *  @brief Container view for show IAN. Use this class to display banner and IANs list.*/
@interface LTIANControllerView : UIView

@end
