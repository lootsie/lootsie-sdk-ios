//
//  LTFreePointsButton.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/21/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTLogoutButton.h"

IB_DESIGNABLE
/*! @class LTFreePointsButton
 *  @brief Button for displaying video controller. it's used in video IAN.*/
@interface LTFreePointsButton : LTLogoutButton

@end
