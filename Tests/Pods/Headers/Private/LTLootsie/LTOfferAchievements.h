//  LTOfferAchievements.h
//  Created by Dovbnya Alexander on 7/10/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTModelEntity.h"

/*! Achievement's difficult level.*/
typedef NS_ENUM(NSInteger,LTAchievementDifficulty) {
    LTEasyDifficulty,
    LTMediumDifficulty,
    LTHardDifficulty
};


/*! @class LTOfferAchievements
 *  @brief Object data fir creating application achievements. this class is used for creating achievement object from app section of init request to server.*/
@interface LTOfferAchievements : LTModelEntity

/*! Unique id of achievement*/
@property (nonatomic, assign) NSInteger idAchievement;

/*! Title of achievement.*/
@property (nonatomic, strong) NSString *title;

/*! Description of achievement.*/
@property (nonatomic, strong) NSString *desc;

/*! Achievement's difficult level. @see LTAchievementDifficulty*/
@property (nonatomic, assign) LTAchievementDifficulty difficulty;

@end
