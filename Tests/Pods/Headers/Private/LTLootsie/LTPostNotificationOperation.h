//  LTPostNotificationOperation.h
//  Created by Fabio Teles on 8/25/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTOperation.h"

/*! @class LTPostNotificationOperation
 *  @brief Operation for sending notifications.*/
@interface LTPostNotificationOperation : LTOperation

/*! @brief Create a new notification with name.
 *  @param notificationName Name of notification. Must be unique.
 *  @param notificationSender Pointer to object which post notification.
 *  @param userInfo Additional info for notification.*/
- (instancetype)initWithNotificationName:(NSString *)notificationName object:(id)notificationSender userInfo:(NSDictionary *)userInfo;

/*! @brief Create a new notification with name.
 *  @param notificationName Name of notification. Must be unique.
 *  @param notificationSender Pointer to object which post notification.
 *  @param userInfoBlock Block of code for generate user info object for current notification.*/
- (instancetype)initWithNotificationName:(NSString *)notificationName object:(id)notificationSender userInfoBlock:(NSDictionary *(^)(void))userInfoBlock;

@end
