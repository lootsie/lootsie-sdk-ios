//  LTRedemtionView.h
//  Created by Dovbnya Alexander on 8/6/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <UIKit/UIKit.h>

#import "LTCatalogReward.h"
/*! Redemption state.*/
typedef NS_ENUM(NSInteger, LTRedemtionViewState) {
    LTRedemtionViewUnknown,         /*! Unkwnown state.*/
    LTRedemtionViewNonRegistered,   /*! Show page for unregistered user.*/
    LTRedemtionViewConfirmEmail,    /*! User need to confirm email.*/
    LTRedemtionViewLoading,         /*! Show page with animation of loading.*/
    LTRedemtionViewSuccess,         /*! Show success page.*/
    LTRedemtionViewError            /*! Show error message.*/
};


/*! @class LTRedemtionView
 *  @brief Redemption container view.*/
@interface LTRedemtionView : UIView

/*! State of wizard. @see LTRedemtionViewState*/
@property (nonatomic, assign) LTRedemtionViewState state;

/*! Reward info object.*/
@property (nonatomic, strong) LTCatalogReward* reward;

/*! callback is called when wizard did change state.*/
@property (nonatomic, copy) void (^didChangeState)(LTRedemtionViewState newState);

/*! callback is called when we need to close the outer window.*/
@property (nonatomic, copy) void (^didFinish)();


@end
