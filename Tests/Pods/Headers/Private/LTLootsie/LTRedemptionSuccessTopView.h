//
//  LTRedemptionSuccessTopView.h
//  LTLootsie iOS Example
//
//  Created by Macmini on 6/9/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTRedemptionSuccessTopView
 *  @brief View for show success information.*/
@interface LTRedemptionSuccessTopView : UIView

/*! Reward name.*/
@property (nonatomic, strong) NSString *rewardName;

@end
