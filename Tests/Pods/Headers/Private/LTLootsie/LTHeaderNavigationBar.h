//
//  LTHeaderNavigationBar.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/11/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>


@class LTHeaderNavigationBar;
/*! @typedef LTHeaderNavigationBarDidChangeSelectionIndex
 *  @brief Callback is called when user change page in navigation bar.
 *  @param navBar Navigation bar instance which was change page.
 *  @param index New index of page.*/
typedef void(^LTHeaderNavigationBarDidChangeSelectionIndex)(LTHeaderNavigationBar* navBar, NSUInteger index);


IB_DESIGNABLE
/*! @class LTHeaderNavigationBar
 *  @brief View for show navigation bar with buttons. This component using for navigate between pages in Lootsie UI.*/
@interface LTHeaderNavigationBar : UIView

/*! Width of line.*/
@property (nonatomic)           IBInspectable NSUInteger lineWidth;

/*! Color for line.*/
@property (nonatomic)           IBInspectable UIColor* lineColor;

/*! Selected page index.*/
@property (nonatomic, assign)   NSInteger selectedIndex;

/*! Page index did update. @see LTHeaderNavigationBarDidChangeSelectionIndex*/
@property (nonatomic, copy)     LTHeaderNavigationBarDidChangeSelectionIndex onSelectionIndexChenged;

/*! Count of buttons in navigation bar.*/
@property (nonatomic, assign, readonly)     NSInteger buttonsCount;

/*! Favorite icon for favorite button.*/
@property (nonatomic, weak) IBOutlet UIView *favoriteIcon;

@end
