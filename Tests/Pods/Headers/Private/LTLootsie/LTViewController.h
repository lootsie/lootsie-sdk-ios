//  LTViewController.h
//  Created by Fabio Teles on 5/18/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <UIKit/UIKit.h>

@class LTData;
/*! @protocol LTIANHaderView
 *  @brief Methods for IAN header view.*/
@protocol LTIANHaderView <NSObject>

/*! @brief Get header view for show IAN.
 *  @return Target view.*/
- (UIView *)headerViewForIan;

@end


/*! @class LTViewController
 *  @brief Main controller for show Lootsie UI. Use this controller show to display all UI controllers.
 *  @note Popup and alerts maybe show on another controller.*/
@interface LTViewController : UIViewController <LTIANHaderView>

/*! Controller whcih now is visible on screen.*/
@property (strong, nonatomic, readonly) UIViewController *currentViewController;

/*! @brief Show controller with class name.
 *  @param classController Name of controller for show.*/
- (void)showViewControllerWithClass:(Class)classController;

@end
