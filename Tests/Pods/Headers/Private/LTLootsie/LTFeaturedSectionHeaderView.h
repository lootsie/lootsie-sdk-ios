//
//  LTFeaturedSectionHeaderView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/18/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFavoriteSectionHeaderView.h"

/*! @class LTFeaturedSectionHeaderView
 *  @brief Rewards list header view.*/
@interface LTFeaturedSectionHeaderView : LTFavoriteSectionHeaderView

@end
