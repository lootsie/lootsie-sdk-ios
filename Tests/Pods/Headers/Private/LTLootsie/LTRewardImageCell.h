//
//  LTRewardImageCell.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/20/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTRewardImageCell
 *  @brief Cell for show image.*/
@interface LTRewardImageCell : UICollectionViewCell

/*! Reward image url.*/
@property  (nonatomic, strong)  NSString* rewardImageUrl;

@end
