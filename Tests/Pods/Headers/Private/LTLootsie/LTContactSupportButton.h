//
//  LTContactSupportButton.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/1/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTBaseButton.h"

/*! @class LTContactSupportButton
 *  @brief Button for show email controller where user can write a message*/
@interface LTContactSupportButton : LTBaseButton

/*! YES if need to use small size of button.*/
@property (nonatomic, assign) IBInspectable BOOL isSmallHeight;

@end
