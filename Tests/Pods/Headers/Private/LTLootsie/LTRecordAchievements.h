//  LTRecordAchievements.h
//  Created by Dovbnya Alexander on 7/10/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTModelEntity.h"

/*! @class LTRecordAchievements
 *  @brief Object for saving user's achievement info. This object is used for saving data from user section in init request.*/
@interface LTRecordAchievements : LTModelEntity

/*! Date when user is open this achievement.*/
@property (nonatomic, strong) NSDate *date;

/*! Achievement uqnie id. This id must be equal to id from LTOfferAchievements.*/
@property (nonatomic, assign) NSInteger achievementID;

/*! Number of points for achievement.*/
@property (nonatomic, assign) NSInteger points;

@end
