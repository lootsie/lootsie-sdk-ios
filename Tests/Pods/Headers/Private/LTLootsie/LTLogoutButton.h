//
//  LTLogoutButton.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/7/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTBaseButton.h"

/*! @class LTLogoutButton
 *  @brief Button is used in User Account page for account deactivation.*/
@interface LTLogoutButton : LTBaseButton

/*! YES if need to use small size of button.*/
@property (nonatomic, assign) IBInspectable BOOL isSmallHeight;

@end
