//  LTUserRewardRedemptionOperation.h
//  Created by Fabio Teles on 7/28/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTDataTaskOperation.h"

@class LTUserRewardRedemption;
/*! @class LTUserRewardRedemptionOperation
 *  @brief Object for work with reward redemption data.*/
@interface LTUserRewardRedemptionOperation : LTDataTaskOperation

/*! Redemption info object.*/
@property (nonatomic, readonly, strong) LTUserRewardRedemption *redemption;


/*! @brief Redeem a reward to the registered user. User must be registered and i.e.:, NOT a guest.
 *  @param rewardId Unique id of reward.*/
+ (instancetype)POSTWithRewardId:(NSInteger)rewardId;

@end
