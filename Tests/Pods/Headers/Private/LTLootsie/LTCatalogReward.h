//
//  LTCatalogReward.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/17/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTBaseReward.h"

/*! Type of rewards.*/
typedef NS_ENUM(NSInteger, LTRewardType) {
    LTUnknowRewardType = 0,     /*! Unkonwn type*/
    LTVerifiedRewardType = 1,   /*! Reward is a reward that Lootsie will validate upon redemption and deliver via email to the user*/
    LTInstantRewardType = 2,    /*! Reward is a digital good specific to the application or publisher, i.e. a virtual currency or a game powerup. 
                                 *  Lootsie will not do any validation upon redemption beyond checking the user’s account balance and will only 
                                 *  deduct the cost from the user’s point balance.*/
};

/*! State of reward*/
typedef NS_ENUM(NSInteger, LTRewardState) {
    LTNewRewardState = 1        /*! The current reward is new.*/
};


/*! @class LTCatalogReward
 *  @brief Info object for save standard reward info.*/
@interface LTCatalogReward : LTBaseReward

/*! Unique id of reward.*/
@property (nonatomic, assign)   NSInteger rewardId;

/*! Instant unique id.*/
@property (nonatomic, assign)   NSInteger instantID;

/*! Name of reward.*/
@property (nonatomic, strong)   NSString* name;

/*! Reward type. @see LTRewardType*/
@property (nonatomic, assign)   LTRewardType type;

/*! Description of reward.*/
@property (nonatomic, strong)   NSString* desc;

/*! YES if current reward is mark as favorite*/
@property (nonatomic, assign)   BOOL isFavorite;

/*! YES if need to hide earn in fast button on detail screen for current reward.*/
@property (nonatomic, assign)   BOOL isHideEarnInFast;

/*! YES if current reward is a featired.*/
@property (nonatomic, assign)   BOOL isFeatured;

/*! YES if need to enable video refresh action for current reward.*/
@property (nonatomic, assign)   BOOL refreshEnabled;

/*! String for show label on reward item.*/
@property (nonatomic, strong)   NSString* markerString;

/*! Reward state.*/
@property (nonatomic, assign)   LTRewardState state;

/*! Link to brand image.*/
@property (nonatomic, strong)   NSURL* brandImage;

/*! List of links for banners.*/
@property (nonatomic, strong)   NSArray<NSURL *> *banners;

/*! List of links for thumbnails.*/
@property (nonatomic, strong)   NSArray<NSURL *> *thumbnails;

@end
