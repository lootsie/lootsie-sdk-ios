//
//  LTFooterView.h
//
//  Copyright © 2017 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LTNibLoadingView.h"
#import "LTMainNavigationView.h"


/*! @brief Implements the footer view that is shown in landscape mode.
 */
@interface LTFooterView : LTNibLoadingView

@property (weak, nonatomic) IBOutlet LTMainNavigationView *mainNavigationView;

@end
