//
//  LTIANManager.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LTIANItem.h"


@class LTIANGenericView;
/*! @class LTIANManager
 *  @brief Manager for work with IAN items.*/
@interface LTIANManager : NSObject

/*! @brief Create UI view for show IAN info.
 *  @param item IAN data object with IAN info.*/
+ (LTIANGenericView*)ianViewWithItem: (LTIANItem*)item;

@end
