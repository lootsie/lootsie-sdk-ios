//  LTExclusivityController.h
//  Created by Fabio Teles on 7/15/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>

@class LTOperation;
/*! @class LTExclusivityController
 *  @brief Controller for work with operations by categories*/
@interface LTExclusivityController : NSObject

/*! @brief Create and return controller.*/
+ (instancetype)sharedExclusivityController;

/*! @brief Registers an operation as being mutually exclusive.
 *  This needs to be a synchronous operation.
 *  If this were async, then we might not get around to adding dependencies
 *  until after the operation had already begun, which would be incorrect.
 *  @param operation Operation object.
 *  @param categories List of categories for operation.*/
- (void)addOperation:(LTOperation *)operation categories:(NSArray *)categories;

/*! @brief Unregisters an operation from being mutually exclusive.
 *  @param operation Operation object.
 *  @param categories List of categories for operation.*/
- (void)removeOperation:(LTOperation *)operation categories:(NSArray *)categories;

@end