//
//  LTRootViewController.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 4/20/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTRootViewController
 *  @brief Use it as a root controller.*/
@interface LTRootViewController : UIViewController

@end
