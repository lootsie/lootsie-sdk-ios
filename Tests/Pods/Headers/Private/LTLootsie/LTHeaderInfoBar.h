//
//  LTHeaderInfoBar.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/11/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @typedef LTHeaderInfoBarDidButtonCloseClicked
 *  @brief Callback is called when need to close UI component.*/
typedef void(^LTHeaderInfoBarDidButtonCloseClicked)();


IB_DESIGNABLE
/*! @class LTHeaderInfoBar
 *  @brief Show header bar with page.*/
@interface LTHeaderInfoBar : UIView

/*! Close action callback.*/
@property (nonatomic, copy)   LTHeaderInfoBarDidButtonCloseClicked onCloseClicked;

/*! Label for show title of page.*/
@property (nonatomic, strong)   NSString* appName;

/*! User avatar image. 
 *  @note Doesn't use for now.*/
@property (nonatomic, strong)   UIImage* userAvatar;

/*! Number of user's points.*/
@property (nonatomic, assign)   NSInteger points;

@end
