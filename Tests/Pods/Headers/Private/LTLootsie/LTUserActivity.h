//  LTUserActivity.h
//  Created by Fabio Teles on 7/10/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTModelEntity.h"

/*! Events ids*/
typedef NS_ENUM(NSInteger,LTBaseUserEventID) {
    LTAppEngagementEventID = 20,            /*! The SDK provides a method for publishers to signal general engagement. 
                                             *  The purpose of this event type is to “heartbeat” a session. It should be 
                                             *  called for general, non-specific activity in the app such as opening views,
                                             *  scrolling through a document, etc.*/
    LTIANShownEventID = 30,                 /*! Fired when an IAN is shown to the user.*/
    LTCatalogViewEventID = 40,              /*! Fired when the catalog is opened.*/
    LTRewardViewEventID = 50,               /*! Fired when a reward card is viewed.*/
    LTRewardedVideoStartEventID = 60,       /*! Fired when a rewarded video view starts.*/
    LTRefreshVideoStartEventID = 70,        /*! Fired when a “Refresh” video view starts*/
    LTVideoWatchedEventID = 80,             /*! Fired when the “minimum watch time” has elapsed ( the event from Aerserv’s SDK ).*/
    LTVideoCompleteEventID = 90,            /*! Fired when a video view is complete.*/
    LTRegisterSpend = 110                   /*! RegisterSpend adds to the event queue the amount / spend */
};


/*! @class LTUserActivity
 *  @brief Event data object for save information.*/
@interface LTUserActivity : LTModelEntity <NSCoding>

/*! An integer ( 1 or 0 ) flag for “Application Event” specifying if this event is a Lootsie or Application defined event. 1 ( True ) is set for application events.
 *  By default 1.
 */
@property (nonatomic, assign) NSInteger applicationFlag;

/*! Event ID: the constant unsigned integer ID of the event. Must be greater than 0.*/
@property (nonatomic, assign) NSInteger eventID;

/*! Timestamp when event is fired.*/
@property (nonatomic, assign, readonly) NSInteger timestamp;

/*! Additional number for event. For example reward id, cost reward.*/
@property (nonatomic, strong) NSNumber *additionalNumber;


/*! @brief Convert event to JSON array. This array will be used when current event should be sent to server.*/
- (NSArray *)eventString;

@end
