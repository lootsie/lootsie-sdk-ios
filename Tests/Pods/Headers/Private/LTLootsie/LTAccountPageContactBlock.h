//
//  LTAccountPageContactBlock.h
//  LTLootsie iOS Example
//

//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTNibLoadingView.h"

@interface LTAccountPageContactBlock : LTNibLoadingView

@end
