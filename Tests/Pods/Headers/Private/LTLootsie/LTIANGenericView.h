//
//  LTIANGenericView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class LTIANItem, LTIANGenericView, LTLogoutButton;
/*! @protocol LTIANViewDelegate
 *  @brief Methods for worh with ian view.*/
@protocol LTIANViewDelegate <NSObject>
@optional

/*! @brief IAN view did close.
 *  @param ianView Instance of view.*/
- (void)ianViewDidClose:(LTIANGenericView*)ianView;

/*! @brief Called when user swipe to left.
 *  @param ianView Instance of view.*/
- (void)ianViewDidSwipeLeft:(LTIANGenericView*)ianView;

/*! @brief Called when user swip to right.
 *  @param ianView Instance of view.*/
- (void)ianViewDidSwipeRight:(LTIANGenericView*)ianView;

@end



IB_DESIGNABLE
/*! @class LTIANGenericView
 *  @brief Base IAN view. @see LTIANGenericItem*/
@interface LTIANGenericView : UICollectionViewCell

/*! Default button for close IAN view.*/
@property (nonatomic, weak) IBOutlet LTLogoutButton* closeButton;

/*! Z-index of ian view.*/
@property (nonatomic, assign)   CGFloat zIndex;

/*! Additional methods for ian view.*/
@property (nonatomic, weak)     id<LTIANViewDelegate> delegate;

/*! IAN object info.*/
@property (nonatomic, strong)   LTIANItem* item;

/*! Title of IAN*/
@property (nonatomic, strong)   IBInspectable NSString* title;

/*! Description of IAN*/
@property (nonatomic, strong)   IBInspectable NSString* desc;

/*! Time to hide ian.*/
@property (nonatomic, assign)   IBInspectable CGFloat seconds;

/*! Content size of ian.*/
@property (nonatomic, assign, readonly) CGSize contentSize;


/*! @brief Show card with animation.
 *  @param height Height of card.
 *  @param duration Animation duration.*/
- (void)showCardWithAnimation:(CGFloat)height duration:(CGFloat)duration;

/*! @brief Play spring animation for card. This method will calls when user remove some another
 *  ian and this is not last item in list.*/
- (void)springAnimation;

/*! @brief Show card without aniamtion.*/
- (void)showCard;

@end
