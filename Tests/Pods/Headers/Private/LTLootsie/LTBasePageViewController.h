//  LTBasePageViewController.h
//  Created by Dovbnya Alexandr on 5/17/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <UIKit/UIKit.h>

/*! @protocol LTNavigationScrollDelegate
 *  @brief Methods for detecting scroll action on page.*/
@protocol LTNavigationScrollDelegate <NSObject>

/*! @brief Called when user change page.
 *  @param delta Delta value for move line on navigation view.*/
- (void)didChangeNavigationBar:(CGFloat)delta;

/*! @brief Called when scroll did end.*/
- (void)didEndScroll;

@end



/*! @class LTBasePageViewController
 *  @brief Base page controller. Use this class to create a new page.*/
@interface LTBasePageViewController : UIViewController <UIScrollViewDelegate>

/*! Methods for navigation bar. @see LTNavigationScrollDelegate*/
@property (nonatomic, weak) id<LTNavigationScrollDelegate> navigationBarDelegate;


/*! @brief Override method. Will call every time when need to refresh data on the page.*/
- (void)needUpdateData;

@end
