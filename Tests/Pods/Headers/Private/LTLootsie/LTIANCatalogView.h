//
//  LTIANCatalogView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/15/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANGenericView.h"

/*! @class LTIANCatalogView
 *  @brief Show catalog IAN content. @see LTIANCatalogItem*/
@interface LTIANCatalogView : LTIANGenericView

@end
