//
//  LTFTWelcomePage.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFirstTimeBaseView.h"

/*! @typedef LTFTWelcomePageMaybeLaterButtonClicked
 *  @brief Callback is called when user click on "maybe later" button.*/
typedef void(^LTFTWelcomePageMaybeLaterButtonClicked)();

/*! @typedef LTFTWelcomePageLetsGoButtonClicked
 *  @brief Callback is called when user click on "Let's Go" button. And start to use wizard steps.*/
typedef void(^LTFTWelcomePageLetsGoButtonClicked)();


/*! @class LTFTWelcomePage
 *  @brief This page displays general info about wizard.*/
@interface LTFTWelcomePage : LTFirstTimeBaseView

/*! Close wizard.*/
@property (nonatomic, copy) LTFTWelcomePageMaybeLaterButtonClicked onMaybeButtonClicked;

/*! Start wizard logic.*/
@property (nonatomic, copy) LTFTWelcomePageLetsGoButtonClicked onLetsGoButtonClicked;

@end
