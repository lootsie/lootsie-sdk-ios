//
//  LTVideoRefreshPlayView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/23/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LTBaseInterstitialReward;
/*! @typedef LTVideoRefreshPlayViewNeedShowVideo
 *  @brief Callback is called when video refresh view need to show video ad.
 *  @param reward Reward item with video info.*/
typedef void(^LTVideoRefreshPlayViewNeedShowVideo)(LTBaseInterstitialReward* reward);


/*! @class LTVideoRefreshPlayView
 *  @brief View for show video refresh view. This view will show until user click to refresh button.*/
@interface LTVideoRefreshPlayView : UIView

/*! Video callback.*/
@property (nonatomic, strong)   LTVideoRefreshPlayViewNeedShowVideo onNeedShowVideo;


/*! @brief Play animation for show view.*/
- (void)playAnimation;

@end
