//
//  LTVideoRefreshView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/23/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>


@class LTCatalogReward, LTBaseInterstitialReward;
/*! @typedef LTVideoRefreshViewNeedShowVideo
 *  @brief Callback is called when need to show video.*/
typedef void(^LTVideoRefreshViewNeedShowVideo)(LTBaseInterstitialReward* reward);

/*! @typedef LTVideoRefreshViewVideoDidClose
 *  @brief Callback is called when video controller was closed.*/
typedef void(^LTVideoRefreshViewVideoDidClose)(LTCatalogReward* reward);


/*! @class LTVideoRefreshView
 *  @brief Refresh view for show animation where user needs to tap and hold play button.*/
@interface LTVideoRefreshView : UIView

/*! Reward item with info.*/
@property (nonatomic, strong)   LTCatalogReward* reward;

/*! Shown callback.*/
@property (nonatomic, copy)     LTVideoRefreshViewNeedShowVideo onWillShowVideo;

/*! Close callback.*/
@property (nonatomic, copy)     LTVideoRefreshViewVideoDidClose onVideoClosed;

/*! Background image view.*/
@property (nonatomic, strong)   UIImage* backgroundImage;

@end
