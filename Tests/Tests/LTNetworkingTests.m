//  LTNetworkingTests.m
//  Created by Fabio Teles on 7/22/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTTestCase.h"

#import "LTLootsie.h"

#import "LTAllOperation.h"
#import "LTFavoriteRewardOperation.h"
#import "LTUserRewardRedemptionOperation.h"
#import "LTAllSequence.h"

#import "LTManager.h"
#import "LTNetworkManager.h"
#import "LTBlockOperation.h"
#import "LTBlockObserver.h"
#import "LTCatalogReward.h"

#import "LTTools.h"
#import "LTHelperUnitTest.h"

@interface LTNetworkingTests : LTTestCase

@end

@implementation LTNetworkingTests

//#pragma mark - Operations

- (void)testGetInit {
    LTInitSequenceOperation *op = [LTInitSequenceOperation initOperation];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect([LTManager sharedManager].data.app).notTo.beNil();
    expect([LTManager sharedManager].data.user).notTo.beNil();
    expect([LTManager sharedManager].data.catalog).notTo.beNil();
}

- (void)testPostEvent {
    LTUserActivity *userActivity = [LTUserActivity new];
    userActivity.eventID = LTRegisterSpend;
    userActivity.applicationFlag = 0;
    
    LTUserActivity *userActivity1 = [LTUserActivity new];
    userActivity1.eventID = 24;
    userActivity1.applicationFlag = 1;
    
    LTSendUserActivitySequenceOperation *op = [LTSendUserActivitySequenceOperation sequenceWithUserActivity:@[userActivity, userActivity1]];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

- (void)testGetUser {
    [self checkUserAccountEmail:nil];
}

- (void)testPutUser {
    LTUserAccountOperation *op = [LTUserAccountOperation PUTWithEmail:LTUserEmailTest];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    //[self checkUserAccountEmail:LTUserEmailTest];
}

- (void)testGetCatalog {
    LTUserRewardsOperation *op = [LTUserRewardsOperation GET];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect(op.catalog).notTo.beNil();
}

- (void)testGetReward {
    LTUserRewardsOperation *op = [LTUserRewardsOperation GETRewardsWithID:LTRewardIDTest];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    NSDictionary *response = op.responseObject;
    LTCatalogReward *reward = [LTCatalogReward entityWithObject:response];
    expect(reward).notTo.beNil();
}

- (void)testPutFavorite {
    LTFavoriteRewardOperation *op = [LTFavoriteRewardOperation favoriteRewardWithID:LTRewardIDTest];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

- (void)testDeleteFavorite {
    LTFavoriteRewardOperation *op = [LTFavoriteRewardOperation unFavoriteRewardWithID:LTRewardIDTest];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

- (void)testPostRedeem {
    LTUserRewardRedemptionOperation *op = [LTUserRewardRedemptionOperation POSTWithRewardId:LTRewardIDTest];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
}

- (void)checkUserAccountEmail:(NSString *)email {
    LTUserAccountOperation *op = [LTUserAccountOperation GET];
    [self addTestOperation:op];
    
    expect(op.isFinished).will.beTruthy();
    expect(op.generatedErrors.count).equal(0);
    expect(op.userAccount).notTo.beNil();
    if (email != nil) {
        expect(op.userAccount.email).equal(email);
    }
}

@end
