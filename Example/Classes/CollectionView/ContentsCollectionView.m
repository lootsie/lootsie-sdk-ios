//
//  ContentsCollectionView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "ContentsCollectionView.h"



@interface ContentsCollectionView () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong)   NSMutableArray<CollectionlDataItemsSection*> *itemsList;

@end


NSString* const kContentsCollectionHeaderViewIdentifier                     =   @"headerView";
NSString* const kContentsCollectionFooterViewIdentifier                     =   @"footerView";


@implementation ContentsCollectionView


#pragma mark - Notifications

- (void)statusBarDidChangeOrientation:(NSNotification*)notification {
    [self reloadData];
}



#pragma mark - Init methods

- (instancetype)initWithFrame:(CGRect)frame collectionViewLayout:(UICollectionViewLayout *)layout {
    self = [super initWithFrame:frame collectionViewLayout:layout];
    if(self) {
        self.itemsList = nil;
        self.delegate = self;
        self.dataSource = self;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib]; // FIX

    self.itemsList = nil;
    self.delegate = self;
    self.dataSource = self;
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(statusBarDidChangeOrientation:)
                                                 name: UIApplicationDidChangeStatusBarOrientationNotification
                                               object: nil];
}


#pragma mark - ColelctionViewDelegates

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if(self.itemsList.count > 0) {
        return self.itemsList.count;
    }
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(self.itemsList.count > 0) {
        CollectionlDataItemsSection* sectionObj = [self.itemsList objectAtIndex: section];
        if (sectionObj.expand) {
            return sectionObj.items.count;
        }
        return 0;
    }
    return 0;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout*)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionlDataItemsSection* sectionObj = [self.itemsList objectAtIndex: indexPath.section];
    CollectionDataItem* itemData = [sectionObj itemAtIndex: indexPath.item];
    CGSize cellSize = itemData.cellItemSize;
    if(cellSize.width == -1.0) {
        CGFloat leftOffset = ((UICollectionViewFlowLayout*)collectionViewLayout).sectionInset.left;
        CGFloat rightOffset = ((UICollectionViewFlowLayout*)collectionViewLayout).sectionInset.left;
        cellSize.width = collectionView.frame.size.width - (leftOffset + rightOffset);
    } else if(cellSize.width == -0.5) {
        CGFloat leftOffset = ((UICollectionViewFlowLayout*)collectionViewLayout).sectionInset.left;
        CGFloat rightOffset = ((UICollectionViewFlowLayout*)collectionViewLayout).sectionInset.left;
        CGFloat minimumLineSpacing = ((UICollectionViewFlowLayout*)collectionViewLayout).minimumLineSpacing;
        cellSize.width = ((collectionView.frame.size.width - (leftOffset + rightOffset + minimumLineSpacing)) / 2.0);
    }
    return cellSize;
}

- (UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView* reusableView = nil;
    if(kind == UICollectionElementKindSectionHeader) {
        CollectionlDataItemsSection* sectionObj = [self.itemsList objectAtIndex: indexPath.section];
        HeaderCollectionReusableView* headerView = [collectionView dequeueReusableSupplementaryViewOfKind: UICollectionElementKindSectionHeader
                                                                                      withReuseIdentifier: kContentsCollectionHeaderViewIdentifier
                                                                                             forIndexPath: indexPath];
        headerView.didTapBlock = ^ {
            sectionObj.expand = !sectionObj.expand;
            [self reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
        };
        headerView.title = sectionObj.title;
        reusableView = headerView;
    } else if(kind == UICollectionElementKindSectionFooter) {
        reusableView = [collectionView dequeueReusableSupplementaryViewOfKind: UICollectionElementKindSectionFooter
                                                          withReuseIdentifier: kContentsCollectionFooterViewIdentifier
                                                                 forIndexPath: indexPath];
    }
    return reusableView;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CollectionlDataItemsSection* sectionObj = [self.itemsList objectAtIndex: indexPath.section];
    CollectionDataItem* itemData = [sectionObj itemAtIndex: indexPath.item];
    NSString* identifierString = itemData.cellIdentifier;
    NSAssert(identifierString.length > 0, @"You need set cell identifier for cell.");
    CollectionViewActionCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier: identifierString
                                                                               forIndexPath: indexPath];
    if([cell isKindOfClass: [CollectionViewActionCell class]]) {
        cell.dataItem = itemData;
        cell.indexPath = indexPath;
    }
    return cell;
}


#pragma mark - setters

- (void)setItems:(NSArray<CollectionlDataItemsSection*> *)items {
    if(items.count > 0) {
        self.itemsList = [NSMutableArray arrayWithArray: items];
        [self reloadData];
    }
}

- (void)addItemToSection:(NSString*)sectionName item:(CollectionDataItem*)item {
    if(sectionName.length > 0) {
        if(self.itemsList == nil) {
            self.itemsList = [NSMutableArray array];
        }
        CollectionlDataItemsSection* section = [self sectionWithName: sectionName];
        if(section == nil) {
            section = [CollectionlDataItemsSection sectionWithTitle: sectionName];
        }
        [section addItem: item];
        [self reloadData];
    }
}

- (void)addItemsToSection:(NSString*)sectionName items:(NSArray<CollectionDataItem*> *)items {
    if(sectionName.length > 0) {
        if(self.itemsList == nil) {
            self.itemsList = [NSMutableArray array];
        }
        CollectionlDataItemsSection* section = [self sectionWithName: sectionName];
        if(section == nil) {
            section = [CollectionlDataItemsSection sectionWithTitle: sectionName];
        }
        [section addItems: items];
        [self reloadData];
    }
}

- (void)removeItemFromSection:(NSString*)sectionName item:(CollectionDataItem*)item {
    if(sectionName.length > 0 && self.itemsList.count > 0) {
        CollectionlDataItemsSection* section = [self sectionWithName: sectionName];
        [section removeItem: item];
        [self reloadData];
    }
}

- (void)removeItemFormSection:(NSString*)sectionName atIndex:(NSInteger)itemIndex {
    if(sectionName.length > 0 && self.itemsList.count > 0) {
        CollectionlDataItemsSection* section = [self sectionWithName: sectionName];
        [section removeItemAtIndex: itemIndex];
        [self reloadData];
    }
}

- (void)itemInSection:(NSString*)sectionName atIndex:(NSInteger)index enabled:(BOOL)flag {
    if(sectionName.length > 0) {
        CollectionDataItem* item = [self itemFromSection: sectionName atIndex: index];
        item.enabled = flag;
        [self reloadData];
    }
}


#pragma mark - getters

- (CollectionlDataItemsSection*)sectionWithName:(NSString*)sectionName {
    if(sectionName.length > 0 && self.itemsList.count > 0) {
        CollectionlDataItemsSection* resItem = nil;
        for(CollectionlDataItemsSection* item in self.itemsList) {
            if([item.title isEqualToString: sectionName]) {
                resItem = item;
                break;
            }
        }
        return resItem;
    }
    return nil;
}

- (CollectionDataItem*)itemFromSection:(NSString*)sectionName atIndex:(NSInteger)itemIndex {
    if(sectionName.length > 0 && self.itemsList.count > 0 && itemIndex >= 0) {
        CollectionDataItem* resItem = nil;
        for(CollectionlDataItemsSection* item in self.itemsList) {
            if([item.title isEqualToString: sectionName]) {
                resItem = [item itemAtIndex: itemIndex];
                break;
            }
        }
        return resItem;
    }
    return nil;
}

- (NSArray<CollectionDataItem*> *)itemsFromSection:(NSString*)sectionName {
    if(sectionName.length > 0 && self.itemsList.count > 0) {
        CollectionlDataItemsSection* section = nil;
        for(CollectionlDataItemsSection* item in self.itemsList) {
            if([item.title isEqualToString: sectionName]) {
                section = item;
                break;
            }
        }
        return section.items;
    }
    return nil;
}


#pragma mark - clear memory

- (void)dealloc {
    self.itemsList = nil;
    self.itemsList = nil;
    self.delegate = nil;
    self.dataSource = nil;
}

@end
