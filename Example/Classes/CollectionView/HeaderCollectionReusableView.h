//
//  HeaderCollectionReusableView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HeaderCollectionReusableView : UICollectionReusableView

@property (nonatomic, strong)   NSString* title;
@property (nonatomic, copy) void (^didTapBlock)(void);

@end
