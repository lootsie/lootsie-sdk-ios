//
//  CollectionlDataItemsSection.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectionDataItem.h"



@interface CollectionlDataItemsSection : NSObject

@property (nonatomic, strong)   NSString* title;
@property (nonatomic, strong, readonly)     NSArray<CollectionDataItem*> *items;
@property (nonatomic, assign)  BOOL expand;


#pragma mark - init methods
- (instancetype)initWithTitle:(NSString*)title items:(NSArray<CollectionDataItem*> *)items;
+ (instancetype)sectionWithTitle:(NSString*)title;
+ (instancetype)sectionWithTitle:(NSString*)title items: (NSArray<CollectionDataItem*> *)items;


#pragma mark - Setters
- (void)addItem:(CollectionDataItem*)item;
- (void)addItems:(NSArray<CollectionDataItem*> *)items;
- (void)itemAtIndex:(NSInteger)index enabled:(BOOL)flag;
- (void)removeItemAtIndex:(NSInteger)index;
- (void)removeItem:(CollectionDataItem*)item;


#pragma mark - Getters
- (CollectionDataItem*)itemAtIndex:(NSInteger)index;


@end
