//
//  CellDataItem.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "CollectionDataItem.h"


@interface CollectionDataItem ()

@end



@implementation CollectionDataItem



#pragma mark - Init methods

- (instancetype)init {
    self = [super init];
    if(self) {
        self.cellIdentifier = nil;
        self.actionCode = nil;
        self.title = nil;
        self.desc = nil;
        self.enabled = YES;
        self.cellItemSize = CGSizeMake(145.0, 37.0);
    }
    return self;
}

- (instancetype)initWithTitle:(NSString*)title actionCode:(CollectionDataItemActionBlock)code {
    self = [super init];
    if(self) {
        self.cellIdentifier = nil;
        self.title = title;
        self.desc = nil;
        self.enabled = YES;
        self.cellItemSize = CGSizeMake(145.0, 37.0);
        self.actionCode = code;
    }
    return self;
}

+ (instancetype)dataItemWithTitle:(NSString*)title actionCode:(CollectionDataItemActionBlock)code {
    return [[CollectionDataItem alloc] initWithTitle: title actionCode: code];
}


#pragma mark - Getters

- (void)runAction {
    if(self.actionCode) {
        self.actionCode(self);
    }
}


#pragma mark - clear memory

- (void)dealloc {
    self.actionCode = nil;
    self.title = nil;
    self.desc = nil;
}

@end
