//
//  CollectionDataItem.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>


@class CollectionDataItem;
typedef void(^CollectionDataItemActionBlock)(CollectionDataItem* item);



@interface CollectionDataItem : NSObject

@property (nonatomic, strong)   NSString* cellIdentifier;
@property (nonatomic, strong)   NSString* title;
@property (nonatomic, strong)   NSString* desc;
@property (nonatomic, assign)   BOOL enabled;
@property (nonatomic, assign)   CGSize cellItemSize;        //default 145 x 37
@property (nonatomic, strong)   CollectionDataItemActionBlock actionCode;


#pragma mark - Init methods
- (instancetype)initWithTitle:(NSString*)title actionCode:(CollectionDataItemActionBlock)code;
+ (instancetype)dataItemWithTitle:(NSString*)title actionCode:(CollectionDataItemActionBlock)code;


#pragma mark - Getters
- (void)runAction;

@end
