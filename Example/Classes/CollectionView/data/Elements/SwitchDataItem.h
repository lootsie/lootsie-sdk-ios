//
//  SwitchDataItem.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "CollectionDataItem.h"

@interface SwitchDataItem : CollectionDataItem
@property (nonatomic, assign)   BOOL on;


+ (instancetype)switchItemWithTitle:(NSString*)title actionCode:(CollectionDataItemActionBlock)code;
+ (instancetype)switchItemWithTitle:(NSString*)title on:(BOOL)flag actionCode:(CollectionDataItemActionBlock)code;

@end
