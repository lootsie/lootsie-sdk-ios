//
//  TextFieldDataItem.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "CollectionDataItem.h"

@interface TextFieldDataItem : CollectionDataItem
@property (nonatomic, strong)   NSString* text;


+ (instancetype)textFieldWithTitle:(NSString*)title actionCode:(CollectionDataItemActionBlock)code;
+ (instancetype)textFieldWithTitle:(NSString*)title text:(NSString*)text actionCode:(CollectionDataItemActionBlock)code;

@end
