//
//  ButtonDataItem.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectionDataItem.h"

@interface ButtonDataItem : CollectionDataItem

+ (instancetype)buttonWithTitle:(NSString*)title actionCode:(CollectionDataItemActionBlock)code;

@end
