//
//  SwitchDataItem.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "SwitchDataItem.h"

@implementation SwitchDataItem


- (instancetype)initWithTitle:(NSString *)title actionCode:(CollectionDataItemActionBlock)code {
    self = [super initWithTitle: title actionCode: code];
    if(self) {
        self.cellItemSize = CGSizeMake(-1.0, 53.0);
        self.cellIdentifier = @"contentSwitchItemCell";
    }
    return self;
}

+ (instancetype)switchItemWithTitle:(NSString*)title actionCode:(CollectionDataItemActionBlock)code
{
    return [[SwitchDataItem alloc] initWithTitle: title actionCode: code];
}

+ (instancetype)switchItemWithTitle:(NSString*)title on:(BOOL)flag actionCode:(CollectionDataItemActionBlock)code
{
    SwitchDataItem* item = [[SwitchDataItem alloc] initWithTitle: title actionCode: code];
    item.on = flag;
    return item;
}


@end
