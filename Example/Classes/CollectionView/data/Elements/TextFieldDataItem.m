//
//  TextFieldDataItem.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "TextFieldDataItem.h"

@implementation TextFieldDataItem


- (instancetype)initWithTitle:(NSString *)title actionCode:(CollectionDataItemActionBlock)code {
    self = [super initWithTitle: title actionCode: code];
    if(self) {
        self.cellItemSize = CGSizeMake(-1.0, 53.0);
        self.cellIdentifier = @"contentTextFiedItemCell";
    }
    return self;
}

+ (instancetype)textFieldWithTitle:(NSString*)title actionCode:(CollectionDataItemActionBlock)code
{
    return [[TextFieldDataItem alloc] initWithTitle: title actionCode: code];
}

+ (instancetype)textFieldWithTitle:(NSString*)title text:(NSString*)text actionCode:(CollectionDataItemActionBlock)code
{
    TextFieldDataItem* item = [[TextFieldDataItem alloc] initWithTitle: title actionCode: code];
    item.text = text;
    return item;
}

@end
