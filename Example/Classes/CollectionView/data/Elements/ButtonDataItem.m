//
//  ButtonDataItem.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "ButtonDataItem.h"

@implementation ButtonDataItem


- (instancetype)initWithTitle:(NSString *)title actionCode:(CollectionDataItemActionBlock)code {
    self = [super initWithTitle: title actionCode: code];
    if(self) {
        self.cellIdentifier = @"contentButtonItemCell";
        self.cellItemSize = CGSizeMake(-0.5, 37.0);
    }
    return self;
}

+ (instancetype)buttonWithTitle:(NSString*)title actionCode:(CollectionDataItemActionBlock)code
{
    return [[ButtonDataItem alloc] initWithTitle: title actionCode: code];
}



@end
