//
//  CollectionlDataItemsSection.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "CollectionlDataItemsSection.h"



@interface CollectionlDataItemsSection ()

@end






@implementation CollectionlDataItemsSection


#pragma mark - init methods

- (instancetype)init {
    self = [super init];
    if(self) {
        self.title = nil;
        _items = nil;
    }
    return self;
}

- (instancetype)initWithTitle:(NSString*)title items:(NSArray<CollectionDataItem*> *)items {
    self = [super init];
    if(self) {
        self.title = title;
        _items = [NSArray arrayWithArray: items];
    }
    return self;
}

+ (instancetype)sectionWithTitle:(NSString*)title
{
    return [[CollectionlDataItemsSection alloc] initWithTitle: title items: nil];
}

+ (instancetype)sectionWithTitle:(NSString*)title items: (NSArray<CollectionDataItem*> *)items
{
    return [[CollectionlDataItemsSection alloc] initWithTitle: title items: items];
}


#pragma mark - Setters

- (void)addItem:(CollectionDataItem*)item {
    if(item) {
        NSMutableArray* tmpArray = [self.items mutableCopy];
        [tmpArray addObject: item];
        _items = tmpArray;
    }
}

- (void)addItems:(NSArray<CollectionDataItem*> *)items {
    if(items.count > 0) {
        for(CollectionDataItem* elem in items) {
            [self addItem: elem];
        }
    }
}

- (void)itemAtIndex:(NSInteger)index enabled:(BOOL)flag {
    if(index >= 0 && index < self.items.count) {
        CollectionDataItem* item = [self.items objectAtIndex: index];
        item.enabled = flag;
    }
}

- (void)removeItemAtIndex:(NSInteger)index {
    if(index >= 0 && index < self.items.count) {
        NSMutableArray* tmpArray = [self.items mutableCopy];
        [tmpArray removeObjectAtIndex: index];
        _items = tmpArray;
    }
}

- (void)removeItem:(CollectionDataItem*)item {
    if(item && self.items.count > 0) {
        NSMutableArray* tmpArray = [self.items mutableCopy];
        [tmpArray removeObject: item];
        _items = tmpArray;
    }
}


#pragma mark - Getters

- (CollectionDataItem*)itemAtIndex:(NSInteger)index {
    if(index >= 0 && index < self.items.count) {
        return [self.items objectAtIndex: index];
    }
    return nil;
}


#pragma mark - Clear method

- (void)dealloc {
    self.title = nil;
    _items = nil;
}

@end
