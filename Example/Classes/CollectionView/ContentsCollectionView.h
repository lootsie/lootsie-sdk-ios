//
//  ContentsCollectionView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CollectionlDataItemsSection.h"
#import "HeaderCollectionReusableView.h"
#import "ButtonCollectionViewCell.h"
#import "SwitchCollectionViewCell.h"


extern NSString* const kContentsCollectionHeaderViewIdentifier;
extern NSString* const kContentsCollectionFooterViewIdentifier;


@interface ContentsCollectionView : UICollectionView

#pragma mark - setters
- (void)setItems:(NSArray<CollectionlDataItemsSection*> *)items;
- (void)addItemToSection:(NSString*)sectionName item:(CollectionDataItem*)item;
- (void)addItemsToSection:(NSString*)sectionName items:(NSArray<CollectionDataItem*> *)items;
- (void)removeItemFromSection:(NSString*)sectionName item:(CollectionDataItem*)item;
- (void)removeItemFormSection:(NSString*)sectionName atIndex:(NSInteger)itemIndex;
- (void)itemInSection:(NSString*)sectionName atIndex:(NSInteger)index enabled:(BOOL)flag;


#pragma mark - getters
- (CollectionlDataItemsSection*)sectionWithName:(NSString*)sectionName;
- (CollectionDataItem*)itemFromSection:(NSString*)sectionName atIndex:(NSInteger)itemIndex;
- (NSArray<CollectionDataItem*> *)itemsFromSection:(NSString*)sectionName;


@end
