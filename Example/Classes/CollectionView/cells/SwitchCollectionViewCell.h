//
//  SwitchCollectionViewCell.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionViewActionCell.h"


@interface SwitchCollectionViewCell : CollectionViewActionCell

@end
