//
//  TextFieldCollectionViewCell.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "TextFieldCollectionViewCell.h"


@interface TextFieldCollectionViewCell () <UITextFieldDelegate>

@property (nonatomic, weak)     IBOutlet UITextField* textField;
@property (nonatomic, weak)     IBOutlet UIButton* btnAction;

@end




@implementation TextFieldCollectionViewCell


#pragma mark - Button action

- (void)buttonClick:(UIButton*)sender {
    if([self.dataItem isKindOfClass: [TextFieldDataItem class]] && self.textField.text.length > 0) {
        ((TextFieldDataItem*)self.dataItem).text = self.textField.text;
        [self.dataItem runAction];
    }
}


#pragma mark - TextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - Init methods

- (void)awakeFromNib {
    [super awakeFromNib];
    self.textField.delegate = self;
    [self.btnAction addTarget: self action: @selector(buttonClick:) forControlEvents: UIControlEventTouchUpInside];
}


#pragma mark - Setters

- (void)setDataItem:(CollectionDataItem *)dataItem {
    [super setDataItem: dataItem];
    if([self.dataItem isKindOfClass: [TextFieldDataItem class]]) {
        self.textField.text = ((TextFieldDataItem*)self.dataItem).text;
    }
    [self.btnAction setTitle: self.dataItem.title forState: UIControlStateNormal];
}


#pragma mark - Clear memory

- (void)dealloc {
}

@end
