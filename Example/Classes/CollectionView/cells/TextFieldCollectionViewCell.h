//
//  TextFieldCollectionViewCell.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "CollectionViewActionCell.h"

@interface TextFieldCollectionViewCell : CollectionViewActionCell

@end
