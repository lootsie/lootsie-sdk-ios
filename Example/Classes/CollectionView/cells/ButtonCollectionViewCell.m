//
//  ButtonCollectionViewCell.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "ButtonCollectionViewCell.h"




@interface ButtonCollectionViewCell ()

@property (nonatomic, weak) IBOutlet UIButton* actionButton;

@end



@implementation ButtonCollectionViewCell


#pragma mark - button actions

- (void)buttonClick:(UIButton*)sender {
    if(self.dataItem) {
        [self.dataItem runAction];
    }
}


#pragma mark - init method

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.actionButton addTarget: self action: @selector(buttonClick:) forControlEvents: UIControlEventTouchUpInside];
}


#pragma mark - setters

- (void)setDataItem:(CollectionDataItem *)newDataItem {
    [super setDataItem: newDataItem];
    [self.actionButton setTitle: self.dataItem.title forState: UIControlStateNormal];
}


#pragma mark - Clear memory

- (void)dealloc {
    [self.actionButton removeFromSuperview];
    self.actionButton = nil;
}

@end
