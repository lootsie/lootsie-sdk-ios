//
//  CollectionViewActionCell.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "CollectionViewActionCell.h"


@interface CollectionViewActionCell ()

@end


@implementation CollectionViewActionCell


#pragma mark - Private methods

- (UIView*)findViewByAccessibilityIdentifier: (NSString*)aid inView: (UIView*)view {
    UIView* resView = nil;
    if(aid.length > 0) {
        if([view.accessibilityIdentifier isEqualToString: aid]) {
            resView = view;
        } else {
            if(view.subviews.count > 0) {
                for(UIView* subview in view.subviews) {
                    resView = [self findViewByAccessibilityIdentifier: aid inView: subview];
                    if(resView) {
                        break;
                    }
                }
            }
        }
    }
    return resView;
}

- (void)updateView:(UIView*)view {
    BOOL flag = self.dataItem.enabled;
    if(view) {
        view.clipsToBounds = YES;
        if([view isKindOfClass:[UIControl class]]) {
            ((UIControl*)view).enabled = flag;
        } else {
            view.alpha = flag ? 1.0 : 0.6;
        }
        if(view.subviews.count > 0) {
            for(UIView* v in view.subviews) {
                [self updateView: v];
            }
        }
    }
}

- (void)updateEnableState {
    if(self.dataItem) {
        NSString* cellId = [self.dataItem.title stringByReplacingOccurrencesOfString: @" " withString: @""];
        cellId = [cellId stringByAppendingString: @"Cell"];
        self.accessibilityIdentifier = cellId;

        [self updateView: self];
    }
}


#pragma mark - init method

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if(self) {
        self.indexPath = nil;
        self.dataItem = nil;
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib]; // FIX

}


#pragma mark - setters

- (void)setDataItem:(CollectionDataItem *)newDataItem {
    _dataItem = newDataItem;
    [self updateEnableState];
}


#pragma mark - Clear memory

- (void)dealloc {
    self.indexPath = nil;
    self.dataItem = nil;
}


@end
