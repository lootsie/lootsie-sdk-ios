//
//  SwitchCollectionViewCell.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "SwitchCollectionViewCell.h"


@interface SwitchCollectionViewCell ()

@property (nonatomic, weak)     IBOutlet UISwitch* switchView;
@property (nonatomic, weak)     IBOutlet UILabel* titleLabel;

@end


@implementation SwitchCollectionViewCell


#pragma mark - action

- (void)changeValue:(UISwitch*)sender {
    if([self.dataItem isKindOfClass: [SwitchDataItem class]]) {
        ((SwitchDataItem*)self.dataItem).on = sender.on;
        [((SwitchDataItem*)self.dataItem) runAction];
    }
}


#pragma mark - init methods

- (void)awakeFromNib {
    [super awakeFromNib]; // FIX

    [self.switchView addTarget: self action: @selector(changeValue:) forControlEvents: UIControlEventValueChanged];
}


#pragma mark - setters

- (void)setDataItem:(CollectionDataItem *)newDataItem {
    [super setDataItem: newDataItem];
    self.titleLabel.text = self.dataItem.title;
}


#pragma mark - clear

- (void)dealloc {
    self.switchView = nil;
    self.titleLabel = nil;
}

@end
