//
//  CollectionViewActionCell.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionDataItem.h"
#import "ButtonDataItem.h"
#import "SwitchDataItem.h"
#import "TextFieldDataItem.h"




@interface CollectionViewActionCell : UICollectionViewCell
@property (nonatomic, strong)   NSIndexPath* indexPath;
@property (nonatomic, strong)   CollectionDataItem* dataItem;

@end
