//
//  HeaderCollectionReusableView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "HeaderCollectionReusableView.h"

@interface HeaderCollectionReusableView ()
@property (nonatomic, weak)     IBOutlet UILabel* titleLabel;

@end


@implementation HeaderCollectionReusableView

- (void)awakeFromNib {
    [super awakeFromNib];
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapGesture:)];
    [self addGestureRecognizer:recognizer];
}

- (void)prepareForReuse {
    [super prepareForReuse];
    self.didTapBlock = nil;
}

- (void)setTitle:(NSString *)newTitle {
    _title = newTitle;
    self.titleLabel.text = _title;
}

- (void)onTapGesture:(id)sender {
    if (self.didTapBlock != nil) {
        self.didTapBlock();
    }
}

@end
