//  AppDelegate.m
//  Created by Fabio Teles on 8/11/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "AppDelegate.h"
#import "LTUILootsie.h"
#import "LTUIManager+Interstitial.h"


@interface AppDelegate () <LTInterstitialDelegate>

@end


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    NSString *appKey = @"4d200dd79daf88f7ef1fc659";  //@"69334a3de1c05929419164eafa53850e93db25f3";
    NSString* appKey = @"4d200dd79daf88f7ef1fc659"; // refresh

//    NSString* appKey = @"979cfff00c06f29f640f9be9"; // no refresh
    
    LTManager *lootsieManger = [LTManager sharedManager];
    //
    [[LTUIManager sharedManager] applyTheme:LTThemeTypeDark];
    [lootsieManger startEngineWithAppKey:appKey success:nil failure:nil];
    lootsieManger.interstitialDelegate = self;
        
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:@"Please select preferred color theme for your application"
                                                            preferredStyle:UIAlertControllerStyleAlert];
    NSArray *colorSheme = @[@"Dark", @"Light", @"Solitaire"];
    for (NSString *text in colorSheme) {
        UIAlertAction* action = [UIAlertAction actionWithTitle:text style:UIAlertActionStyleDefault
                                                      handler:^(UIAlertAction * action) {
                                                          LTThemeType type = (LTThemeType)[colorSheme indexOfObject:text];
                                                          [[LTUIManager sharedManager] applyTheme:type];
                                                      }];
        [alert addAction:action];
    }

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.window.rootViewController presentViewController:alert animated:YES completion:nil];
    });
    return YES;
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window {
    LTUIManager *lootsieManger = [LTUIManager sharedManager];
    if ([lootsieManger isShowingUI]) {
        return [lootsieManger supportedInterfaceOrientations];
    }
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
