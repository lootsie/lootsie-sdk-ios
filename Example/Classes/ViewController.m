//  ViewController.m
//  Created by Fabio Teles on 8/11/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

@import MessageUI;

#import <asl.h>

#import "LTUILootsie.h"
#import "ViewController.h"
#import "LTMWLogging.h"
#import "LTEventManager.h"

#import "LTUIManager+Achievement.h"

#import "LTManager.h"
#import "LTManager+Catalog.h"
#import "LTUIManager+Catalog.h"
#import "LTCatalogReward.h"
#import "LTCatalogRewardsList.h"

#import "LTUIManager+Interstitial.h"

#import "LTUIManager+Account.h"

#import "ContentsCollectionView.h"
#import "LTTestGlobalStylesViewController.h"

#import "LTIANGenericItem.h"
#import "LTIANCatalogItem.h"
#import "LTIANRewardItem.h"
#import "LTIANVideoItem.h"
#import "LTData.h"

@interface ViewController () <LTUIManagerDelegate, MFMailComposeViewControllerDelegate>

@property (weak, nonatomic) IBOutlet ContentsCollectionView* contentsCollectionView;

@end

@implementation ViewController

#pragma mark - Achievement methods
- (void)showAchievements {
    [[LTUIManager sharedManager] showAchievementsPage];
}
- (void)getAchievements {
    [[LTManager sharedManager] achievementsWithSuccess:nil failure:nil];
}
- (void)forceIAN {
    [[LTUIManager sharedManager] showIANWithItems: [LTIANItem getTestIANItems]
                                      bannerTitle: @"Test Notification"
                                    handlerAction: nil handlerClose: nil];
}
- (void)postAchievement {
    /* This will send one achievement although
     * when viewed might only show one tableViewCell
     */
    [[LTManager sharedManager] queueEvent:23 additionalNumber:nil success:nil failure:nil];
    [[LTManager sharedManager].eventManager sendEvents];
}
- (CollectionlDataItemsSection*)achievementsSection {
    __weak typeof(self) tmp = self;
    CollectionlDataItemsSection* achievements = [CollectionlDataItemsSection sectionWithTitle: @"Achievements"];
    [achievements addItem: [ButtonDataItem buttonWithTitle:@"Show Achievements" actionCode: ^(CollectionDataItem *item) {
        [tmp showAchievements];
    }]];
    [achievements addItem: [ButtonDataItem buttonWithTitle: @"Get Achievements" actionCode:^(CollectionDataItem *item) {
        [tmp getAchievements];
    }]];
    [achievements addItem: [ButtonDataItem buttonWithTitle: @"Force IAN" actionCode: ^(CollectionDataItem *item) {
        [tmp forceIAN];
    }]];
    
    [achievements addItem: [ButtonDataItem buttonWithTitle: @"Post Achievement" actionCode: ^(CollectionDataItem *item) {
        [tmp postAchievement];
    }]];
    return achievements;
}

#pragma mark - Catalog methods
- (void)getRewards {
    [[LTManager sharedManager] rewardsWithSuccess: nil failure: nil];
}
- (void)redeemRewards {
    LTCatalogReward *reward = nil;
    LTCatalogRewardsList *catalog = [LTManager sharedManager].data.catalog;
    if (catalog.featuredRewards.count > 0) {
        reward = [catalog.featuredRewards firstObject];
    }
    else if (catalog.rewards.count > 0) {
        reward = [catalog.rewards firstObject];
    }
    [[LTManager sharedManager] redeemRewardWithId:reward.rewardId
                                             cost: reward.points
                                          success:nil failure:nil];
}
- (void)showRewards {
    [LTManager sharedManager].data.app.refreshEnabled = YES;
    [[LTUIManager sharedManager] showRewardsPage];
}
- (void)showRewardsNoRefresh {
    [LTManager sharedManager].data.app.refreshEnabled = NO;
    [[LTUIManager sharedManager] showRewardsPage];
}
- (CollectionlDataItemsSection*)catalogSection {
    __weak typeof(self) tmp = self;
    CollectionlDataItemsSection* redeems = [CollectionlDataItemsSection sectionWithTitle: @"Catalog"];
    [redeems addItem: [ButtonDataItem buttonWithTitle: @"Show Rewards" actionCode: ^(CollectionDataItem *item) {
        [tmp showRewards];
    }]];
    
    [redeems addItem: [ButtonDataItem buttonWithTitle: @"Get Rewards" actionCode: ^(CollectionDataItem *item) {
        [tmp getRewards];
    }]];
    [redeems addItem: [ButtonDataItem buttonWithTitle: @"Redeem Reward" actionCode: ^(CollectionDataItem *item) {
        [tmp redeemRewards];
    }]];
    return redeems;
}

#pragma mark - Account methods
- (void)showMyAccount {
    [[LTUIManager sharedManager] showAccountPage];
}
- (CollectionlDataItemsSection*)accountSection {
    __weak typeof(self) tmp = self;
    CollectionlDataItemsSection* account = [CollectionlDataItemsSection sectionWithTitle: @"Account"];
    [account addItem: [ButtonDataItem buttonWithTitle: @"Show My Account" actionCode: ^(CollectionDataItem *item) {
        [tmp showMyAccount];
    }]];
    return account;
}

#pragma mark - Activate Account methods
- (void)showReactivateAccountWithUI {
    [[LTUIManager sharedManager] reactivateAccountWithUI: YES];
}
- (void)showReactivateAccount {
    [[LTUIManager sharedManager] reactivateAccountWithUI: NO];
}
- (CollectionlDataItemsSection*)reactivateAccountSection {
    __weak typeof(self) tmp = self;
    CollectionlDataItemsSection* account = [CollectionlDataItemsSection sectionWithTitle: @"Reactivate Account"];
    [account addItem: [ButtonDataItem buttonWithTitle: @"Reactivate With UI" actionCode: ^(CollectionDataItem *item) {
        [tmp showReactivateAccountWithUI];
    }]];
    
    
    [account addItem: [ButtonDataItem buttonWithTitle: @"Reactivate" actionCode: ^(CollectionDataItem *item) {
        [tmp showReactivateAccount];
    }]];
    
    
    
    return account;
}

#pragma mark - Others methods
- (void)sendLocation {
    
    //    [[LTManager sharedManager] queueEvent:24 additionalNumber:nil success:nil failure:nil];
    //    [[LTManager sharedManager] queueEngagementEventWithSuccess: nil failure: nil];
    
    
    NSMutableArray *events = [NSMutableArray array];
    NSArray *eventID = @[@(24)];//@[@(10), @(13), @(16), @(18), @(19), @(20), @(21), @(22), @(23), @(24)];
    for (NSNumber *number in eventID) {
        LTUserActivity *activity = [LTUserActivity new];
        activity.applicationFlag = 1;
        activity.eventID = [number integerValue];
        [events addObject:activity];
    }
    [[LTManager sharedManager] sendUserActivity:events success:nil failure:nil];
    // [[LTManager sharedManager].eventManager addEvent:events];
}
- (void)resetInterstitialCounter {
    //[LTVideoInterstitialSequenceOperation resetCounter];
}
- (void)showAbout {
    [[LTUIManager sharedManager] showAboutPageOnViewController:self fromStartPoint:self.view.center];
}
- (void)showPopup {
    [[LTUIManager sharedManager] showPopupWithTitle: @"UH OH!"
                                        description: @"Looks like something went wrong. Try logging out and then right back in. If that doesn't work, contact support@lootsie.com or throw your phone against the wall. Just kidding, definitely don't do that."
                                        withSadFace:YES
                                         buttonText: @"CLOSE"
                                     didCloseAction: nil];
}
- (void)showTerms {
    [[LTUIManager sharedManager] showTermsPageOnViewController:self fromStartPoint:self.view.center];
}
- (void)showGuideStyles {
    LTTestGlobalStylesViewController *controller = [[LTTestGlobalStylesViewController alloc] initWithNibName:@"LTTestGlobalStylesViewController" bundle:nil];
    [self presentViewController:controller animated:YES completion:nil];
}
- (void)sendLog {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* mailController = [[MFMailComposeViewController alloc] init];
        mailController.mailComposeDelegate = self;
        NSString *subject = [NSString stringWithFormat:@"LOG %@", [NSDate date]];
        [mailController setSubject: subject];
        [mailController setMessageBody:self.logTextView.text isHTML:NO];
        [self presentViewController: mailController animated: YES completion: nil];
    }
}
- (void)displayAlertsOverAppUserInterface:(BOOL)flag {
    [LTUIManager sharedManager].displayAlertsOverAppUserInterface = flag;
    
}
- (CollectionlDataItemsSection*)othersSection {
    __weak typeof(self) sendEvents = self;
    CollectionlDataItemsSection* others = [CollectionlDataItemsSection sectionWithTitle: @"Others"];
    [others addItem: [ButtonDataItem buttonWithTitle: @"Send Events" actionCode: ^(CollectionDataItem *item) {
        [sendEvents sendLocation];
    }]];
    [others addItem: [ButtonDataItem buttonWithTitle: @"Reset Interstitial Counter" actionCode: ^(CollectionDataItem *item) {
        [sendEvents resetInterstitialCounter];
    }]];
    [others addItem: [ButtonDataItem buttonWithTitle: @"Show About" actionCode: ^(CollectionDataItem *item) {
        [sendEvents showAbout];
    }]];
    [others addItem: [ButtonDataItem buttonWithTitle: @"Show Popup" actionCode: ^(CollectionDataItem *item) {
        [sendEvents showPopup];
    }]];
    [others addItem: [ButtonDataItem buttonWithTitle: @"Show Terms Service" actionCode: ^(CollectionDataItem *item) {
        [sendEvents showTerms];
    }]];
    [others addItem: [ButtonDataItem buttonWithTitle: @"Show Global styles" actionCode: ^(CollectionDataItem *item) {
        [sendEvents showGuideStyles];
    }]];
    [others addItem: [ButtonDataItem buttonWithTitle: @"Send log" actionCode: ^(CollectionDataItem *item) {
        [sendEvents sendLog];
    }]];
    SwitchDataItem* item = [SwitchDataItem switchItemWithTitle: @"Display Allerts Over App User Interface" actionCode: ^(CollectionDataItem *item) {
        [sendEvents displayAlertsOverAppUserInterface: ((SwitchDataItem*)item).on];
    }];
    item.on = [LTUIManager sharedManager].displayAlertsOverAppUserInterface;
    [others addItem: item];
    return others;
}

#pragma mark - View controller
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _ltLogUIInput = self.logTextView;
    [LTUIManager sharedManager].delegate = self;

    [self loadData];
}
- (void)loadData {
    
    LTManager* manager = [LTManager sharedManager];
    
    NSMutableArray* dataItems = [NSMutableArray array];
    
    if(manager.hasAccountBeenDeactivated  ) {
        [dataItems addObject: [self reactivateAccountSection]];
    } else {
        [dataItems addObject: [self othersSection]];
        [dataItems addObject: [self achievementsSection]];
        [dataItems addObject: [self catalogSection]];
        [dataItems addObject: [self accountSection]];
    }
    
    [self.contentsCollectionView setItems: dataItems];
}
- (BOOL)shouldAutorotate {
    return YES;
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return (UIInterfaceOrientationMaskAllButUpsideDown);
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)handleLogSwitch:(UISwitch *)sender {
    if (sender.isOn) {
        _ltLogUIInput = self.logTextView;
    } else {
        self.logTextView.text = nil;
        _ltLogUIInput = nil;
    }
}

#pragma mark - LTUIManager delegate
- (void)lootsieUIManagerWillShowUI {
    LTLogDebug(@"%@", @"Lootsie UI will show");
}
- (void)lootsieUIManagerDidCloseUI {
    LTLogDebug(@"%@", @"Lootsie UI did close");
}
- (void)lootsieUIManagerAccountActivationStatusChange {
    LTLogDebug(@"%@", @"Lootsie Activation Status changed.");
    [self loadData];
}
- (void)lootsieUIManagerStatusChanged:(BOOL)enabled {
    LTLogDebug(@"%@", @"lootsieUIManagerStatusChanged");
    
    if (enabled == YES) {
        LTLogDebug(@"%@", @"show ui elements");
    } else {
        LTLogDebug(@"%@", @"hide ui elements");
    }
}
- (UIViewController*)presentationContextForUIManager {
    //    return self;
    return nil;
}
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(nullable NSError *)error {
    [controller dismissViewControllerAnimated: YES completion: nil];
}

@end
