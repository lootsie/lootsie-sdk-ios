//
//  LTUITestCase.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 4/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTUITestCase.h"
#import <sys/sysctl.h>

@implementation LTUITestCase

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    self.app = [[XCUIApplication alloc] init];
    [self.app launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
    [self.app terminate];
}


//- (BOOL)isElementSize:(CGSize)fSize equeslTo:(CGSize)sSize {
//    NSString* deviceName = nil;
//    
//#if TARGET_IPHONE_SIMULATOR
//    deviceName = [[NSProcessInfo processInfo].environment objectForKey: @"SIMULATOR_DEVICE_NAME"];
//#else
//    size_t size;
//    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
//    char *name = malloc(size);
//    sysctlbyname("hw.machine", name, &size, NULL, 0);
//    NSString* machine = [NSString stringWithUTF8String:name];
//    free(name);
//    
//    if([machine isEqualToString:@"iPad1,1"] ||
//       [machine isEqualToString:@"iPad1,2"])
//    {
//        deviceName = @"iPad";
//    }
//    else if([machine isEqualToString:@"iPad2,1"] ||
//            [machine isEqualToString:@"iPad2,2"] ||
//            [machine isEqualToString:@"iPad2,3"] ||
//            [machine isEqualToString:@"iPad2,4"])
//    {
//        deviceName = @"iPad 2";
//    }
//    else if([machine isEqualToString:@"iPad2,5"] ||
//            [machine isEqualToString:@"iPad2,6"] ||
//            [machine isEqualToString:@"iPad2,7"])
//    {
//        deviceName = @"iPad mini";
//    }
//    else if([machine isEqualToString:@"iPad3,1"] ||
//            [machine isEqualToString:@"iPad3,2"] ||
//            [machine isEqualToString:@"iPad3,3"])
//    {
//        deviceName = @"iPad 3";
//    }
//    else if([machine isEqualToString:@"iPad3,4"] ||
//            [machine isEqualToString:@"iPad3,5"] ||
//            [machine isEqualToString:@"iPad3,6"])
//    {
//        deviceName = @"iPad 4";
//    }
//    else if([machine isEqualToString:@"iPad4,1"] ||
//            [machine isEqualToString:@"iPad4,2"] ||
//            [machine isEqualToString:@"iPad4,3"])
//    {
//        deviceName = @"iPad Air";
//    }
//    else if([machine isEqualToString:@"iPad4,4"] ||
//            [machine isEqualToString:@"iPad4,5"] ||
//            [machine isEqualToString:@"iPad4,6"])
//    {
//        deviceName = @"iPad mini 2";
//    }
//    else if([machine isEqualToString:@"iPad5,3"] ||
//            [machine isEqualToString:@"iPad5,4"])
//    {
//        deviceName = @"iPad Air 2";
//    }
//    else if([machine isEqualToString:@"iPad4,7"] ||
//            [machine isEqualToString:@"iPad4,8"])
//    {
//        deviceName = @"iPad mini 3";
//    }
//    else if([machine isEqualToString:@"iPhone1,1"])
//    {
//        deviceName = @"iPhone";
//    }
//    else if([machine isEqualToString:@"iPhone1,2"])
//    {
//        deviceName = @"iPhone 3G";
//    }
//    else if([machine isEqualToString:@"iPhone2,1"])
//    {
//        deviceName = @"iPhone 3GS";
//    }
//    else if([machine isEqualToString:@"iPhone3,1"] ||
//            [machine isEqualToString:@"iPhone3,2"] ||
//            [machine isEqualToString:@"iPhone3,3"])
//    {
//        deviceName = @"iPhone 4";
//    }
//    else if([machine isEqualToString:@"iPhone4,1"])
//    {
//        deviceName = @"iPhone 4S";
//    }
//    else if([machine isEqualToString:@"iPhone5,1"] ||
//            [machine isEqualToString:@"iPhone5,2"])
//    {
//        deviceName = @"iPhone 5";
//    }
//    else if([machine isEqualToString:@"iPhone5,3"] ||
//            [machine isEqualToString:@"iPhone5,4"])
//    {
//        deviceName = @"iPhone 5C";
//    }
//    else if([machine isEqualToString:@"iPhone6,1"] ||
//            [machine isEqualToString:@"iPhone6,2"])
//    {
//        deviceName = @"iPhone 5S";
//    }
//    else if([machine isEqualToString:@"iPhone7,1"])
//    {
//        deviceName = @"iPhone 6";
//    }
//    else if([machine isEqualToString:@"iPhone7,2"])
//    {
//        deviceName = @"iPhone 6 Plus";
//    }
//    else if([machine isEqualToString:@"iPhone8,1"])
//    {
//        deviceName = @"iPhone 6s";
//    }
//    else if([machine isEqualToString:@"iPhone8,2"])
//    {
//        deviceName = @"iPhone 6s Plus";
//    }
//    else if([machine isEqualToString:@"iPod1,1"])
//    {
//        deviceName = @"iPod touch";
//    }
//    else if([machine isEqualToString:@"iPod2,1"])
//    {
//        deviceName = @"iPod touch 2";
//    }
//    else if([machine isEqualToString:@"iPod3,1"])
//    {
//        deviceName = @"iPod touch 3";
//    }
//    else if([machine isEqualToString:@"iPod4,1"])
//    {
//        deviceName = @"iPod touch 4";
//    }
//    else if([machine isEqualToString:@"iPod5,1"])
//    {
//        deviceName = @"iPod touch 5";
//    }
//#endif
//    
//    CGFloat k = 1.0;
//    if(deviceName.length > 0)
//    {
//        if([deviceName isEqualToString: @"iPhone 6"] || [deviceName isEqualToString: @"iPhone 6s"])
//        {
//            k = 1.171895;
//        }
//        else if([deviceName isEqualToString: @"iPhone 6 Plus"] || [deviceName isEqualToString: @"iPhone 6s Plus"])
//        {
//            k = 1.29375;
//        }
//    }
//    
//    CGSize firstSize = CGSizeMake(ceil(fSize.width * k), ceil(fSize.height * k));
//    CGSize secondSize = CGSizeMake(ceil(sSize.width), ceil(sSize.height));
//    
//    return CGSizeEqualToSize(firstSize, secondSize);
//}

- (XCUIElement *)waitUIElement:(XCUIElementQuery *)elementQuery withID:(NSString *)idElement {
    XCUIElement* element = elementQuery[idElement];
    NSPredicate* predicate = [NSPredicate predicateWithFormat: @"exists == 1"];
    [self expectationForPredicate:predicate evaluatedWithObject:element handler: nil];
    [self waitForExpectationsWithTimeout:15 handler:nil];
    return element;
}

- (BOOL)scrollToCellIdentifier:(NSString*)cellIdentifier
         collectionViewElement:(XCUIElement*)collectionView
                      scrollUp:(BOOL)scrollUp
                  fullyVisible:(BOOL)fullyVisible {
    BOOL rtn = NO;
    NSString* lastMidCellID = @"";
    CGRect lastMidCellRect = CGRectZero;
    XCUIElement* currentMidCell = [collectionView.cells elementBoundByIndex:(collectionView.cells.count / 2)];
    
    // Scroll until the requested cell is hittable, or until we try and scroll but nothing changes
    while (lastMidCellID != currentMidCell.identifier || !CGRectEqualToRect(lastMidCellRect, currentMidCell.frame)) {
        if ([collectionView.cells matchingIdentifier: cellIdentifier].count > 0 && collectionView.cells[cellIdentifier].exists &&
            collectionView.cells[cellIdentifier].hittable) {
            rtn = YES;
            break;
        }
        
        lastMidCellID = currentMidCell.identifier;
        lastMidCellRect = currentMidCell.frame;      // Need to capture this before the scroll
        
        if (scrollUp) {
            XCUICoordinate* coordinate = [collectionView coordinateWithNormalizedOffset: (CGVectorMake(0.99, 0.4))];
            XCUICoordinate* endCoordinate = [collectionView coordinateWithNormalizedOffset: (CGVectorMake(0.99, 0.9))];
            [coordinate pressForDuration: 0.01 thenDragToCoordinate: endCoordinate];
        } else {
            XCUICoordinate* coordinate = [collectionView coordinateWithNormalizedOffset: (CGVectorMake(0.99, 0.9))];
            XCUICoordinate* endCoordinate = [collectionView coordinateWithNormalizedOffset: (CGVectorMake(0.99, 0.4))];
            [coordinate pressForDuration: 0.01 thenDragToCoordinate: endCoordinate];
        }
        currentMidCell = [collectionView.cells elementBoundByIndex: (collectionView.cells.count / 2)];
    }
    
    // If we want cell fully visible, do finer scrolling (1/2 height of cell relative to collection view) until cell frame fully contained by collection view frame
    if (fullyVisible) {
        XCUIElement* cell = collectionView.cells[cellIdentifier];
        CGFloat scrollDistance = (cell.frame.size.height / 2) / collectionView.frame.size.height;
        
        while (!CGRectContainsRect(collectionView.frame, cell.frame)) {
            if (cell.frame.origin.y < collectionView.frame.origin.y) {
                XCUICoordinate* coordinate = [collectionView coordinateWithNormalizedOffset: (CGVectorMake(0.99, 0.5))];
                XCUICoordinate* endCoordinate = [collectionView coordinateWithNormalizedOffset: (CGVectorMake(0.99, 0.5 + scrollDistance))];
                [coordinate pressForDuration: 0.01 thenDragToCoordinate: endCoordinate];
            } else {
                XCUICoordinate* coordinate = [collectionView coordinateWithNormalizedOffset: (CGVectorMake(0.99, 0.5))];
                XCUICoordinate* endCoordinate = [collectionView coordinateWithNormalizedOffset: (CGVectorMake(0.99, 0.5 - scrollDistance))];
                [coordinate pressForDuration: 0.01 thenDragToCoordinate: endCoordinate];
            }
        }
    }
    
    return rtn;
}

@end
