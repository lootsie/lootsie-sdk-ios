//
//  LTUITestCase.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 4/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface LTUITestCase : XCTestCase

@property (nonatomic, strong)   XCUIApplication* app;

//- (BOOL)isElementSize:(CGSize)fSize equeslTo:(CGSize)sSize;
- (XCUIElement *)waitUIElement:(XCUIElementQuery *)elementQuery withID:(NSString *)idElement;
- (BOOL)scrollToCellIdentifier:(NSString*)cellIdentifier
         collectionViewElement:(XCUIElement*)collectionView
                      scrollUp:(BOOL)scrollUp
                  fullyVisible:(BOOL)fullyVisible;

@end
