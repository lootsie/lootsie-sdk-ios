//
//  LTLootsieiOSUITests.m
//  LTLootsieiOSUITests
//
//  Created by Andrew Shapovalov on 4/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface LTLootsieiOSUITests : XCTestCase

@property (nonatomic, strong)   XCUIApplication* app;

@end

@implementation LTLootsieiOSUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    self.app = [[XCUIApplication alloc] init];
    [self.app launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testRedeemUI {
    XCUIElement* btnShowRewards = self.app.buttons[@"btnShowRewards"];
    NSPredicate* btnShowRewardsExists = [NSPredicate predicateWithFormat: @"exists == 1"];
    
    [self expectationForPredicate: btnShowRewardsExists evaluatedWithObject: btnShowRewards handler: nil];
    [self waitForExpectationsWithTimeout:5 handler:nil];
    XCTAssert(btnShowRewards.exists);
    [btnShowRewards tap];
    
    XCUIElement* cell = [self.app.cells elementBoundByIndex:1];
    const CGFloat cellHeight = cell.frame.size.height;
    XCTAssert(cell.exists);
    
    //Check details UI logic.
    XCUIElement* btnDetailsButton = cell.buttons[@"btnDetails_0"];
    XCTAssert(btnDetailsButton.exists, @"Details Button doesn't exist.");
    XCTAssert(cellHeight == cell.frame.size.height, @"Details View must be hidden");
    [btnDetailsButton tap];
    CGFloat cellHeightWithDetails = cell.frame.size.height;
    XCTAssert(cellHeight < cellHeightWithDetails, @"Details View must be shown");
    [btnDetailsButton tap];
    XCTAssert(cellHeight == cell.frame.size.height, @"Details View must be hidden");

    //Check redeem now UI logic
    XCUIElement* btnRedeemButton = cell.buttons[@"btnRedeemNow_0"];
    XCTAssert(btnRedeemButton.exists);
    XCTAssert(cellHeight == cell.frame.size.height, @"Redeem view must be hidden");
    [btnRedeemButton tap];
    XCTAssert(cell.frame.size.height > cellHeightWithDetails, @"Redeem view must be shown");
    [btnRedeemButton tap];
    XCTAssert(cellHeight == cell.frame.size.height, @"Details View must be hidden");
    
    //Redeem points UI logic
    [btnRedeemButton tap];
    sleep(3);
    XCTAssert(cell.frame.size.height > cellHeightWithDetails, @"Redeem view must be shown");
    XCUIElement* emailField = self.app.textFields[@"emailField"];
    XCUIElement* redeemButton = self.app.buttons[@"btnRedeem"];
    XCTAssert(emailField.exists);
    XCTAssert(redeemButton.exists);
    [emailField tap];
    [emailField typeText:@"andrew.shapovalov@consulti.ca"];
    [redeemButton tap];
    
    XCUIElement *okButton = self.app.alerts[@"Uh oh!"].collectionViews.buttons[@"OK"];
    [okButton tap];
    [self.app.buttons[@"btnClose"] tap];
}

@end
