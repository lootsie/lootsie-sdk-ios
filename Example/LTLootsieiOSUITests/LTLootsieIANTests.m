//
//  LTLootsieIANTests.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 4/21/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LTUITestCase.h"

@interface LTLootsieIANTests : LTUITestCase

@end



@implementation LTLootsieIANTests

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testIANView {
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCUIElement* achievements = [self waitUIElement: collectionViewsQuery.staticTexts withID:@"Achievements"];
    XCTAssert(achievements.exists);
    //...
    [achievements tap];

    XCUIElement* btnForceIAN = [self waitUIElement: self.app.collectionViews.buttons withID: @"Force IAN"];
    XCTAssert(btnForceIAN.exists);
    //...
    [btnForceIAN tap];
    
    XCUIElement* ianView = [self waitUIElement: self.app.otherElements withID: @"ianView"];
    XCTAssert(ianView.exists);
    
    XCUIElement* pointLabel = [self waitUIElement: self.app.staticTexts withID: @"pointLabel"];
    XCUIElement* typePointsLabel = [self waitUIElement: self.app.staticTexts withID: @"typePointsLabel"];
    XCUIElement* titleLabel = [self waitUIElement: self.app.staticTexts withID: @"titleLabel"];
    XCUIElement* descLabel = [self waitUIElement: self.app.staticTexts withID: @"descLabel"];
    XCUIElement* btnAction = [self waitUIElement: self.app.buttons withID: @"btnAction"];
    XCUIElement* btnClose = [self waitUIElement: self.app.buttons withID: @"btnClose"];
    
    XCTAssert(pointLabel.exists);
    XCTAssert(typePointsLabel.exists);
    XCTAssert(titleLabel.exists);
    XCTAssert(descLabel.exists);
    XCTAssert(btnAction.exists);
    XCTAssert(btnClose.exists);
    
    XCTAssert([pointLabel.label isKindOfClass: [NSString class]] && ((NSString*)pointLabel.label).length > 0);
    XCTAssert([typePointsLabel.label isKindOfClass: [NSString class]] && ((NSString*)typePointsLabel.label).length > 0);
    XCTAssert([titleLabel.label isKindOfClass: [NSString class]] && ((NSString*)titleLabel.label).length > 0);
    XCTAssert([descLabel.label isKindOfClass: [NSString class]] && ((NSString*)descLabel.label).length > 0);
    XCTAssert(CGSizeEqualToSize(CGSizeMake(121.0, 35.0), btnAction.frame.size));
    XCTAssert(CGSizeEqualToSize(CGSizeMake(121.0, 35.0), btnClose.frame.size));
    
    [btnClose tap];
    
    XCTAssert(!ianView.exists);
}

- (void)testIANViewActionButton {
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCUIElement* achievements = [self waitUIElement: collectionViewsQuery.staticTexts withID:@"Achievements"];
    XCTAssert(achievements.exists);
    //...
    [achievements tap];
    
    XCUIElement* btnForceIAN = [self waitUIElement: self.app.collectionViews.buttons withID: @"Force IAN"];
    XCTAssert(btnForceIAN.exists);
    //...
    [btnForceIAN tap];
    
    XCUIElement* ianView = [self waitUIElement: self.app.otherElements withID: @"ianView"];
    XCTAssert(ianView.exists);
    
    XCUIElement* pointLabel = [self waitUIElement: self.app.staticTexts withID: @"pointLabel"];
    XCUIElement* typePointsLabel = [self waitUIElement: self.app.staticTexts withID: @"typePointsLabel"];
    XCUIElement* titleLabel = [self waitUIElement: self.app.staticTexts withID: @"titleLabel"];
    XCUIElement* descLabel = [self waitUIElement: self.app.staticTexts withID: @"descLabel"];
    XCUIElement* btnAction = [self waitUIElement: self.app.buttons withID: @"btnAction"];
    XCUIElement* btnClose = [self waitUIElement: self.app.buttons withID: @"btnClose"];
    
    XCTAssert(pointLabel.exists);
    XCTAssert(typePointsLabel.exists);
    XCTAssert(titleLabel.exists);
    XCTAssert(descLabel.exists);
    XCTAssert(btnAction.exists);
    XCTAssert(btnClose.exists);
    
    XCTAssert([pointLabel.label isKindOfClass: [NSString class]] && ((NSString*)pointLabel.label).length > 0);
    XCTAssert([typePointsLabel.label isKindOfClass: [NSString class]] && ((NSString*)typePointsLabel.label).length > 0);
    XCTAssert([titleLabel.label isKindOfClass: [NSString class]] && ((NSString*)titleLabel.label).length > 0);
    XCTAssert([descLabel.label isKindOfClass: [NSString class]] && ((NSString*)descLabel.label).length > 0);
    XCTAssert(CGSizeEqualToSize(CGSizeMake(121.0, 35.0), btnAction.frame.size));
    XCTAssert(CGSizeEqualToSize(CGSizeMake(121.0, 35.0), btnClose.frame.size));

    //...
    [btnAction tap];
    
    XCUIElement* rewardsView = [self waitUIElement: self.app.otherElements withID: @"rewardsView"];
    XCTAssert(rewardsView.exists);
    XCTAssert(!ianView.exists);
    
    XCUIElement* btnRewardsClose = [self waitUIElement: self.app.buttons withID: @"btnClose"];
    XCTAssert(btnRewardsClose.exists);
    [btnRewardsClose tap];
    
    XCTAssert(!rewardsView.exists);
}

@end
