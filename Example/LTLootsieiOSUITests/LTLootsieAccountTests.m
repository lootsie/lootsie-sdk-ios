//
//  LTLootsieAccountTests.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 4/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface LTLootsieAccountTests : XCTestCase

@property (nonatomic, strong)   XCUIApplication* app;

@end




@implementation LTLootsieAccountTests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    self.app = [[XCUIApplication alloc] init];
    [self.app launch];
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testAccountUI {
//    XCUIElement* btnMyAccount = self.app.buttons[@"btnMyAccount"];
//    NSPredicate* btnMyAccountExists = [NSPredicate predicateWithFormat: @"exists == 1"];
//    
//    [self expectationForPredicate: btnMyAccountExists evaluatedWithObject: btnMyAccount handler: nil];
//    [self waitForExpectationsWithTimeout: 5 handler:nil];
//    XCTAssert(btnMyAccount.exists);
//    [btnMyAccount tap];
//    
//    XCUIElementQuery *elementsQuery = self.app.scrollViews.otherElements;
//    
//    XCUIElement* mergeButton = elementsQuery.buttons[@"btnMerge"];
//    XCUIElement* accountEmailField = elementsQuery.textFields[@"emailField"];
//    
//    XCUIElement* editButton = elementsQuery.buttons[@"btnEdit"];
//    XCUIElement* updateButton = elementsQuery.buttons[@"btnUpdate"];
//    
//    XCUIElement* firstNameField = elementsQuery.textFields[@"firstNameField"];
//    XCUIElement* lastNameField = elementsQuery.textFields[@"lastNameField"];
//    XCUIElement* emailField = elementsQuery.textFields[@"userEmailField"];
//    XCUIElement* cityField = elementsQuery.textFields[@"cityField"];
//    XCUIElement* zipcodeField = elementsQuery.textFields[@"zipcodeField"];
//    
//    XCTAssert(accountEmailField.exists, @"Account Email Field doesn't exist.");
//    XCTAssert(mergeButton.exists);
//    
//    XCTAssert(editButton.exists);
//    XCTAssert(updateButton.exists);
//    
//    XCTAssert(firstNameField.exists);
//    XCTAssert(lastNameField.exists);
//    XCTAssert(emailField.exists);
//    XCTAssert(cityField.exists);
//    XCTAssert(zipcodeField.exists);
    
    
//    if(![accountEmailField.value isEqualToString: @"andrew.shapovalov@consulti.ca"])
//    {
//        [accountEmailField tap];
//        [elementsQuery.textFields[@"emailField"] typeText: @"andrew.shapovalov@consulti.ca\n"];
//    }
//    else
//    {
//        if([mergeButton.label isEqualToString: @"Logout"])
//        {
//            [mergeButton tap];
//            XCTAssert([mergeButton.label isEqualToString: @"Merge Points"]);
//            
//            [accountEmailField tap];
//            [elementsQuery.textFields[@"emailField"] typeText: @"andrew.shapovalov@consulti.ca\n"];
//        }
//        else
//        {
//            [accountEmailField tap];
//            [elementsQuery.textFields[@"emailField"] typeText: @"andrew.shapovalov@consulti.ca\n"];
//        }
//    }
//    
//    XCTAssert([mergeButton.label isEqualToString: @"Merge Points"]);
//    [mergeButton tap];
//    XCTAssert([mergeButton.label isEqualToString: @"Logout"]);
//    XCTAssert([emailField.value isEqualToString: accountEmailField.value]);
//    
//    XCTAssert(editButton.selected == NO && (firstNameField.enabled == NO && lastNameField.enabled == NO && emailField.enabled == NO &&
//                                            cityField.enabled == NO && zipcodeField.enabled == NO && updateButton.enabled == NO));
//    [editButton tap];
//    XCTAssert(editButton.selected == YES && (firstNameField.enabled == YES && lastNameField.enabled == YES && emailField.enabled == YES &&
//                                             cityField.enabled == YES && zipcodeField.enabled == YES && updateButton.enabled == YES));
//    
//    [firstNameField tap];
//    [firstNameField typeText: @"Andrew\n"];
//    
//    [lastNameField tap];
//    [lastNameField typeText: @"Shapovalov\n"];
//    
//    [cityField tap];
//    [cityField typeText: @"Kharkiv\n"];
//    
//    [zipcodeField tap];
//    [zipcodeField typeText: @"61000\n"];
//    
//    [updateButton tap];
    //    XCTAssert(<#expression, ...#>)
}

@end
