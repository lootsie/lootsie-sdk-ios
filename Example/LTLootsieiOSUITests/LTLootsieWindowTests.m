//
//  LTLootsieWindowTests.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 4/21/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//


#import <XCTest/XCTest.h>
#import "LTUITestCase.h"


@interface LTLootsieWindowTests : LTUITestCase

@end



@implementation LTLootsieWindowTests

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testRewardsWindow
{
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCUIElement* catalog = [self waitUIElement: collectionViewsQuery.staticTexts withID:@"Catalog"];
    XCTAssert(catalog.exists);
    //...
    [catalog tap];
    
    XCUIElement* btnShowRewards = [self waitUIElement: self.app.buttons withID: @"Show Rewards"];
    XCTAssert(btnShowRewards.exists);
    //...
    [btnShowRewards tap];
    
    XCUIElement* rewardsView = [self waitUIElement: self.app.otherElements withID: @"rewardsView"];
    XCTAssert(rewardsView.exists);
    
    XCUIElement* btnClose = [self waitUIElement: self.app.buttons withID: @"btnClose"];
    XCTAssert(btnClose.exists);
    [btnClose tap];
    
    XCTAssert(!rewardsView.exists);
}

- (void)testAchievementsWindow
{
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCUIElement* achievements = [self waitUIElement: collectionViewsQuery.staticTexts withID:@"Achievements"];
    XCTAssert(achievements.exists);
    //...
    [achievements tap];
    
    XCUIElement* btnShowAchievents = [self waitUIElement: self.app.buttons withID: @"Show Achievements"];
    XCTAssert(btnShowAchievents.exists);
    //...
    [btnShowAchievents tap];
    
    XCUIElement* achievementsView = [self waitUIElement: self.app.otherElements withID: @"achievementsView"];
    XCTAssert(achievementsView.exists);
    
    XCUIElement* btnClose = [self waitUIElement: self.app.buttons withID: @"btnClose"];
    XCTAssert(btnClose.exists);
    [btnClose tap];
    
    NSPredicate* achievementsViewNotExists = [NSPredicate predicateWithFormat: @"exists == 0"];
    [self expectationForPredicate: achievementsViewNotExists evaluatedWithObject: achievementsView handler: nil];
    [self waitForExpectationsWithTimeout: 5 handler:nil];
    XCTAssert(!achievementsView.exists);
}

- (void)testTermsAndServiceWindow
{
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCUIElement* others = [self waitUIElement: collectionViewsQuery.staticTexts withID:@"Others"];
    XCTAssert(others.exists);
    //...
    [others tap];
    
    XCUIElement* btnShowTerms = [self waitUIElement: self.app.buttons withID: @"Show Terms Service"];
    XCTAssert(btnShowTerms.exists);
    //...
    [btnShowTerms tap];
    
    XCUIElement* termsAndServiceView = [self waitUIElement: self.app.otherElements withID: @"termsAndServiceView"];
    XCTAssert(termsAndServiceView.exists);
    
    XCUIElement* btnClose = [self waitUIElement: self.app.buttons withID: @"btnClose"];
    XCTAssert(btnClose.exists);
    [btnClose tap];
    
    XCTAssert(!termsAndServiceView.exists);
}

- (void)testAboutWindow
{
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCUIElement* others = [self waitUIElement: collectionViewsQuery.staticTexts withID:@"Others"];
    XCTAssert(others.exists);
    //...
    [others tap];
    
    XCUIElement* btnShowAbout = [self waitUIElement: self.app.buttons withID: @"Show About"];
    XCTAssert(btnShowAbout.exists);
    //...
    [btnShowAbout tap];
    
    XCUIElement* aboutView = [self waitUIElement: self.app.otherElements withID: @"aboutView"];
    XCTAssert(aboutView.exists);
    
    XCUIElement* btnClose = [self waitUIElement: self.app.buttons withID: @"btnClose"];
    XCTAssert(btnClose.exists);
    [btnClose tap];
    
    XCTAssert(!aboutView.exists);
}

- (void)testIANWindow
{
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCUIElement* achievements = [self waitUIElement: collectionViewsQuery.staticTexts withID:@"Achievements"];
    XCTAssert(achievements.exists);
    //...
    [achievements tap];
    
    XCUIElement* btnForceIAN = [self waitUIElement: self.app.buttons withID: @"Force IAN"];
    XCTAssert(btnForceIAN.exists);
    //...
    [btnForceIAN tap];
    
    XCUIElement* ianView = [self waitUIElement: self.app.otherElements withID: @"ianView"];
    XCTAssert(ianView.exists);
    
    XCUIElement* btnClose = [self waitUIElement: self.app.buttons withID: @"btnClose"];
    XCTAssert(btnClose.exists);
    [btnClose tap];
    
    XCTAssert(!ianView.exists);
}

- (void)testAccountWindow
{
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCUIElement* account = [self waitUIElement: collectionViewsQuery.staticTexts withID:@"Account"];
    XCTAssert(account.exists);
    //...
    [account tap];
    
    XCUIElement* collectionView = [self.app.collectionViews elementBoundByIndex: 0];
    BOOL flag = [self scrollToCellIdentifier: @"ShowMyAccountCell" collectionViewElement: collectionView scrollUp: NO fullyVisible: YES];
    XCTAssert(flag);
    XCUIElement* btnMyAccount = [self waitUIElement:self.app.collectionViews.buttons withID:@"Show My Account"];
    XCTAssert(btnMyAccount.exists);
    //...
    [btnMyAccount tap];

    XCUIElement* accountView = [self waitUIElement: self.app.otherElements withID: @"accountView"];
    XCTAssert(accountView.exists);
    
    XCUIElement* btnClose = [self waitUIElement: self.app.buttons withID: @"btnClose"];
    XCTAssert(btnClose.exists);
    [btnClose tap];
    
    XCTAssert(!accountView.exists);
}

@end
