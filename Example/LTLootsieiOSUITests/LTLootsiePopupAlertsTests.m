//
//  LTLootsiePopupAlertsTests.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 4/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LTUITestCase.h"

@interface LTLootsiePopupAlertsTests : LTUITestCase

@end


@implementation LTLootsiePopupAlertsTests


- (void)testShowPopup {
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCUIElement* others = [self waitUIElement: collectionViewsQuery.staticTexts withID:@"Others"];
    XCTAssert(others.exists);
    //...
    [others tap];
    
    XCUIElement* btnShowPopup = [self waitUIElement:self.app.collectionViews.buttons withID:@"Show Popup"];
    XCTAssert(btnShowPopup.exists);
    //...
    [btnShowPopup tap];
    
    XCUIElement* backgroundView = [self waitUIElement:self.app.otherElements withID:@"popupBackgroundView"];
    XCTAssert(backgroundView.exists);
    XCTAssert(CGSizeEqualToSize(CGSizeMake(290.0, 213.0), backgroundView.frame.size));
    
    XCUIElement* titleLabel = self.app.staticTexts[@"titleLabel"];
    XCUIElement* descLabel = self.app.staticTexts[@"descLabel"];
    XCUIElement* btnOk = self.app.buttons[@"btnOk"];
    
    XCTAssert(titleLabel.exists);
    XCTAssert(descLabel.exists);
    
    XCTAssert([((NSString*)titleLabel.label) isEqualToString: @"My Alert"]);
    XCTAssert([((NSString*)descLabel.label) isEqualToString: @"Hello, Lootsie"]);
    
    XCTAssert(btnOk.exists);
    XCTAssert([((NSString*)btnOk.label) isEqualToString: @"OK"]);
    XCTAssert(CGSizeEqualToSize(CGSizeMake(163.0, 44.0), btnOk.frame.size));

    [self.app.buttons[@"btnOk"] tap];
    XCTAssert(!self.app.otherElements[@"popupBackgroundView"].exists);
}

- (void)testCatchErrorMessage {
    XCUIElementQuery *collectionViewsQuery = self.app.collectionViews;
    XCUIElement* catalog = [self waitUIElement: collectionViewsQuery.staticTexts withID:@"Catalog"];
    XCTAssert(catalog.exists);
    //...
    [catalog tap];
    
    XCUIElement* collectionView = [self.app.collectionViews elementBoundByIndex: 0];
    BOOL flag = [self scrollToCellIdentifier: @"RedeemRewardCell" collectionViewElement: collectionView scrollUp: NO fullyVisible: YES];
    XCTAssert(flag);
    XCUIElement* btnRedeem = [self waitUIElement:self.app.collectionViews.buttons withID:@"Redeem Reward"];
    XCTAssert(btnRedeem.exists);
    //...
    [btnRedeem tap];
    
    XCUIElement* backgroundView = [self waitUIElement:self.app.otherElements withID:@"popupBackgroundView"];
    XCTAssert(backgroundView.exists);
}

@end
