Pod::Spec.new do |s|
  s.name                    = 'LTLootsie'
  s.version                 = '5.2.0'
  s.license                 = 'MIT'
  s.summary                 = 'The World’s Most Powerful Loyalty Engine'
  s.homepage                = 'http://www.lootsie.com'
  s.social_media_url        = 'https://twitter.com/lootsie'
  s.authors                 = { 'Fabio Teles' => 'pixel4@gmail.com' }
  s.source                  = { :git => 'https://bitbucket.org/LootsieIOS/ios-sdk', :tag => s.version}
  s.requires_arc            = true
  s.ios.deployment_target   = '8.0'

  s.subspec 'Core' do |ss|
    ss.source_files         = 'LTLootsie/**/*.{h,m}'
    ss.public_header_files  = 'LTLootsie/**/*.h'
    ss.private_header_files = 'LTLootsie/Vendor/**/*.h'
    ss.frameworks           = 'Security', 'SystemConfiguration', 'MobileCoreServices'
  end

  s.subspec 'UI' do |ss|
    ss.dependency 'LTLootsie/Core'

    ss.source_files         = 'LTLootsieUI/**/*.{h,m}'
    ss.public_header_files  = 'LTLootsieUI/**/*.h'
    ss.private_header_files = 'LTLootsieUI/Vendor/**/*.h'
    ss.frameworks           = 'CoreGraphics'
    ss.resources            = ['LTLootsieUI/Resources/*.{storyboard,xib}', 'LTLootsieUI/Resources/images/*.png']
  end

    ss.xcconfig = {'OTHER_LDFLAGS' => '-ObjC'}
    ss.libraries = 'xml2.2'
  end
end

