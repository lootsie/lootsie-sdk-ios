//  LTTimelinePageController.m
//  Created by Konstantin Yurchenko, Jr. on 5/17/2017.
//  Copyright (c) 2017 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTTimelinePageController.h"
#import "LTAchievementsCell.h"
#import "LTOfferedAchievementsCell.h"

#import "LTUIManager.h"
#import "LTUIManager+Achievement.h"
#import "LTTools.h"
#import "LTTheme.h"

#import "LTApp.h"
#import "LTData.h"
#import "LTAchievement.h"
#import "LTAchievementsGroup.h"

#import "LTIANGenericItem.h"
#import "LTIANCatalogItem.h"
#import "LTIANRewardItem.h"
#import "LTIANVideoItem.h"

#import "LTAchievementHeaderView.h"
#import "LTAchievementLayout.h"

#import "LTTimelineHeaderView.h"

#import "LTPrimaryButton.h"


/*! Identifier for offered achievement cell.*/
static NSString * OfferedAchievementCellIdentifier = @"OfferedAchievementCell";

/*! Identifier for achievement header view.*/
static NSString * AchievementHeaderIdentifier = @"AchievementHeader";

/*! Identifier for achievement cell.*/
static NSString * AchievementCellIdentifier = @"AchievementCell";

/*! Identifier for timeline header view.*/
static NSString * TimelineHeaderIdentifier = @"TimelineHeader";

@interface LTTimelinePageController () <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIView *toggleView;

/*! Button for showing availaible achievements.*/
@property (weak, nonatomic) IBOutlet LTPrimaryButton *achievementsButton;

/*! Button for showing earned achievements.*/
@property (weak, nonatomic) IBOutlet LTPrimaryButton *timelineButton;

/*! Property is a boolean that specifies if the view should be configured for available or earned achievements. */
@property (nonatomic, assign) LTAchievementScreenMode screenMode;

/*! List of achievement items.*/
@property (nonatomic, strong) NSMutableArray<LTAchievementsGroup*> *achievements;

/*! List of offered achievements items.*/
//@property (nonatomic, strong) NSMutableArray<LTOfferAchievements*> *offeredAchievements;
@property (nonatomic, strong) NSMutableArray<LTOfferAchievements*> *offeredAchievements;

@property (weak, nonatomic) IBOutlet UILabel *nullResultLabel;

@property (nonatomic, strong) NSMutableArray<NSDictionary*> *offeredAchievementsMap;

/*! Collection view for show achievement items.*/
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end

@implementation LTTimelinePageController

/*! @brief Create a new object.*/
- (instancetype)init {
    if (self = [super init]) {
        self.title = NSLocalizedStringFromTable(@"Achievements", @"LTLootsie", nil);
    }
    return self;
}

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    
    self.toggleView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    
    self.collectionView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    
    // LTTimelineMode
    [self.collectionView registerNib:[UINib nibWithNibName:@"LTAchievementsCell" bundle:nil] forCellWithReuseIdentifier:AchievementCellIdentifier];

    [self.collectionView registerNib:[UINib nibWithNibName:@"LTTimelineHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:TimelineHeaderIdentifier];
    
    // LTAchievementMode
    [self.collectionView registerNib:[UINib nibWithNibName:@"LTOfferedAchievementsCell" bundle:nil] forCellWithReuseIdentifier:OfferedAchievementCellIdentifier];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"LTAchievementHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:AchievementHeaderIdentifier];
    
    if ([self.collectionView respondsToSelector:@selector(setPrefetchingEnabled:)]) {
        self.collectionView.prefetchingEnabled = false;
    }
    
    self.screenMode = LTTimelineMode;
    
    [self configureTableView];
}

/*! @brief Called when controller will appear on screen.
 *  @param animated YES if controllwe was appear with animation.*/
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
    
    [self updateEarnedAchievements];
    [self updateOfferedAchievements];
    
    [self toggleNullResultLabel];
}

- (void)toggleNullResultLabel {
    switch(self.screenMode) {
        case LTTimelineMode:
            if (self.achievements.count > 0 ) {
                [self.nullResultLabel setHidden:true];
            } else {
                [self.nullResultLabel setHidden:false];
            }
            
            break;
        case LTAchievementMode:
            [self.nullResultLabel setHidden:true];
            
            break;
    }
}

/*! @brief Return YES if date is today.
 *  @param aDate Date fro check.*/
- (BOOL)isDateToday:(NSDate*)aDate {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: aDate];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    return [today isEqualToDate:otherDate];
}

/*! @brief Get date at beginning of day.
 *  @param inputDate Source date.
 *  @return Date at beginning of date from source date.*/
- (NSDate*)dateAtBeginningOfDayForDate:(NSDate*)inputDate {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    NSDateComponents *dateComps = [calendar components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

/*! @brief Generate achievements group for date.
 *  @param date Date object.
 *  @return Achievements items group.*/
- (LTAchievementsGroup*)groupByDate:(NSDate*)date {
    LTAchievementsGroup* group = nil;
    for(LTAchievementsGroup* item in self.achievements) {
        if([item.date isEqualToDate: date]) {
            group = item;
            break;
        }
    }
    return group;
}

/*! @brief Update achievements list.*/
- (void)updateEarnedAchievements {
    [[LTManager sharedManager] achievementsWithSuccess:^(NSArray *achievements) {
        self.achievements = [NSMutableArray array];
        for(LTAchievement* item in achievements) {
            NSDate* dateRepresentingThisDay = [self dateAtBeginningOfDayForDate: item.date];
            LTAchievementsGroup* eventsOnThisDay = [self groupByDate: dateRepresentingThisDay];
            if(eventsOnThisDay == nil) {
                eventsOnThisDay = [[LTAchievementsGroup alloc] init];
                [self.achievements addObject: eventsOnThisDay];
            }
            [eventsOnThisDay addAchievement: item];
        }
        [self.collectionView reloadData];
    } failure:nil];
}

/*! @brief Update offered achievements list.*/
- (void)updateOfferedAchievements {
    
    self.offeredAchievements = [NSMutableArray array];
    self.offeredAchievementsMap = [NSMutableArray array];
    
//    for (LTOfferAchievements * item in [LTManager sharedManager].data.app.allAchievements) {
    for (int i = 0; i < [[LTManager sharedManager].data.app.allAchievements count]; i++) {
        bool was_earned = false;
        
        LTOfferAchievements * item = [[LTManager sharedManager].data.app.allAchievements objectAtIndex:i];
        
        for (LTRecordAchievements *achievement in [LTManager sharedManager].data.user.achievements) {
            NSLog(@"%ld %ld", (long)achievement.achievementID,  (long)item.idAchievement);
            if (achievement.achievementID == item.idAchievement) {
                was_earned = true;
            }
        }
        
        NSDictionary * mapItem = @{
                                       @"index": [NSNumber numberWithInt:i],
                                       @"was_earned": [NSNumber numberWithBool: was_earned]
                                       };
        
        NSLog(@"%@", mapItem);
        
        [self.offeredAchievements addObject: item];
        [self.offeredAchievementsMap addObject: mapItem];
    }
    
    [self.collectionView reloadData];
}

/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Base overwrite methods

- (void)needUpdateData {
    [super needUpdateData];
    
    [self updateOfferedAchievements];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
//        NSLog(@"didSelectItemAtIndexPath: %ld", (long)indexPath.row);
    
    if (self.screenMode == LTAchievementMode) {
        LTOfferAchievements * achievement = self.offeredAchievements[indexPath.row];
        
        bool wasAchieved = [[self.offeredAchievementsMap[indexPath.row] objectForKey:@"was_earned"] boolValue];
        
        [[LTUIManager sharedManager] showAchievementPopupWithTitle: achievement.title
                                            description: achievement.desc
                                            difficulty: achievement.difficulty
                                            wasAchieved:wasAchieved
                                             buttonText: @"CLOSE"
                                         didCloseAction: nil];
    }
}

#pragma mark - UITableView Delegate and DataSource
/*! @brief Get number of section for collection view
 *  @param collectionView Collection view object.
 *  @return Number of section.*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    NSUInteger count = 0;
    switch(self.screenMode) {
        case LTTimelineMode:
            count = self.achievements.count;
            break;
        case LTAchievementMode:
            count = 1;
            break;
    }
    
    return count;
}

/*! @brief Get number of items for show in section.
 *  @param collectionView Collection view object.
 *  @param section Index of collection view section.
 *  @return Number of items.*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    LTAchievementsGroup* group = self.achievements[section];
//    return group.achievements.count;
    
    NSUInteger count = 0;
    
    if (self.screenMode == LTTimelineMode) {
        LTAchievementsGroup* group = self.achievements[section];
        count = group.achievements.count;
    } else if (self.screenMode == LTAchievementMode) {
        count = self.offeredAchievements.count;
    }
    
    return count;
}

/*! @brief Get cell for collection view with index path.
 *  @param collectionView Collection view object.
 *  @param indexPath Index path for item.
 *  @return Cell object.*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.screenMode == LTTimelineMode) {
        LTAchievementsCell *cell = (LTAchievementsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:AchievementCellIdentifier forIndexPath:indexPath];

        LTAchievementsGroup* group = self.achievements[indexPath.section];
        LTAchievement *achievement = group.achievements[indexPath.row];
        NSString* pointsStr = nil;
        if(achievement.points > 0) {
            pointsStr = [NSString stringWithFormat:@"%lu %@", (long)achievement.points, [LTManager sharedManager].data.app.currencyAbbreviation];
        }
        cell.titleLabel.text = achievement.name;
        cell.descLabel.text = achievement.desc;
        cell.pointsLabel.text = pointsStr;
        cell.isTodayItem = group.isTodayGroup;
        [cell updateUIForPoints];

        return cell;
    } else if (self.screenMode == LTAchievementMode) {
        LTOfferedAchievementsCell *cell = (LTOfferedAchievementsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:OfferedAchievementCellIdentifier forIndexPath:indexPath];
        
        LTOfferAchievements * achievement = self.offeredAchievements[indexPath.row];
                                             
        
        cell.titleLabel.text = achievement.title;
        cell.descLabel.text = achievement.desc;
        
        if (achievement.difficulty == LTHardDifficulty) {
            cell.difficultyLabel.text = @"Hard";
            cell.difficultyLabel.textColor = [[LTTheme sharedTheme] colorWithType: LTColorAltText3];
            
        } else if (achievement.difficulty == LTMediumDifficulty) {
            cell.difficultyLabel.text = @"Medium";
            cell.difficultyLabel.textColor = [[LTTheme sharedTheme] colorWithType: LTColorAltText2];
            
        } else if (achievement.difficulty == LTEasyDifficulty) {
            cell.difficultyLabel.text = @"Easy";
            cell.difficultyLabel.textColor = [[LTTheme sharedTheme] colorWithType: LTColorAltText1];
        }
        
        NSDictionary *mapRecord = self.offeredAchievementsMap[indexPath.row];

        if ([[mapRecord objectForKey:@"was_earned"] boolValue]) {
            [cell markAchieved];
        } else {
            [cell ummarkAchieved];
        }

        return cell;
    }
    
    return nil;
}

/*! @brief Get header or footer view for section.
 *  @param collectionView Collection view object.
 *  @param kind Type of view.
 *  @param indexPath Index path of view.
 *  @return Reusable view object.*/
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    
    if(self.screenMode == LTAchievementMode) {
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
            LTAchievementHeaderView* header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:AchievementHeaderIdentifier forIndexPath:indexPath];
            return header;
        }
        
    } else if (self.screenMode == LTTimelineMode) {
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
            LTAchievementsGroup* group = self.achievements[indexPath.section];
            LTTimelineHeaderView* header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:TimelineHeaderIdentifier forIndexPath:indexPath];
            header.group = group;
            return header;
        }
    }

    return nil;
}

/*! @brief Get size of item for index path.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param indexPath Index path for item.
 *  @return Size of item*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.bounds.size.width, 95);
}

/*! @brief Get reference size for header in section.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param section Index of section.
 *  @return Size of header.*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(collectionView.bounds.size.width, 68);
}

/*! @brief Called when user click to deactivate/logout button.
 *  @param sender Button object.*/
- (IBAction)handleAchievementsClick:(UIButton*)sender {
    NSLog(@"handleAchievementsClick");
    
    self.screenMode = LTAchievementMode;
    
    [self.nullResultLabel setHidden:true];

    [self configureTableView];
}
/*! @brief Called when user click to deactivate/logout button.
 *  @param sender Button object.*/
- (IBAction)handleTimelineClick:(UIButton*)sender {
    NSLog(@"handleTimelineClick");
    
    self.screenMode = LTTimelineMode;

    [self configureTableView];
}

/*! @brief Configure achievements interface. */
- (void)configureTableView {
    
    [self toggleNullResultLabel];
    
    switch(self.screenMode) {
        case LTTimelineMode:
            
            [self updateEarnedAchievements];
            
            // HIGHLIGHT
            self.achievementsButton.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
            [self.achievementsButton highlight];

            // UNHIGHLIGHT
            self.timelineButton.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryButton];
            [self.timelineButton unhighlight];

            break;
        case LTAchievementMode:
            [self updateOfferedAchievements];
            
            // HIGHLIGHT
            self.timelineButton.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
            [self.timelineButton highlight];
            
            // UNHIGHLIGHT
            self.achievementsButton.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryButton];
            [self.achievementsButton unhighlight];
            
            [self.nullResultLabel setHidden:true];
            
            break;
    }
    
    LTAchievementLayout *layout = (LTAchievementLayout *)self.collectionView.collectionViewLayout;
    layout.screenMode = self.screenMode;
}
@end
