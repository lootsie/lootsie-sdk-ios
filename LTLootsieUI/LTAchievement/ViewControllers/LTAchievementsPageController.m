//  LTAchievementsPageController.m
//  Created by Fabio Teles on 5/20/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTAchievementsPageController.h"
#import "LTAchievementsCell.h"
#import "LTUIManager.h"
#import "LTUIManager+Achievement.h"
#import "LTTools.h"
#import "LTTheme.h"

#import "LTApp.h"
#import "LTData.h"
#import "LTAchievement.h"
#import "LTAchievementsGroup.h"

#import "LTIANGenericItem.h"
#import "LTIANCatalogItem.h"
#import "LTIANRewardItem.h"
#import "LTIANVideoItem.h"

#import "LTTimelineHeaderView.h"
#import "LTAchievementLayout.h"


/*! Identifier for achievement cell.*/
static NSString * AchievementCellIdentifier = @"AchievementCell";

/*! Identifier for achievement header view.*/
static NSString * AchievementHeaderIdentifier = @"TimelineHeader";


@interface LTAchievementsPageController () <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
/*! List of achievement items.*/
@property (nonatomic, strong)   NSMutableArray<LTAchievementsGroup*> *achievements;

/*! Collection view for show achievement items.*/
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@end


@implementation LTAchievementsPageController

/*! @brief Create a new object.*/
- (instancetype)init {
    if (self = [super init]) {
        self.title = NSLocalizedStringFromTable(@"Achievements", @"LTLootsie", nil);
    }
    return self;
}

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    self.collectionView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    [self.collectionView registerNib:[UINib nibWithNibName:@"LTAchievementsCell" bundle:nil] forCellWithReuseIdentifier:AchievementCellIdentifier];
    [self.collectionView registerNib:[UINib nibWithNibName:@"LTAchievementHeaderView" bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:AchievementHeaderIdentifier];
    if ([self.collectionView respondsToSelector:@selector(setPrefetchingEnabled:)]) {
        
        self.collectionView.prefetchingEnabled = false;
        
    }
}

/*! @brief Called when controller will appear on screen.
 *  @param animated YES if controllwe was appear with animation.*/
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

    [self updateAchievements];
}

/*! @brief Return YES if date is today.
 *  @param aDate Date fro check.*/
- (BOOL)isDateToday:(NSDate*)aDate {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: aDate];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    return [today isEqualToDate:otherDate];
}

/*! @brief Get date at beginning of day.
 *  @param inputDate Source date.
 *  @return Date at beginning of date from source date.*/
- (NSDate*)dateAtBeginningOfDayForDate:(NSDate*)inputDate {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    NSDateComponents *dateComps = [calendar components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

/*! @brief Generate achievements group for date.
 *  @param date Date object.
 *  @return Achievements items group.*/
- (LTAchievementsGroup*)groupByDate:(NSDate*)date {
    LTAchievementsGroup* group = nil;
    for(LTAchievementsGroup* item in self.achievements) {
        if([item.date isEqualToDate: date]) {
            group = item;
            break;
        }
    }
    return group;
}

/*! @brief Update achievements list.*/
- (void)updateAchievements {
    [[LTManager sharedManager] achievementsWithSuccess:^(NSArray *achievements) {
        self.achievements = [NSMutableArray array];
        for(LTAchievement* item in achievements) {
            NSDate* dateRepresentingThisDay = [self dateAtBeginningOfDayForDate: item.date];
            LTAchievementsGroup* eventsOnThisDay = [self groupByDate: dateRepresentingThisDay];
            if(eventsOnThisDay == nil) {
                eventsOnThisDay = [[LTAchievementsGroup alloc] init];
                [self.achievements addObject: eventsOnThisDay];
            }
            [eventsOnThisDay addAchievement: item];
        }
        [self.collectionView reloadData];
    } failure:nil];
}

/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Base overwrite methods

- (void)needUpdateData {
    [super needUpdateData];
    
    [self updateAchievements];
}

#pragma mark - UITableView Delegate and DataSource
/*! @brief Get number of section for collection view
 *  @param collectionView Collection view object.
 *  @return Number of section.*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return self.achievements.count;
}

/*! @brief Get number of items for show in section.
 *  @param collectionView Collection view object.
 *  @param section Index of collection view section.
 *  @return Number of items.*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    LTAchievementsGroup* group = self.achievements[section];
    return group.achievements.count;
}

/*! @brief Get cell for collection view with index path.
 *  @param collectionView Collection view object.
 *  @param indexPath Index path for item.
 *  @return Cell object.*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LTAchievementsCell *cell = (LTAchievementsCell *)[collectionView dequeueReusableCellWithReuseIdentifier:AchievementCellIdentifier forIndexPath:indexPath];
    
    LTAchievementsGroup* group = self.achievements[indexPath.section];
    LTAchievement *achievement = group.achievements[indexPath.row];
    NSString* pointsStr = nil;
    if(achievement.points > 0) {
        pointsStr = [NSString stringWithFormat:@"%lu %@", (long)achievement.points, [LTManager sharedManager].data.app.currencyAbbreviation];
    }
    cell.titleLabel.text = achievement.name;
    cell.descLabel.text = achievement.desc;
    cell.pointsLabel.text = pointsStr;
    cell.isTodayItem = group.isTodayGroup;
    [cell updateUIForPoints];
    
    return cell;
}

/*! @brief Get header or footer view for section.
 *  @param collectionView Collection view object.
 *  @param kind Type of view.
 *  @param indexPath Index path of view.
 *  @return Reusable view object.*/
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        LTAchievementsGroup* group = self.achievements[indexPath.section];
        LTTimelineHeaderView* header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:AchievementHeaderIdentifier forIndexPath:indexPath];
        header.group = group;
        return header;
    }
    return nil;
}

/*! @brief Get size of item for index path.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param indexPath Index path for item.
 *  @return Size of item*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.bounds.size.width, 95);
}

/*! @brief Get reference size for header in section.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param section Index of section.
 *  @return Size of header.*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(collectionView.bounds.size.width, 68);
}

@end
