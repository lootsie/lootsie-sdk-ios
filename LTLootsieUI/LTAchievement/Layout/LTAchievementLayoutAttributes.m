//
//  LTAchievementLayoutAttributes.m
//  LTLootsie iOS Example
//
//  Created by Macmini on 7/19/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTAchievementLayoutAttributes.h"

@implementation LTAchievementLayoutAttributes

/*! @brief Copy object.
 *  @param zone Zone object.*/
- (id)copyWithZone:(nullable NSZone *)zone {
    LTAchievementLayoutAttributes *copy = [super copyWithZone:zone];
    if (copy != nil) {
        copy.aligmentLabel = self.aligmentLabel;
        copy.alphaDarkBackground = self.alphaDarkBackground;
        copy.titleWidthScale = self.titleWidthScale;
        copy.isShowBlackBackground = self.isShowBlackBackground;
    }
    return copy;
}

/*! @brief Compare two objects.
 *  @return YES is two objects is equal.*/
- (BOOL)isEqual:(id)object {
    if ([object isKindOfClass:[LTAchievementLayoutAttributes class]]) {
        LTAchievementLayoutAttributes *checkAttribute = object;
        if ([checkAttribute.representedElementKind isEqualToString:UICollectionElementKindSectionHeader] ) {
            return NO;
        }
        if (checkAttribute.aligmentLabel == self.aligmentLabel &&
            checkAttribute.alphaDarkBackground == self.alphaDarkBackground &&
            checkAttribute.titleWidthScale == self.titleWidthScale &&
            checkAttribute.isShowBlackBackground == self.isShowBlackBackground) {
            return [super isEqual:object];
        }
        return NO;
    }
    return NO;
}

@end
