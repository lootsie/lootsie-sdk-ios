//
//  LTAchievementLayoutAttributes.h
//  LTLootsie iOS Example
//
//  Created by Macmini on 7/19/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTAchievementLayoutAttributes
 *  @brief Achievement layout attributes.*/
@interface LTAchievementLayoutAttributes : UICollectionViewLayoutAttributes

/*! Aligment label.*/
@property (nonatomic, assign) CGFloat aligmentLabel;

/*! Alpha dark background.*/
@property (nonatomic, assign) CGFloat alphaDarkBackground;

/*! Title width scale.*/
@property (nonatomic, assign) CGFloat titleWidthScale;

/*! Need to show black background.*/
@property (nonatomic, assign) BOOL isShowBlackBackground;

@end
