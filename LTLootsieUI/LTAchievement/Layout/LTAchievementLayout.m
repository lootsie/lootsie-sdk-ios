//  LTAchievementLayout.m
//  Created by Alexander Dovbnya on 7/24/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTAchievementLayout.h"
#import "LTAchievementLayoutAttributes.h"
//#import "LTAchievementsPageController.h"
#import "LTAchievementDecorationView.h"

@implementation LTAchievementLayout


/*! @brief Get class name for layout attributes.
 *  @return Class name.*/
+ (Class)layoutAttributesClass {
    return [LTAchievementLayoutAttributes class];
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    [self registerNib:[UINib nibWithNibName:@"LTAchievementDecorationView" bundle:nil] forDecorationViewOfKind:[LTAchievementDecorationView decorKind]];
}

/*! @brief Return YES to cause the collection view to requery the layout for geometry information.
 *  @param newBounds New bounds value.*/
- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

/*! @brief Return layout attributes for for supplementary or decoration views, or to perform layout in an as-needed-on-screen fashion.
 *  @param rect Frame for elements.*/
- (nullable NSArray<__kindof UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSMutableArray *allItems = [NSMutableArray arrayWithArray:[super layoutAttributesForElementsInRect:rect]];
    [allItems addObjectsFromArray:[self foundMissingSectionHeader:allItems]];
    [self layoutForStickyHeader:allItems];
    [self layoutForAnimation:allItems];
    [allItems addObjectsFromArray:[self decorativeView:allItems]];

    return allItems;
}

/*! @brief Create attributes for decoration view.
 *  @param decorationViewKind Identifier of decoration view.
 *  @param indexPath Index path.*/
- (UICollectionViewLayoutAttributes *)layoutAttributesForDecorationViewOfKind:(NSString *)decorationViewKind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes *decorView = [super layoutAttributesForDecorationViewOfKind:decorationViewKind atIndexPath:indexPath];
    if ([decorationViewKind isEqualToString:[LTAchievementDecorationView decorKind]] && decorView == nil) {
        CGSize sizeColection = [self collectionViewContentSize];
        decorView = [UICollectionViewLayoutAttributes layoutAttributesForDecorationViewOfKind:[LTAchievementDecorationView decorKind] withIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        decorView.zIndex = 1;
        decorView.frame = CGRectMake(0, 0, sizeColection.width, sizeColection.height);
    }
    return decorView;
}

/*! @brief Get list of attributes for decorative view with items.
 *  @param allItems Items list.
 *  @return List of attributes.*/
- (NSArray *)decorativeView:(NSArray *)allItems {
    
    NSLog(@"%ld", (long)self.screenMode);
    
    if (self.screenMode == LTTimelineMode)  {
        if([self.collectionView numberOfSections] > 0) {
            for (UICollectionViewLayoutAttributes *layoutAttributes in allItems) {
                if ([layoutAttributes.representedElementKind isEqualToString:[LTAchievementDecorationView decorKind]]) {
                    layoutAttributes.zIndex = 1;
                    return @[];
                }
            }
            return @[[self layoutAttributesForDecorationViewOfKind:[LTAchievementDecorationView decorKind] atIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]];
        }
        else {
            return @[];
        }
    } else {
        return @[];
    }
}

/*! @brief Found missing section header.
 *  @param allItems Items list.
 *  @return List of headers.*/
- (NSArray *)foundMissingSectionHeader:(NSArray *)allItems {
    NSMutableIndexSet *missingSections = [NSMutableIndexSet indexSet];
    for (UICollectionViewLayoutAttributes *layoutAttributes in allItems) {
        if (layoutAttributes.representedElementCategory == UICollectionElementCategoryCell) {
            [missingSections addIndex:layoutAttributes.indexPath.section];
        }
        layoutAttributes.zIndex = 100;
    }
    for (UICollectionViewLayoutAttributes *layoutAttributes in allItems) {
        if ([layoutAttributes.representedElementKind isEqualToString:UICollectionElementKindSectionHeader]) {
            [missingSections removeIndex:layoutAttributes.indexPath.section];
        }
    }
    NSMutableArray *missingArray = [NSMutableArray array];
    [missingSections enumerateIndexesUsingBlock:^(NSUInteger idx, BOOL *stop) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:0 inSection:idx];
        
        UICollectionViewLayoutAttributes *layoutAttributes = [self layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader atIndexPath:indexPath];
        
        [missingArray addObject:layoutAttributes];
        
    }];
    return [missingArray copy];
}

/*! @brief Create layout for stickly header.
 *  @param allItems Items list.*/
- (void)layoutForStickyHeader:(NSArray *)allItems {
    UICollectionView * const cv = self.collectionView;
    CGPoint const contentOffset = cv.contentOffset;
    NSInteger count = 0;
    for (LTAchievementLayoutAttributes *layoutAttributes in allItems) {
        
        if ([layoutAttributes.representedElementKind isEqualToString:UICollectionElementKindSectionHeader]) {
            count++;
            NSInteger section = layoutAttributes.indexPath.section;
            NSInteger numberOfItemsInSection = [cv numberOfItemsInSection:section];
            
            NSIndexPath *firstObjectIndexPath = [NSIndexPath indexPathForItem:0 inSection:section];
            NSIndexPath *lastObjectIndexPath = [NSIndexPath indexPathForItem:MAX(0, (numberOfItemsInSection - 1)) inSection:section];
            
            BOOL cellsExist;
            UICollectionViewLayoutAttributes *firstObjectAttrs;
            UICollectionViewLayoutAttributes *lastObjectAttrs;
            
            if (numberOfItemsInSection > 0) { // use cell data if items exist
                cellsExist = YES;
                firstObjectAttrs = [self layoutAttributesForItemAtIndexPath:firstObjectIndexPath];
                lastObjectAttrs = [self layoutAttributesForItemAtIndexPath:lastObjectIndexPath];
            } else { // else use the header and footer
                cellsExist = NO;
                firstObjectAttrs = [self layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionHeader
                                                                        atIndexPath:firstObjectIndexPath];
                lastObjectAttrs = [self layoutAttributesForSupplementaryViewOfKind:UICollectionElementKindSectionFooter
                                                                       atIndexPath:lastObjectIndexPath];
                
            }
            
            CGFloat topHeaderHeight = (cellsExist) ? CGRectGetHeight(layoutAttributes.frame) : 0;
            CGFloat bottomHeaderHeight = CGRectGetHeight(layoutAttributes.frame);
            CGRect frameWithEdgeInsets = UIEdgeInsetsInsetRect(layoutAttributes.frame,
                                                               cv.contentInset);
            
            CGPoint origin = frameWithEdgeInsets.origin;
            
            origin.y = MIN(
                           MAX(
                               contentOffset.y + cv.contentInset.top,
                               (CGRectGetMinY(firstObjectAttrs.frame) - topHeaderHeight)
                               ),
                           (CGRectGetMaxY(lastObjectAttrs.frame) - bottomHeaderHeight)
                           );
            layoutAttributes.zIndex = 1024;
            layoutAttributes.frame = (CGRect){.origin = origin,.size = layoutAttributes.frame.size};
        }
    }
}

/*! @brief Create layout for animation.
 *  @param allItems Items list.*/
- (void)layoutForAnimation:(NSArray *)allItems {
    LTAchievementLayoutAttributes *topLayoutAttributes = nil;
    for (LTAchievementLayoutAttributes *layoutAttributes in allItems) {
        if ([layoutAttributes.representedElementKind isEqualToString:UICollectionElementKindSectionHeader]) {
            if (topLayoutAttributes == nil || topLayoutAttributes.indexPath.section > layoutAttributes.indexPath.section) {
                topLayoutAttributes = layoutAttributes;
            }
            
            layoutAttributes.aligmentLabel = 1;
            layoutAttributes.titleWidthScale = 1;
            layoutAttributes.alphaDarkBackground = 1;
            layoutAttributes.isShowBlackBackground = NO;
        }
    }
    
    topLayoutAttributes.alphaDarkBackground = 0;
    topLayoutAttributes.titleWidthScale = 1;
    topLayoutAttributes.aligmentLabel = 0;
    topLayoutAttributes.isShowBlackBackground = YES;
    
    for (LTAchievementLayoutAttributes *layoutAttributes in allItems) {
        if ([layoutAttributes.representedElementKind isEqualToString:UICollectionElementKindSectionHeader] && layoutAttributes != topLayoutAttributes) {
            if (layoutAttributes.indexPath.section == topLayoutAttributes.indexPath.section + 1) {
                
                if (topLayoutAttributes.frame.origin.y <= self.collectionView.contentOffset.y) {
                    if (layoutAttributes.frame.origin.y <= self.collectionView.contentOffset.y) {
                        
                        CGFloat aligment = layoutAttributes.frame.origin.y - CGRectGetMaxY(topLayoutAttributes.frame);
                        aligment = 1 - aligment / (layoutAttributes.frame.size.height - 10);
                        layoutAttributes.alphaDarkBackground = [self calculateAlpha:aligment];
                        aligment = MAX(MIN(1, aligment), 0);
                        layoutAttributes.aligmentLabel = aligment;
                        layoutAttributes.titleWidthScale = [self calculateScaleWidth:aligment];
                    }

                    layoutAttributes.zIndex = 2048;
                    CGRect frame = topLayoutAttributes.frame;
                    frame.origin.y = self.collectionView.contentOffset.y;
                    topLayoutAttributes.frame = frame;
                }
            }
        }
    }
}

/*! @brief Calculate width scale for label.
 *  @param aligmentLabel aligment label.*/
- (CGFloat)calculateScaleWidth:(CGFloat)aligmentLabel {
    CGFloat minWidthRange = 0;
    CGFloat midleRange = 0.5f;
    CGFloat coeficientWidth = 1;
    CGFloat offsetScale = 0.4f;
    CGFloat corectAligment = 1 - aligmentLabel;
    if (corectAligment >= minWidthRange && corectAligment < minWidthRange + midleRange) {
        coeficientWidth = 1 + offsetScale * (corectAligment - minWidthRange) / (midleRange);
    }
    else if (corectAligment >= minWidthRange + midleRange && corectAligment < (minWidthRange + 2 * midleRange)) {
        coeficientWidth = 1 + offsetScale - offsetScale * (corectAligment - minWidthRange - midleRange) / (midleRange);
    }
    return coeficientWidth;
}

/*! @brief Calculate alpha value for label.
 *  @param aligmentLabel aligment label.*/
- (CGFloat)calculateAlpha:(CGFloat)aligmentLabel {
    CGFloat startAlpha = -0.2f;
    CGFloat distane = 0.2f;
    CGFloat alpha = aligmentLabel > startAlpha ? 1 : 0;

    if (aligmentLabel >= startAlpha && aligmentLabel <= distane + startAlpha) {
        alpha = (aligmentLabel - startAlpha) / distane;
    }
    return alpha;
}

@end
