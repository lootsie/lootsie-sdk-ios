//  LTUIManager+Achievement.m
//  Created by Alexander Dovbnya on 3/24/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTManager.h"
#import "LTUIManager+Achievement.h"
#import "LTUIManager+Catalog.h"
//#import "LTAchievementsPageController.h"
#import "LTTimelinePageController.h"

NSString * LTAchievementsSegueIdentifier	= @"AchievementsSegue";

@interface LTUIManager ()
/*! Wrap contoller for show UI.*/
@property (nonatomic, strong) UIViewController* wrapRootViewController;

/*! Presentation context. This property is using for show controller.*/
@property (nonatomic, strong) UIViewController* presentationContext;

/*! Core manager instance.*/
@property (nonatomic, weak, readonly) LTManager* coreManager;


/*! @brief Show page with segue identifier.
 *  @param classController Class controller.*/
- (void)showPageWithSegueIdentifier:(Class)classController;

@end


@implementation LTUIManager (Achievement)

- (void)showAchievementsPage {
//    [self showPageWithSegueIdentifier:[LTAchievementsPageController class]];
    [self showPageWithSegueIdentifier:[LTTimelinePageController class]];
}

@end
