//
//  LTAchievementNavigationHeaderView.h
//  LTLootsie iOS Example
//
//  Created by Konstantin Yurchenko on 05/17/17.
//  Copyright © 2017 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LTAchievementsGroup;
/*! @class LTAchievementHeaderView
 *  @brief Achievement header view.*/
@interface LTAchievementNavigationHeaderView : UICollectionReusableView

/*! Achievement group. Using for show section title.*/
@property (nonatomic, strong)   LTAchievementsGroup* group;

@end
