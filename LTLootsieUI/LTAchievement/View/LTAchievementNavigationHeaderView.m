//
//  LTAchievementNavigationHeaderView.m
//  LTLootsie iOS Example
//
//  Created by Konstantin Yurchenko on 05/17/17.
//  Copyright © 2017 Lootsie. All rights reserved.
//

#import "LTAchievementNavigationHeaderView.h"

#import "LTTheme.h"
#import "LTAchievementsGroup.h"
#import "LTAchievementLayoutAttributes.h"


@interface LTAchievementNavigationHeaderView ()
/*! Title background view.*/
@property (weak, nonatomic) IBOutlet UIView *titleBackgroundView;

/*! Title label.*/
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! Title dark background.*/
@property (weak, nonatomic) IBOutlet UIView *darkBackground;

/*! Title black header.*/
@property (weak, nonatomic) IBOutlet UIView *blackHeader;


/*! Title horizontal center constrain.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* titleHCenterConstrain;

/*! Title width constrain.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthTitleContainer;

@end


@implementation LTAchievementNavigationHeaderView

#pragma mark - Init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    LTFontSize fontSize = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? LTMediumCFontSize : LTMediumCFontSize;
    self.titleLabel.font = [[LTTheme sharedTheme] fontWithFamily:LTPrimaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:fontSize];
    
    self.darkBackground.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorSecondaryBackground];
    self.titleBackgroundView.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground1];
    self.blackHeader.alpha = 0.5f;
    self.blackHeader.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    
    [self intiLayerSettings:self.darkBackground];
    [self intiLayerSettings:self.titleBackgroundView];
}

/*! @brief Prepare for reuse cell.*/
- (void)prepareForReuse {
    [super prepareForReuse];
    [self layoutFromAligment:1.f];
    self.widthTitleContainer.constant = [self width];
    self.darkBackground.alpha = 1;
    self.titleLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];
    self.blackHeader.hidden = YES;
}

/*! @brief Init layer settings for view.
 *  @param view View object.*/
- (void)intiLayerSettings:(UIView *)view {
    view.layer.cornerRadius = view.frame.size.height / 2.0;
    view.clipsToBounds = YES;
    view.layer.borderColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryBackground].CGColor;
    view.layer.borderWidth = 2.f;
}


#pragma mark - Setters
/*! @brief Setter for group property.
 *  @param newGroup New value for property.*/
- (void)setGroup:(LTAchievementsGroup *)newGroup {
    _group = newGroup;

    self.titleLabel.text = _group.title;
}

/*! @brief Apply layout attributes.
 *  @param layoutAttributes Attributes object.*/
- (void)applyLayoutAttributes:(LTAchievementLayoutAttributes *)layoutAttributes {
    [super applyLayoutAttributes:layoutAttributes];
    [self layoutFromAligment:layoutAttributes.aligmentLabel];
    self.widthTitleContainer.constant = [self width] * layoutAttributes.titleWidthScale;
    self.darkBackground.alpha = layoutAttributes.alphaDarkBackground;
    self.titleLabel.textColor = [[LTTheme sharedTheme] colorWithType: (layoutAttributes.aligmentLabel <= 0) ? LTColorPrimaryText : LTColorAltText1];
    self.blackHeader.hidden = !layoutAttributes.isShowBlackBackground;
    [self layoutIfNeeded];
}

/*! @brief Update layout from aligment label.
 *  @param aligmentLabel Label offset.*/
- (void)layoutFromAligment:(CGFloat)aligmentLabel {
    CGSize size = self.titleBackgroundView.bounds.size;
    CGFloat offset = -(self.bounds.size.width  - size.width + size.height) / 2.f;
    offset *= aligmentLabel;
    self.titleHCenterConstrain.constant = offset;
}

/*! @brief Get with of view.*/
- (CGFloat)width {
    return 119;
}

@end
