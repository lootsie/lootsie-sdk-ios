//  LTAchievementsCell.m
//  Created by Fabio Teles on 5/20/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTAchievementsCell.h"
#import "LTTools.h"
#import "LTTheme.h"


@interface LTAchievementsCell ()
/*! Arrow image height constrain.*/
@property (nonatomic, weak)     IBOutlet NSLayoutConstraint* arrowImageHeight;

/*! Arrow image width constrain.*/
@property (nonatomic, weak)     IBOutlet NSLayoutConstraint* arrowImageWidth;

/*! Circle border view*/
@property (weak, nonatomic)     IBOutlet UIView *circleBorderView;

/*! Circle border width constrain.*/
@property (weak, nonatomic)     IBOutlet NSLayoutConstraint *circleBorderWidth;

/*! Title label top offset constrain.*/
@property (weak, nonatomic)     IBOutlet NSLayoutConstraint *titleTopOffset;

/*! Description bottom offset.*/
@property (weak, nonatomic)     IBOutlet NSLayoutConstraint *descBottomOffset;

/*! @brief Apply UI changes for checkmark view.*/
- (void)applyUIChangesForCheckmarkView;

/*! @brief Apply UI changes for checkmark arrow view.*/
- (void)applyUIChangesForCheckmarkArrowView;

@end

@implementation LTAchievementsCell

- (void)applyUIChangesForCheckmarkView {
    self.checkmarkView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    self.checkmarkView.layer.cornerRadius = self.checkmarkView.frame.size.height / 2.0;
    CGFloat size = 46;
    self.circleBorderWidth.constant = size;
    self.circleBorderView.layer.cornerRadius = size / 2.f;
    self.checkmarkView.layer.borderColor = [UIColor clearColor].CGColor;
    self.checkmarkView.clipsToBounds = YES;
}

- (void)applyUIChangesForCheckmarkArrowView {
    self.arrowImageWidth.constant = 18.0;
    self.arrowImageHeight.constant = 15.0;
    self.checkmarkView.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground4];
    self.checkmarkImage.image = [[UIImage imageNamed: @"achievements_arrow.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    self.checkmarkImage.backgroundColor = [UIColor clearColor];
}

/*! @brief Called when all subviews was updated.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self applyUIChangesForCheckmarkView];
    [self applyUIChangesForCheckmarkArrowView];

    self.circleBorderView.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryBackground];
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    self.checkmarkView.layer.cornerRadius = CGRectGetHeight(self.checkmarkView.frame) / 2.f;
}

#pragma mark - setters

- (void)setIsTodayItem:(BOOL)flag {
    _isTodayItem = flag;
    CGFloat size = 22;
    self.circleBorderWidth.constant = size;
    self.circleBorderView.layer.cornerRadius = size / 2.f;
    
    if(_isTodayItem) {
        [self applyUIChangesForCheckmarkView];
        [self applyUIChangesForCheckmarkArrowView];
    } else {
        self.checkmarkView.backgroundColor = [UIColor clearColor];
        self.checkmarkView.layer.borderWidth = 0.0;
        self.arrowImageWidth.constant = 13.0;
        self.arrowImageHeight.constant = 13.0;
        self.checkmarkImage.image = nil;
        self.checkmarkImage.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground4];
        self.checkmarkImage.layer.cornerRadius = self.checkmarkImage.frame.size.height / 2.0;
        self.checkmarkImage.layer.borderColor = [UIColor clearColor].CGColor;
        self.checkmarkImage.layer.borderWidth = 0;

    }
    
    self.pointsLabel.textColor = [[LTTheme sharedTheme] colorWithType: (_isTodayItem) ? LTColorTertiaryText : LTColorAltText1];
    self.titleLabel.textColor = [[LTTheme sharedTheme] colorWithType: (_isTodayItem) ? LTColorPrimaryText : LTColorAltText1];
    self.descLabel.textColor = [[LTTheme sharedTheme] colorWithType: (_isTodayItem) ? LTColorAltText4 : LTColorAltText1];
}

- (void)updateUIForPoints {
    if(self.pointsLabel.text.length > 0) {
        self.titleLabel.textColor = [[LTTheme sharedTheme] colorWithType: (_isTodayItem) ? LTColorPrimaryText : LTColorAltText1];
        self.titleTopOffset.constant = 5.0f;
        self.descBottomOffset.constant = 6.0f;
    } else {
        self.titleLabel.textColor = [[LTTheme sharedTheme] colorWithType: (_isTodayItem) ? LTColorTertiaryText : LTColorAltText1];
        self.titleTopOffset.constant = -10.0f;
        self.descBottomOffset.constant = 30.0f;
    }
}

@end
