//
//  LTAchievementHeaderView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/1/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

//@class LTAchievementsGroup;
/*! @class LTAchievementHeaderView
 *  @brief Achievement header view.*/
@interface LTAchievementHeaderView : UICollectionReusableView

///*! Achievement group. Using for show section title.*/
//@property (nonatomic, strong)   LTAchievementsGroup* group;

@end
