//  LTOfferedAchievementsCell.h
//  Created by Konstantin Yurchenko, Jr. on 5/18/17.
//  Copyright (c) 2017 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTOfferedAchievementsCell.h"
#import "LTTools.h"
#import "LTTheme.h"


@interface LTOfferedAchievementsCell ()
/*! Arrow image height constrain.*/
@property (nonatomic, weak)     IBOutlet NSLayoutConstraint* arrowImageHeight;

/*! Arrow image width constrain.*/
@property (nonatomic, weak)     IBOutlet NSLayoutConstraint* arrowImageWidth;

/*! Circle border view*/
@property (weak, nonatomic)     IBOutlet UIView *circleBorderView;

/*! Circle border width constrain.*/
@property (weak, nonatomic)     IBOutlet NSLayoutConstraint *circleBorderWidth;

/*! Title label top offset constrain.*/
@property (weak, nonatomic)     IBOutlet NSLayoutConstraint *titleTopOffset;

/*! Description bottom offset.*/
@property (weak, nonatomic)     IBOutlet NSLayoutConstraint *descBottomOffset;


/*! @brief Apply UI changes for checkmark view.*/
- (void)applyUIChangesForCheckmarkView;

/*! @brief Apply UI changes for checkmark arrow view.*/
- (void)applyUIChangesForCheckmarkArrowView;
@property (weak, nonatomic) IBOutlet UIView *tabFillBackground;

@property (weak, nonatomic) IBOutlet UIImageView *circleBackground;

@end


@implementation LTOfferedAchievementsCell

- (void)applyUIChangesForCheckmarkView {
    self.checkmarkView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
//    self.checkmarkView.layer.cornerRadius = self.checkmarkView.frame.size.height / 2.0;
    CGFloat size = 46;
    self.circleBorderWidth.constant = size;
    self.circleBorderView.layer.cornerRadius = size / 2.f;
    self.checkmarkView.layer.borderColor = [UIColor clearColor].CGColor;
//    self.checkmarkView.clipsToBounds = YES;
    
    // TINT CIRCLE
    self.circleBackground.image = [self.circleBackground.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.circleBackground setTintColor:[[LTTheme sharedTheme] colorWithType: LTColorAltBackground6]];
    
    // TINT TAB FILL BACKGROUND
    [self.tabFillBackground setBackgroundColor:[[LTTheme sharedTheme] colorWithType: LTColorAltBackground6]];
}

- (void)markAchieved {
    self.checkmarkView.hidden = false;
}

- (void)ummarkAchieved {
    self.checkmarkView.hidden = true;
}

- (void)applyUIChangesForCheckmarkArrowView {
    self.arrowImageWidth.constant = 18.0;
    self.arrowImageHeight.constant = 15.0;
    self.checkmarkView.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground4];
    self.checkmarkImage.image = [[UIImage imageNamed: @"achievements_arrow.png"] imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    self.checkmarkImage.backgroundColor = [UIColor clearColor];
}

/*! @brief Called when all subviews was updated.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self applyUIChangesForCheckmarkView];
    [self applyUIChangesForCheckmarkArrowView];

    self.circleBorderView.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryBackground];
    
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorNavigationBackground];
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    self.checkmarkView.layer.cornerRadius = CGRectGetHeight(self.checkmarkView.frame) / 2.f;
}

@end
