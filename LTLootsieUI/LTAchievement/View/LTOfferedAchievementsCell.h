//  LTOfferedAchievementsCell.h
//  Created by Konstantin Yurchenko, Jr. on 5/18/17.
//  Copyright (c) 2017 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <UIKit/UIKit.h>

/*! @class LTOfferedAchievementsCell
 *  @brief Cell for show offered achievement info.*/
@interface LTOfferedAchievementsCell : UICollectionViewCell

/*! Achievement title label.*/
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! Achievement description label.*/
@property (weak, nonatomic) IBOutlet UILabel *descLabel;

/*! Achievement points label.*/
@property (weak, nonatomic) IBOutlet UILabel *difficultyLabel;

/*! Achievement checkmark image.*/
@property (weak, nonatomic) IBOutlet UIImageView *checkmarkImage;

/*! Achievement checkmark background view.*/
@property (weak, nonatomic) IBOutlet UIView *checkmarkView;

/*! YES is current achievement is today.*/
@property (nonatomic, assign)   BOOL isTodayItem;

- (void)markAchieved;

- (void)ummarkAchieved;

@end
