//  LTTimelineHeaderView.m
//  Created by Konstantin Yurchenko, Jr on 5/19/2017.
//  Copyright (c) 2017 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

#import <UIKit/UIKit.h>

@class LTAchievementsGroup;
/*! @class LTAchievementHeaderView
 *  @brief Achievement header view.*/
@interface LTTimelineHeaderView : UICollectionReusableView

/*! Achievement group. Using for show section title.*/
@property (nonatomic, strong)   LTAchievementsGroup* group;

@end
