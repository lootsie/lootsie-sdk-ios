//  LTFontFactory.m
//  Created by Alexander Dovbnya on 5/11/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTFontFactory.h"

@implementation LTFontFactory

- (UIFont *)fontWithFamily:(LTFontFamily)family weight:(LTFontWeight)weight style:(LTFontStyle)style size:(LTFontSize)size {
    UIFont *font;
    if (family == LTSecondaryFontFamily) {
        font = [UIFont fontWithName:@"SoftPressPro" size:26];
    }
    else if (family == LTPrimaryFontFamily) {
        NSString *family = @"BrandonGrotesque";
        NSString *fontStyle = @"Medium";
        if (weight == LTMediumFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Medium";
        }
        else if (weight == LTMediumFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"MediumItalic";
        }
        else if (weight == LTBoldFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Bold";
        }
        else if (weight == LTBoldFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"BoldItalic";
        }
        else if (weight == LTLightFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Light";
        }
        else if (weight == LTLightFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"LightItalic";
        }
        else if (weight == LTThinFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Thin";
        }
        else if (weight == LTThinFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"ThinItalic";
        }
        else if (weight == LTBlackFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Black";
        }
        else if (weight == LTBlackFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"BlackItalic";
        }
        else if (weight == LTRegularFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Regular";
        }
        else if (weight == LTRegularFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"RegularItalic";
        }

        NSString *fontName = [NSString stringWithFormat:@"%@-%@", family, fontStyle];
        CGFloat fontSize = [self sizeFont:size];
        if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].scale > 2.9)) {
            fontSize = [self sizeFontIphone6Plus:size];
        }
        font = [UIFont fontWithName:fontName size:fontSize];
    }
    return font;
}

- (UIFont*)fontWithFamily:(LTFontFamily)family weight:(LTFontWeight)weight style:(LTFontStyle)style ptSize:(CGFloat)size {
    UIFont *font;
    if (family == LTSecondaryFontFamily) {
        font = [UIFont fontWithName:@"SoftPressPro" size: 26];
    }
    else if (family == LTPrimaryFontFamily) {
        NSString *family = @"BrandonGrotesque";
        NSString *fontStyle = @"Medium";
        if (weight == LTMediumFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Medium";
        }
        else if (weight == LTMediumFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"MediumItalic";
        }
        else if (weight == LTBoldFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Bold";
        }
        else if (weight == LTBoldFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"BoldItalic";
        }
        else if (weight == LTLightFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Light";
        }
        else if (weight == LTLightFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"LightItalic";
        }
        else if (weight == LTThinFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Thin";
        }
        else if (weight == LTThinFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"ThinItalic";
        }
        else if (weight == LTBlackFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Black";
        }
        else if (weight == LTBlackFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"BlackItalic";
        }
        else if (weight == LTRegularFontWeight && style == LTRegularFontStyle) {
            fontStyle = @"Regular";
        }
        else if (weight == LTRegularFontWeight && style == LTItalicFontStyle) {
            fontStyle = @"RegularItalic";
        }
        
        NSString *fontName = [NSString stringWithFormat:@"%@-%@", family, fontStyle];
        font = [UIFont fontWithName: fontName size: size];
    }
    
    return font;
}

/*! @brief Get size of font.
 *  @param size Size value. @see LTFontSize.
 *  @return Size of font.*/
- (CGFloat)sizeFont:(LTFontSize)size  {
    CGFloat fontSize = 12;
    switch (size) {
        case LTExtraLargeFontSize:
            fontSize = 30;
            break;
        case LTLargeFontSize:
            fontSize = 21;
            break;
        case LTMediumAFontSize:
            fontSize = 18;
            break;
        case LTMediumBFontSize:
            fontSize = 15;
            break;
        case LTMediumCFontSize:
            fontSize = 12;
            break;
        case LTSmallAFontSize:
            fontSize = 10;
            break;
        case LTSmallBFontSize:
            fontSize = 9;
            break;
    }
    return fontSize;
}

/*! @brief Get size of font for iPhone 6+
 *  @param size Size value. @see LTFontSize.
 *  @return Size of font.*/
- (CGFloat)sizeFontIphone6Plus:(LTFontSize)size  {
    CGFloat fontSize = 12;
    switch (size) {
        case LTExtraLargeFontSize:
            fontSize = 33;
            break;
        case LTLargeFontSize:
            fontSize = 24;
            break;
        case LTMediumAFontSize:
            fontSize = 21;
            break;
        case LTMediumBFontSize:
            fontSize = 18;
            break;
        case LTMediumCFontSize:
            fontSize = 15;
            break;
        case LTSmallAFontSize:
            fontSize = 13;
            break;
        case LTSmallBFontSize:
            fontSize = 12;
            break;
    }
    return fontSize;
}

@end
