//
//  LTColorFactorySolitaire.m
//  LTLootsie iOS Example
//
//  Created by Macmini on 8/24/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTColorFactorySolitaire.h"

@implementation LTColorFactorySolitaire

- (id)init {
    self = [super init];
    if (self) {
        self.allColors = @{
                           //Refresh aimation outter circle color
                           @(LTColorVideoRefreshOutterCircle)           : @(0x4be581),
                           @(LTColorPrimaryTextButton)                  : @(0xffffff),
                           @(LTColorEditTextButton)                     : @(0x5c7bf7),
                           @(LTColorFreePointsTextButton)               : @(0x231f20),
                           @(LTColorHeaderText)                         : @(0xffffff),
                           @(LTColorInactiveTextPrimaryButton)          : @(0x707070),
                           @(LTColorInactivePrimaryButton)              : @(0xececec),
                           @(LTColorWelcomeIconColor)                   : @(0x4ecdff),
                           @(LTColorFirstTimeWizardIconColor)           : @(0xaaf1c3),
                           @(LTColorModalBackground)                    : @(0x80ffffff),
                           @(LTColorNavigationBackground)               : @(0x0c4451),
                           // main color
                           @(LTColorPrimaryBackground)                  : @(0x061928),
                           @(LTColorSecondaryBackground)                : @(0x0c4451),
                           @(LTColorTertiaryBackground)                 : @(0xffffff),
                           @(LTColorAltBackground1)                     : @(0xdcbc8b),
                           @(LTColorAltBackground2)                     : @(0xe3bd58),
                           @(LTColorAltBBackground3)                    : @(0xececec),
                           @(LTColorAltBackground4)                     : @(0xaaf1c3),
                           @(LTColorAltBackground5)                     : @(0x04a092),
                           @(LTColorAltBackground6)                     : @(0x008000),
                           
                           @(LTColorPrimaryButton)                      : @(0x04a092),
                           @(LTColorSecondaryButton)                    : @(0x061928),
                           @(LTColorTertiaryButton)                     : @(0xdcbc8b),
                           @(LTColorAltButton1)                         : @(0xe3bd58),
                           @(LTColorAltButton2)                         : @(0xaaf1c3),
                           
                           @(LTColorPrimaryText)                        : @(0xffffff),
                           @(LTColorSecondaryText)                      : @(0x000000),
                           @(LTColorTertiaryText)                       : @(0x04a092),
                           @(LTColorPointText)                          : @(0xe3bd58),
                           @(LTColorAltText1)                           : @(0x707070),
                           @(LTColorAltText2)                           : @(0xffd200),
                           @(LTColorAltText3)                           : @(0xe73821),
                           @(LTColorAltText4)                           : @(0xb7b7b7),
                           @(LTColorAltText5)                           : @(0x5c7bf7),
                           @(LTColorAltText6)                           : @(0x4ecdff),

                           
                           @(LTColorPrimaryDiv)                         : @(0x707070),
                           @(LTColorSecondaryDiv)                       : @(0x04a092),
                           @(LTColorTertiaryDiv)                        : @(0xdcbc8b)
                           }
        ;
    }
    return self;
}

@end
