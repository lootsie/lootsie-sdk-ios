//
//  LTColorFactoryLight.h
//  LTLootsie iOS Example
//
//  Created by neon on 10.08.16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LTColorFactory.h"


/*! @class LTColorFactoryLight
 *  @brief Light color style for UI elements.*/
@interface LTColorFactoryLight : LTColorFactory


@end
