//
//  LTColorFactory.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/12/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTColorFactory.h"
#import "LTTools.h"


@implementation LTColorFactory

- (UIColor*)colorWithType:(LTColorType)type {
    return [LTTools colorWithHex:[self.allColors[@(type)] unsignedIntegerValue]];
}

@end
