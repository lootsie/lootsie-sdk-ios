//  LTFontFactory.h
//  Created by Alexander Dovbnya on 5/11/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>
@import UIKit;

/*! Font family type.*/
typedef NS_ENUM(NSInteger, LTFontFamily) {
    LTPrimaryFontFamily,        /*! Primary font. */
    LTSecondaryFontFamily       /*! Secondary font.*/
};

/*! Font weight style.*/
typedef NS_ENUM(NSInteger, LTFontWeight) {
    LTMediumFontWeight,     /*! Medium style.*/
    LTBoldFontWeight,       /*! Bold style.*/
    LTRegularFontWeight,    /*! Regular style.*/
    LTLightFontWeight,      /*! Light style.*/
    LTThinFontWeight,       /*! Thin style.*/
    LTBlackFontWeight       /*! Black style.*/
};

/*! Font style*/
typedef NS_ENUM(NSInteger, LTFontStyle) {
    LTRegularFontStyle,     /*! Regular text.*/
    LTItalicFontStyle       /*! Italic text.*/
};

/*! Special size of font.*/
typedef NS_ENUM(NSInteger, LTFontSize) {
    LTExtraLargeFontSize,       /*! Extra large size. For all iPhone 30pt, for iPhone 6+ 33pt.*/
    LTLargeFontSize,            /*! Large size. For all iPhone 21pt, for iPhone 6+ 24pt.*/
    LTMediumAFontSize,          /*! Medium A size. For all iPhone 18pt, for iPhone 6+ 21pt.*/
    LTMediumBFontSize,          /*! Medium B size. For all iPhone 15pt, for iPhone 6+ 18pt.*/
    LTMediumCFontSize,          /*! Medium C size. For all iPhone 12pt, for iPhone 6+ 15pt.*/
    LTSmallAFontSize,           /*! Small A size. For all iPhone 10pt, for iPhone 6+ 13pt.*/
    LTSmallBFontSize            /*! Small B size. For all iPhone 9pt, for iPhone 6+ 12pt.*/
};


/*! @class LTFontFactory
 *  @brief Class for working with fonts*/
@interface LTFontFactory : NSObject

/*! @brief Get corrent font with settings.
 *  @param family Font family style. @see LTFontFamily
 *  @param weight Font weight style. @see LTFontWeight
 *  @param style Style of text. @see LTFontStyle
 *  @param size Font text size. @see LTFontSize*/
- (UIFont *)fontWithFamily:(LTFontFamily)family weight:(LTFontWeight)weight style:(LTFontStyle)style size:(LTFontSize)size;

/*! @brief Get corrent font with settings.
 *  @param family Font family style. @see LTFontFamily
 *  @param weight Font weight style. @see LTFontWeight
 *  @param style Style of text. @see LTFontStyle
 *  @param size Custom value for font.*/
- (UIFont *)fontWithFamily:(LTFontFamily)family weight:(LTFontWeight)weight style:(LTFontStyle)style ptSize:(CGFloat)size;

@end
