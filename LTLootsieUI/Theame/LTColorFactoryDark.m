//
//  LTColorFactoryDark.m
//  LTLootsie iOS Example
//
//  Created by neon on 10.08.16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTColorFactoryDark.h"

@implementation LTColorFactoryDark

/*! @brief Create a new object.*/
- (id)init {
    self = [super init];
    if (self) {
        self.allColors = @{
                             //Refresh aimation outter circle color
                           @(LTColorVideoRefreshOutterCircle)           : @(0x4be581),
                           
                           @(LTColorInactiveTextPrimaryButton)          : @(0x707070),
                           @(LTColorInactivePrimaryButton)              : @(0xececec),
                           @(LTColorHeaderText)                         : @(0xffffff),
                           @(LTColorWelcomeIconColor)                   : @(0x4ecdff),
                           @(LTColorFirstTimeWizardIconColor)           : @(0xaaf1c3),
                           @(LTColorModalBackground)                    : @(0x80ffffff),
                           @(LTColorNavigationBackground)               : @(0x39302e),
                           // main color
                           @(LTColorPrimaryBackground)                  : @(0x231f20),
                           @(LTColorSecondaryBackground)                : @(0x39302e),
                           @(LTColorTertiaryBackground)                 : @(0xffffff),
                           @(LTColorAltBackground1)                     : @(0x707070),
                           @(LTColorAltBackground2)                     : @(0x4ecdff),
                           @(LTColorAltBBackground3)                    : @(0xececec),
                           @(LTColorAltBackground4)                     : @(0xaaf1c3),
                           @(LTColorAltBackground5)                     : @(0x5c7bf7),
                           @(LTColorAltBackground6)                     : @(0x008000),
                           
                           @(LTColorPrimaryButton)                      : @(0x5c7bf7),
                           @(LTColorPrimaryTextButton)                  : @(0xffffff),
                           @(LTColorSecondaryButton)                    : @(0x707070),
                           @(LTColorTertiaryButton)                     : @(0xececec),
                           @(LTColorAltButton1)                         : @(0xa49a87),
                           @(LTColorAltButton2)                         : @(0xaaf1c3),
                           
                           @(LTColorPrimaryText)                        : @(0xffffff),
                           @(LTColorSecondaryText)                      : @(0x231f20),
                           @(LTColorEditTextButton)                     : @(0x5c7bf7),
                           @(LTColorFreePointsTextButton)               : @(0x231f20),
                           
                           @(LTColorTertiaryText)                       : @(0xaaf1c3),
                           @(LTColorPointText)                          : @(0xaaf1c3),
                           @(LTColorAltText1)                           : @(0x707070),
                           @(LTColorAltText2)                           : @(0xffd200),
                           @(LTColorAltText3)                           : @(0xe73821),
                           @(LTColorAltText4)                           : @(0xb7b7b7),
                           @(LTColorAltText5)                           : @(0x5c7bf7),
                           @(LTColorAltText6)                           : @(0x4ecdff),

                           
                           @(LTColorPrimaryDiv)                         : @(0x707070),
                           @(LTColorSecondaryDiv)                       : @(0x5c7bf7),
                           @(LTColorTertiaryDiv)                        : @(0xececec)
                        }
        ;
    }
    return self;
}

@end
