//  LTTheme.m
//  Created by Alexander Dovbnya on 5/11/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTTheme.h"
#import "LTColorFactoryLight.h"
#import "LTColorFactoryDark.h"
#import "LTColorFactorySolitaire.h"

@interface LTTheme ()

/*! Manager for work with fonts*/
@property (nonatomic, strong) LTFontFactory *fontFactory;

/*! Manager for work with colors*/
@property (nonatomic, strong) LTColorFactory *colorFactory;

/*! Type of current theme.*/
@property (nonatomic, readwrite) LTThemeType themeType;

@end


@implementation LTTheme

- (void)configureColorWithThemeType:(LTThemeType)type {
//    
//    if (_colorFactory) return;
//    
    self.themeType = type;
    
    switch (type) {
        case LTThemeTypeDark:
            self.colorFactory = [LTColorFactoryDark new];
            break;
        case LTThemeTypeLight:
            self.colorFactory = [LTColorFactoryLight new];
            break;
        case LTThemeTypeSolitaire:
            self.colorFactory = [LTColorFactorySolitaire new];
            break;
        default:
            self.colorFactory = [LTColorFactoryDark new];
            break;
    }
}

+ (instancetype)sharedTheme {
    static LTTheme *_engine;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _engine = [self new];
        _engine.fontFactory = [LTFontFactory new];
    });
    return _engine;
}

/*! @brief Getter for colorFactory property.
 *  @return Color factory object.*/
- (LTColorFactory *)colorFactory {
    if (!_colorFactory) [self configureColorWithThemeType:self.themeType];
    return _colorFactory;
}

- (UIFont *)fontWithFamily:(LTFontFamily)family weight:(LTFontWeight)weight style:(LTFontStyle)style size:(LTFontSize)size {
    return [self.fontFactory fontWithFamily:family weight:weight style:style size:size];
}

- (UIFont *)fontWithFamily:(LTFontFamily)family weight:(LTFontWeight)weight style:(LTFontStyle)style ptSize:(CGFloat)size {
    return [self.fontFactory fontWithFamily: family weight: weight style: style ptSize: size];
}

- (UIColor *)colorWithType:(LTColorType)type {
    return [self.colorFactory colorWithType: type];
}

- (UIStatusBarStyle)statusBarStyleForCurrentTheme {
    if (self.themeType == LTThemeTypeDark || self.themeType == LTThemeTypeSolitaire) {
        return UIStatusBarStyleLightContent;
    }
    else {
        return UIStatusBarStyleDefault;
    }
}
@end
