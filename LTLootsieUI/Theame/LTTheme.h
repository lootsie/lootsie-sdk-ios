//  LTTheme.h
//  Created by Alexander Dovbnya on 5/11/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>
#import "LTFontFactory.h"
#import "LTColorFactory.h"

/*! @typedef LTThemeType
 *  @brief Color styles for Lootsie UI.*/
typedef NS_ENUM(NSUInteger, LTThemeType) {
    LTThemeTypeDark,        /*! Dark style.*/
    LTThemeTypeLight,       /*! Light style.*/
    LTThemeTypeSolitaire    /*! Light style.*/
};


/*! @class LTTheme
 *  @brief Manager for work with theme data. Using for set color and font for UI component.*/
@interface LTTheme : NSObject

/*! @brief Create and get instance of manager.*/
+ (instancetype)sharedTheme;

/*! @brief Change style for Lootsie UI.
 *  @param type Color style. @see LTThemeType*/
- (void)configureColorWithThemeType:(LTThemeType)type;

/*! @brief Get corrent font with settings.
 *  @param family Font family style. @see LTFontFamily
 *  @param weight Font weight style. @see LTFontWeight
 *  @param style Style of text. @see LTFontStyle
 *  @param size Font text size. @see LTFontSize*/
- (UIFont *)fontWithFamily:(LTFontFamily)family weight:(LTFontWeight)weight style:(LTFontStyle)style size:(LTFontSize)size;

/*! @brief Get corrent font with settings.
 *  @param family Font family style. @see LTFontFamily
 *  @param weight Font weight style. @see LTFontWeight
 *  @param style Style of text. @see LTFontStyle
 *  @param size Custom value for font.*/
- (UIFont *)fontWithFamily:(LTFontFamily)family weight:(LTFontWeight)weight style:(LTFontStyle)style ptSize:(CGFloat)size;

/*! @brief Get color for UI element with type.
 *  @param type Type of color.
 *  @return Color object.*/
- (UIColor *)colorWithType:(LTColorType)type;

/*! @brief Generate status bar for current style.
 *  @return Status bar style.*/
- (UIStatusBarStyle)statusBarStyleForCurrentTheme;

@end
