//
//  LTColorFactory.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/12/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
@import UIKit;

/*! @typedef LTColorType
 *  @brief Type of colors for UI elements.*/
typedef NS_ENUM(NSUInteger, LTColorType){
    //Refresh aimation outter circle color
    LTColorVideoRefreshOutterCircle,
    LTColorWelcomeIconColor,
    LTColorFirstTimeWizardIconColor,
    LTColorModalBackground,
    LTColorNavigationBackground,
    // main color
    LTColorPrimaryBackground,
    LTColorSecondaryBackground,
    LTColorTertiaryBackground,
    LTColorAltBackground1,
    LTColorAltBackground2,
    LTColorAltBBackground3,
    LTColorAltBackground4,
    LTColorAltBackground5,
    LTColorAltBackground6,
    
    LTColorPrimaryButton,
    LTColorSecondaryButton,
    LTColorTertiaryButton,
    LTColorAltButton1,
    LTColorAltButton2,
    
    LTColorPrimaryText,
    LTColorSecondaryText,
    LTColorTertiaryText,
    LTColorPointText,
    LTColorAltText1,
    LTColorAltText2,
    LTColorAltText3,
    LTColorAltText4,
    LTColorAltText5,
    LTColorAltText6,
    LTColorHeaderText,
    
    LTColorPrimaryDiv,
    LTColorSecondaryDiv,
    LTColorTertiaryDiv,
    
    LTColorPrimaryTextButton,
    LTColorFreePointsTextButton,
    LTColorEditTextButton,
    LTColorInactiveTextPrimaryButton,
    LTColorInactivePrimaryButton
};


/*! @class LTColorFactory
 *  @brief Special controller for show with colors of UI component. Use this call for get correct color of UI component.*/
@interface LTColorFactory : NSObject

/*! List of all supported colors.*/
@property (nonatomic, strong) NSDictionary *allColors;

/*! @brief Get color for UI element with type.
 *  @param type Type of color.
 *  @return Color object.*/
- (UIColor*)colorWithType:(LTColorType)type;

@end
