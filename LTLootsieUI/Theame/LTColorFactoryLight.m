//
//  LTColorFactoryLight.m
//  LTLootsie iOS Example
//
//  Created by neon on 10.08.16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTColorFactoryLight.h"

@implementation LTColorFactoryLight

/*! @brief Create a new object.*/
- (id)init {
    self = [super init];
    if (self) {
        self.allColors = @{
                           @(LTColorVideoRefreshOutterCircle)           : @(0x4be581),
                           @(LTColorInactiveTextPrimaryButton)          : @(0xa49a87),
                           @(LTColorInactivePrimaryButton)              : @(0x707070),
                           @(LTColorPrimaryTextButton)                  : @(0xffffff),
                           @(LTColorEditTextButton)                     : @(0xffffff),
                           @(LTColorFreePointsTextButton)               : @(0xffffff),
                           @(LTColorHeaderText)                         : @(0xffffff),
                           @(LTColorWelcomeIconColor)                   : @(0x4ecdff),
                           @(LTColorFirstTimeWizardIconColor)           : @(0xaaf1c3),
                           @(LTColorModalBackground)                    : @(0x80000000),
                           @(LTColorNavigationBackground)               : @(0x39302e),
                           // main color
                           @(LTColorPrimaryBackground)                  : @(0xececec),
                           @(LTColorSecondaryBackground)                : @(0xffffff),
                           @(LTColorTertiaryBackground)                 : @(0xffffff),
                           @(LTColorAltBackground1)                     : @(0x707070),
                           @(LTColorAltBackground2)                     : @(0x4ecdff),
                           @(LTColorAltBBackground3)                    : @(0xececec),
                           @(LTColorAltBackground4)                     : @(0x5c7bf7),
                           @(LTColorAltBackground5)                     : @(0x5c7bf7),
                           @(LTColorAltBackground6)                     : @(0x008000),
                           
                           @(LTColorPrimaryButton)                      : @(0x5c7bf7),

                           @(LTColorSecondaryButton)                    : @(0x707070),
                           @(LTColorTertiaryButton)                     : @(0x707070),
                           @(LTColorAltButton1)                         : @(0xa49a87),
                           @(LTColorAltButton2)                         : @(0xaaf1c3),
                           
                           @(LTColorPrimaryText)                        : @(0x39302e),
                           @(LTColorSecondaryText)                      : @(0x707070),
                           @(LTColorTertiaryText)                       : @(0x5c7bf7),
                           @(LTColorPointText)                          : @(0x5c7bf7),
                           @(LTColorAltText1)                           : @(0x707070),
                           @(LTColorAltText2)                           : @(0xffd200),
                           @(LTColorAltText3)                           : @(0xe73821),
                           @(LTColorAltText4)                           : @(0xb7b7b7),
                           @(LTColorAltText5)                           : @(0x5c7bf7),
                           @(LTColorAltText6)                           : @(0x4ecdff),

                           
                           @(LTColorPrimaryDiv)                         : @(0x707070),
                           @(LTColorSecondaryDiv)                       : @(0x5c7bf7),
                           @(LTColorTertiaryDiv)                        : @(0xececec)
                           }
        ;
    }
    return self;
}

@end
