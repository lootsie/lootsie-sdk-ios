//  LTUIManager.h
//  Created by Fabio Teles on 5/19/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTLootsie.h"
#import "LTTheme.h"
#import "LTCoreMangerProtocol.h"

#define LT_SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

extern NSString* const _Nonnull LTDismissedAlertKey;
/*! @protocol LTUIManagerDelegate
 *  @brief LTUIManagerDelegate is a protocol that can be implemented to be notified when
 *  important UI changes occur to the Lootsie'UI. For example, you can optionally be
 *  notified when when the UI is shown or dismissed so you can pause or resume your
 *  game.*/
@protocol LTUIManagerDelegate <NSObject>
@optional
/*! @brief Called when user will open Lootsie UI controller.*/
- (void)lootsieUIManagerWillShowUI;

/*! @brief Called when Lootsie UI controller did open.*/
- (void)lootsieUIManagerDidCloseUI;

/*! @brief Return controller whene will be show Lootsie UI controllers.*/
- (nullable UIViewController *)presentationContextForUIManager;

- (void)lootsieUIManagerAccountActivationStatusChange;

- (void)lootsieUIManagerStatusChanged:(BOOL)enabled;;

@end


@class LTAchievement, LTAlertOperation, LTIANItem;
/*! @class LTUIManager
 *  @brief Main class that serves for Lootsie UI display.*/
@interface LTUIManager : NSObject {
    NSOperationQueue *_mainQueue;   /*! Main queue. Using for work with all action.*/
}

/*! Display of warnings, error messages, other alerts, originated from Lootsie,
 *  on top of user application screens. Defaults to YES.*/
@property (nonatomic, assign) BOOL displayAlertsOverAppUserInterface;

/*! A delegate object that's notified when important UI changes occur. @see LTUIManagerDelegate*/
@property (nonatomic, weak, nullable) id<LTUIManagerDelegate> delegate;


/*! @brief Create and return Looties UI manager for work with UI element.*/
+ (nonnull instancetype)sharedManager;

/*! @brief Return mask of supported interface orientations for UI controllers.*/
- (UIInterfaceOrientationMask)supportedInterfaceOrientations;

/*! @brief Shows the Terms of Service page of Lootsie's UI by opening it if not being displayed
 *  or by switching to this page is already being displayed.*/
- (void)showTermsPageOnViewController:(nonnull UIViewController *)parentController fromStartPoint:(CGPoint)startPoint;

/*! @brief Shows the About page of Lootsie's UI by opening it if not being displayed or by
 *  switching to this page is already being displayed.*/
- (void)showAboutPageOnViewController:(nonnull UIViewController *)parentController fromStartPoint:(CGPoint)startPoint;


/*! @brief Check is Lootis UI is shown.
 *  @return YES if Lootsie's UI is currently being displayed.*/
- (BOOL)isShowingUI;

/*! @brief Closes Lootsies's UI if it's currently begin displayed, otherwise it does not perform any action.*/
- (void)closeUI;

/*! @brief Hide special controller.
 *  @param controller Controller which need to hide.*/
- (void)hideViewController:(nonnull UIViewController *)controller;

/*! @brief Hide controller with code.
 *  @param controller Controller which need to hide.
 *  @param callBack Callback is called when controller did close.*/
- (void)hideViewController:(nonnull UIViewController *)controller withComplition:(void(^__nullable)(void))callBack;

/*! @brief Easy way to show an alert dialog by providing a NSError object.
 *  @param error Error object with error info.*/
- (void)showAlertWithError:(nonnull NSError *)error;

/*! @brief Show popup view.
 *  @param title       Title popup.
 *  @param description Description popup.
 *  @param isShowIcon  Need to show sad icon on the popup.
 *  @param buttonText  Text of button.
 *  @param closeAction Action when user press button in popup.*/
- (void)showPopupWithTitle:(nonnull NSString *)title description:(nonnull NSString *)description withSadFace:(BOOL)isShowIcon buttonText:(nonnull NSString *)buttonText didCloseAction:(void (^ __nullable)(void))closeAction;

/*! @brief Show success popup view.
 *  @param title       Title popup.
 *  @param description Description popup.
 *  @param isShowIcon  Need to show sad icon on the popup.
 *  @param buttonText  Text of button.
 *  @param closeAction Action when user press button in popup.*/
- (void)showSuccessPopupWithTitle:(nonnull NSString *)title description:(nonnull NSString *)description buttonText:(nonnull NSString *)buttonText didCloseAction:(void (^ __nullable)(void))closeAction;


- (void)showPopupWithTitle:(nonnull NSString *)title description:(nonnull NSString *)description leftButtonText:(nonnull NSString *)leftbuttonText rightButtonText:(nonnull NSString *)rightButtonText didCloseAction:(void (^ __nullable)(void))closeAction;

/*! @brief Show achievement popup view.
 *  @param title       Title popup.
 *  @param description Description popup.
 *  @param wasAchieved  Need to show checkmark on the popup.
 *  @param buttonText  Text of button.
 *  @param closeAction Action when user press button in popup.*/
- (void)showAchievementPopupWithTitle:(nonnull NSString *)title description:(nonnull NSString *)description difficulty:(LTAchievementDifficulty) difficulty wasAchieved:(BOOL)wasAchieved buttonText:(nonnull NSString *)buttonText didCloseAction:(void (^ __nullable)(void))closeAction;


/*! @brief Show IAN element.
 *  @param item IAN item for show information.
 *  @param bannerTitle Banner text. This thes will show on banner.
 *  @param onAction Callback is called when ian has action logic and user click to this button. This action can use for Catalog, Reward and Video ians.
 *  @param onClose Callback is called whe user close ian.*/
- (void)showIANWithItem: (nonnull LTIANItem*)item bannerTitle:(nonnull NSString*)bannerTitle handlerAction:(void(^__nullable)(LTIANItem* _Nonnull  item))onAction handlerClose:(void(^__nullable)())onClose;

/*! @brief Show multiple ians elements.
 *  @param items List of ians.
 *  @param bannerTitle Banner text. This thes will show on banner.
 *  @param onAction Callback is called when ian has action logic and user click to this button. This action can use for Catalog, Reward and Video ians.
 *  @param onClose Callback is called whe user close ian.*/
- (void)showIANWithItems: (nonnull NSArray<LTIANItem*> *)items bannerTitle:(nonnull NSString*)bannerTitle handlerAction:(void(^__nullable)(LTIANItem* _Nonnull  item))onAction handlerClose:(void(^__nullable)())onClose;

/*!
 *  Apply theme to UI
 *
 *  @param type theme type
 */
- (void)applyTheme:(LTThemeType)type;

/*! @brief Deactivates the account if a user has deactivated it before.
 */

- (void) deactivateAccount;

/*! @brief Reactivates the account if a user has deactivated it before.
  * @param withUI @YES if a prompt should be shown, otherwise @NO.
  */
- (void) reactivateAccountWithUI: (BOOL) withUI;


@end
