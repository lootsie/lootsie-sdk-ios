//
//  LTLootsieWindow.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 4/20/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTLootsieWindow.h"

#import "LTManager.h"
#import "LTUIManager.h"
#import "LTEventManager.h"

#import "LTUserActivity.h"


@implementation LTLootsieWindow


#pragma mark - Init methods

/*! @brief Get visible controller for window.
 *  @param vc Source controller for search.
 *  @return Controller object.*/
- (UIViewController *)getVisibleViewControllerFrom:(UIViewController *)vc {
    if([vc isKindOfClass:[UINavigationController class]]) {
        return [self getVisibleViewControllerFrom:[((UINavigationController *) vc) visibleViewController]];
    } else if([vc isKindOfClass:[UITabBarController class]]) {
        return [self getVisibleViewControllerFrom:[((UITabBarController *) vc) selectedViewController]];
    } else {
        if(vc.presentedViewController) {
            return [self getVisibleViewControllerFrom: vc.presentedViewController];
        } else {
            return vc;
        }
    }
}

/*! @brief Create a new view with frame.
 *  @param frame Frame of view*/
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if(self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

+ (instancetype)createWindow {
    return [[LTLootsieWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
}

- (UIViewController*)visibleViewController {
    return [self getVisibleViewControllerFrom: self.rootViewController];
}

/*! @brief Get view on which user is tapped.
 *  @param point User's touch point.
 *  @param event Event object.
 *  @return View object.*/
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView* v = [super hitTest: point withEvent: event];
    if(v == self.rootViewController.view || v == self) {
        return nil;
    }
    return v;
}

@end
