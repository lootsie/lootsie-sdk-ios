//  LTContainerScrollView.m
//  Created by Alexander Dovbnya on 5/23/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
#import "LTContainerScrollView.h"

@interface LTContainerScrollView ()

/*! List of content views.*/
@property (nonatomic, strong) NSMutableArray *contentViews;

@end


@implementation LTContainerScrollView

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    CGSize contentSize = CGSizeMake(CGRectGetWidth(self.bounds) * self.contentViews.count, CGRectGetHeight(self.bounds));
    UIView *fristController = [self.contentViews firstObject];
    if (!CGSizeEqualToSize(contentSize, self.contentSize) || fristController.frame.origin.x != 0) {
        CGRect frame = self.bounds;
        if (!CGPointEqualToPoint(CGPointZero, frame.origin)) {
            frame.origin = CGPointZero;
        }
        for (UIView *contentView in self.contentViews) {
            contentView.frame = frame;
            frame.origin.x += frame.size.width;
        }
        
        self.contentSize = contentSize;
    }
}

- (void)addContentSubview:(UIView *)view {
    [self.contentViews addObject:view];
    [self addSubview:view];
}

/*! @brief Get list of all content views.*/
- (NSMutableArray *)contentViews {
    if (_contentViews == nil) {
        _contentViews = [NSMutableArray array];
    }
    return _contentViews;
}

@end
