//
//  LTMainNavigationView.m
//  LTLootsie iOS Example
//
//  Copyright © 2017 Lootsie. All rights reserved.
//

#import "LTMainNavigationView.h"
#import "LTTheme.h"



@interface LTMainNavigationView()


/*! List of navigation bar buttons.*/
@property (nonatomic)           IBOutletCollection(UIButton) NSArray* buttons;


/*! Bottom line for mark which page is selected.*/
@property (nonatomic, strong)   UIView* bottomLine;


/*! @brief Update button with current index.*/
- (void)updateButtonsWithCurrentIndex;

/*! @brief Update selection view for button.
 *  @param btn Button for select.
 *  @param animated If YES will play animation.*/
- (void)updateSelectedViewToButton: (UIButton*)btn withAnimation: (BOOL)animated;




/*! @brief Initialized the UI. */
- (void) initUI;

@end

@implementation LTMainNavigationView

@synthesize delegate;


- (void) nibSetup
{
    [super nibSetup];

    [self initUI];
}

- (void) initUI {


    NSLayoutConstraint* widthToParentConstraint = [NSLayoutConstraint constraintWithItem: self.buttons[0]
                                                                               attribute: NSLayoutAttributeWidth
                                                                               relatedBy: NSLayoutRelationEqual
                                                                                  toItem: self.view
                                                                               attribute: NSLayoutAttributeWidth
                                                                              multiplier: 0.25
                                                                                constant: 1.0];

    widthToParentConstraint.priority = 1000;
    [self.view addConstraint: widthToParentConstraint];

    
    self.lineWidth = 4.0;
    self.lineColor = [[LTTheme sharedTheme] colorWithType: LTColorSecondaryDiv];
    self.bottomLine = [[UIView alloc] initWithFrame: CGRectZero];
    self.bottomLine.backgroundColor = self.lineColor;
    [self addSubview: self.bottomLine];
    self.view.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorNavigationBackground];
    _selectedIndex = -1;



    if(self.buttons.count > 0) {
        NSUInteger i = 0;
        [self deselectButtons];
        for(UIButton* btn in self.buttons) {
            UIImage* img = [[btn imageForState: UIControlStateNormal] imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
            [btn setImage: img forState: UIControlStateNormal];
            [btn addTarget: self action: @selector(buttonSelected:) forControlEvents: UIControlEventTouchUpInside];
            btn.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorSecondaryDiv];
            if(i == self.selectedIndex) {
                btn.selected = YES;
                btn.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorTertiaryBackground];
            }
            ++i;
        }
        _buttonsCount = self.buttons.count;
    }
    self.favoriteIcon.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorSecondaryDiv];
    self.favoriteIcon.hidden = YES;
    self.favoriteIcon.layer.cornerRadius = self.favoriteIcon.bounds.size.width / 2.f;
}


- (void)updateButtonsWithCurrentIndex {
    if(self.buttons.count > 0 && self.selectedIndex >= 0 && self.selectedIndex < self.buttons.count) {
        UIButton* btn = [self.buttons objectAtIndex: self.selectedIndex];
        [self deselectButtons];
        btn.selected = YES;
        btn.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorTertiaryBackground];
        BOOL animated = (CGRectIsEmpty(self.bottomLine.frame)) ? NO : YES;
        [self updateSelectedViewToButton: btn withAnimation: animated];
    }
}

- (void)updateSelectedViewToButton: (UIButton*)btn withAnimation: (BOOL)animated {
    if(btn) {
        if(animated) {
            [UIView animateWithDuration: 0.3f
                                  delay: 0
                                options: UIViewAnimationOptionCurveEaseInOut
                             animations: ^{
                                 [self updateBottomLineFrame:btn];
                             } completion: nil];
        } else {
            [self updateBottomLineFrame:btn];
        }
    }
    self.bottomLine.backgroundColor = self.lineColor;
}

- (void)deselectButtons {
    for(UIButton* btn in self.buttons) {
        btn.selected = NO;
        btn.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorAltButton1];
    }
}

#pragma mark - buttons actions
/*! @brief Called when user click to button.
 *  @param sender Button object.*/
- (void)buttonSelected:(UIButton*)sender {
    [self deselectButtons];
    sender.selected = YES;
    sender.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorTertiaryBackground];
    self.selectedIndex = [self.buttons indexOfObject: sender];

     [self.delegate navigateToPage:self andPageSelected: self.selectedIndex];

    
   // if(self.onSelectionIndexChenged) {
    //    self.onSelectionIndexChenged(self, self.selectedIndex);
   // }
}



#pragma mark - setters
/*! @brief Called when user update line color property value.
 *  @param newLineColor New value of property.*/
- (void)setLineColor:(UIColor *)newLineColor {
    _lineColor = newLineColor;
}

/*! @brief Called when user update line width property value.
 *  @param newLineWidth New value of property.*/
- (void)setLineWidth:(NSUInteger)newLineWidth {
    _lineWidth = newLineWidth;
}

/*! @brief Called when user update selected index property value.
 *  @param newSelectedIndex New value of property.*/
- (void)setSelectedIndex:(NSInteger)newSelectedIndex {
    if (_selectedIndex != newSelectedIndex) {
        _selectedIndex = newSelectedIndex;
        [self updateButtonsWithCurrentIndex];
    }
}


#pragma mark - Draw methods
/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    [self updateButtonsWithCurrentIndex];
}

/*! @brief Update bottom line position when user select some button.
 *  @param button New button object.*/
- (void)updateBottomLineFrame:(UIButton *)button {
    CGRect rectSuperView = button.superview.frame;
    CGRect buttonFrame = button.frame;
    CGFloat yy = CGRectGetMaxY(buttonFrame) - self.lineWidth;
    self.bottomLine.frame = CGRectMake(CGRectGetMidX(rectSuperView) - buttonFrame.size.width / 2.f,
                                       yy,
                                       buttonFrame.size.width,
                                       self.lineWidth);
    self.bottomLine.backgroundColor = self.lineColor;
}


#pragma mark - Clear memory

/*! @brief Called when object was released.*/
- (void)dealloc {
 //   self.onSelectionIndexChenged = nil;
}



@end
