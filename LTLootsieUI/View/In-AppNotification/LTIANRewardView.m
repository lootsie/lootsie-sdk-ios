//
//  LTIANRewardView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/21/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANRewardView.h"
#import "LTTheme.h"
#import "LTCatalogReward.h"
#import "UIImageView+LTAFNetworking.h"
#import "LTIANRewardItem.h"


@interface LTIANRewardView ()

/*! Icon of IAN type.*/
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

/*! Reward content view.*/
@property (weak, nonatomic) IBOutlet UIView *rewardView;

/*! Reward image view.*/
@property (weak, nonatomic) IBOutlet UIImageView *rewardImageView;

/*! Reward title label.*/
@property (weak, nonatomic) IBOutlet UILabel *rewardTitleLabel;

@end


@implementation LTIANRewardView


#pragma mark - Init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.iconImageView.image = [self.iconImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.iconImageView.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground2];
    self.rewardView.layer.cornerRadius = 5.0;
    self.rewardView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    self.rewardTitleLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];

}


#pragma mark - Setters

- (void)setItem:(LTIANItem *)newItem {
    [super setItem: newItem];
    
    if(((LTIANRewardItem *)newItem).reward) {
        self.reward = ((LTIANRewardItem *)newItem).reward;
    }
}

/*! @brief Setter method for reward property.
 *  @param newReward New value for property.*/
- (void)setReward:(LTCatalogReward *)newReward {
    _reward = newReward;
    if(_reward) {
        if (_reward.thumbnails != nil) {
            [self.rewardImageView setImageWithURL: [_reward.thumbnails firstObject]];
        }

        self.rewardTitleLabel.text = _reward.name;
    }
}


@end
