//
//  LTIANTipHeaderView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/22/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANTipHeaderView.h"
#import "LTTheme.h"


@interface LTIANTipHeaderView ()
/*! Content view for show all UI elements.*/
@property (nonatomic, weak)     IBOutlet UIView* contentView;

/*! Text label of tool tip.*/
@property (nonatomic, weak)     IBOutlet UILabel* textLabel;

/*! Width constraine of view.*/
@property (nonatomic, weak)     IBOutlet NSLayoutConstraint* contentViewWidth;

@end


@implementation LTIANTipHeaderView


#pragma mark - Init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.contentViewWidth.constant = ([UIScreen mainScreen].bounds.size.width >= 667.0) ? 336.0 : 287.0;
    self.textLabel.font = [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily weight: LTRegularFontWeight style: LTRegularFontStyle size: LTMediumBFontSize];
    if(([UIScreen mainScreen].bounds.size.width < 667.0)) {
        self.textLabel.font = [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily weight: LTRegularFontWeight style: LTRegularFontStyle ptSize: 14.0];
    }
    self.contentView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    self.textLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorAltText6];
    self.contentView.layer.cornerRadius = self.contentView.frame.size.height / 2.0;
    self.contentView.clipsToBounds = YES;
}

@end
