//
//  LTIANManager.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANManager.h"
//objects
#import "LTIANGenericItem.h"
#import "LTIANCatalogItem.h"
#import "LTIANRewardItem.h"
#import "LTIANVideoItem.h"

//views
#import "LTIANGenericView.h"
#import "LTIANCatalogView.h"

#import "LTIANRewardView.h"


@implementation LTIANManager

+ (LTIANGenericView*)ianViewWithItem: (LTIANItem*)item {
    LTIANGenericView* view = nil;
    NSBundle *bundle = [NSBundle bundleForClass:[LTIANManager class]];
    switch (item.type) {
        case LTIANGeneric: {
            if([item isKindOfClass:[LTIANGenericView class]]) {
                view = [[[UINib nibWithNibName: @"LTIANGenericView" bundle: bundle] instantiateWithOwner: self options: nil] firstObject];
                ((LTIANGenericView*)view).item = (LTIANGenericItem*)item;
            }
            break;
        }
        case LTIANCatalog: {
            if([item isKindOfClass:[LTIANCatalogView class]]) {
                view = [[[UINib nibWithNibName: @"LTIANCatalogView" bundle: bundle] instantiateWithOwner: self options: nil] firstObject];
                ((LTIANCatalogView*)view).item = (LTIANCatalogItem*)item;
            }
            break;
        }
        case LTIANReward: {
            if([item isKindOfClass:[LTIANRewardView class]]) {
                view = [[[UINib nibWithNibName: @"LTIANRewardView" bundle: bundle] instantiateWithOwner: self options: nil] firstObject];
                ((LTIANRewardView*)view).item = (LTIANRewardItem*)item;
            }
            break;
        }
        case LTIANVideo: {
            break;
        }
        case LTIANUnknown:
        default: {
            break;
        }
    }
    return view;
}

@end
