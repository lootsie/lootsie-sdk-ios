//
//  LTIANControllerView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/17/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANControllerView.h"
#import "LTIANBannerView.h"


@interface LTIANControllerView ()

/*! Banner of IAN. This will show when SDK did detect new notifications.*/
@property (nonatomic, weak)     IBOutlet LTIANBannerView* bannerView;

@end



@implementation LTIANControllerView

#pragma mark - Init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor clearColor];
}

/*! @brief Called when user tap to view.
 *  @param point user's touch point.
 *  @param event Event object.
 *  @return YES if current view is contains point.*/
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if((_bannerView.expanded && CGRectContainsPoint(self.frame, point)) || CGRectContainsPoint(_bannerView.frame, point)) {
        return YES;
    }
    return NO;
}

@end
