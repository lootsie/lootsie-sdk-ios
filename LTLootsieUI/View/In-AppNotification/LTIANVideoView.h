//
//  LTIANVideoView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/21/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANGenericView.h"

@class LTIANVideoView;
/*! @protocol LTIANVideoViewDelegate
 *  @brief Method to press buttons.*/
@protocol LTIANVideoViewDelegate <LTIANViewDelegate>

/*! @brief Called when user click to free points button.
 *  @param view View which called this method. @see LTIANVideoView*/
- (void)videoViewDidClickFreePointsButton:(LTIANVideoView*)view;

@end



/*! @class LTIANVideoView
 *  @brief Show video IAN content. @see LTIANVideoItem*/
@interface LTIANVideoView : LTIANGenericView

@end
