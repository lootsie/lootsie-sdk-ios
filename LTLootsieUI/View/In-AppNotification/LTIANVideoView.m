//
//  LTIANVideoView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/21/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANVideoView.h"
#import "LTFreePointsButton.h"

@interface LTIANVideoView ()

/*! Free points button.*/
@property (weak, nonatomic) IBOutlet LTFreePointsButton *freePointsButton;

@end



@implementation LTIANVideoView

#pragma mark - Buttons actions
/*! @brief Called when user click to free points button.
 *  @param sender Button object.*/
- (void)onFreePointsButtonClicked:(LTFreePointsButton*)sender {
    if(self.delegate && [((id<LTIANVideoViewDelegate>)self.delegate) respondsToSelector: @selector(videoViewDidClickFreePointsButton:)]) {
        [((id<LTIANVideoViewDelegate>)self.delegate) videoViewDidClickFreePointsButton: self];
    }
}


#pragma mark - Init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.freePointsButton addTarget: self action: @selector(onFreePointsButtonClicked:) forControlEvents: UIControlEventTouchUpInside];
}

@end
