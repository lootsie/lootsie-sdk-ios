//
//  LTIANBannerView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANBannerView.h"
#import "LTTheme.h"
#import "LTIANItem.h"


@interface LTIANBannerView ()

/*! Left view. */
@property (nonatomic, weak)     IBOutlet UIButton* leftView;

/*! Title label. Show text on the banner.*/
@property (nonatomic, weak)     IBOutlet UILabel* titleLabel;

/*! Right view.*/
@property (nonatomic, weak)     IBOutlet UIButton* viewView;


/*! @brief Show IANs list if neede.*/
- (void)expandIfNeeded;

/*! @brief Generate attributes for right button title.
 *  @return Attribute structure.*/
- (NSDictionary*)viewButtonTitleAttributes;

/*! @brief Generate attributes for points symbols.
 *  @return Attribute structure.*/
- (NSDictionary*)pointsSymbolAttributes;

/*! @brief Generate attributes for points value.
 *  @return Attribute structure.*/
- (NSDictionary*)pointsValueAttributes;

/*! @brief Create path for collapsed state.
 *  @return Path object.*/
- (CGPathRef)collapsedPath;

/*! @brief Create path for expanded state.
 *  @return Path object.*/
- (CGPathRef)expandedPath;

/*! @brief Play animation for show points.*/
- (void)animatePointView;

/*! @brief Play animation for title view.*/
- (void)animateTitleView;

/*! @brief Play animation for right button view.*/
- (void)animateButtonView;

/*! @brief Show view with animation.
 *  @param view Target view for apply animation.
 *  @param offset Start value of aniamtion.*/
- (void)appearAnimation:(UIView *)view offsetByX:(NSInteger)offset;

/*! @brief Play animation for hide view.*/
- (void)animateHideButton;

@end


@implementation LTIANBannerView

#pragma mark - Private

- (void)expandIfNeeded {
    if(!self.expanded) {
        self.expanded = YES;
        [self animateHideButton];
    } else {
        self.expanded = NO;
        [self animateHideButton];
    }
    
    [self setNeedsDisplay];
    [self setNeedsLayout];
}

- (NSDictionary*)viewButtonTitleAttributes {
    return @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                                 weight: LTRegularFontWeight
                                                                  style: LTRegularFontStyle
                                                                   size: LTMediumCFontSize],
             NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorHeaderText]};
}

- (NSDictionary*)pointsSymbolAttributes {
    return @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                                 weight: LTBoldFontWeight
                                                                  style: LTRegularFontStyle
                                                                   size: LTMediumAFontSize],
             NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorHeaderText],
             NSBaselineOffsetAttributeName: @(3.0)};
}

- (NSDictionary*)pointsValueAttributes {
    return @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                                 weight: LTBoldFontWeight
                                                                  style: LTRegularFontStyle
                                                                   size: LTExtraLargeFontSize],
             NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorHeaderText]};
}

- (CGPathRef)collapsedPath {
    UIBezierPath* path = [UIBezierPath bezierPathWithRect:self.bounds];
    return path.CGPath;
}

- (CGPathRef)expandedPath {
    CGPoint center = self.center;
    
    UIBezierPath* path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(0, 0)];
    [path addLineToPoint:CGPointMake(self.bounds.size.width, 0)];
    [path addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.size.height)];
    [path addLineToPoint:CGPointMake(center.x + 7, self.bounds.size.height)];
    [path addLineToPoint:CGPointMake(center.x, self.bounds.size.height + 9)];
    [path addLineToPoint:CGPointMake(center.x - 7, self.bounds.size.height)];
    [path addLineToPoint:CGPointMake(0, self.bounds.size.height)];
    [path addLineToPoint:CGPointMake(0, 0)];
    
    return path.CGPath;
}

- (void)animatePointView {
    [self appearAnimation:self.leftView offsetByX:self.titleLabel.frame.size.width * -2];
}

- (void)animateTitleView {
    [self appearAnimation:self.titleLabel offsetByX:self.titleLabel.frame.size.width * -1.5];
}

- (void)animateButtonView {
    [self appearAnimation:self.viewView offsetByX:self.viewView.frame.size.width * 1.5];
}

- (void)appearAnimation:(UIView *)view offsetByX:(NSInteger)offset {
    CASpringAnimation* showButtonViewAnim = [CASpringAnimation animationWithKeyPath: @"position"];
    NSValue *startPosition = [NSValue valueWithCGPoint: CGPointMake(view.center.x + offset,
                                                                    view.center.y)];
    showButtonViewAnim.fromValue = startPosition;
    showButtonViewAnim.toValue = [NSValue valueWithCGPoint: view.center];
    showButtonViewAnim.duration = 1.0f;
    showButtonViewAnim.damping = 25.0f;
    showButtonViewAnim.stiffness = 300.f;
    showButtonViewAnim.removedOnCompletion = YES;
    [view.layer addAnimation: showButtonViewAnim forKey: @"showAnim"];
}

- (void)animateHideButton {
    [CATransaction begin];
    CABasicAnimation* viewButtonAnim = [CABasicAnimation animationWithKeyPath:@"position"];
    viewButtonAnim.fromValue = [NSValue valueWithCGPoint: self.viewView.center];
    viewButtonAnim.toValue = [NSValue valueWithCGPoint: CGPointMake(self.viewView.center.x + (self.viewView.frame.size.width * 1.5), self.viewView.center.y)];
    viewButtonAnim.duration = 0.3;
    viewButtonAnim.removedOnCompletion = YES;
    [CATransaction setCompletionBlock:^{
        NSString* btnTitle = NSLocalizedStringFromTable(((self.expanded) ? @"Dismiss" : @"View"), @"LTLootsie", nil);
        self.viewButtonTitle = [btnTitle uppercaseString];
        [self animateButtonView];
    }];
    [self.viewView.layer addAnimation:viewButtonAnim forKey: @"updateTextOnButton"];
    [CATransaction commit];
}


#pragma mark - Actions
/*! @brief Called when user tap to view.
 *  @param sender Gesture object.*/
- (void)onTapGesture:(UITapGestureRecognizer*)sender {
    [self expandIfNeeded];
    
    if(self.onBannerTouch) {
        self.onBannerTouch(self);
    }
}

/*! @brief Called when user tap to view button.
 *  @param sender Button object.*/
- (void)onViewButtonClicked:(UIButton*)sender {
    [self expandIfNeeded];
    
    if(self.onViewButtonTap) {
        self.onViewButtonTap(self);
    }
}


#pragma mark - UI methods
/*! @brief Load all UI component.*/
- (void)loadUI {
    self.titleLabel.font = [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                          weight: LTMediumFontWeight
                                                           style: LTRegularFontStyle
                                                            size: LTMediumCFontSize];
    self.titleLabel.textColor = [[LTTheme sharedTheme] colorWithType: LTColorHeaderText];
    if(self.viewButtonTitle.length > 0) {
        NSAttributedString* str = [[NSAttributedString alloc] initWithString: self.viewButtonTitle
                                                                  attributes: [self viewButtonTitleAttributes]];
        [self.viewView setAttributedTitle: str forState: UIControlStateNormal];
    }
    
    if(self.leftPoints > 0) {
        NSMutableAttributedString* attrStr = [NSMutableAttributedString new];
        [attrStr appendAttributedString: [[NSAttributedString alloc] initWithString: @"+" attributes: [self pointsSymbolAttributes]]];
        [attrStr appendAttributedString: [[NSAttributedString alloc] initWithString: [NSString stringWithFormat: @"%ld", (long)_leftPoints]
                                                                         attributes: [self pointsValueAttributes]]];
        [self.leftView setImage: nil forState: UIControlStateNormal];
        [self.leftView setAttributedTitle: attrStr forState: UIControlStateNormal];
    } else if(self.leftImage) {
        _leftImage = [_leftImage imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
        self.leftView.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorHeaderText];
        [self.leftView setAttributedTitle: nil forState: UIControlStateNormal];
        [self.leftView setTitle: nil forState: UIControlStateNormal];
        [self.leftView setImage: _leftImage forState: UIControlStateNormal];
    } else {
        [self.leftView setImage: nil forState: UIControlStateNormal];
        [self.leftView setAttributedTitle: nil forState: UIControlStateNormal];
        [self.leftView setTitle: nil forState: UIControlStateNormal];
        
    }
}


#pragma mark - init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib]; // FIX
    [self loadUI];
    self.expanded = NO;
    self.leftView.userInteractionEnabled = NO;
    self.leftPoints = 0;
    self.viewButtonTitle = [NSLocalizedStringFromTable(@"View", @"LTLootsie", nil) uppercaseString];
    self.leftView.hidden = YES;
    self.titleLabel.hidden = YES;
    self.viewView.hidden = YES;
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(onTapGesture:)];
    [self addGestureRecognizer: tap];
    [self.viewView addTarget: self action: @selector(onViewButtonClicked:) forControlEvents: UIControlEventTouchUpInside];
}


#pragma mark - Setters
/*! @brief Setter method for title property.
 *  @param newTitle New value for property.*/
- (void)setTitle:(NSString *)newTitle {
    _title = newTitle;
    self.titleLabel.text = [_title uppercaseString];
}

/*! @brief Setter method for points property.
 *  @param newPoints New value for property.*/
- (void)setLeftPoints:(NSInteger)newPoints {
    _leftPoints = newPoints;
    [self loadUI];
}

/*! @brief Setter method for image property.
 *  @param newImage New value for property.*/
- (void)setLeftImage:(UIImage *)newImage {
    _leftImage = newImage;
    [self loadUI];
}

/*! @brief Setter method for expanded property.
 *  @param newExpanded New value for property.*/
- (void)setExpanded:(BOOL)newExpanded {
    _expanded = newExpanded;
    [self loadUI];
}

/*! @brief Setter method for view button title property.
 *  @param newViewButtonTitle New value for property.*/
- (void)setViewButtonTitle:(NSString *)newViewButtonTitle {
    _viewButtonTitle = newViewButtonTitle;
    [self loadUI];
}

- (void)playAnimationsWithCompletion:(LTIANBannerDidAnimationComplete)completion {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.leftView.hidden = NO;
        self.titleLabel.hidden = NO;
        self.viewView.hidden = NO;
        
        [CATransaction begin];
        [CATransaction setCompletionBlock:^{
            if(completion) {
                completion();
            }
        }];
        [self animatePointView];
        [self animateTitleView];
        [self animateButtonView];
        [CATransaction commit];
    });
}

- (void)hideWithAnimationAndCompletion:(LTIANBannerDidAnimationComplete)completion {
    CGPoint oldPoint = self.center;
    self.center = CGPointMake(self.center.x, -self.frame.size.height);
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        if(completion) {
            completion();
        }
    }];
    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath: @"position"];
    anim.fromValue = [NSValue valueWithCGPoint: oldPoint];
    anim.toValue = [NSValue valueWithCGPoint: self.center];
    anim.duration = 0.3;
    [self.layer addAnimation: anim forKey: @"hideView"];
    [CATransaction commit];
}



#pragma mark - Draw methods
/*! @brief Get layer class name for current view.
 *  @return Nale of layer class.*/
+ (Class)layerClass {
    return [CAShapeLayer class];
}

/*! @brief Called when component is shows in interface builder.*/
- (void)prepareForInterfaceBuilder {
    [self loadUI];
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    
    ((CAShapeLayer*)self.layer).fillColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground5].CGColor;
    ((CAShapeLayer*)self.layer).path = (self.expanded) ? [self expandedPath] : [self collapsedPath];
}

@end
