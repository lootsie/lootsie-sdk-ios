//
//  LTIANGenericView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANGenericView.h"
#import "LTIANItem.h"
#import "LTLogoutButton.h"
#import "LTTheme.h"

@interface LTIANGenericView ()

/*! Detail container view.*/
@property (nonatomic, weak) IBOutlet UIView* detailContentView;

/*! Title of IAN.*/
@property (nonatomic, weak) IBOutlet UILabel* titleLabel;

/*! Description of IAN.*/
@property (nonatomic, weak) IBOutlet UILabel* descLabel;

/*! Constrain for set width of container view.*/
@property (nonatomic, weak) IBOutlet NSLayoutConstraint* detailContentViewWidth;

@end


@implementation LTIANGenericView


#pragma mark - button actions
/*! @brief Called when user click to close button.
 *  @param sender Button object.*/
- (void)onCloseButtonTap:(LTLogoutButton*)sender {
    if(self.delegate && [self.delegate respondsToSelector:@selector(ianViewDidClose:)]) {
        [self.delegate ianViewDidClose: self];
    }
}

/*! @brief Called when user do left swipe.
 *  @param sender Swipe gesture object.*/
- (void)onLeftSwipe:(UISwipeGestureRecognizer*)sender {
    if(self.delegate && [self.delegate respondsToSelector:@selector(ianViewDidSwipeLeft:)]) {
        [self.delegate ianViewDidSwipeLeft: self];
    }
}

/*! @brief Called when user do right swipe.
 *  @param sender Swipe gesture object.*/
- (void)onRightSwipe:(UISwipeGestureRecognizer*)sender {
    if(self.delegate && [self.delegate respondsToSelector:@selector(ianViewDidSwipeRight:)]) {
        [self.delegate ianViewDidSwipeRight: self];
    }
}


#pragma mark - init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    self.clipsToBounds = NO;
    self.detailContentView.hidden = YES;
    self.detailContentView.layer.cornerRadius = 5.0;
    self.detailContentView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    
    self.titleLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];
    self.descLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];

    [self.closeButton addTarget: self action: @selector(onCloseButtonTap:) forControlEvents: UIControlEventTouchUpInside];
    
    self.detailContentViewWidth.constant = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? 336.0 : 287.0;

    UISwipeGestureRecognizer* leftSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onLeftSwipe:)];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    UISwipeGestureRecognizer* rightSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(onRightSwipe:)];
    rightSwipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.detailContentView addGestureRecognizer: leftSwipe];
    [self.detailContentView addGestureRecognizer: rightSwipe];
}

/*! @brief Called by the collection view before the instance is returned from the reuse queue.*/
- (void)prepareForReuse {
    [super prepareForReuse];
    self.detailContentView.hidden = YES;
}


#pragma mark - setters
/*! @brief Setter method for zIndex property.
 *  @param new_zIndex New value for property.*/
- (void)setZIndex:(CGFloat)new_zIndex {
    _zIndex = new_zIndex;
    self.layer.zPosition = _zIndex;
}

/*! @brief Show card with animation.
 *  @param height Height of card.
 *  @param duration Aniamtion duration.*/
- (void)showCardWithAnimation:(CGFloat)height duration:(CGFloat)duration {
    self.detailContentView.hidden = NO;
    [CATransaction begin];
    CASpringAnimation* anim = [CASpringAnimation animationWithKeyPath: @"position"];
    anim.fromValue = [NSValue valueWithCGPoint: CGPointMake(self.detailContentView.center.x, self.detailContentView.center.y - height)];
    anim.toValue = [NSValue valueWithCGPoint: CGPointMake(self.detailContentView.center.x, self.detailContentView.center.y)];
    anim.removedOnCompletion = YES;
    anim.duration = duration;
    anim.damping = 20.0f;
    anim.stiffness = 300.f;
    [self.detailContentView.layer addAnimation: anim forKey: @"expandAnim"];
    [CATransaction commit];
}

/*! @brief Play spring animation for current view.*/
- (void)springAnimation {
    CASpringAnimation* anim = [CASpringAnimation animationWithKeyPath: @"position"];
    anim.fromValue = [NSValue valueWithCGPoint: CGPointMake(self.detailContentView.center.x, self.detailContentView.center.y - 10.0)];
    anim.toValue = [NSValue valueWithCGPoint: CGPointMake(self.detailContentView.center.x, self.detailContentView.center.y)];
    anim.removedOnCompletion = YES;
    anim.duration = 1.0f;
    anim.damping = 20.0f;
    anim.stiffness = 300.f;
    [self.detailContentView.layer addAnimation: anim forKey: @"updateAnim"];
}

/*! @brief Update card visible flag.*/
- (void)showCard {
    self.detailContentView.hidden = NO;
}

/*! @brief Get content size of content view.
 *  @return Content size of view.*/
- (CGSize)contentSize {
    return self.detailContentView.frame.size;
}

/*! @brief Setter method for description property.
 *  @param newDesc New value for property.*/
- (void)setDesc:(NSString *)newDesc {
    _desc = newDesc;
    self.descLabel.text = _desc;
}

/*! @brief Setter method for title property.
 *  @param newTitle New value for property.*/
- (void)setTitle:(NSString *)newTitle {
    _title = newTitle;
    self.titleLabel.text = _title;
}

/*! @brief Setter method for IAN item property.
 *  @param newItem New value for property.*/
- (void)setItem:(LTIANItem *)newItem {
    _item = newItem;
    self.title = _item.title;
    self.desc = _item.descStr;
}

/*! @brief Setter method for delay property.
 *  @param newSeconds New value for property.*/
- (void)setSeconds:(CGFloat)newSeconds {
    _seconds = newSeconds;
    self.closeButton.hidden = (_seconds > 0.0 && self.item.type == LTIANGeneric);
    if(_seconds > 0.0) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(newSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            if(self.delegate && [self.delegate respondsToSelector:@selector(ianViewDidClose:)]) {
                [self.delegate ianViewDidClose: self];
            }
        });
    }
}


#pragma mark - clear memory
/*! @brief Called when object was released.*/
- (void)dealloc {
    self.title = nil;
    self.desc = nil;
}

@end
