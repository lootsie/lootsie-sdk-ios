//
//  LTFooterView.m
//  LTLootsie iOS Example
//
//  Copyright © 2017 Lootsie. All rights reserved.
//
// @TODO Add black top divider view

#import "LTFooterView.h"
#import "LTTheme.h"


@interface LTFooterView()

/*! @brief Divider view. Shows at the bottom of the footer.*/
@property (weak, nonatomic) IBOutlet UIView *dividerView;

/*! @brief Initialized the UI. */
- (void) initUI;


@end

@implementation LTFooterView

- (void) nibSetup
{
    [super nibSetup];

    [self initUI];
}

- (void) initUI {
    self.view.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorNavigationBackground];
    self.dividerView.backgroundColor = [UIColor blackColor];



}

@end
