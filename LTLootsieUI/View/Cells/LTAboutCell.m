//
//  LTAboutCell.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/2/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTAboutCell.h"
#import "LTTheme.h"
@interface LTAboutCell ()
/*! Title label.*/
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! Value label.*/
@property (weak, nonatomic) IBOutlet UILabel *valueLabel;

@end


@implementation LTAboutCell

#pragma mark - init method

- (void)awakeFromNib {
    [super awakeFromNib];
    self.valueLabel.textColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText];

}

#pragma mark - Setters

/*! @brief Setter for update title.
 *  @param newTitle New title string.*/
- (void)setTitle:(NSString *)newTitle {
    _title = newTitle;
    self.titleLabel.text = _title;
}

/*! @brief Setter for update value.
 *  @param newSubtitle New value sctring.*/
- (void)setSubtitle:(NSString *)newSubtitle {
    _subtitle = newSubtitle;
    self.valueLabel.text = _subtitle;
}

@end
