//
//  LTAboutCell.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/2/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTAboutCell
 *  @brief Cell with information about property.*/
@interface LTAboutCell : UITableViewCell

/*! Tile of property.*/
@property (nonatomic, strong)   NSString* title;

/*! Value of property.*/
@property (nonatomic, strong)   NSString* subtitle;

@end
