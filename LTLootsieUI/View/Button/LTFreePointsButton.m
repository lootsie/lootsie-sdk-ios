//
//  LTFreePointsButton.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/21/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFreePointsButton.h"

@implementation LTFreePointsButton

/*! @brief Called when component is shows in interface builder.*/
- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
    [self initilizateUI];
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    [self initilizateUI];
}

- (void)initilizateUI {
    [super initilizateUI];
    self.tintColor = [[LTTheme sharedTheme] colorWithType:LTColorFreePointsTextButton];
    self.family = LTPrimaryFontFamily;
    self.weight = LTBoldFontWeight;
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground4];
    
    self.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    UIImage *image = [self.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self setImage:image forState:self.state];
    [self setTitleColor:self.tintColor  forState:UIControlStateNormal];
    [self setTitle:self.currentTitle forState:UIControlStateNormal];
}

#pragma mark - draw methods
/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.bounds;
    frame.size.height = self.isSmallHeight ? 23.f : 35.f;
    self.bounds = frame;
    self.cornerRadius = self.bounds.size.height / 2.f;
    CGFloat offset = self.isSmallHeight ? 5 : 15;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, offset, 0, offset);
}

#pragma mark - setters
/*! @brief Called when user update small height property value.
 *  @param isSmallHeight New value of property.*/
- (void)setIsSmallHeight:(BOOL)isSmallHeight {
    [super setIsSmallHeight: isSmallHeight];
    self.size = isSmallHeight ? LTSmallAFontSize : LTMediumCFontSize;
}

@end
