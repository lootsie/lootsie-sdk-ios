//  LTBaseButton.h
//  Created by Alexander Dovbnya on 5/11/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <UIKit/UIKit.h>
#import "LTTheme.h"

IB_DESIGNABLE
/*! @class LTBaseButton
 *  @brief Base button component.*/
@interface LTBaseButton : UIButton

/*! Corner radius of background button.*/
@property (nonatomic, assign) IBInspectable CGFloat cornerRadius;

/*! Font family style. @see LTFontFamily*/
@property (nonatomic, assign) IBInspectable NSInteger family;

/*! Font weight style. @see LTFontWeight*/
@property (nonatomic, assign) IBInspectable NSInteger weight;

/*! Font style style. @see LTFontStyle*/
@property (nonatomic, assign) IBInspectable NSInteger style;

/*! Font size style. @see LTFontSize*/
@property (nonatomic, assign) IBInspectable NSInteger size;


/*! @brief Override method for initilize UI. Use this method in subclass for
 *  update default UI style for button*/
- (void)initilizateUI;

@end
