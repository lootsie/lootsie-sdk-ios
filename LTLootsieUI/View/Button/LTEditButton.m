//
//  LTEditButton.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/31/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTEditButton.h"

@implementation LTEditButton

/*! @brief Called when component is shows in interface builder.*/
- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
    [self initilizateUI];
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    [self initilizateUI];
}

- (void)initilizateUI {
    [super initilizateUI];
    
//    [self setTitleColor: [[LTTheme sharedTheme] colorWithType: LTColorAltBackground5] forState: UIControlStateNormal];
    
    [self setImage:[self.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.tintColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryTextButton];
    
    [self setTitleColor:self.tintColor forState:UIControlStateNormal];
    
    self.family = LTPrimaryFontFamily;
    self.weight = LTBoldFontWeight;
    
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorAltBackground5];
    
    if(self.currentImage) {
        self.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    }
    
//    self.backgroundColor = [UIColor clearColor];
//    self.layer.borderColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground5].CGColor;
//    self.layer.borderWidth = 1.0;
}

#pragma mark - draw methods
/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.bounds;
    frame.size.height = self.isSmallHeight ? 23.f : 35.f;
    self.bounds = frame;
    self.cornerRadius = self.bounds.size.height / 2.f;
    CGFloat offset = self.isSmallHeight ? 5 : 15;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, offset, 0, offset);
}

#pragma mark - setters
/*! @brief Called when user update small height property value.
 *  @param isSmallHeight New value of property.*/
- (void)setIsSmallHeight:(BOOL)isSmallHeight {
    _isSmallHeight = isSmallHeight;
    self.size = isSmallHeight ? LTSmallAFontSize : LTMediumCFontSize;
}

/*! @brief Called when user update selected property value.
 *  @param selected New value of property.*/
- (void)setSelected:(BOOL)selected {
    [super setSelected: selected];
    
    self.backgroundColor = selected ? [[LTTheme sharedTheme] colorWithType: LTColorAltBackground4] : [[LTTheme sharedTheme] colorWithType: LTColorAltBackground5];
    self.layer.borderColor = selected ? [UIColor clearColor].CGColor : [[LTTheme sharedTheme] colorWithType: LTColorAltBackground5].CGColor;
    
    NSString* currentTitle = (selected) ? [self titleForState: UIControlStateSelected] : [self titleForState: UIControlStateNormal];
    UIColor* color = [[LTTheme sharedTheme] colorWithType: selected ? LTColorEditTextButton : LTColorAltBackground5];
    UIFont *font = [[LTTheme sharedTheme] fontWithFamily: self.family weight: self.weight style: self.style size: self.size];
    NSAttributedString* strAttribute = [[NSAttributedString alloc] initWithString: currentTitle attributes: @{ NSFontAttributeName : font,
                                                                                                               NSForegroundColorAttributeName : color}];
    [self setAttributedTitle: strAttribute forState: UIControlStateSelected];
}

@end
