//
//  LTCatalogButton.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/21/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTCatalogButton.h"

@implementation LTCatalogButton

- (void)initilizateUI {
    [super initilizateUI];
    [self setTitleColor:[[LTTheme sharedTheme] colorWithType: LTColorPrimaryTextButton] forState: UIControlStateNormal];
    self.family = LTPrimaryFontFamily;
    self.weight = LTBoldFontWeight;
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground1];
    
    self.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    self.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
}


@end
