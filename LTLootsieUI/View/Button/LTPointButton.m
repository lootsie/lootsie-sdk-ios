//  LTPointButton.m
//  Created by Alexander Dovbnya on 5/13/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTPointButton.h"
#import "LTManager.h"
#import "LTTools.h"

@implementation LTPointButton

- (void)initilizateUI {
    [super initilizateUI];
    self.family = LTPrimaryFontFamily;
    self.weight = LTBoldFontWeight;
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.minimumScaleFactor = 0.2f;
}

/*! @brief Called when user update small height property value.
 *  @param isSmallHeight New value of property.*/
- (void)setIsSmallHeight:(BOOL)isSmallHeight {
    _isSmallHeight = isSmallHeight;
    self.size = isSmallHeight ? LTMediumBFontSize : LTMediumAFontSize;
}

/*! @brief Called when user update points property value.
 *  @param newPoints New value of property.*/
- (void)setPoints:(NSInteger)newPoints {
    _points = newPoints;

    UIFont *font = [[LTTheme sharedTheme] fontWithFamily: self.family weight: self.weight style: self.style size: self.size];
    UIFont *fontSmall = [[LTTheme sharedTheme] fontWithFamily: self.family weight: self.weight style: self.style size: self.size + 1];
    NSAttributedString* strAttribute = [LTTools pointsStringValue:newPoints withPointFont:font currencyFont:fontSmall];
    [self setAttributedTitle: strAttribute forState: UIControlStateNormal];
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.bounds;
    frame.size.height = self.isSmallHeight ? 24.f : 33.f;
    self.bounds = frame;
    self.cornerRadius = self.bounds.size.height / 2.f;
    CGFloat offset = self.isSmallHeight ? 15 : 25;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, offset, 0, offset);
}

@end
