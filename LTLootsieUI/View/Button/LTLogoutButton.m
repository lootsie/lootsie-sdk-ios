//
//  LTLogoutButton.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/7/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTLogoutButton.h"

@implementation LTLogoutButton

- (void)initilizateUI {
    [super initilizateUI];
    [self setImage:[self.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    self.tintColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryTextButton];
    
    [self setTitleColor:self.tintColor forState:UIControlStateNormal];
    
    self.family = LTPrimaryFontFamily;
    self.weight = LTBoldFontWeight;
    
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorAltBackground5];
    
    if(self.currentImage) {
        self.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.titleLabel.transform = CGAffineTransformMakeScale(-1.0, 1.0);
        self.imageView.transform = CGAffineTransformMakeScale(-1.0, 1.0);
    }
}


#pragma mark - draw methods
/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.bounds;
    frame.size.height = self.isSmallHeight ? 23.f : 35.f;
    self.bounds = frame;
    self.cornerRadius = self.bounds.size.height / 2.f;
    CGFloat offset = self.isSmallHeight ? 5 : 15;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, offset, 0, offset);
}


#pragma mark - setters
/*! @brief Called when user update small height property value.
 *  @param isSmallHeight New value of property.*/
- (void)setIsSmallHeight:(BOOL)isSmallHeight {
    _isSmallHeight = isSmallHeight;
    self.size = isSmallHeight ? LTSmallAFontSize : LTMediumCFontSize; //LTSmallBFontSize : LTSmallAFontSize; 
}

@end
