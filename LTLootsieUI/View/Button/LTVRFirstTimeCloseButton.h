//
//  LTVRFirstTimeCloseButton.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/23/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTBaseButton.h"

/*! @class LTVRFirstTimeCloseButton
 *  @brief Button for close firs time wizard.*/
@interface LTVRFirstTimeCloseButton : LTBaseButton

/*! YES if need to use small size of button.*/
@property (nonatomic, assign) IBInspectable BOOL isSmallHeight;

@end
