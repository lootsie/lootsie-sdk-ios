//  LTBaseButton.m
//  Created by Alexander Dovbnya on 5/11/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
#import "LTBaseButton.h"

@implementation LTBaseButton

/*! @brief Called when component is shows in interface builder.*/
- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
    [self initilizateUI];
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    [self initilizateUI];
}

- (void)initilizateUI {
    
}

/*! @brief Called when need update corner rarius for component.
 *  @param cornerRadius New value of property.*/
- (void)setCornerRadius:(CGFloat)cornerRadius {
    self.layer.cornerRadius = cornerRadius;
}

#if !TARGET_INTERFACE_BUILDER
/*! @brief Called when need update title
 *  @param title Title string.
 *  @param state State of component for which need update title.*/
- (void)setTitle:(NSString *)title forState:(UIControlState)state {
    UIFont *font = [[LTTheme sharedTheme] fontWithFamily:self.family weight:self.weight style:self.style size:self.size];
    NSDictionary *attribute = @{ NSFontAttributeName : font,
                                 NSForegroundColorAttributeName : [self currentTitleColor]};
    NSAttributedString *titleAttributeString = [[NSAttributedString alloc] initWithString:title attributes:attribute];
    [self setAttributedTitle:titleAttributeString forState:state];
}
#endif


#pragma mark Font Property
/*! @brief Called when user update family property value.
 *  @param family New value of property.*/
- (void)setFamily:(NSInteger)family {
    _family = family;
    [self updateFont];
}

/*! @brief Called when user update weight property value.
 *  @param weight New value of property.*/
- (void)setWeight:(NSInteger)weight {
    _weight = weight;
    [self updateFont];
}

/*! @brief Called when user update style property value.
 *  @param style New value of property.*/
- (void)setStyle:(NSInteger)style {
    _style = style;
    [self updateFont];
}

/*! @brief Called when user update size property value.
 *  @param size New value of property.*/
- (void)setSize:(NSInteger)size {
    _size = size;
    [self updateFont];
}

/*! @brief Called whne need to update font.*/
- (void)updateFont {
    [self setTitle:self.currentTitle forState:self.state];
}

@end
