//
//  LTContactSupportButton.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/1/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTContactSupportButton.h"

@implementation LTContactSupportButton

/*! @brief Called when component is shows in interface builder.*/
- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
    [self initilizateUI];
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    [self initilizateUI];
}

- (void)initilizateUI {
    [super initilizateUI];
    [self setTitleColor:[[LTTheme sharedTheme] colorWithType: LTColorSecondaryText] forState:UIControlStateNormal];
    self.family = LTPrimaryFontFamily;
    self.weight = LTBoldFontWeight;
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorAltButton1];
}

#pragma mark Property
/*! @brief Called when user update small height property value.
 *  @param isSmallHeight New value of property.*/
- (void)setIsSmallHeight:(BOOL)isSmallHeight {
    _isSmallHeight = isSmallHeight;
    self.size = isSmallHeight ? LTMediumCFontSize : LTMediumBFontSize; // LTSmallBFontSize : LTSmallAFontSize;
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect frame = self.bounds;
    frame.size.height = self.isSmallHeight ? 22.f : 35.f;
    self.bounds = frame;
    self.cornerRadius = self.bounds.size.height / 2.f;
    CGFloat offset = self.isSmallHeight ? 5 : 15;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, offset, 0, offset);
}


#pragma mark - setters
/*! @brief Called when user update enabled property value.
 *  @param enabled New value of property.*/
- (void)setEnabled:(BOOL)enabled {
    [super setEnabled: enabled];
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType: enabled ? LTColorPrimaryBackground : LTColorSecondaryButton];
    
    NSString* currentTitle = (enabled) ? [self titleForState: UIControlStateDisabled] : [self titleForState: UIControlStateNormal];
    UIColor* color = [[LTTheme sharedTheme] colorWithType: enabled ? LTColorPrimaryText : LTColorAltButton1];
    UIFont *font = [[LTTheme sharedTheme] fontWithFamily: self.family weight: self.weight style: self.style size: self.size];
    
    NSAttributedString* strAttribute = [[NSAttributedString alloc] initWithString: currentTitle attributes: @{ NSFontAttributeName : font,
                                                                                                               NSForegroundColorAttributeName : color}];
    [self setAttributedTitle: strAttribute forState: UIControlStateNormal];
}


@end
