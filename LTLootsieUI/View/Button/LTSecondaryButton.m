//  LTSecondaryButton.m
//  Created by Alexander Dovbnya on 5/13/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTSecondaryButton.h"

@implementation LTSecondaryButton

- (void)initilizateUI {
    [super initilizateUI];
    
    [self setTitleColor:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryTextButton] forState:UIControlStateNormal];
    self.family = LTPrimaryFontFamily;
    self.weight = LTBoldFontWeight;
    self.size = LTMediumCFontSize;
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryButton];
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.bounds;
    frame.size.height = 35.f;
//    frame.size.width = 106.f;
    self.bounds = frame;
    self.cornerRadius = self.frame.size.height / 2.f;
}

@end
