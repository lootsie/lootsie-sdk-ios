//
//  LTVRFirstTimeCloseButton.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/23/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTVRFirstTimeCloseButton.h"

@implementation LTVRFirstTimeCloseButton

/*! @brief Called when component is shows in interface builder.*/
- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
    [self initilizateUI];
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    [self initilizateUI];
}

- (void)initilizateUI {
    [super initilizateUI];
    [self setTitleColor:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryTextButton] forState:UIControlStateNormal];
    self.family = LTPrimaryFontFamily;
    self.weight = LTBoldFontWeight;
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorAltBackground5];
}

#pragma mark Property
/*! @brief Called when user update small height property value.
 *  @param isSmallHeight New value of property.*/
- (void)setIsSmallHeight:(BOOL)isSmallHeight {
    _isSmallHeight = isSmallHeight;
    self.size = isSmallHeight ? LTMediumCFontSize : LTMediumBFontSize;
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.bounds;
    frame.size.height = self.isSmallHeight ? 25.f : 35.f;
    self.bounds = frame;
    self.cornerRadius = self.bounds.size.height / 2.f;
    CGFloat offset = self.isSmallHeight ? 5 : 15;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, offset, 0, offset);
}


#pragma mark Setters
/*! @brief Called when user update enabled property value.
 *  @param enabled New value of property.*/
- (void)setEnabled:(BOOL)enabled {
    [super setEnabled: enabled];
    
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType: (enabled) ? LTColorAltBackground5 : LTColorAltBackground1];
    [self setTitleColor: [[LTTheme sharedTheme] colorWithType: LTColorPrimaryTextButton]
               forState: UIControlStateDisabled];
}

@end
