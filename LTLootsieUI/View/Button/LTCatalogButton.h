//
//  LTCatalogButton.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/21/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTLogoutButton.h"

/*! @class LTCatalogButton
 *  @brief Button for show catalog list. It's used in catalog IAN.*/
@interface LTCatalogButton : LTLogoutButton

@end
