//  LTPrimaryButton.m
//  Created by Alexander Dovbnya on 5/12/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTPrimaryButton.h"
#import "LTTheme.h"
#import "LTTools.h"

@interface LTPrimaryButton ()
/*! Height constrain for button.*/
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *heightConstraint;

@end


@implementation LTPrimaryButton

/*! @brief Called when component is shows in interface builder.*/
- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
    [self initilizateUI];
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    [self initilizateUI];
}

- (void)initilizateUI {
    [super initilizateUI];
    [self setTitleColor:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryTextButton] forState:UIControlStateNormal];
    self.family = LTPrimaryFontFamily;
    self.weight = LTBoldFontWeight;
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorAltBackground5];
}

#pragma mark Property
/*! @brief Called when user update small height property value.
 *  @param isSmallHeight New value of property.*/
- (void)setIsSmallHeight:(BOOL)isSmallHeight {
    _isSmallHeight = isSmallHeight;
    self.size = isSmallHeight ? LTMediumCFontSize : LTMediumBFontSize;
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.bounds;
    frame.size.height = self.isSmallHeight ? 23.f : 35.f;
    self.bounds = frame;
    self.cornerRadius = self.bounds.size.height / 2.f;
    CGFloat offset = self.isSmallHeight ? 5 : 15;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, offset, 0, offset);
}


#pragma mark - setters
/*! @brief Called when user update enabled property value.
 *  @param enabled New value of property.*/
- (void)setEnabled:(BOOL)enabled {
    [super setEnabled: enabled];
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType: enabled ? LTColorPrimaryButton : LTColorInactivePrimaryButton];

    NSString* currentTitle = [self titleForState:self.state];
    NSAttributedString *titleAttribut = [self attributedTitleForState:self.state];
    if (titleAttribut != nil) {
        currentTitle = titleAttribut.string;
    }
    UIColor* color = [[LTTheme sharedTheme] colorWithType: enabled ? LTColorPrimaryTextButton : LTColorInactiveTextPrimaryButton];
    UIFont *font = [[LTTheme sharedTheme] fontWithFamily: self.family weight: self.weight style: self.style size: self.size];
    NSAttributedString* strAttribute = [[NSAttributedString alloc] initWithString: currentTitle attributes: @{ NSFontAttributeName : font,
                                                                                                               NSForegroundColorAttributeName : color}];
    [self setAttributedTitle: strAttribute forState: self.state];
}

- (void)highlight {    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:[self attributedTitleForState:UIControlStateNormal]];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange(0, [self.titleLabel.text length])];
    [self setAttributedTitle:attributedString forState:UIControlStateNormal];
}

- (void)unhighlight {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithAttributedString:[self attributedTitleForState:UIControlStateNormal]];
    [attributedString addAttribute:NSForegroundColorAttributeName value:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryTextButton] range:NSMakeRange(0, [self.titleLabel.text length])];
    [self setAttributedTitle:attributedString forState:UIControlStateNormal];
}

@end
