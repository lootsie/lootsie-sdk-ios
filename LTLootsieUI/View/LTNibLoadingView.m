//
//  LTNibLoadingView.m
//  LTLootsie iOS Example
//

//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTNibLoadingView.h"

@implementation LTNibLoadingView

@synthesize view;


- (instancetype)init
{
    self = [super init];
    if (self) {
        [self nibSetup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self nibSetup];
    }
    return self;
}

- (void) nibSetup
{
    [self setBackgroundColor: [UIColor clearColor]];
    UIView* localView = [self loadViewFromNib];
    localView.frame = [self bounds];
    localView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    localView.translatesAutoresizingMaskIntoConstraints = true;
    [self addSubview: localView];
    self.view = localView;

}

- ( UIView* ) loadViewFromNib
{
    NSBundle* bundle = [NSBundle bundleForClass: [self class]];
    UINib* nib = [UINib nibWithNibName:NSStringFromClass([self class]) bundle:bundle];
    UIView* nibView = [[nib instantiateWithOwner: self options: nil] objectAtIndex:0];

    return nibView;
}


@end
