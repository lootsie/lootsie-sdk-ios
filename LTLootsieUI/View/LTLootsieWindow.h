//
//  LTLootsieWindow.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 4/20/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTLootsieWindow
 *  @brief This window is used for displaying all LootsieUI controllers and pages if user hasn't implemented special protocol methods and returns special
 *  controller where SDK can display controller.
 *  @see LTUIManagerDelegate*/
@interface LTLootsieWindow : UIWindow

/*! @brief Create a window instance.*/
+ (instancetype)createWindow;

/*! @brief Return visible controller which now is show on screen.*/
- (UIViewController*)visibleViewController;

@end
