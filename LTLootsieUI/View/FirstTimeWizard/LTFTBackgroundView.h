//
//  LTFTBackgroundView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/6/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>


IB_DESIGNABLE
/*! @class LTFTBackgroundView
 *  @brief View for show background on the wizard step page.*/
@interface LTFTBackgroundView : UIView

/*! Image for background.*/
@property (nonatomic, weak)     IBOutlet UIImageView* backgroundImageView;

/*! Gradient view for show on the background.*/
@property (nonatomic, weak)     IBOutlet UIImageView* gradientBackground;

@end
