//
//  LTFTWelcomePage.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFTWelcomePage.h"
#import "LTSecondaryButton.h"
#import "LTTheme.h"
#import "LTFTBackgroundView.h"


@interface LTFTWelcomePage ()

/*! Container view for buttons.*/
@property (weak, nonatomic) IBOutlet UIView* buttonsView;

/*! Close button.*/
@property (weak, nonatomic) IBOutlet LTBaseButton *meybeLaterButton;

/*! Start watch wizard steps.*/
@property (weak, nonatomic) IBOutlet LTBaseButton *letsGoButton;

/*! Width constrain for close button.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* maybeLaterWidth;

/*! Width constrain for sing up button.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* letsGoWidth;

@end



@implementation LTFTWelcomePage

#pragma mark - Buttons actions
/*! @brief Called when user tap to maybe later button.
 *  @param sender Button object which was called action.*/
- (IBAction)onMaybeButtonClicked:(id)sender {
    if(self.onMaybeButtonClicked) {
        self.onMaybeButtonClicked();
    }
}

/*! @brief Called when user tap to let's go button.
 *  @param sender Button object which was called action.*/
- (IBAction)onLetsGoButtonClicked:(id)sender {
    if(self.onLetsGoButtonClicked) {
        self.onLetsGoButtonClicked();
    }
}


#pragma mark - Animation
/*! @brief Animation buttons view to up*/
- (void)animateButtonsViewToUp {
    self.buttonsView.hidden = NO;
    [self createAnimationViewToUp: self.buttonsView];
}

/*! @brief Animation buttons view for hide moving to left.*/
- (void)animationHideButtonsViewToLeft {
    CGFloat dx = self.titleLabel.frame.size.width / 2.0;
    [self createHideAnimationForView: self.buttonsView toNewPoint: CGPointMake(self.buttonsView.center.x + dx, self.buttonsView.center.y)];
}

- (void)animateAllViewsToUp {
    [super animateAllViewsToUp];
    
    [self performSelector: @selector(animateButtonsViewToUp) withObject: nil afterDelay: 0.3];
}

- (void)animateAllViewForHideToLeft {
    [super animateAllViewForHideToLeft];
    
    [self performSelector: @selector(animationHideButtonsViewToLeft) withObject: nil afterDelay: 0.3];
}


#pragma mark - Init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.maybeLaterWidth.constant = ([UIScreen mainScreen].bounds.size.height < 736.0) ? 124.0 : 150.0;
    self.letsGoWidth.constant = ([UIScreen mainScreen].bounds.size.height < 736.0) ? 124.0 : 150.0;
    
    self.buttonsView.hidden = YES;
    
    self.animatedContentView.clipsToBounds = YES;
    self.animatedContentView.layer.cornerRadius = 5.0;
    self.iconView.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorWelcomeIconColor];
}


#pragma mark - Setters

- (void)needToHideAllSubviews {
    [super needToHideAllSubviews];
    self.buttonsView.hidden = YES;
}
@end
