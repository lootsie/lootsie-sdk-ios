//
//  LTFTWizardView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/7/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTFTWizardView
 *  @brief Container for wizard pages display.*/
@interface LTFTWizardView : UIView

/*! Card offset.*/
@property (nonatomic, assign)   CGFloat cardOffset;

/*! Scale factor for subcard.*/
@property (nonatomic, assign)   CGFloat cardScale;

/*! Scale for current page.*/
@property (nonatomic, assign)   CGFloat scale;


#pragma mark - Update content
/*! @brief Update UI with new list of pages.
 *  @param views List of pages.*/
- (void)updateViews: (NSArray<UIView*>*)views;

/*! @brief Update current pages.*/
- (void)update;

@end
