//
//  LTFTCatalogPage.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFTCatalogPage.h"
#import "LTTheme.h"
#import "LTFTBackgroundView.h"

@interface LTFTCatalogPage ()

/*! Icon for show swipe.*/
@property (weak, nonatomic) IBOutlet UIImageView *swipeIcon;

@end


@implementation LTFTCatalogPage

#pragma mark - Animations
/*! @brief Animate page view for move to left.*/
- (void)animatePageLabelToLeft {
    self.pageNumberLabel.hidden = NO;
    [self createAnimationViewToLeft: self.pageNumberLabel];
}

/*! @brief Animate swipe view for move to left.*/
- (void)animateSwipeView {
    self.swipeIcon.hidden = NO;
    [self createAnimationViewToLeft: self.swipeIcon];
}

- (void)animateAllViewsToLeft {
    [super animateAllViewsToLeft];
    [self performSelector: @selector(animatePageLabelToLeft) withObject: nil afterDelay: 0.4];
    [self performSelector: @selector(animateSwipeView) withObject: nil afterDelay: 0.5];
}


#pragma mark - init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.animatedContentView.clipsToBounds = YES;
    self.animatedContentView.layer.cornerRadius = 5.0;

    self.pageNumberLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];
    
    self.swipeIcon.image = [self.swipeIcon.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    self.swipeIcon.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorAltButton1];
}


#pragma mark - Setters

- (void)needToHideAllSubviews {
    [super needToHideAllSubviews];
    self.pageNumberLabel.hidden = YES;
    self.swipeIcon.hidden = YES;
}
@end
