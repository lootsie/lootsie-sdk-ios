//
//  LTFTThatsItPage.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/6/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFTThatsItPage.h"
#import "LTTheme.h"
#import "LTFTBackgroundView.h"
#import "LTSecondaryButton.h"


@interface LTFTThatsItPage ()

/*! Close button for wizard.*/
@property (weak, nonatomic) IBOutlet LTBaseButton *meybeLaterButton;

/*! Register button. This button must open account page.*/
@property (weak, nonatomic) IBOutlet LTSecondaryButton *signUpNowButton;

/*! Width constrain for close button.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* maybeLaterWidth;

/*! Width constrain for sing up button.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* signUpNowWidth;

@end


@implementation LTFTThatsItPage

#pragma mark - Buttons actions
/*! @brief Generate attribute for main text of description.
 *  @return Attributes structure.*/
- (NSDictionary*)attributedMainTextDescription {
    LTFontSize fontSize = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? LTMediumAFontSize : LTMediumBFontSize;
    UIFont* font = [[LTTheme sharedTheme] fontWithFamily:LTPrimaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:fontSize];
    return @{NSFontAttributeName: font,
             NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText]};
}

/*! @brief Generate attribute for subtext of description.
 *  @return Attributes structure.*/
- (NSDictionary*)attributedSubtextDescription {
    LTFontSize fontSize = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? LTMediumBFontSize : LTMediumCFontSize;
    UIFont* font = [[LTTheme sharedTheme] fontWithFamily:LTPrimaryFontFamily weight:LTBoldFontWeight style:LTItalicFontStyle size:fontSize];
    return @{NSFontAttributeName: font,
             NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorAltText6]};
}

/*! @brief Called when user tap to maybe later button.
 *  @param sender Button structure.*/
- (IBAction)onMaybeLaterButtonClicked:(LTBaseButton*)sender {
    if(self.onMaybeLaterButtonClicked) {
        self.onMaybeLaterButtonClicked();
    }
}

/*! @brief Called when user tap to sign now button.
 *  @param sender Button structure.*/
- (IBAction)onSignUpNowButtonClicked:(LTSecondaryButton*)sender {
    if(self.onSignUpNowButtonClicked) {
        self.onSignUpNowButtonClicked();
    }
}


#pragma mark - Init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.maybeLaterWidth.constant = ([UIScreen mainScreen].bounds.size.height < 736.0) ? 124.0 : 150.0;
    self.signUpNowWidth.constant = ([UIScreen mainScreen].bounds.size.height < 736.0) ? 124.0 : 150.0;
    
    self.animatedContentView.clipsToBounds = YES;
    self.animatedContentView.layer.cornerRadius = 5.0;
    
    self.pageNumberLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];
    NSMutableAttributedString* text = [NSMutableAttributedString new];
    [text appendAttributedString: [[NSAttributedString alloc] initWithString: @"That wasn't so bad was it? Now you're ready to explore your new rewards catalog.\n\n"
                                                                  attributes: [self attributedMainTextDescription]]];
    [text appendAttributedString: [[NSAttributedString alloc] initWithString: @"FYI - We won't be able to track or bank your points to you account until you have one! So, just throw in your email and we'll handle the rest!"
                                                                  attributes: [self attributedSubtextDescription]]];
    self.descLabel.attributedText = text;
    self.meybeLaterButton.size = LTMediumCFontSize;
    self.signUpNowButton.size = LTMediumCFontSize;
}


@end
