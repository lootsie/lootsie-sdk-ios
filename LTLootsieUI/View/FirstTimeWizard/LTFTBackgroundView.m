//
//  LTFTBackgroundView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/6/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFTBackgroundView.h"
#import "LTTools.h"
#import "LTTheme.h"


@interface LTFTBackgroundView ()
/*! Container view for background.*/
@property (nonatomic, weak)     IBOutlet UIView* containerView;


/*! @brief Update UI component.*/
- (void)updateUI;

/*! @brief Load all UI component.*/
- (void)LoadUI;

@end


@implementation LTFTBackgroundView


#pragma mark - Private methods

- (void)updateUI {
    UIImage* image = [UIImage imageNamed: @"tileCongratsBackgroud"];
    image = [image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    image = [image resizableImageWithCapInsets: UIEdgeInsetsZero resizingMode: UIImageResizingModeTile];
    self.backgroundImageView.image = image;
    self.backgroundImageView.alpha = 0.2;
    self.containerView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    
    UIColor* backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorSecondaryBackground];
    self.gradientBackground.image = [LTTools gradientBgImageWithHeight: self.gradientBackground.bounds.size.height
                                                              topColor: [backgroundColor colorWithAlphaComponent: 0.2]
                                                           bottomColor: [backgroundColor colorWithAlphaComponent: 1]];
}

- (void)LoadUI {
    UIView* view = [[[NSBundle bundleForClass:[self class]] loadNibNamed: @"LTFTBackgroundView" owner: self options: nil] firstObject];
    view.frame = self.bounds;
    view.layer.cornerRadius = 5.0;
    view.clipsToBounds = YES;
    [self addSubview: view];
    [self sendSubviewToBack: view];

    [self updateUI];
}

#pragma mark - Init methods
/*! @brief Create a new object.
 *  @param aDecoder Decoder obejct.*/
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if(self) {
        [self LoadUI];
    }
    return self;
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    [self updateUI];
}


#pragma mark - Draw methods
/*! @brief Called when component is shows in interface builder.*/
- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
    [self LoadUI];
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];

    self.clipsToBounds = NO;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowPath = [UIBezierPath bezierPathWithRect:self.bounds].CGPath;
    self.layer.shadowOffset = CGSizeZero;
    self.layer.shadowRadius = 9.0;
    self.layer.shadowOpacity = 0.44;
}

@end
