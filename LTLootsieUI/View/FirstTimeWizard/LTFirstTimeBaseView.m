//
//  LTFirstTimeBaseView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFirstTimeBaseView.h"
#import "LTTheme.h"
#import "LTTools.h"
#import "LTFTBackgroundView.h"

@implementation LTFirstTimeBaseView


#pragma mark - Animations
/*! @brief Create animation for move view to point.
 *  @param view View object.
 *  @param startPoint Start point of view.*/
- (void)createAnimationForView:(UIView*)view withStartPoint:(CGPoint)startPoint {
    CASpringAnimation* anim = [CASpringAnimation animationWithKeyPath: @"position"];
    anim.fromValue = [NSValue valueWithCGPoint: startPoint];
    anim.toValue = [NSValue valueWithCGPoint: view.center];
    anim.duration = 1.0f;
    anim.damping = 25.0f;
    anim.stiffness = 300.f;
    anim.removedOnCompletion = YES;
    [view.layer addAnimation: anim forKey: @"showAnim"];
}

/*! @brief Create animation for return view to some position.
 *  @param view View object.
 *  @param point Start point of view.*/
- (void)returnToCorrectPositionWithAnimation:(UIView*)view toNewPoint:(CGPoint)point {
    CGPoint old = view.center;
    view.center = point;
    CASpringAnimation* anim = [CASpringAnimation animationWithKeyPath: @"position"];
    anim.fromValue = [NSValue valueWithCGPoint: old];
    anim.toValue = [NSValue valueWithCGPoint: view.center];
    anim.duration = 1.0f;
    anim.damping = 25.0f;
    anim.stiffness = 300.f;
    anim.removedOnCompletion = YES;
    [view.layer addAnimation: anim forKey: @"returnAnim"];
}

- (void)createHideAnimationForView:(UIView*)view toNewPoint:(CGPoint)newPoint {
    CGPoint oldPoint = view.center;
    view.center = newPoint;
    [CATransaction begin];
    CABasicAnimation* moveRightAnim = [CABasicAnimation animationWithKeyPath: @"position"];
    moveRightAnim.fromValue = [NSValue valueWithCGPoint: oldPoint];
    moveRightAnim.toValue = [NSValue valueWithCGPoint: view.center];
    moveRightAnim.removedOnCompletion = YES;
    moveRightAnim.duration = 0.3;
    [CATransaction setCompletionBlock:^{
        [self returnToCorrectPositionWithAnimation: view toNewPoint: oldPoint];
    }];
    [view.layer addAnimation: moveRightAnim forKey: @"moveRight"];
    [CATransaction commit];
}

- (void)createAnimationViewToUp:(UIView*)view {
    CGFloat yy = self.frame.size.height + view.bounds.size.height;
    [self createAnimationForView: view withStartPoint: CGPointMake(view.center.x, yy)];
}

- (void)createAnimationViewToLeft:(UIView*)view {
    CGFloat xx = self.frame.size.width + view.bounds.size.width / 2.0;
    [self createAnimationForView: view withStartPoint: CGPointMake(xx, view.center.y)];
}

- (void)createAnimationViewToRight:(UIView*)view {
    CGFloat xx = -view.bounds.size.width / 2.0;
    [self createAnimationForView: view withStartPoint: CGPointMake(xx, view.center.y)];
}


#pragma mark Title label animation
/*! @brief Animate title view for move to up.*/
- (void)animateTitleLabelToUp {
    self.titleLabel.hidden = NO;
    [self createAnimationViewToUp: self.titleLabel];
}

/*! @brief Animate title view for move to left.*/
- (void)animateTitleLabelToLeft {
    self.titleLabel.hidden = NO;
    [self createAnimationViewToLeft: self.titleLabel];
}

/*! @brief Animate title view for move to right.*/
- (void)animateTitleLabelToRight {
    self.titleLabel.hidden = NO;
    [self createAnimationViewToRight: self.titleLabel];
}

/*! @brief Animate title view for hide to left.*/
- (void)animateTitleForHideToLeft {
    CGFloat dx = self.titleLabel.frame.size.width / 2;
    [self createHideAnimationForView:self.titleLabel toNewPoint:CGPointMake(self.titleLabel.center.x + dx, self.titleLabel.center.y)];
}


#pragma mark Icon view animation
/*! @brief Animate icon view for move to up.*/
- (void)animateIconViewToUp {
    self.iconView.hidden = NO;
    [self createAnimationViewToUp: self.iconView];
}

/*! @brief Animate icon view for move to left.*/
- (void)animateIconViewToLeft {
    self.iconView.hidden = NO;
    [self createAnimationViewToLeft: self.iconView];
}

/*! @brief Animate icon view for move to right.*/
- (void)animateIconViewToRight {
    self.iconView.hidden = NO;
    [self createAnimationViewToRight: self.iconView];
}

/*! @brief Animate icon view for hide to left.*/
- (void)animateIconViewForHideToLeft {
    CGFloat dx = self.titleLabel.frame.size.width / 2;
    [self createHideAnimationForView:self.iconView toNewPoint:CGPointMake(self.iconView.center.x + dx, self.iconView.center.y)];
}


#pragma mark Description label animation

/*! @brief Animate description view for move to up.*/
- (void)animateDescToUp {
    self.descLabel.hidden = NO;
    [self createAnimationViewToUp: self.descLabel];
}

/*! @brief Animate description view for move to left.*/
- (void)animateDescToLeft {
    self.descLabel.hidden = NO;
    [self createAnimationViewToLeft: self.descLabel];
}

/*! @brief Animate description view for move to right.*/
- (void)animateDescToRight {
    self.descLabel.hidden = NO;
    [self createAnimationViewToRight: self.descLabel];
}

/*! @brief Animate description view for hide to left.*/
- (void)animateDescForHideToLeft {
    CGFloat dx = self.titleLabel.frame.size.width / 2;
    [self createHideAnimationForView:self.descLabel toNewPoint:CGPointMake(self.descLabel.center.x + dx, self.descLabel.center.y)];
}


#pragma mark All views animations

- (void)animateAllViewsToUp {
    self.titleLabel.hidden = YES;
    self.iconView.hidden = YES;
    self.descLabel.hidden = YES;

    [self performSelector:@selector(animateTitleLabelToUp) withObject:nil afterDelay:0.0];
    [self performSelector:@selector(animateIconViewToUp) withObject:nil afterDelay:0.05];
    [self performSelector:@selector(animateDescToUp) withObject:nil afterDelay:0.1];
}

- (void)animateAllViewsToLeft {
    self.titleLabel.hidden = YES;
    self.iconView.hidden = YES;
    self.descLabel.hidden = YES;

    [self performSelector:@selector(animateTitleLabelToLeft) withObject:nil afterDelay:0.0];
    [self performSelector:@selector(animateIconViewToLeft) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(animateDescToLeft) withObject:nil afterDelay:0.2];
}

- (void)animateAllViewsToRight {
    self.titleLabel.hidden = YES;
    self.iconView.hidden = YES;
    self.descLabel.hidden = YES;

    [self performSelector:@selector(animateTitleLabelToRight) withObject:nil afterDelay:0.0];
    [self performSelector:@selector(animateIconViewToRight) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(animateDescToRight) withObject:nil afterDelay:0.2];
}

- (void)animateAllViewForHideToLeft {
    [self performSelector:@selector(animateTitleForHideToLeft) withObject:nil afterDelay:0.0];
    [self performSelector:@selector(animateIconViewForHideToLeft) withObject:nil afterDelay:0.1];
    [self performSelector:@selector(animateDescForHideToLeft) withObject:nil afterDelay:0.2];
}

- (void)animateShowFromDownWithSubviewsAnimation:(BOOL)flag {
    self.hidden = NO;
    self.titleLabel.hidden = flag;
    self.iconView.hidden = flag;
    self.descLabel.hidden = flag;
    
    CGPoint startPoint = CGPointMake(self.center.x,
                                     [UIScreen mainScreen].bounds.size.height + self.frame.size.height / 2.0);
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        if(flag) {
            [self performSelector:@selector(animateAllViewsToUp) withObject:nil afterDelay:0.2];
        }
    }];
    [self createAnimationForView: self withStartPoint: startPoint];
    [CATransaction commit];
}

/*! @brief Animate current view for hide to left.*/
- (void)animateToLeft {
    CGPoint oldPos = self.center;
    self.center = CGPointMake(-(self.frame.size.width), self.center.y);
    
    [CATransaction begin];
    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath: @"position"];
    anim.fromValue = [NSValue valueWithCGPoint: oldPos];
    anim.toValue = [NSValue valueWithCGPoint: self.center];
    anim.duration = 0.6;
    anim.removedOnCompletion = YES;
    [CATransaction setCompletionBlock:^{
        self.hidden = YES;
        [self removeFromSuperview];
        if(self.onHideAnimationFinished) {
            self.onHideAnimationFinished();
        }
    }];
    [self.layer addAnimation: anim forKey: @"hideAnimation"];
    [CATransaction commit];
}

- (void)animateHideToLeft {
    [self animateAllViewForHideToLeft];
    [self performSelector: @selector(animateToLeft) withObject: nil afterDelay: 0.3];
}


#pragma mark - UIViewController methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    LTFontSize fontSize = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? LTMediumAFontSize : LTMediumBFontSize;
    
    self.titleLabel.font = [[LTTheme sharedTheme] fontWithFamily:LTPrimaryFontFamily weight:LTBlackFontWeight style:LTRegularFontStyle size:LTLargeFontSize];
    self.descLabel.font = [[LTTheme sharedTheme] fontWithFamily:LTPrimaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:fontSize];
    
    [self.titleLabel setTextColor:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryText]];
    [self.descLabel setTextColor:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryText]];
    
    self.iconView.image = [self.iconView.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    self.iconView.tintColor = [[LTTheme sharedTheme] colorWithType:LTColorFirstTimeWizardIconColor];
    
    self.animatedContentView.clipsToBounds = YES;
    self.animatedContentView.layer.cornerRadius = 5.0;
    
    self.pageNumberLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];
}


#pragma mark - setters

- (void)needToHideAllSubviews {
    self.titleLabel.hidden = YES;
    self.iconView.hidden = YES;
    self.descLabel.hidden = YES;
}

@end
