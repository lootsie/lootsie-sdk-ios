//
//  LTFTCatalogPage.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFirstTimeBaseView.h"

/*! @class LTFTCatalogPage
 *  @brief Page for show information about catalog.*/
@interface LTFTCatalogPage : LTFirstTimeBaseView

@end
