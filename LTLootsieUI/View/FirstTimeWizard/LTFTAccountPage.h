//
//  LTFTAccountPage.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/6/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFirstTimeBaseView.h"

/*! @class LTFTAccountPage
 *  @brief Page for show information about account.*/
@interface LTFTAccountPage : LTFirstTimeBaseView

@end
