//
//  LTFTFavoritesPage.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/6/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFirstTimeBaseView.h"

/*! @class LTFTFavoritesPage
 *  @brief Page for show information about favorites rewards.*/
@interface LTFTFavoritesPage : LTFirstTimeBaseView

@end
