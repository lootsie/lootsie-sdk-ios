//
//  LTFTWizardView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/7/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFTWizardView.h"

#import "LTFTWelcomePage.h"
#import "LTFTCatalogPage.h"
#import "LTFTAchievementsPage.h"
#import "LTFTFavoritesPage.h"
#import "LTFTAccountPage.h"
#import "LTFTThatsItPage.h"


@interface LTFTWizardView ()

/*! List of pages for show in wizard.*/
@property (nonatomic, strong)   NSArray<UIView*> *cardViews;

/*! Point of touch. Update when user is draging card.*/
@property (nonatomic, assign)   CGFloat touchOffset;

/*! YES if user start action with card.*/
@property (nonatomic, assign)   BOOL interacting;

/*! YES if user try to mode first or last card.*/
@property (nonatomic, assign)   BOOL bouncing;

/*! Index of page.*/
@property (nonatomic, assign)   NSUInteger index;

/*! Gesture for change page.*/
@property (nonatomic, strong)   UIPanGestureRecognizer *panGesture;


/*! @brief Logic for noninteraction state.*/
- (void)noInterectionLogic;

/*! @brief User start interacte with wizard.*/
- (void)userInteractWithSomeView;

/*! @brief Show view with index and offset.
 *  @param indexView View which need to show.
 *  @param offset Offset of animation.*/
- (void)showViewWithIndex:(NSInteger)indexView offsetX:(CGFloat)offset;

/*! @brief Move view when user interect with page.
 *  @param active Current view which user try to move.
 *  @param prev Previous view.
 *  @param sign Direction sign for move.*/
- (void)moveView:(UIView*)active belowView:(UIView*)prev sign:(NSInteger)sign;

/*! @brief Update all pages which was loaded.*/
- (void)updateCards;

/*! @brief Hide all views.*/
- (void)hideIrrelevantViews;

/*! @brief Activate view.
 *  @param view View which mark as active.*/
- (void)activeCardLayout: (UIView* _Nonnull)view;

/*! @brief Deactivate view.
 *  @param view View which mark as inactive.*/
- (void)nextCardLayout: (UIView* _Nonnull)view;

/*! @brief Gesture action which calle every time whem user swipe from let ot right.*/
- (void)onPan:(UIPanGestureRecognizer*)gesture;

@end


@implementation LTFTWizardView

#pragma mark - Private methods

- (void)noInterectionLogic {
    NSUInteger numberOfCards = self.cardViews.count;
    self.touchOffset = 0.0f;
    if (self.index > 0) {
        [self showViewWithIndex: self.index - 1 offsetX: -self.cardOffset];
    }
    if (self.index + 1 < numberOfCards) {
        [self showViewWithIndex: self.index + 1 offsetX: self.cardOffset];
    }
    if (self.index < numberOfCards) {
        [self activeCardLayout: self.cardViews[self.index]];
    }
}

- (void)userInteractWithSomeView {
    NSUInteger numberOfCards = self.cardViews.count;
    // bounce right
    if (self.index == 0 && numberOfCards > 0 && self.touchOffset > 0.0f) {
        self.bouncing = YES;
        [self activeCardLayout: self.cardViews[0]];
        if (numberOfCards > 1) {
            [self nextCardLayout: self.cardViews[1]];
        }
    } else {
        // bounce left
        if (self.index == numberOfCards - 1 && numberOfCards > 0 && self.touchOffset < 0.0f) {
            self.bouncing = YES;
            [self activeCardLayout: self.cardViews[numberOfCards - 1]];
            if (numberOfCards > 1) {
                [self showViewWithIndex: numberOfCards - 2 offsetX: -35.0];
            }
        } else {
            self.bouncing = NO;
            UIView* active = self.cardViews[self.index];
            UIView* next = (self.index + 1 < numberOfCards)?self.cardViews[self.index + 1]:nil;
            UIView* prev = (self.index > 0)?self.cardViews[self.index - 1]:nil;
            [self bringSubviewToFront: active];

            // moving card left
            if (self.touchOffset > 0.0f) {
                if (self.touchOffset > self.frame.size.width / 3.0f && next.superview) {
                    next.hidden = YES;
                }
                [self sendSubviewToBack: prev];
                [self sendSubviewToBack: next];
                next.transform = CGAffineTransformMakeScale(self.cardScale, self.cardScale);
                prev.transform = CGAffineTransformMakeScale(self.cardScale, self.cardScale);
                // phase one
                [self moveView: active belowView: prev sign: 1];
            } else { // moving card left
                if (fabsf((float)self.touchOffset) > self.frame.size.width / 3.0f && prev.superview) {
                    prev.hidden = YES;
                }
                [self sendSubviewToBack: next];
                [self sendSubviewToBack: prev];
                next.transform = CGAffineTransformMakeScale(self.cardScale, self.cardScale);
                prev.transform = CGAffineTransformMakeScale(self.cardScale, self.cardScale);
                // phase one
                [self moveView:active belowView: next sign: -1];
            }
        }
    }
}

- (void)showViewWithIndex:(NSInteger)indexView offsetX:(CGFloat)offset {
    UIView* view = self.cardViews[indexView];
    view.hidden = NO;
    CGAffineTransform transform = CGAffineTransformMakeScale(self.cardScale, self.cardScale);
    transform.tx = offset;
    view.transform = transform;
    
    [view.superview sendSubviewToBack: view];
}

- (void)moveView:(UIView*)active belowView:(UIView*)prev sign:(NSInteger)sign {
    CGFloat halfWidth = self.frame.size.width / 2.0f;
    
    CGAffineTransform belowViewTransform = CGAffineTransformIdentity;
    CGAffineTransform activeViewTransform = CGAffineTransformIdentity;
    if (fabs(self.touchOffset) < halfWidth) {
        activeViewTransform.tx =  self.touchOffset;
        
        float scale = self.scale * fabs(self.touchOffset) / halfWidth;
        
        belowViewTransform = CGAffineTransformMakeScale(self.cardScale + scale, self.cardScale + scale);
        belowViewTransform.tx =  - (self.cardOffset * sign) - self.touchOffset;
    } else { // phase two
        NSInteger offsetSign = (self.touchOffset > 0) ? 1 : -1;
        CGFloat scale = self.scale * (1 - (fabs(self.touchOffset) - halfWidth) / halfWidth);
        CGFloat offset = (fabs(self.touchOffset) - (2 * halfWidth));
        
        activeViewTransform = CGAffineTransformMakeScale(self.cardScale + scale, self.cardScale + scale);
        activeViewTransform.tx =  - (offset * offsetSign);
        
        belowViewTransform.tx = - (self.cardOffset * sign) + (offset * offsetSign);
        [prev.superview bringSubviewToFront: prev];
    }
    prev.hidden = NO;
    prev.transform = belowViewTransform;
    active.hidden = NO;
    active.transform = activeViewTransform;
}

- (void)updateCards {
    if (!self.interacting) {
        [self noInterectionLogic];
    } else {
        [self userInteractWithSomeView];
    }
    [self hideIrrelevantViews];
}

- (void)hideIrrelevantViews {
    NSInteger numberOfViews = [self.cardViews count];
    for(NSInteger i = self.index - 2; i >= 0; --i) {
        UIView* view = self.cardViews[i];
        view.hidden = YES;
        view.transform = CGAffineTransformIdentity;
    }
    for(NSInteger i = self.index + 2; i < numberOfViews; ++i) {
        UIView* view = self.cardViews[i];
        view.hidden = YES;
        view.transform = CGAffineTransformIdentity;
    }
}

- (void)activeCardLayout: (UIView* _Nonnull)view {
    view.transform = CGAffineTransformIdentity;
    view.center = CGPointMake(self.frame.size.width / 2.0f + (self.bouncing?(self.touchOffset / 10.0f):self.touchOffset*20.0), self.frame.size.height / 2.0f);
    view.hidden = NO;
}

- (void)nextCardLayout: (UIView* _Nonnull)view {
    view.center = CGPointMake(self.frame.size.width / 2.0f + self.cardOffset + (self.bouncing?(self.touchOffset / 5.0f):self.touchOffset*20.0), self.frame.size.height / 2.0f);
    view.transform = CGAffineTransformMakeScale(self.cardScale, self.cardScale);
    [self insertSubview: view belowSubview: self.cardViews[self.index]];
}

#pragma mark - Actions

- (void)onPan:(UIPanGestureRecognizer*)gesture {
    static NSUInteger initX = 0;
    switch(gesture.state){
        case UIGestureRecognizerStateBegan: {
            initX = [gesture locationInView: gesture.view].x;
            self.interacting = YES;
            self.touchOffset = 0.0;
            break;
        }
        case UIGestureRecognizerStateChanged: {
            self.touchOffset = [gesture locationInView: gesture.view].x - initX;
            [self updateCards];
            break;
        }
        case UIGestureRecognizerStateEnded:
        case UIGestureRecognizerStateCancelled:
        case UIGestureRecognizerStateFailed: {
            NSInteger switchIndex = 0;
            CGFloat dx = self.bounds.size.width / 3.0;
            if(fabs(self.touchOffset) > dx) {
                if(self.touchOffset > 0) {
                    switchIndex = -1;
                } else if(self.touchOffset < 0) {
                    switchIndex = 1;
                }
            }
            self.interacting = NO;
            [UIView animateWithDuration: 0.4f delay: 0.0f usingSpringWithDamping: 0.7f initialSpringVelocity: 1.0f
                                options: (UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction)
                             animations: ^{
                                 NSInteger newValue = self.index + switchIndex;
                                 if (newValue < 0) {
                                     newValue = 0;
                                 }
                                 if (newValue >= [self.cardViews count]) {
                                     newValue = [self.cardViews count] - 1;
                                 }
                                 self.index = newValue;
                                 [self updateCards];
                             }
                             completion: nil];
            break;
        }
        default:
            break;
    }
}


#pragma mark - Draw methods
/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    [self update];
}


#pragma mark - init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.clipsToBounds = NO;
    self.cardScale = 0.9;
    self.scale = 1.0 - self.cardScale;
    self.cardOffset = 30.0;
    self.panGesture = [[UIPanGestureRecognizer alloc] initWithTarget: self action: @selector(onPan:)];
    [self addGestureRecognizer: self.panGesture];
}


#pragma mark - Update content

- (void)updateViews: (NSArray<UIView*>*)views {
    self.cardViews = views;
    self.interacting = NO;
    for(UIView* view in self.cardViews) {
        [view removeFromSuperview];
    }
    self.index = 0;
    self.touchOffset = 0;
    
    NSUInteger pageNumber = [self.cardViews count];
    
    for(LTFirstTimeBaseView* view in [self.cardViews reverseObjectEnumerator]) {
        view.frame = self.bounds;
        view.clipsToBounds = NO;
        view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        view.pageNumberLabel.text = [NSString stringWithFormat:@"%lu/%lu", (unsigned long)pageNumber, (unsigned long)[self.cardViews count]];
        
        pageNumber -= 1;
        
        [self addSubview: view];
    }
    [self.panGesture setEnabled: YES];
    [self updateCards];
}

- (void)update {
    for (UIView *view in self.cardViews) {
        view.transform = CGAffineTransformIdentity;
        view.frame = self.bounds;
    }
    [self updateCards];
}

@end
