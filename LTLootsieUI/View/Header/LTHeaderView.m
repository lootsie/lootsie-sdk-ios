 //
//  LTHeaderView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/11/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTHeaderView.h"


@interface LTHeaderView ()
/*! Divider view. Shows in the bottom of navigation bar.*/
@property (nonatomic, strong)   UIView* divView;


/*! @brief Create special derived view.*/
- (void)createDivView;

@end


@implementation LTHeaderView


#pragma mark - Private methods

- (void)createDivView {
    if(!self.divView) {
        self.divView = [UIView new];
        self.divView.translatesAutoresizingMaskIntoConstraints = NO;
        self.divView.backgroundColor = [UIColor blackColor];
        [self addSubview: self.divView];
        
        [self addConstraints: [NSLayoutConstraint constraintsWithVisualFormat: @"H:|-0-[_divView]-0-|"
                                                                      options: 0
                                                                      metrics: nil
                                                                        views: NSDictionaryOfVariableBindings(_divView)]];
        [self addConstraints: [NSLayoutConstraint constraintsWithVisualFormat: @"V:[_divView]-0-|"
                                                                      options: 0
                                                                      metrics: nil
                                                                        views: NSDictionaryOfVariableBindings(_divView)]];
        [self addConstraint: [NSLayoutConstraint constraintWithItem: self.divView
                                                          attribute: NSLayoutAttributeHeight
                                                          relatedBy: NSLayoutRelationEqual
                                                             toItem: nil
                                                          attribute: NSLayoutAttributeNotAnAttribute
                                                         multiplier: 1.0
                                                           constant: (1.0 / [UIScreen mainScreen].scale)]];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(processCloseRewardCard:)
                                                     name:@"toggleBackButton"
                                                   object:nil];
    }
}

- (void)processCloseRewardCard:(NSNotification *) notification {
    
    if ([[notification name] isEqualToString:@"toggleBackButton"])
        NSLog (@"Successfully received toggleBackButton notification!");

    [self.infoBar toggleCloseButton];
}

#pragma mark - Init methods
/*! @brief Create a new view with frame.
 *  @param frame Frame of view*/
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if(self) {
        [self createDivView];
    }
    return self;
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib]; // FIX
    [self createDivView];
}


#pragma mark - Clear memory
/*! @brief Called when object was released.*/
- (void)dealloc {
    [self.divView removeFromSuperview];
    self.divView = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"toggleBackButton" object:nil];
    
}

@end
