//
//  LTHeaderInfoBar.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/11/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTHeaderInfoBar.h"
#import "LTTheme.h"
#import "LTTools.h"

#import "LTManager.h"


@interface LTHeaderInfoBar ()

/*! Close button.*/
@property (nonatomic, weak)     IBOutlet UIButton* btnClose;

/*! Label of title.*/
@property (nonatomic, weak)     IBOutlet UILabel* appNameLabel;

/*! View for show user's avatar.*/
@property (nonatomic, weak)     IBOutlet UIImageView* userAvatarImageView;

/*! View for show background view for points label.*/
@property (nonatomic, weak)     IBOutlet UIView* pointsView;

/*! Label for show user's points.*/
@property (nonatomic, weak)     IBOutlet UILabel* pointsLabel;
@property (nonatomic, weak)     IBOutlet UIView* statusBarBackgroundView;


/*! @brief Update points label with new value.
 *  @param points New value of points.*/
- (void)updatePointsLabelWithPoints: (NSInteger)points;

/*! @brief Init defaul configuration for view.*/
- (void)initDefault;

@end

@implementation LTHeaderInfoBar

#pragma mark - Private methods

- (void)updatePointsLabelWithPoints: (NSInteger)points {
    self.pointsLabel.attributedText = [LTTools pointsStringValue: points];
}
- (void)initDefault {
    self.appNameLabel.font = [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                              weight: LTRegularFontWeight
                                                               style: LTRegularFontStyle
                                                                size: LTMediumAFontSize];
    self.appNameLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    self.statusBarBackgroundView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
}

#pragma mark - Buttons' Actions
/*! @brief Called when user tap to close button.
 *  @param sender Button object which called action.*/
- (void)buttonCloseClicked:(UIButton*)sender {
    if(self.onCloseClicked) {
        self.onCloseClicked();
    }
}

- (void)toggleCloseButton {
//    [self.btnClose setHidden:YES];
    
    [self.btnClose removeTarget:nil
                         action:nil
               forControlEvents:UIControlEventTouchUpInside];
    

    if ([LTManager sharedManager].isShowingReward) {
        
        [self.btnClose addTarget: self action: @selector(buttonCloseClicked:) forControlEvents: UIControlEventTouchUpInside];
        
        UIImage *image = [UIImage imageNamed:@"header-lt-close-icon"];
        
        [self.btnClose setImage:image forState:UIControlStateNormal];
        
        [LTManager sharedManager].isShowingReward = false;
//        self.isShowingReward = false;
    } else {
        [self.btnClose addTarget: self action: @selector(buttonCloseClicked:) forControlEvents: UIControlEventTouchUpInside];
        
        UIImage *image = [UIImage imageNamed:@"lt-close-icon"];
        
        [self.btnClose setImage:image forState:UIControlStateNormal];
//        self.isShowingReward = true;
        [LTManager sharedManager].isShowingReward = true;
    }
//    [LTManager sharedManager].isShowingReward = ![LTManager sharedManager].isShowingReward;
}

#pragma mark - Init methods
/*! @brief Create a new view with frame.
 *  @param frame Frame of view*/
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame: frame];
    if(self) {
        [self initDefault];
    }
    return self;
}

/*! @brief Create a new view.
 *  @param aDecoder Decoder object.*/
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder: aDecoder];
    if(self) {
        [self initDefault];
    }
    
    return self;
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib]; // FIX
    [self initDefault];
    
    [self.btnClose addTarget: self action: @selector(buttonCloseClicked:) forControlEvents: UIControlEventTouchUpInside];
    UIImage *image = [self.btnClose.currentImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.btnClose setImage:image forState:UIControlStateNormal];
    self.btnClose.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorTertiaryBackground];
    self.userAvatarImageView.layer.cornerRadius = self.userAvatarImageView.frame.size.height / 2.0;
    self.userAvatarImageView.clipsToBounds = YES;
    self.pointsLabel.hidden = NO;
}


#pragma mark - setters
/*! @brief Called when user update title of page.
 *  @param newAppName New value of property.*/
- (void)setAppName:(NSString *)newAppName {
    _appName = newAppName;
    self.appNameLabel.text = self.appName;
}

/*! @brief Called when user avartar was updated.
 *  @param newUserAvatar New value of property.*/
- (void)setUserAvatar:(UIImage *)newUserAvatar {
    _userAvatar = newUserAvatar;
    self.userAvatarImageView.image = self.userAvatar;
}

/*! @brief Called when user's points updated.
 *  @param newPoints New value of property.*/
- (void)setPoints:(NSInteger)newPoints {
    _points = newPoints;
    [self updatePointsLabelWithPoints: self.points];
}

#pragma mark - Clear memory
/*! @brief Called when object was released.*/
- (void)dealloc {
    self.onCloseClicked = nil;
}
@end
