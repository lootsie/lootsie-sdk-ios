//
//  LTHeaderView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/11/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTHeaderInfoBar.h"
#import "LTHeaderNavigationBar.h"


IB_DESIGNABLE
/*! @class LTHeaderView
 *  @brief View for show heade bar in Lootsie UI controller.*/
@interface LTHeaderView : UIView

/*! Background view for header.*/
@property (nonatomic, strong)   IBInspectable UIColor* backgroundColor;

/*! Information bar. Will show close button, title of selected page and user's points.*/
@property (nonatomic, weak)     IBOutlet LTHeaderInfoBar* infoBar;

/*! Navigation bar. This view shows list of pages which user can select.*/
@property (nonatomic, weak)     IBOutlet LTHeaderNavigationBar* navigationBar;

@end
