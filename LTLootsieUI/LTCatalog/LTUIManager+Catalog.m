//  LTUIManager+Catalog.m
//  Created by Fabio Teles on 3/31/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTUIManager+Catalog.h"
#import "LTManager.h"
#import "LTManager+Catalog.h"

#import "LTRewardsPageController.h"
#import "LTFavoritesPageController.h"
#import "LTRewardDetailViewController.h"
#import "LTFirstTimeFavoriteController.h"


NSString * LTFirstTimeFavoriteKey           = @"LTFirstTimeFavoriteKey";

@interface LTUIManager ()

/*! Core manager instance.*/
@property (nonatomic, weak, readonly) LTManager *coreManager;

/*! @brief Shows the Achievements page of Lootsie's UI by opening it if not being displayed
 *  or by switching to this page is already being displayed.*/
- (void)showPageWithSegueIdentifier:(Class)classController;

/*! @brief Show controller of parent controller with start point of animation.
 *  @param controller Controller for show.
 *  @param parentController Parent controller when will be shown first time wizard.
 *  @param startPoint Point of start animation.*/
- (void)showController:(UIViewController*)controller onController:(UIViewController*)parentController withStartPoint:(CGPoint)startPoint;

@end


@implementation LTUIManager (Catalog)

- (void)showRewardsPage {
    [self showPageWithSegueIdentifier:[LTRewardsPageController class]];
}

- (void)showFavoritePage {
    [self showPageWithSegueIdentifier:[LTFavoritesPageController class]];
}

- (void)showDetailRewardPage:(LTCatalogReward *)reward onViewController:(UIViewController *)parentController fromStartPoint:(CGPoint)startPoint onClose:(void(^)())block {
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        LTRewardDetailViewController *controller = [[LTRewardDetailViewController alloc] initWithNibName:@"LTRewardDetailViewController" bundle:nil];
        controller.reward = reward;
        controller.onClose = block;
        
        [self showController:controller onController:parentController withStartPoint:startPoint];
    }];
    [self->_mainQueue addOperation:operation];
}

- (void)showFirtsTimeFavoriteOnViewController:(UIViewController *)parentController fromStartPoint:(CGPoint)startPoint {
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *isDidShow = [defaults objectForKey:LTFirstTimeFavoriteKey];
        if (isDidShow == nil || ![isDidShow boolValue]) {
            LTFirstTimeFavoriteController *controller = [[LTFirstTimeFavoriteController alloc] initWithNibName:@"LTFirstTimeFavoriteController" bundle:nil];
            [self showController:controller onController:parentController withStartPoint:startPoint];
        }
        [defaults setObject:@(YES) forKey:LTFirstTimeFavoriteKey];
    }];
    [self->_mainQueue addOperation:operation];
}

@end
