//
//  LTFavoriteSectionHeaderView.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/24/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTFavoriteSectionHeaderView
 *  @brief Favorite list section header view.*/
@interface LTFavoriteSectionHeaderView : UICollectionReusableView

/*! Background view.*/
@property (nonatomic, weak) IBOutlet UIView* backgroundView;

/*! Section title label.*/
@property (nonatomic, weak) IBOutlet UILabel* titleLabel;

/*! Favorites items count.*/
@property (nonatomic, assign)   NSInteger itemsCount;

@end
