//
//  LTCatalogRewardCell.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/17/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTCatalogRewardCell.h"
#import "LTManager+Catalog.h"

#import "LTTheme.h"


@interface LTCatalogRewardCell ()
/*! Content reward view*/
@property (weak, nonatomic)     IBOutlet UIView *contentRewardView;

/*! White background view.*/
@property (weak, nonatomic)     IBOutlet UIView *whiteBackground;

/*! Refresh video icon*/
@property (weak, nonatomic)     IBOutlet UIImageView* refreshIconImage;

/*! Marker backgroud view. This view using for show marker text for reward.*/
@property (weak, nonatomic)     IBOutlet UIView* markerView;

/*! Title marker label view.*/
@property (weak, nonatomic)     IBOutlet UILabel* titleMarkerLabel;


/*! Favorite button horizontal constrain. Using for animation.*/
@property (weak, nonatomic)     IBOutlet NSLayoutConstraint *favoriteButtonHorizontalConstrait;

/*! Marker view center constrain.*/
@property (weak, nonatomic)     IBOutlet NSLayoutConstraint* markerViewCenter;

/*! Marker view top offset constrain.*/
@property (weak, nonatomic)     IBOutlet NSLayoutConstraint* markerViewTopOffset;

@end


@implementation LTCatalogRewardCell


/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
    self.whiteBackground.hidden = YES;
    
    self.refreshIconImage.hidden = YES;
    self.markerView.layer.cornerRadius = self.markerView.frame.size.height / 2.0f;
    self.markerViewCenter.constant = 0.0f;
    
    self.contentRewardView.clipsToBounds = YES;
    self.contentRewardView.layer.cornerRadius = 4.0;
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongRecongnizer:)];
    longPress.minimumPressDuration = 0.4f;
    [self addGestureRecognizer:longPress];
}

- (void)setReward:(LTCatalogReward *)reward {
    [super setReward:reward];
    if (reward != nil) {
        self.titleMarkerLabel.text = reward.markerString;
        if(self.titleMarkerLabel.text.length > 0) {
            self.markerView.hidden = NO;
        } else {
            self.markerView.hidden = YES;
        }
        if(reward.state == LTNewRewardState) {
            self.markerView.hidden = NO;
            self.titleMarkerLabel.text = NSLocalizedStringFromTable(@"New!", @"LTLootsie", nil);
            self.titleMarkerLabel.textColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText];
            self.markerView.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground2];
        } else {
            self.titleMarkerLabel.textColor = [[LTTheme sharedTheme] colorWithType: LTColorTertiaryText];
            self.markerView.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorSecondaryBackground];
        }

        self.favoriteButton.hidden = !reward.isFavorite;
//        self.refreshIconImage.hidden = !reward.refreshEnabled;
        self.refreshIconImage.hidden = true;
        
        [self layoutIfNeeded];
    }
    self.contentRewardView.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorTertiaryBackground];
}

/*! @brief Prepare for reuse cell.*/
- (void)prepareForReuse {
    [super prepareForReuse];
    self.contentRewardView.transform = CGAffineTransformIdentity;
    self.contentRewardView.alpha = 1;
    self.favoriteButtonHorizontalConstrait.constant = [self showConstraintConstant];
    self.favoriteButton.alpha = 1;
}

/*! @brief Called when user use long tap gesture.
 *  @param sender Tap gesture object.*/
- (void)onLongRecongnizer:(UITapGestureRecognizer*)sender {
    if ([self.delegate respondsToSelector:@selector(isCanFavoriteCell:)]) {
        if (![self.delegate isCanFavoriteCell:self]) {
            return;
        }
    }
    if (sender.state == UIGestureRecognizerStateBegan) {
        [UIView animateWithDuration:0.3f
                              delay:0.1f
             usingSpringWithDamping:0.5f
              initialSpringVelocity:10
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             CGFloat scale = 0.9f;
                             self.whiteBackground.transform = self.contentRewardView.transform = CGAffineTransformMakeScale(scale, scale);
                         }
                         completion:nil];
    }
    else if (sender.state == UIGestureRecognizerStateEnded ||
             sender.state == UIGestureRecognizerStateFailed ||
             sender.state ==UIGestureRecognizerStateCancelled) {
        [self updateFavoriteState];
    }
}

/*! @brief Update favorite state.*/
- (void)updateFavoriteState {
    if ([self.delegate respondsToSelector:@selector(rewardCellDidFavoriteClick:)]) {
        [self.delegate rewardCellDidFavoriteClick:self];
    }
    self.favoriteButton.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground2];
    
    if (self.reward.isFavorite) {
        [self showFavoriteAnimation];
    }
    else {
        [self hideFavoriteAnimation];
    }
}

/*! @brief Base aniamtion for content view of cell.*/
- (void)baseAnimation {
    //self.window.layer.speed = 0.2f;
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.fromValue = @(1);
    animation.toValue = @(0.5);
    animation.autoreverses = YES;
    animation.duration = 0.3f;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    animation.removedOnCompletion = YES;
    [self.contentRewardView.layer addAnimation:animation forKey:@"alpha"];
    
    [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
        self.whiteBackground.transform = self.contentRewardView.transform = CGAffineTransformIdentity;
    } completion:nil];
}

/*! @brief Play animation for hide favorite button.*/
- (void)hideFavoriteAnimation {
    [self baseAnimation];
    self.favoriteButtonHorizontalConstrait.constant = [self showConstraintConstant];
    [self.favoriteButton layoutIfNeeded];
    [UIView animateWithDuration:0.3f
                          delay:0.0f
         usingSpringWithDamping:0.5f
          initialSpringVelocity:10
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         self.favoriteButtonHorizontalConstrait.constant = [self hideConstraintConstant];
                         [self.favoriteButton layoutIfNeeded];
                         self.favoriteButton.alpha = 0.5f;
                     }
                     completion:^(BOOL finished) {
                         self.favoriteButton.hidden = YES;
                     }];
}

/*! @brief Play aniamtion for show favorite button.*/
- (void)showFavoriteAnimation {
    [self baseAnimation];
    self.favoriteButton.hidden = NO;
    self.favoriteButton.alpha = 0;
    self.favoriteButtonHorizontalConstrait.constant = [self hideConstraintConstant];
    [self.favoriteButton layoutIfNeeded];
    [UIView animateWithDuration:0.4f
                          delay:0.1f
         usingSpringWithDamping:0.5f
          initialSpringVelocity:10
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.favoriteButton.alpha = 1;
                         self.favoriteButtonHorizontalConstrait.constant = [self showConstraintConstant];
                         [self.favoriteButton layoutIfNeeded];
                     }
                     completion:nil];
}

- (CGFloat)showConstraintConstant {
    return 0;
}

- (CGFloat)hideConstraintConstant {
    return  -self.bounds.size.width;
}


#pragma mark - Draw methods 
/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat centerOffset = ((self.frame.size.width / 2.0f) - (self.markerView.frame.size.width / 2.0f) - 11.0f);
    self.markerViewCenter.constant = (self.reward.refreshEnabled) ? centerOffset : 0.0f;
    self.markerViewTopOffset.constant = (self.reward.refreshEnabled) ? 17.0 : 10.0;
}

@end
