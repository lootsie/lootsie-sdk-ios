//
//  LTRewardsSectionHeaderView.m
//  LTLootsie iOS Example
//
//  Created by Macmini on 8/18/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTRewardsSectionHeaderView.h"
#import "LTTheme.h"

@implementation LTRewardsSectionHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.divinderView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryDiv];
}

@end
