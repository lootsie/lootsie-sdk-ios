//
//  LTRewardImageCell.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/20/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTRewardImageCell.h"
#import "UIImageView+LTAFNetworking.h"



@interface LTRewardImageCell ()
/*! Image view for show.*/
@property (nonatomic, weak)     IBOutlet UIImageView* rewardImage;

@end


@implementation LTRewardImageCell

/*! @brief Setter for reward image url property.
 *  @param newRewardImageUrl New value for property.*/
- (void)setRewardImageUrl:(NSString *)newRewardImageUrl {
    _rewardImageUrl = newRewardImageUrl;
    
    if(_rewardImageUrl.length > 0) {
        [self.rewardImage setImageWithURL: [NSURL URLWithString: _rewardImageUrl]];
    }
}

@end
