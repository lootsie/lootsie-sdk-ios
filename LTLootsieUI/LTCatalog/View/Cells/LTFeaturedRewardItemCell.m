//
//  LTFeaturedRewardItemCell.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/18/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFeaturedRewardItemCell.h"
#import "UIImageView+LTAFNetworking.h"
#import "LTPointButton.h"
#import "LTManager+Catalog.h"

#import "LTTheme.h"


@interface LTFeaturedRewardItemCell ()
/*! Reward image view.*/
@property (nonatomic, weak)     IBOutlet UIImageView* rewardImage;

/*! Reward brand image view.*/
@property (nonatomic, weak)     IBOutlet UIImageView* brandImage;
@property (nonatomic, weak)     IBOutlet UIView *divideView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *brandImageMinWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *brandImageMaxWidthConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *brandImageLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *divideViewLeadingConstraint;

@end


@implementation LTFeaturedRewardItemCell

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    self.divideView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorTertiaryDiv];
    [self clearUI];
}

/*! @brief Prepare for reuse cell.*/
- (void)prepareForReuse {
    [super prepareForReuse];
    [self clearUI];
}

/*! @brief Clear all UI components.*/
- (void)clearUI {
    self.rewardImage.image = nil;
    self.brandImage.image = nil;
}

/*! @brief Setter for reward property.
 *  @param newReward New value for property.*/
- (void)setReward:(LTCatalogReward *)newReward {
    [super setReward: newReward];
    
    if(self.reward) {
        if (self.reward.banners != nil) {
            [self.rewardImage setImageWithURL: [self.reward.banners firstObject]];
        }

        if(self.reward.brandImage != nil) {
            [self.brandImage setImageWithURL: self.reward.brandImage];
            self.brandImageMinWidthConstraint.constant = 50.0;
            self.brandImageMaxWidthConstraint.constant = 100.0;
            self.brandImageLeadingConstraint.constant = 14.0;
            self.divideViewLeadingConstraint.constant = 14.0;

        } else {
            self.brandImageMinWidthConstraint.constant = 0.0;
            self.brandImageMaxWidthConstraint.constant = 0.0;
            self.brandImageLeadingConstraint.constant = 0.0;
            self.divideViewLeadingConstraint.constant = 0.0;
        }


        self.brandImage.hidden = self.reward.brandImage == nil;
        self.divideView.hidden = self.reward.brandImage == nil;


        [self layoutIfNeeded];
    }
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
}

- (CGFloat)showConstraintConstant {
    return 68;
}

- (CGFloat)hideConstraintConstant {
    return -60.f;
}

@end
