//
//  LTFeaturedRewardItemCell.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/18/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTBaseCatalogRewardCell.h"
#import "LTCatalogRewardCell.h"

/*! @class LTFeaturedRewardItemCell
 *  @brief Featured reward cell. Using for show rewrad info.*/
@interface LTFeaturedRewardItemCell : LTCatalogRewardCell

@end
