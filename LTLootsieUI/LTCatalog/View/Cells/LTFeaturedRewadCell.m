//
//  LTFeaturedRewadCell.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/18/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFeaturedRewadCell.h"
#import "LTFeaturedRewardItemCell.h"
#import "LTFeaturedCatalogLayout.h"

@interface LTFeaturedRewadCell () <UICollectionViewDelegate, UICollectionViewDataSource, LTCatalogRewardCellDelegate>
/*! List of featured rewards.*/
@property (nonatomic, weak)     IBOutlet UICollectionView* rewardsCollectionView;

/*! Point of start drag component. Using for animation.*/
@property (nonatomic, assign) CGFloat startDragOffset;

/*! Index path for first item.*/
@property (nonatomic, assign) NSIndexPath *firstIndex;

@end


/*! Identifier for cell to show featured reward UI.*/
static NSString* const LTFeaturedRewadItemCellIdentifier        =   @"featuredRewadItemCell";


@implementation LTFeaturedRewadCell


#pragma mark - LTCatalogRewardCellDelegate

- (BOOL)isCanFavoriteCell:(UICollectionViewCell *)cell {
    CGRect corectFrame = cell.frame;
    corectFrame.origin.x += 1;
    BOOL isCanFavoriteParent = YES;
    if ([self.delegate respondsToSelector:@selector(isCanFavoriteCell:)]) {
        isCanFavoriteParent = [self.delegate isCanFavoriteCell:self];
    }
    return CGRectContainsRect(self.rewardsCollectionView.bounds, corectFrame) && isCanFavoriteParent;
}

- (void)rewardCell:(LTBaseCatalogRewardCell*)cell didSelect:(LTCatalogReward*)reward {
    if ([self.delegate respondsToSelector:@selector(rewardCell:didSelect:)]) {
        [self.delegate rewardCell:cell didSelect:reward];
    }
}

- (void)rewardCellDidFavoriteClick:(LTBaseCatalogRewardCell*)cell {
    if ([self.delegate respondsToSelector:@selector(rewardCellDidFavoriteClick:)]) {
        [self.delegate rewardCellDidFavoriteClick:cell];
    }
}

- (void)rewardCellDidDoubleTap:(LTBaseCatalogRewardCell *)cell {
    if ([self.delegate respondsToSelector:@selector(rewardCellDidDoubleTap:)]) {
        [self.delegate rewardCellDidDoubleTap:cell];
    }
}


#pragma mark - init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.rewardsCollectionView registerNib: [UINib nibWithNibName: @"LTFeaturedRewardItemCell" bundle: nil]
                 forCellWithReuseIdentifier: LTFeaturedRewadItemCellIdentifier];
}


#pragma mark - UICollectionViewDelegate
/*! @brief Get number of section for collection view
 *  @param collectionView Collection view object.
 *  @return Number of section.*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

/*! @brief Get number of items for show in section.
 *  @param collectionView Collection view object.
 *  @param section Index of collection view section.
 *  @return Number of items.*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.rewards.count;
}

/*! @brief Get size of item for index path.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param indexPath Index path for item.
 *  @return Size of item*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize newSize = self.rewardsCollectionView.bounds.size;
    CGFloat minimumLineSpacing = ((UICollectionViewFlowLayout*)collectionViewLayout).minimumLineSpacing;
    newSize.width -= minimumLineSpacing;
    return newSize;
}

/*! @brief Get cell for collection view with index path.
 *  @param collectionView Collection view object.
 *  @param indexPath Index path for item.
 *  @return Cell object.*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LTFeaturedRewardItemCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier: LTFeaturedRewadItemCellIdentifier forIndexPath: indexPath];
    cell.reward = [self.rewards objectAtIndex: indexPath.item];
    cell.index = indexPath.item;
    cell.delegate = self;
    return cell;
}

/*! @brief Called when user start dragging view.
 *  @param scrollView Scroll view object.*/
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.startDragOffset = scrollView.contentOffset.x;
}


#pragma mark - setters
/*! @brief Setter for rewards property.
 *  @param newRewards New value for property.*/
- (void)setRewards:(NSArray *)newRewards {
    _rewards = newRewards;
    [self.rewardsCollectionView reloadData];
}


#pragma mark - draw methods
/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    [self.rewardsCollectionView.collectionViewLayout invalidateLayout];
}

@end
