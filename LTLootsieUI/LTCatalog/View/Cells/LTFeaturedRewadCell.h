//
//  LTFeaturedRewadCell.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/18/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTBaseCatalogRewardCell.h"

/*! @class LTFeaturedRewadCell
 *  @brief Featured container cell.*/
@interface LTFeaturedRewadCell : UICollectionViewCell

/*! List of rewards.*/
@property (nonatomic, strong)   NSArray* rewards;

/*! Reward additional methods.*/
@property (nonatomic, weak)     id<LTCatalogRewardCellDelegate> delegate;

@end
