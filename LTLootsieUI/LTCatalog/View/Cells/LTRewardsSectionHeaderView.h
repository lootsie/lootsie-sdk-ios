//
//  LTRewardsSectionHeaderView.h
//  LTLootsie iOS Example
//
//  Created by Macmini on 8/18/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LTRewardsSectionHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UIView *divinderView;

@end
