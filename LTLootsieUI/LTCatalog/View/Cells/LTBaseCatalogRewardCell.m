//  LTBaseCatalogRewardCell.m
//  Created by Alexander Dovbnya on 6/14/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTBaseCatalogRewardCell.h"
#import "LTPointButton.h"
#import "LTPointButton.h"
#import "UIImageView+LTAFNetworking.h"

@interface LTBaseCatalogRewardCell ()
/*! Image of reward.*/
@property (nonatomic, weak)     IBOutlet UIImageView* rewardImage;

/*! Title of reward.*/
@property (nonatomic, weak)     IBOutlet UILabel* titleLabel;

@end


@implementation LTBaseCatalogRewardCell

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.rewardImage.clipsToBounds = YES;
    self.layer.cornerRadius = 4.0;
    
    UITapGestureRecognizer *oneTap = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(onTap:)];
    oneTap.numberOfTapsRequired = 1;
    [self addGestureRecognizer: oneTap];
    
    UITapGestureRecognizer* doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onDoubleTap:)];
    doubleTap.numberOfTapsRequired = 2;
    [self addGestureRecognizer: doubleTap];
    
    [oneTap requireGestureRecognizerToFail: doubleTap];

    UIImage* img = [self.favoriteButton imageForState: UIControlStateSelected];
    if(img) {
        [self.favoriteButton setImage: [img imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate]
                             forState: UIControlStateSelected];
        [self.favoriteButton setImage: [img imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate] forState:UIControlStateNormal];
    }
    self.favoriteButton.userInteractionEnabled = NO;
    self.rewardImage.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorTertiaryBackground];
}

/*! @brief Setter for reward property.
 *  @param newReward New value for property.*/
- (void)setReward:(LTCatalogReward *)newReward {
    _reward = newReward;
    
    if(_reward) {
        if (self.reward.thumbnails != nil) {
            [self.rewardImage setImageWithURL: [self.reward.thumbnails firstObject]];
        }
        
        self.titleLabel.text = self.reward.name;
        self.titleLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryText];
        self.favoriteButton.selected = _reward.isFavorite;
        if(self.favoriteButton.selected) {
            self.favoriteButton.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground2];
        } else {
            self.favoriteButton.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorTertiaryBackground];
        }
    }
}

/*! @brief Called when user tap to current view.
 *  @param sender Tap gesture object.*/
- (void)onTap:(UITapGestureRecognizer*)sender {
    if([self.delegate respondsToSelector: @selector(rewardCell:didSelect:)]) {
        [self.delegate rewardCell: self didSelect: self.reward];
    }
}

/*! @brief Called when user use double tap to current view.
 *  @param sender Tap gesture object.*/
- (void)onDoubleTap:(UITapGestureRecognizer*)sender {
    if([self.delegate respondsToSelector: @selector(rewardCellDidDoubleTap:)]) {
        [self.delegate rewardCellDidDoubleTap: self];
    }
}

@end
