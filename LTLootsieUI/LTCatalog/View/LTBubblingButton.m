//
//  LTBubblingButton.m
//  LTLootsie iOS Example
//
//  Created by Ankur Vashi on 11/30/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

#import "LTSpinningLabel.h"

#import "LTTools.h"

#if !__has_feature(objc_arc)
#error UICountingLabel is ARC only. Either turn on ARC for the project or use -fobjc-arc flag
#endif

#pragma mark - UILabelCounter

#ifndef kUILabelCounterRate
#define kUILabelCounterRate 3.0
#endif

#pragma mark - LTSpinningLabel

#import "LTBubblingButton.h"
#import "LTManager.h"
#import "LTTools.h"


@interface LTBubblingButton ()

/*! Vertical scroll view to pull-to-refresh and show details*/
@property (strong, nonatomic) NSTimer* timer;

@property (assign, nonatomic) BOOL start;

@end

@implementation LTBubblingButton

- (void)initilizateUI {
    [super initilizateUI];
    self.family = LTPrimaryFontFamily;
    self.weight = LTBoldFontWeight;
    self.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.minimumScaleFactor = 0.2f;
}

/*! @brief Called when user update small height property value.
 *  @param isSmallHeight New value of property.*/
- (void)setIsSmallHeight:(BOOL)isSmallHeight {
    _isSmallHeight = isSmallHeight;
    self.size = isSmallHeight ? LTMediumBFontSize : LTMediumAFontSize;
}

/*! @brief Called when user update points property value.
 *  @param newPoints New value of property.*/
- (void)setPoints:(NSInteger)newPoints {
    _points = newPoints;
    
    UIFont *font = [[LTTheme sharedTheme] fontWithFamily: self.family weight: self.weight style: self.style size: self.size];
    UIFont *fontSmall = [[LTTheme sharedTheme] fontWithFamily: self.family weight: self.weight style: self.style size: self.size + 1];
    NSAttributedString* strAttribute = [LTTools pointsStringValue:newPoints withPointFont:font currencyFont:fontSmall];
    [self setAttributedTitle: strAttribute forState: UIControlStateNormal];
}

/*! @brief Called when all subviews was updated.*/
- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect frame = self.bounds;
    frame.size.height = self.isSmallHeight ? 24.f : 33.f;
    self.bounds = frame;
    self.cornerRadius = self.bounds.size.height / 2.f;
    CGFloat offset = self.isSmallHeight ? 15 : 25;
    self.contentEdgeInsets = UIEdgeInsetsMake(0, offset, 0, offset);
}



- (void) animateBubbles {
    
    [self setClipsToBounds:YES];
    
    if(!self.start) {
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:.025
                                                      target:self
                                                    selector:@selector(createBubble)
                                                    userInfo:nil
                                                     repeats:YES];
        
        self.start = true;
        
        
        
    } else {
        
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.25];
        [self.titleLabel setAlpha:1.0];
        [UIView commitAnimations];
        
        
        [self.timer invalidate];
        self.timer = nil;
        
        self.start = false;
    }
    
    
}

- (void)createBubble {
    
    CGFloat size = [self randomFloatBetween:3 and:16];
    
    CGFloat positionX = [self randomFloatBetween:0 and: self.frame.size.width];

    UIView *circleView =  [[UIView alloc] initWithFrame:CGRectMake(positionX, self.frame.size.height, size, size)];
    circleView.alpha = 1.0;
    circleView.layer.cornerRadius = size/2;
    circleView.backgroundColor = [UIColor colorWithRed:((float)((0xaaf1c3 & 0xFF0000) >> 16))/255.0 \
                                                 green:((float)((0xaaf1c3 & 0x00FF00) >>  8))/255.0 \
                                                  blue:((float)((0xaaf1c3 & 0x0000FF) >>  0))/255.0 \
                                                 alpha:1.0];
    
    [self addSubview:circleView];
    
    UIBezierPath *zigzagPath = [[UIBezierPath alloc] init];
    CGFloat oX = circleView.frame.origin.x;
    CGFloat oY = circleView.frame.origin.y;
    CGFloat eX = oX;
    CGFloat eY = 0;
    CGFloat t = [self randomFloatBetween:-25 and:25];
    CGPoint cp1 = CGPointMake(oX - t, ((oY + eY) / 2));
    CGPoint cp2 = CGPointMake(oX + t, cp1.y);
    
    // randomly switch up the control points so that the bubble
    // swings right or left at random
    NSInteger r = arc4random() % 2;
    if (r == 1) {
        CGPoint temp = cp1;
        cp1 = cp2;
        cp2 = temp;
    }
    
    // the moveToPoint method sets the starting point of the line
    [zigzagPath moveToPoint:CGPointMake(oX, oY)];
    // add the end point and the control points
    [zigzagPath addCurveToPoint:CGPointMake(eX, eY) controlPoint1:cp1 controlPoint2:cp2];
    
    [CATransaction begin];
    [CATransaction setCompletionBlock:^{
        [UIView transitionWithView:circleView
                          duration:0.1f
                           options:UIViewAnimationOptionTransitionNone
                        animations:^{
                            circleView.transform = CGAffineTransformMakeScale(0, 0);
                        } completion:^(BOOL finished) {
                            [circleView removeFromSuperview];
                        }];
        
        
        if(self.start == YES) {
            [UIView animateWithDuration:0.25
                                  delay:0.0
                                options: UIViewAnimationOptionTransitionNone
                             animations:^{ [self.titleLabel setAlpha:0.0];}
                             completion:^(BOOL finished) {}];
        }
        
    }];
    
    CAKeyframeAnimation *pathAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    pathAnimation.duration = 2;
    pathAnimation.path = zigzagPath.CGPath;
    // remains visible in it's final state when animation is finished
    // in conjunction with removedOnCompletion
    pathAnimation.fillMode = kCAFillModeForwards;
    pathAnimation.removedOnCompletion = NO;
    
    [circleView.layer addAnimation:pathAnimation forKey:@"movingAnimation"];
    
    [CATransaction commit];
    
}

- (float)randomFloatBetween:(float)smallNumber and:(float)bigNumber {
    float diff = bigNumber - smallNumber;
    return (((float) (arc4random() % ((unsigned)RAND_MAX + 1)) / RAND_MAX) * diff) + smallNumber;
}
@end
