//
//  LTFavoriteSectionHeaderView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/24/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFavoriteSectionHeaderView.h"

#import "LTTheme.h"


@implementation LTFavoriteSectionHeaderView


#pragma mark - init
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib]; // FIX
    self.backgroundView.backgroundColor = [UIColor clearColor];
    self.backgroundColor = [UIColor clearColor];
}


#pragma mark - setters
/*! @brief Setter for items count property
 *  @param newItemsCount New value of property.*/
- (void)setItemsCount:(NSInteger)newItemsCount {
    _itemsCount = newItemsCount;
    
    NSDictionary* myFavoritesAttr = @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                                                            weight: LTBoldFontWeight
                                                                                             style: LTRegularFontStyle
                                                                                              size: LTMediumBFontSize],
                                      NSForegroundColorAttributeName:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryText]};
    
    NSDictionary* countRewardsAttr = @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                                                             weight: LTMediumFontWeight
                                                                                              style: LTRegularFontStyle
                                                                                               size: LTMediumBFontSize],
                                       NSForegroundColorAttributeName:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryText]};
    
    NSMutableAttributedString* text = [[NSMutableAttributedString alloc] init];
    [text appendAttributedString: [[NSAttributedString alloc] initWithString: NSLocalizedStringFromTable(@"My Favorites", @"LTLootsie", nil)
                                                                  attributes: myFavoritesAttr]];
    [text appendAttributedString: [[NSAttributedString alloc] initWithString: [NSString stringWithFormat: @"  %ld rewards", (long)_itemsCount]
                                                                  attributes: countRewardsAttr]];
    self.titleLabel.attributedText = text;
}

@end
