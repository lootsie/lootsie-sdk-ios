//
//  LTFeaturedSectionHeaderView.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/18/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFeaturedSectionHeaderView.h"
#import "LTTheme.h"


@implementation LTFeaturedSectionHeaderView

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    NSDictionary* myFavoritesAttr = @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                                                            weight: LTBoldFontWeight
                                                                                             style: LTRegularFontStyle
                                                                                              size: LTMediumBFontSize],
                                      NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText]};
   self.titleLabel.attributedText = [[NSAttributedString alloc] initWithString: self.titleLabel.text attributes: myFavoritesAttr];
}

@end
