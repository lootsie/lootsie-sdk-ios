//
//  LTRedemptionConfirmEmail.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTRedemptionConfirmEmail.h"
#import "LTTheme.h"


@interface LTRedemptionConfirmEmail ()
/*! Title of view.*/
@property (nonatomic, weak)     IBOutlet UILabel* titleLabel;

/*! Subtile of view.*/
@property (nonatomic, weak)     IBOutlet UILabel* subtitleLabel;

/*! Description label.*/
@property (nonatomic, weak)     IBOutlet UILabel* descLabel;

/*! Subdescription label.*/
@property (nonatomic, weak)     IBOutlet UILabel* subdescLabel;


/*! Description top offset constrain.*/
@property (nonatomic, weak)     IBOutlet NSLayoutConstraint* descTopOffset;

/*! Description bottom offset constrain.*/
@property (nonatomic, weak)     IBOutlet NSLayoutConstraint* descBottomOffset;


/*! @brief Generate attributes by font and color.
 *  @param size Font size.
 *  @param color Text color.*/
- (NSDictionary*)attributesWithFontSize:(CGFloat)size color:(UIColor*)color;

@end


@implementation LTRedemptionConfirmEmail

#pragma mark - private methods

- (NSDictionary*)attributesWithFontSize:(CGFloat)size color:(UIColor*)color {
    return @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily:LTPrimaryFontFamily weight:LTMediumFontWeight style:LTItalicFontStyle ptSize: size],
             NSForegroundColorAttributeName: color};
}


#pragma mark - init methods
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    CGFloat subtitleSize = 13.5;
    CGFloat descSize = 13.5;
    CGFloat subdecSize = 10.0;
    if([UIScreen mainScreen].bounds.size.height == 667.0) {
        subtitleSize = 16.0;
        descSize = 16.0;
        subdecSize = 12.0;
    } else if([UIScreen mainScreen].bounds.size.height == 736.0) {
        subtitleSize = 18.0;
        descSize = 18.0;
        subdecSize = 13.0;
    }

    UIColor* subtitleColor = [[LTTheme sharedTheme] colorWithType: LTColorAltText6];
    
    self.subtitleLabel.attributedText = [[NSAttributedString alloc] initWithString: self.subtitleLabel.text
                                                                        attributes: [self attributesWithFontSize:descSize color: subtitleColor]];
    self.descLabel.attributedText = [[NSAttributedString alloc] initWithString: self.descLabel.text
                                                                    attributes: [self attributesWithFontSize:descSize color:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryText]]];
    self.subdescLabel.attributedText = [[NSAttributedString alloc] initWithString: self.subdescLabel.text
                                                                       attributes: [self attributesWithFontSize:subdecSize color:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryText]]];
    
    self.descTopOffset.constant = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? 10.0 : -2.0;
    self.descBottomOffset.constant = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? 10.0 : 5.0;
}

@end
