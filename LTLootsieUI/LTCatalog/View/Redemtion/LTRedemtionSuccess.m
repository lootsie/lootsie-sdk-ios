//  LTRedemtionSuccess.m
//  Created by Dovbnya Alexander on 8/6/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTRedemtionSuccess.h"

#import "LTManager.h"
#import "LTData.h"

#import "LTTheme.h"

@interface LTRedemtionSuccess ()

/*! Points label.*/
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;

/*! Description subtitle label.*/
@property (weak, nonatomic) IBOutlet UILabel *descriptionSubTitle;

/*! Description title label.*/
@property (weak, nonatomic) IBOutlet UILabel *descriptionTitle;


/*! Points top offset constrain.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* pointsYOffset;

/*! Devider top offset constrain.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* deviderYOffset;

/*! Text view top offset constrain.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint* textViewYOffset;
@property (weak, nonatomic) IBOutlet UIImageView *triangleImageView;

@end


@implementation LTRedemtionSuccess

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.pointsYOffset.constant = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? 28.0 : 12.0;
    self.deviderYOffset.constant = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? 24.0 : 10.0;
    self.textViewYOffset.constant = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? 36.0 : 12.0;
    
    LTFontSize pointFontSize = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? LTLargeFontSize : LTMediumBFontSize;
    
    LTTheme *theamManager = [LTTheme sharedTheme];
    
    self.pointsLabel.font = [theamManager fontWithFamily:LTSecondaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:pointFontSize];
    self.descriptionSubTitle.font = [theamManager fontWithFamily:LTPrimaryFontFamily weight:LTRegularFontWeight  style:LTRegularFontStyle size:LTMediumCFontSize];
    self.descriptionTitle.font = [theamManager fontWithFamily:LTPrimaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:LTMediumCFontSize];
    self.descriptionSubTitle.text = [self.descriptionSubTitle.text uppercaseString];
    
    self.pointsLabel.textColor = [theamManager colorWithType:LTColorTertiaryText];
    self.descriptionTitle.textColor = [theamManager colorWithType:LTColorPrimaryText];
    self.descriptionSubTitle.textColor = [theamManager colorWithType:LTColorPrimaryText];
    self.triangleImageView.tintColor = [theamManager colorWithType:LTColorTertiaryBackground];
    self.triangleImageView.image = [self.triangleImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.backgroundColor = [theamManager colorWithType:LTColorSecondaryButton];
}

/*! @brief Setter for point property.
 *  @param point New value of property.*/
- (void)setPoint:(NSInteger)point {
    _point = point;
    NSString *currencyAbbreviation = @"Pts used";
    self.pointsLabel.text = [NSString stringWithFormat:@"%@:   %ld", currencyAbbreviation, (long)point];
}

@end
