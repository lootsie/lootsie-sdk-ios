//  LTRedemtionView.m
//  Created by Dovbnya Alexander on 8/6/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTRedemtionView.h"
#import "LTPrimaryButton.h"
#import "LTRedemtionSuccess.h"
#import "LTRedemptionNoAccount.h"
#import "LTRedemptionConfirmEmail.h"
#import "LTRedeemingLoader.h"
#import "LTRedemptionSuccessTopView.h"

#import "LTUIManager.h"
#import "LTManager+Catalog.h"
#import "LTManager+Account.h"

#import "LTRedemptionNoAccount.h"
#import "LTTools.h"

@interface LTRedemtionView ()
/*! Action button. Show on a few pages.*/
@property (weak, nonatomic) IBOutlet LTPrimaryButton *actionButton;

/*! Content view for state.*/
@property (weak, nonatomic) IBOutlet UIView *contentViewForState;

/*! Image view for show gradient overlay.*/
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

/*! User's email string.*/
@property (nonatomic, strong)   NSString* emailString;
@property (weak, nonatomic) IBOutlet UIView *footerContentView;

@end


@implementation LTRedemtionView

/*! @brief Called when user remove finger from play button.
 *  @param sender Button object.*/
- (IBAction)onButtonClick:(UIButton *)sender {
    if (self.state == LTRedemtionViewNonRegistered) {
        if(self.emailString.length > 0) {
            if([LTTools validateEmailWithString: self.emailString]) {
                self.state = LTRedemtionViewConfirmEmail;
                __weak typeof(self) weakSelf = self;
                NSString *email = self.emailString;
                LTManager *manager = [LTManager sharedManager];
                [manager userAccountWithSuccess:^(LTUserAccount *userAccount) {
                    userAccount.email = email;
                    [manager updateUserAccountWithAccount:userAccount success:^{
                        weakSelf.state = LTRedemtionViewConfirmEmail;
                    } failure:^(NSError *error) {
                        weakSelf.state = LTRedemtionViewError;
                    }];
                } failure:^(NSError *error) {
                    weakSelf.state = LTRedemtionViewError;
                }];
            } else {
                [[LTUIManager sharedManager] showPopupWithTitle: NSLocalizedStringFromTable(@"Uh oh!", @"LTLootsie", nil)
                                                    description: NSLocalizedStringFromTable(@"Invalid email address.", @"LTLootsie", nil)
                                                    withSadFace: YES
                                                     buttonText: NSLocalizedStringFromTable(@"OK", @"LTLootsie", nil)
                                                 didCloseAction: nil];
            }
        }
    }
    else if(self.state == LTRedemtionViewConfirmEmail) {
        [[LTManager sharedManager] synchronizeUserAccount:nil failure:nil];
        [self removeFromSuperview];
    }
    else if (self.state == LTRedemtionViewSuccess || self.state == LTRedemtionViewError) {
        if (self.didFinish != nil) {
            self.didFinish();
        }
        [self removeFromSuperview];
    }
}

/*! @brief Setter for state property.
 *  @param state New value for property.*/
- (void)setState:(LTRedemtionViewState)state {
    if (_state != state) {
        UIView *view = [self viewFromState:state];
        [self addView:view toSuperview:self.contentViewForState];
        
        self.actionButton.hidden = state == LTRedemtionViewLoading;
        if (state == LTRedemtionViewLoading && _state == LTRedemtionViewUnknown) {
            LTManager *manager = [LTManager sharedManager];
            
            [manager redeemRewardWithId:self.reward.rewardId
                                   cost: self.reward.points
                                success:^{
                                    
                                    [manager synchronizeUserAccount:^(LTUserAccount *userAccount) {
                                        double delayInSeconds = 2.0;
                                        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                                        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                                            //this will be executed after 2 seconds
                                            self.state = LTRedemtionViewSuccess;
                                        });
                                        
                                        
                                    } failure:^(NSError *error) {
                                        self.state = LTRedemtionViewError;
                                    }];
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                } failure:^(NSError *error) {
                                    self.state = LTRedemtionViewError;
                                }];
        }
        
        if (self.didChangeState != nil) {
            self.didChangeState(state);
        }
        _state = state;
    }
}

/*! @brief Get view object with state.
 *  @param viewState State value for search.
 *  @return View object.*/
- (UIView *)viewFromState:(LTRedemtionViewState)viewState {
    UIView *view;
    NSBundle *bundle = [NSBundle bundleForClass:[LTUIManager class]];
    switch (viewState) {
        case LTRedemtionViewNonRegistered: {
            self.actionButton.backgroundColor = [[LTTheme sharedTheme] colorWithType: (self.actionButton.enabled) ? LTColorAltBackground5 : LTColorAltBackground1];
            [self.actionButton setTitle: NSLocalizedStringFromTable(@"CONFIRM", @"LTLootsie", nil)
                               forState: UIControlStateNormal];
            [self.actionButton setTitle: NSLocalizedStringFromTable(@"SEND CONFIRMATION", @"LTLootsie", nil)
                               forState: UIControlStateDisabled];
            view = [[[UINib nibWithNibName:@"LTRedemptionNoAccount" bundle:bundle] instantiateWithOwner:self options:nil] firstObject];
            __weak typeof(self) tmp = self;
            tmp.actionButton.enabled = NO;
            for(UIView* v in view.subviews) {
                if([v isKindOfClass:[LTRedemptionNoAccount class]]) {
                    ((LTRedemptionNoAccount*)v).onTextChanged = ^(NSString* email) {
                        tmp.actionButton.enabled = (email.length > 0);
                        tmp.emailString = email;
                    };
                    ((LTRedemptionNoAccount*)v).onStartEditing = ^(BOOL flag) {
                        tmp.actionButton.enabled = flag;
                    };
                    break;
                }
            }
            break;
        }
        case LTRedemtionViewConfirmEmail: {
            view = [[[UINib nibWithNibName:@"LTRedemptionConfirmEmail" bundle:bundle] instantiateWithOwner:self options:nil] firstObject];
            self.actionButton.enabled = YES;
            self.actionButton.backgroundColor = [[LTTheme sharedTheme] colorWithType: (self.actionButton.enabled) ? LTColorAltBackground5 : LTColorAltBackground1];
            [self.actionButton setTitle: NSLocalizedStringFromTable(@"CLOSE", @"LTLootsie", nil)
                               forState: UIControlStateNormal];
            [self.actionButton setTitle: NSLocalizedStringFromTable(@"CLOSE", @"LTLootsie", nil)
                               forState: UIControlStateDisabled];
            break;
        }
        case LTRedemtionViewLoading: {
            view = [[[UINib nibWithNibName:@"LTRedeemingLoader" bundle:bundle] instantiateWithOwner:self options:nil] firstObject];
            break;
        }
        case LTRedemtionViewSuccess: {
            self.footerContentView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryButton];
            
            [self.actionButton setTitle: NSLocalizedStringFromTable(@"OK, GOT IT", @"LTLootsie", nil)
                               forState: UIControlStateNormal];
            [self.actionButton setTitle: NSLocalizedStringFromTable(@"OK, GOT IT", @"LTLootsie", nil)
                               forState: UIControlStateDisabled];
            LTRedemtionSuccess *successView = [[[UINib nibWithNibName:@"LTRedemtionSuccess" bundle:bundle] instantiateWithOwner:self options:nil] firstObject];
            successView.point = self.reward.points;
            LTRedemptionSuccessTopView *topView = [[[UINib nibWithNibName:@"LTRedemptionSuccessTopView" bundle:bundle] instantiateWithOwner:self options:nil] firstObject];
            topView.rewardName = self.reward.name;
            [self addView:topView toSuperview:self.imageView];
            view = successView;
            break;
        }
        case LTRedemtionViewError: {
            // view = [[[UINib nibWithNibName:@"LTRedemtionSuccess" bundle:bundle] instantiateWithOwner:self options:nil] firstObject];
            [self removeFromSuperview];
            break;
        }
        default:
            view = nil;
    }
    [self.contentViewForState.subviews  makeObjectsPerformSelector:@selector(removeFromSuperview)];
    return view;
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib]; // FIX
    [self.actionButton setTitleColor: [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText]
                            forState: UIControlStateNormal];
    [self.actionButton setTitleColor: [[LTTheme sharedTheme] colorWithType: LTColorAltButton1]
                            forState: UIControlStateDisabled];
    self.footerContentView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    // keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChangeFrame:) name:UIKeyboardWillChangeFrameNotification object:nil];
}

/*! @brief Add new to another view.
 *  @param view Source view which need to add to superview.
 *  @param superview Target view*/
- (void)addView:(UIView *)view toSuperview:(UIView *)superview {
    view.frame = superview.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [superview addSubview:view];
}


#pragma mark - keyboard notifications
/*! @brief Called when keyboard will show.
 *  @param notificaion Object with new data*/
- (void)keyboardWillShow:(NSNotification *)notificaion {
    [self updateOffsetForKeyboardWithUserInfo:notificaion.userInfo];
}

/*! @brief Called when keyboard will hide.
 *  @param notificaion Object with new data*/
- (void)keyboardWillHide:(NSNotification *)notificaion {
    self.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
}

/*! @brief Called when keyboard will change size.
 *  @param notificaion Object with new data*/
- (void)keyboardWillChangeFrame:(NSNotification *)notificaion {
    [self updateOffsetForKeyboardWithUserInfo:notificaion.userInfo];
}

/*! @brief Update UI using keyboard frame info.
 *  @param userInfo User info object.*/
- (void)updateOffsetForKeyboardWithUserInfo:(NSDictionary *)userInfo {
    CGRect keyboardFrame = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    
    //if (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight) {
    if (LT_SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(@"8.0") && (orientation == UIDeviceOrientationLandscapeLeft || orientation == UIDeviceOrientationLandscapeRight)) {
        keyboardFrame.size = CGSizeMake(keyboardFrame.size.height, keyboardFrame.size.width);
    }
    
    UIViewAnimationCurve curve = [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    NSTimeInterval duration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    CGFloat yy = (keyboardFrame.size.height - 16.0);
    self.frame = CGRectMake(0, (-yy), self.bounds.size.width, self.bounds.size.height);
    
    [UIView commitAnimations];
}


@end
