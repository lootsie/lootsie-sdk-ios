//
//  LTRedemptionNoAccount.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/7/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @typedef LTRedemptionDidClickRedeemNow
 *  @brief Block of code for set email.*/
typedef void(^LTRedemptionDidClickRedeemNow)(NSString* email);

/*! @typedef LTRedemptionDidClickRedeemNow
 *  @brief Block of code for change email text.*/
typedef void(^LTRedemptionEmailTextDidChange)(NSString* text);

/*! @typedef LTRedemptionDidClickRedeemNow
 *  @brief Block of code for start edit email text.*/
typedef void(^LTRedemptionEmailDidStartEditing)(BOOL flag);


/*! @class LTRedemptionNoAccount
 *  @brief View will show if user doesn't have account.*/
@interface LTRedemptionNoAccount : UIView

/*! Callback is called when user click to redeem now button.*/
@property (nonatomic, copy)     LTRedemptionDidClickRedeemNow onRedeemNowClick;

/*! Callback is called when user update email text.*/
@property (nonatomic, copy)     LTRedemptionEmailTextDidChange onTextChanged;

/*! Callback is called when user start editing email.*/
@property (nonatomic, copy)     LTRedemptionEmailDidStartEditing onStartEditing;

@end
