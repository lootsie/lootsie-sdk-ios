//
//  LTRedemptionConfirmEmail.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>

/*! @class LTRedemptionConfirmEmail
 *  @brief Confirm step of wizard.*/
@interface LTRedemptionConfirmEmail : UIView

@end
