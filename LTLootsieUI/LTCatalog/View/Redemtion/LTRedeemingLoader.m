//
//  LTRedeemingLoader.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/8/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTRedeemingLoader.h"
#import "LTTheme.h"
#import "LTTools.h"


@interface LTRedeemingLoader ()
/*! Title of view.*/
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! Subtitle of view*/
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;

/*! Loader image view.*/
@property (weak, nonatomic) IBOutlet UIImageView *loaderImageView;


/*! @brief Generate image with tint color.
 *  @param sourceImage Image with source content.
 *  @param color Tint color for new image.*/
- (UIImage*)image:(UIImage*)sourceImage withTintColor:(UIColor*)color;

/*! @brief Load UI component.*/
- (void)load;

@end


@implementation LTRedeemingLoader

#pragma mark - Private

- (UIImage*)image:(UIImage*)sourceImage withTintColor:(UIColor*)color {
    UIGraphicsBeginImageContextWithOptions(sourceImage.size, NO, sourceImage.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, sourceImage.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, sourceImage.size.width, sourceImage.size.height);
    CGContextClipToMask(context, rect, sourceImage.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)load {
    NSMutableParagraphStyle* paragraph = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraph.alignment = NSTextAlignmentCenter;
    paragraph.lineHeightMultiple = 1.24;
    NSDictionary* subtitleAttributes = @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                                                               weight: LTRegularFontWeight
                                                                                                style: LTRegularFontStyle
                                                                                                 size: LTMediumBFontSize],
                                         NSParagraphStyleAttributeName: paragraph,
                                         NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText]};
    self.subtitleLabel.attributedText = [[NSAttributedString alloc] initWithString: self.subtitleLabel.text
                                                                        attributes: subtitleAttributes];
    
    NSMutableArray* images = [NSMutableArray arrayWithCapacity: 22];
    for(int i = 1; i < 22; ++i) {
        NSInteger index = 320 + i;
        UIImage* img = [UIImage imageNamed: [NSString stringWithFormat: @"blobs02_%ld.png", (long)index]];
        img = [self image:img withTintColor: [[LTTheme sharedTheme] colorWithType: LTColorAltBackground5]];
        [images addObject: img];
    }
    
    self.loaderImageView.animationImages = images;
    self.loaderImageView.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground5];
    self.loaderImageView.animationDuration = 1.0;
    [self.loaderImageView startAnimating];
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}


#pragma mark - init
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib]; // FIX
    [self load];
}

@end
