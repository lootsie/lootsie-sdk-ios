//  LTRedemptionSuccessTopView.h
//  Created by Dovbnya Alexander on 9/6/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTRedemptionSuccessTopView.h"
#import "LTTheme.h"

@interface LTRedemptionSuccessTopView ()

/*! Background view.*/
@property (weak, nonatomic) IBOutlet UIImageView *backgroundView;

/*! Congrants label.*/
@property (weak, nonatomic) IBOutlet UILabel *congrantsLabel;

/*! Subtitle congrats.*/
@property (weak, nonatomic) IBOutlet UILabel *subtitleCongrats;

/*! Name reward label.*/
@property (weak, nonatomic) IBOutlet UILabel *nameRewardLabel;

@end


@implementation LTRedemptionSuccessTopView

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    UIImage *image = [UIImage imageNamed:@"tileCongratsBackgroud"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeTile];
    self.backgroundView.image = image;
    LTTheme *theamManager = [LTTheme sharedTheme];
    
    NSInteger offsetSizeForFonts = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].scale > 2.9) ? 0 : 0;
    
    self.congrantsLabel.font = [theamManager fontWithFamily:LTPrimaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:LTLargeFontSize - offsetSizeForFonts];
    self.subtitleCongrats.font = [theamManager fontWithFamily:LTPrimaryFontFamily weight:LTRegularFontWeight style:LTRegularFontStyle size:LTMediumBFontSize - offsetSizeForFonts];
    self.nameRewardLabel.font = [theamManager fontWithFamily:LTPrimaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:LTMediumAFontSize - offsetSizeForFonts];
}

/*! @brief Setter for reward name property.
 *  @param rewardName New value of property.*/
- (void)setRewardName:(NSString *)rewardName {
    _rewardName = rewardName;
    self.nameRewardLabel.text = rewardName;
}

@end
