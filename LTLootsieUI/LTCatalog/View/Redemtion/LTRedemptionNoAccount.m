//
//  LTRedemptionNoAccount.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/7/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTRedemptionNoAccount.h"
#import "LTTheme.h"
#import "LTPrimaryButton.h"
#import "LTTools.h"

#import "LTUIManager.h"


@interface LTRedemptionNoAccount () <UITextFieldDelegate>

/*! Title view.*/
@property (nonatomic, weak) IBOutlet UILabel* titleLabel;

/*! Subtitle view.*/
@property (nonatomic, weak) IBOutlet UILabel* subtitleLabel;

/*! Email field for set user's email.*/
@property (nonatomic, weak) IBOutlet UITextField* emailField;

/*! Email line view.*/
@property (nonatomic, weak) IBOutlet UIView* lineView;

/*! Email field top offset constrain.*/
@property (nonatomic, weak) IBOutlet NSLayoutConstraint* emailFieldYOffset;

@end


@implementation LTRedemptionNoAccount

#pragma mark - Notifications
/*! @brief Called when user is editing email.
 *  @param notification Object with new data*/
- (void)emailFieldDidEditing:(NSNotification*)notification {
    UITextField* textField = notification.object;
    if([textField isKindOfClass: [UITextField class]] && textField == self.emailField) {
        self.lineView.backgroundColor = [[LTTheme sharedTheme] colorWithType: (textField.text.length > 0) ? LTColorSecondaryDiv : LTColorAltButton1];
        
        if(self.onTextChanged) {
            self.onTextChanged(textField.text);
        }
    }
}

/*! @brief Called when user did start edit email.
 *  @param notification Object with new data*/
- (void)emailFieldDidBeging:(NSNotification*)notification {
    UITextField* textField = notification.object;
    if([textField isKindOfClass: [UITextField class]] && textField == self.emailField) {
        self.lineView.backgroundColor = [[LTTheme sharedTheme] colorWithType:  LTColorSecondaryDiv];
        self.emailField.rightView.alpha = 0.0;
        if(self.onStartEditing) {
            self.onStartEditing(YES);
        }
    }
}


#pragma mark - UITextFieldDelegate
/*! @brief Called when user tap to return (done) button on keyboard.
 *  @param textField Text field object.*/
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    self.lineView.backgroundColor = [[LTTheme sharedTheme] colorWithType: (textField.text.length > 0) ? LTColorSecondaryDiv : LTColorAltButton1];
    self.emailField.rightView.alpha = (self.emailField.text.length > 0) ? 0.0 : 1.0;
    if(self.onRedeemNowClick) {
        self.onRedeemNowClick(self.emailField.text);
    }
    if(self.onStartEditing) {
        self.onStartEditing((self.emailField.text.length > 0) ? YES : NO);
    }
    
    return YES;
}


#pragma mark - Private

- (void)load {
    if(self.emailField) {
        NSDictionary* placeholderAttribute = @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                                                                     weight: LTMediumFontWeight
                                                                                                      style: LTRegularFontStyle
                                                                                                       size: LTMediumAFontSize],
                                               NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorAltBackground1]};
        self.emailField.delegate = self;
        self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString: self.emailField.placeholder attributes: placeholderAttribute];
        self.emailField.textColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText];
        UIImage* img = [[UIImage imageNamed: @"redeem_email_arrow"] imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
        self.emailField.rightView = [[UIImageView alloc] initWithImage: img];
        self.emailField.rightView.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground1];
        self.emailField.rightViewMode = UITextFieldViewModeAlways;
        self.emailField.rightView.frame = CGRectMake(0, 0, 4, 7);
    }
    
    if(self.subtitleLabel) {
        NSMutableParagraphStyle* paragraph = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
        paragraph.alignment = NSTextAlignmentCenter;
        if(![LTTools isIphoneFive]) {
            paragraph.lineHeightMultiple = 1.24;
        }
        LTFontSize fontSize = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? LTMediumBFontSize : LTMediumCFontSize;
        NSDictionary* subtitleAttributes = @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                                                                   weight: LTRegularFontWeight
                                                                                                    style: LTRegularFontStyle
                                                                                                     size: fontSize],
                                             NSParagraphStyleAttributeName: paragraph,
                                             NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText]};
        self.subtitleLabel.attributedText = [[NSAttributedString alloc] initWithString: self.subtitleLabel.text
                                                                            attributes: subtitleAttributes];

        self.lineView.backgroundColor = [[LTTheme sharedTheme] colorWithType: (self.emailField.text.length > 0) ? LTColorSecondaryDiv : LTColorAltButton1];
    }
    if (self.titleLabel) {
        self.titleLabel.textColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText];
    }

    [self setNeedsLayout];
    [self layoutIfNeeded];
}


#pragma mark - init
/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib]; // FIX
    [self load];
    
    self.emailFieldYOffset.constant = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? 25.0 : 10.0;
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(emailFieldDidEditing:)
                                                 name: UITextFieldTextDidChangeNotification
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(emailFieldDidBeging:)
                                                 name: UITextFieldTextDidBeginEditingNotification
                                               object: nil];
}


#pragma mark - Clear memory
/*! @brief Called when object was released.*/
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
    self.onTextChanged = nil;
    self.onRedeemNowClick = nil;
}

@end
