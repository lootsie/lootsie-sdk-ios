#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "LTBaseButton.h"

/*! @class LTBubblingButton
 *  @brief Show points as a button.*/
@interface LTBubblingButton : LTBaseButton

/*! YES if need to use small size of button.*/
@property (nonatomic, assign) IBInspectable BOOL isSmallHeight;

/*! Points valie for button.*/
@property (nonatomic, assign) IBInspectable NSInteger points;


-(void) animateBubbles;

@end
