//
//  LTFavoritesPageController.m
//  LTLootsie iOS Example
//
//  Created by Macmini on 5/23/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFavoritesPageController.h"
#import "LTCatalogReward.h"
#import "LTCatalogRewardsList.h"

#import "LTFavoriteSectionHeaderView.h"
#import "LTCatalogRewardCell.h"
#import "LTRewardDetailViewController.h"

#import "LTTheme.h"

#import "LTManager+Catalog.h"
#import "LTUIManager+Catalog.h"


@interface LTFavoritesPageController () <UICollectionViewDelegate, UICollectionViewDataSource, LTCatalogRewardCellDelegate>

/*! View for show information view if you don't have any favorites rewards.*/
@property (weak, nonatomic) IBOutlet UIView *noFavRewardsView;

/*! Collection view for show favorites.*/
@property (weak, nonatomic) IBOutlet UICollectionView* favRewardsCollectionView;

/*! Title label for show title view.*/
@property (weak, nonatomic) IBOutlet UILabel *messageTitleLabel;

/*! Description of text.*/
@property (weak, nonatomic) IBOutlet UILabel *messageDescLabel;

/*! View for show information about favorites count.*/
@property (nonatomic, strong) LTFavoriteSectionHeaderView *headerView;


/*! List of rewards list which you save.*/
@property (nonatomic, strong)   LTCatalogRewardsList *rewardsList;

/*! Selected reward.*/
@property (nonatomic, strong)   LTCatalogReward* selectedReward;


/*! @brief Enable/disable interecation for rewards cell in collection.
 *  @param flag YES for enable.*/
- (void)interactionForRewardsCollectionViewEnabled:(BOOL)flag;

/*! @brief Update fevorites list.*/
- (void)updateFavorites;

@end


/*! Identifier for favorite reward cell.*/
static NSString* const LTFavoriteRewardsCell            =   @"favoriteRewardsCell";

/*! Identifier for rewards list section header.*/
static NSString* const LTFavoriteSectionHeader          =   @"favoriteSectionHeader";

/*! Identifier for show reward detail controller.*/
static NSString* const LTDetailRewardSegueIdentifier    =   @"FavoriteDetailSegue";



@implementation LTFavoritesPageController

#pragma mark - Private methods

- (void)interactionForRewardsCollectionViewEnabled:(BOOL)flag {
    self.favRewardsCollectionView.userInteractionEnabled = flag;
}

- (void)updateFavorites {
    self.rewardsList = [LTManager sharedManager].data.catalog;
    [self.rewardsList updateData];
    [self.favRewardsCollectionView reloadData];
    
    if(self.rewardsList.favoriteRewards.count > 0) {
        self.noFavRewardsView.hidden = YES;
    } else {
        self.noFavRewardsView.hidden = NO;
    }
}


#pragma mark - UIViewController methods
/*! @brief Create a new object.*/
- (instancetype)init {
    if (self = [super init]) {
        self.title = NSLocalizedStringFromTable(@"Favorites", @"LTLootsie", nil);
    }
    return self;
}

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];


    [self.favRewardsCollectionView registerNib: [UINib nibWithNibName:@"LTCatalogRewardCell" bundle:nil]
                    forCellWithReuseIdentifier: LTFavoriteRewardsCell];
    [self.favRewardsCollectionView registerNib: [UINib nibWithNibName:@"LTFavoriteSectionHeaderView" bundle:nil]
                    forSupplementaryViewOfKind: UICollectionElementKindSectionHeader
                           withReuseIdentifier: LTFavoriteSectionHeader];
    self.view.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryBackground];
    
    self.messageDescLabel.textColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText];
    self.messageTitleLabel.textColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText];

    // Do any additional setup after loading the view.
}

/*! @brief Called when controller will appear on screen.
 *  @param animated YES if controllwe was appear with animation.*/
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    [self updateFavorites];
}

/*! @brief Called when controller did appear on screen.
 *  @param animated YES if controllwe was appear with animation.*/
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.didAppearFavoriteScreen) {
        self.didAppearFavoriteScreen();
    }
}

/*! @brief Called when user start rotate device and need to refresh UI.
 *  @param toInterfaceOrientation New interface orientation.
 *  @param duration Animation duration.*/
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.favRewardsCollectionView.collectionViewLayout invalidateLayout];
}

/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionViewDelegate
/*! @brief Get number of section for collection view
 *  @param collectionView Collection view object.
 *  @return Number of section.*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

/*! @brief Get number of items for show in section.
 *  @param collectionView Collection view object.
 *  @param section Index of collection view section.
 *  @return Number of items.*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(self.rewardsList.favoriteRewards.count > 0) {
        return self.rewardsList.favoriteRewards.count;
    }
    return 0;
}

/*! @brief Get reference size for header in section.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param section Index of section.
 *  @return Size of header.*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(collectionView.bounds.size.width, 30.0);
}

/*! @brief Get size of item for index path.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param indexPath Index path for item.
 *  @return Size of item*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat sectionLeftOffset = ((UICollectionViewFlowLayout*)collectionViewLayout).sectionInset.left;
    CGFloat sectionRightOffset = ((UICollectionViewFlowLayout*)collectionViewLayout).sectionInset.right;
    CGFloat minimumLineSpacing = ((UICollectionViewFlowLayout*)collectionViewLayout).minimumLineSpacing;
    CGFloat ww = (collectionView.bounds.size.width - sectionLeftOffset - sectionRightOffset - minimumLineSpacing) / 2.0;
    CGFloat scale = ww / ((UICollectionViewFlowLayout*)collectionViewLayout).itemSize.width;
    return CGSizeMake(ww, ((UICollectionViewFlowLayout*)collectionViewLayout).itemSize.height * scale);
}

/*! @brief Get header or footer view for section.
 *  @param collectionView Collection view object.
 *  @param kind Type of view.
 *  @param indexPath Index path of view.
 *  @return Reusable view object.*/
- (UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView* reusableView = nil;
    if(kind == UICollectionElementKindSectionHeader) {
        LTFavoriteSectionHeaderView* headerView = [collectionView dequeueReusableSupplementaryViewOfKind: UICollectionElementKindSectionHeader
                                                                                     withReuseIdentifier: LTFavoriteSectionHeader
                                                                                            forIndexPath: indexPath];
        headerView.itemsCount = self.rewardsList.favoriteRewards.count;
        reusableView = headerView;
        self.headerView = headerView;
    }
    return reusableView;
}

/*! @brief Get cell for collection view with index path.
 *  @param collectionView Collection view object.
 *  @param indexPath Index path for item.
 *  @return Cell object.*/
- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LTCatalogRewardCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:LTFavoriteRewardsCell forIndexPath:indexPath];
    cell.reward = [self.rewardsList.favoriteRewards objectAtIndex: indexPath.item];
    cell.delegate = self;
    
    return cell;
}


#pragma mark - Base overwrite methods

- (void)needUpdateData {
    [super needUpdateData];

    [self updateFavorites];
    [self interactionForRewardsCollectionViewEnabled: YES];
}


#pragma mark - LTCatalogRewardCellDelegate

- (void)rewardCell:(LTBaseCatalogRewardCell *)cell didSelect:(LTCatalogReward *)reward {
    if(self.favRewardsCollectionView.userInteractionEnabled) {
        [self interactionForRewardsCollectionViewEnabled: NO];
        self.selectedReward = reward;
        CGRect frameInView = [self.view convertRect:cell.bounds fromView:cell];
        __weak typeof(self) tmp = self;
        [[LTUIManager sharedManager] showDetailRewardPage:reward onViewController:self fromStartPoint:CGPointMake(CGRectGetMidX(frameInView), CGRectGetMidY(frameInView)) onClose:^{
            [tmp interactionForRewardsCollectionViewEnabled: YES];
        }];
    }
}

- (void)rewardCellDidFavoriteClick:(LTBaseCatalogRewardCell *)cell {
    LTCatalogReward *reward = cell.reward;
    if(reward) {
        BOOL curentValue = reward.isFavorite;
        [[LTManager sharedManager] changeFavoriteStateForReward:cell.reward.rewardId success:^{
            [self.rewardsList updateData];
        } failure:^(NSError *error) {
            reward.isFavorite = curentValue;
            [self.rewardsList updateData];
            [self.favRewardsCollectionView reloadData];
            self.noFavRewardsView.hidden = self.rewardsList.favoriteRewards.count > 0;
        }];

        reward.isFavorite = !curentValue;
        [self.rewardsList updateData];
        if (!cell.reward.isFavorite) {
            NSIndexPath *indexPath = [self.favRewardsCollectionView indexPathForCell:cell];
            [self.favRewardsCollectionView deleteItemsAtIndexPaths:@[indexPath]];
            self.headerView.itemsCount = self.rewardsList.favoriteRewards.count;
        }
        self.noFavRewardsView.hidden = self.rewardsList.favoriteRewards.count > 0;
    }
}

- (BOOL)isCanFavoriteCell:(UICollectionViewCell *)cell {
    return CGRectContainsRect(self.favRewardsCollectionView.bounds, cell.frame);
}


#pragma mark - Navigation
/*! @brief Prepare for use segue.
 *  @param segue Segue object.
 *  @param sender Sender object.*/
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:LTDetailRewardSegueIdentifier]) {
        LTRewardDetailViewController *controller = (LTRewardDetailViewController *)segue.destinationViewController;
        controller.reward = self.selectedReward;
    }
}

@end
