//
//  LTRewardDetailViewController.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/19/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTRewardDetailViewController.h"

#import "LTManager+Catalog.h"
#import "LTManager+Account.h"
#import "LTUIManager+Catalog.h"
#import "LTEventManager+Private.h"

#import "UIImageView+LTAFNetworking.h"

#import "LTSpinningLabel.h"
#import "LTCloseButton.h"
#import "LTPrimaryButton.h"
#import "LTRedemtionView.h"
#import "LTRewardImageCell.h"

#import "LTTools.h"
#import "LTCatalogReward.h"
#import "LTTheme.h"
#import "LTCatalogRewardsList.h"
#import "LTBasePageViewController.h"
#import "LTRedemptionNoAccount.h"
#import "LTBaseInterstitialReward.h"
#import "LTBubblingButton.h"

#import "LTUserRewardsOperation.h"


#import "LTViewController.h"

#import "LTMWLogging.h"

@interface LTRewardDetailViewController () <UIScrollViewDelegate>

/*! Vertical scroll view to pull-to-refresh and show details*/
@property (weak, nonatomic) IBOutlet UIScrollView* scrollView;

/*! Content view to be scrolled and snapped */
@property (weak, nonatomic) IBOutlet UIView* scrollableView;

/*! Container view for pull to refresh action.*/
@property (weak, nonatomic) IBOutlet UIView *pullToRefreshContainer;

/*! Close button for close current controller.*/
@property (weak, nonatomic) IBOutlet LTCloseButton *closeButton;

/*! Brand image view. Show brand image.*/
@property (weak, nonatomic) IBOutlet UIImageView *brandImage;

/*! Title label for show reward title.*/
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! Label for show reward description.*/
@property (weak, nonatomic) IBOutlet UILabel *descView;

/*! Button for start redeem current reward.*/
@property (weak, nonatomic) IBOutlet LTPrimaryButton *redeemThisButton;

/*! Details view that is snapped to.*/
@property (weak, nonatomic) IBOutlet UIView *detailsContainer;

/*! Details arrow icon that rotates on scroll.*/
@property (weak, nonatomic) IBOutlet UIImageView *detailsArrow;

/*! Reward details text to be shown*/
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

/*! Button for add/remove rewad to/from favorite*/
@property (weak, nonatomic) IBOutlet UIButton* favoriteButton;

/*! Redemtion view for hsow redemtion wizard.*/
@property (nonatomic, strong)   LTRedemtionView *redemtionView;

/*! Image of current reward */
@property (weak, nonatomic) IBOutlet UIImageView *rewardImage;

/*! View whcih will show if user don't registered yet.*/
@property (strong, nonatomic)   LTRedemptionNoAccount *redeemNowNoAccount;

/*! Content image view.*/
@property (strong, nonatomic)   UIImage* contentViewImage;

/*! Content image view.*/
@property (strong, nonatomic)   UILabel* checkOutLabel;
/*! Content image view.*/
@property (strong, nonatomic)   UILabel* thankyouTitle;
/*! Content image view.*/
@property (strong, nonatomic)   UIImageView* arrowDown;

/*! YES if redemption view was shown.*/
@property (assign, nonatomic)   BOOL redeemViewShown;

@property (nonatomic, assign) CGFloat lastContentOffsetY;
@property (nonatomic, assign) CGFloat scrollDirection;


@property (strong, nonatomic) LTViewController *mainController;

- (CGSize) calculateBoundingSizeForText: (NSString*) text withFont: (UIFont*) font;
- (void)updateRewardInfo;
- (void)updateRedeemButtonState;

@end

@implementation LTRewardDetailViewController

#pragma mark - View controller methods

- (void)processCloseRewardCard:(NSNotification *) notification {
    // [notification name] should always be @"TestNotification"
    // unless you use this method for observation of other notifications
    // as well.
    
    if ([[notification name] isEqualToString:@"closeRewardCard"])
        NSLog (@"Successfully received closeRewardCard notification!");
    
    if(!self.redeemViewShown) {
        [[LTUIManager sharedManager] hideViewController:self];
    } else {
        [self.redemtionView removeFromSuperview];
        self.redemtionView = nil;
        self.redeemViewShown = NO;
    }
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"toggleBackButton" object:self];
}

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializationBaseUI];
    
    self.contentView.hidden = YES;

    assert(self.reward != nil);

    //fixes blank spacing issue :(
    self.scrollView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorAltBackground5];
    
    self.scrollView.delegate = self;
    [self.scrollView setDecelerationRate: UIScrollViewDecelerationRateFast];
    
    self.rewardImage.backgroundColor =  [[LTTheme sharedTheme] colorWithType: LTColorTertiaryBackground];
    
    [self updateRewardInfo];
    
    self.contentViewImage = [self imageFromContentView];
}

/*! @brief Called when controller will appear on screen.
 *  @param animated YES if controllwe was appeared with animation.*/
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processCloseRewardCard:)
                                                 name:@"closeRewardCard"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toggleBackButton" object:self];
    
    [self hideContentView];
    
    /*
     if(self.reward.refreshEnabled) {
     NSLog(@"REFRESH ENABLED");
     } else {
     NSLog(@"REFRESH DISABLED");
     }
     */
    
    [self.pullToRefreshContainer.subviews setValue:@YES forKeyPath:@"hidden"];
    self.scrollView.bounces = false;
    
    #if DISABLE_REDEMPTION
        NSLog(@"without redemption");
        self.redeemThisButton.hidden = true;
    #else
        NSLog(@"with redemption");
    #endif
    
    self.redeemThisButton.enabled = NO;
    self.favoriteButton.selected = self.reward.isFavorite;
    self.favoriteButton.tintColor = (self.reward.isFavorite) ? [[LTTheme sharedTheme] colorWithType:LTColorAltBackground5] : [[LTTheme sharedTheme] colorWithType: LTColorInactivePrimaryButton];
    
    [self updateRedeemButtonState];
}

/*! @brief Called when controller did appear on screen.
 *  @param animated YES if controllwe was appeared with animation.*/
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
}

/*! @brief Called when controller will disappear on screen.
 *  @param animated YES if controllwe was disappeared with animation.
 *
 *  Stop the video play.
 */
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear: animated];
    if([self.parentViewController isKindOfClass:[LTBasePageViewController class]]) {
        [((LTBasePageViewController*)self.parentViewController) needUpdateData];
    }

    [[NSNotificationCenter defaultCenter] postNotificationName:@"toggleBackButton" object:self];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"closeRewardCard" object:nil];
    
}

/*! @brief Called when controller did disappear on screen.
 *  @param animated YES if controllwe was disappeared with animation.*/
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if ([self isBeingDismissed] || [self isMovingFromParentViewController]) {
        if(self.onClose) {
            self.onClose();
        }
    }
}

/*! @brief Disable users ability to navigate when video is playing.*/
-(void)disableNavigationBar {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"disableNavigationBar" object:nil userInfo:nil];
}

// ASAdView Delegate
- (UIViewController *)viewControllerForPresentingModalView {
    return [UIViewController init];
}

/*! @brief Called when component is shows in interface builder.*/
- (void)prepareForInterfaceBuilder {
    [super prepareForInterfaceBuilder];
    [self initializationBaseUI];
}

/*! @brief Called when all subviews was updated.*/
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
}

/*! @brief Setter for reward property.
 *  @param newReward New value reward.*/
- (void)setReward:(LTCatalogReward *)newReward {
    _reward = newReward;
    [self updateRewardInfo];
}

/*! @brief Load all UI for component.*/
- (void)initializationBaseUI {
    
    self.titleLabel.textColor =  [[LTTheme sharedTheme] colorWithType: LTColorSecondaryText];
    self.titleLabel.font = [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                          weight: LTMediumFontWeight
                                                           style: LTRegularFontStyle
                                                            size: LTMediumAFontSize];
    
    self.descView.font = [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                        weight: LTRegularFontWeight
                                                         style: LTRegularFontStyle
                                                          size: LTMediumBFontSize];
    self.descView.textColor = [[LTTheme sharedTheme] colorWithType: LTColorSecondaryText];
    
    UIImage* imgNormal = [self.favoriteButton imageForState: UIControlStateNormal];
    UIImage* imgSelected = [self.favoriteButton imageForState: UIControlStateSelected];
    [self.favoriteButton setImage:[imgNormal imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate] forState: UIControlStateNormal];
    [self.favoriteButton setImage:[imgSelected imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate] forState: UIControlStateSelected];
    self.favoriteButton.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorInactivePrimaryButton];
    
    [self.detailsArrow setImage:[[self.detailsArrow image] imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate]];
    self.detailsArrow.tintColor = [[LTTheme sharedTheme] colorWithType: LTColorAltBackground5];

}

/*! @brief Enable User to navigate when video is complete.*/
- (void)enableNavigationBar {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"enableNavigationBar" object:nil userInfo:nil];
}

/*! @brief Update reward info.*/
- (void)updateRewardInfo {
    if(self.reward) {
        [self enableNavigationBar];
        
        self.titleLabel.text = self.reward.name;
        self.descView.text = self.reward.desc;
        self.favoriteButton.selected = self.reward.isFavorite;
        self.favoriteButton.tintColor = (self.reward.isFavorite) ? [[LTTheme sharedTheme] colorWithType:LTColorAltBackground5] : [[LTTheme sharedTheme] colorWithType: LTColorInactivePrimaryButton];
        
        if(self.reward.banners[0].absoluteString.length > 0) {
            [self.rewardImage setImageWithURL: [NSURL URLWithString: self.reward.banners[0].absoluteString]];
        }
        
        CGRect contentRect = CGRectZero;
        for (UIView *view in self.scrollView.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        self.scrollView.contentSize = contentRect.size;
        [self.scrollView invalidateIntrinsicContentSize];
        
    }

    [self updateRedeemButtonState];
}

- (void)updateRedeemButtonState {
    __weak typeof(self) weakSelf = self;

    [[LTManager sharedManager] userAccountWithSuccess:^(LTUserAccount *userAccount) {
        if (userAccount.points >= weakSelf.reward.points) {
            weakSelf.redeemThisButton.enabled = YES;
        } else {
            weakSelf.redeemThisButton.enabled = NO;
        }

    } failure:nil];

}

/*! @brief Called when user tap to close button.
 *  @param sender Button object which called action.*/
- (IBAction)onCloseButtonClick:(LTBaseButton*)sender {
    if(!self.redeemViewShown) {
        [[LTUIManager sharedManager] hideViewController:self];
    } else {
        [self.redemtionView removeFromSuperview];
        self.redemtionView = nil;
        self.redeemViewShown = NO;
    }
}

/*! @brief Called when user tap to favorite button.
 *  @param sender Button object which called action.*/
- (IBAction)onFavoriteButtonClick:(UIButton*)sender {
    BOOL curentValue = self.reward.isFavorite;
    __weak typeof(self) weakSelf = self;
    [[LTManager sharedManager] changeFavoriteStateForReward:self.reward.rewardId success:^{
        
    }failure:^(NSError *error) {
        [weakSelf updateFavoriteState:curentValue];
    }];
    [self updateFavoriteState:!self.reward.isFavorite];
    if (self.reward.isFavorite) {
        CGSize size = self.view.bounds.size;
        [[LTUIManager sharedManager] showFirtsTimeFavoriteOnViewController:self fromStartPoint:CGPointMake(size.width / 2.f, size.height / 2.f)];
    }
}

/*! @brief Update favorite flag for current reward.
 *  @param isFavorite Favorite flag value.*/
- (void)updateFavoriteState:(BOOL)isFavorite {
    self.reward.isFavorite = isFavorite;
    [[LTManager sharedManager].data.catalog updateData];
    self.favoriteButton.selected = self.reward.isFavorite;
    self.favoriteButton.tintColor = (self.reward.isFavorite) ? [[LTTheme sharedTheme] colorWithType:LTColorAltBackground5] : [[LTTheme sharedTheme] colorWithType: LTColorInactivePrimaryButton];
    //Update data on the parent controller.
    LTBasePageViewController* parentController = (LTBasePageViewController*)self.parentViewController;
    if([parentController isKindOfClass:[LTBasePageViewController class]]) {
        [parentController needUpdateData];
    }
}

/*! @brief Called when user tap to redeem this button.
 *  @param sender Button object which called action.*/
- (IBAction)onRedeemThisButtonClick:(LTBaseButton *)sender {


    self.contentView.hidden = NO; // animate this in.


    NSBundle *bundle = [NSBundle bundleForClass:[LTUIManager class]];
    self.redemtionView = [[[UINib nibWithNibName:@"LTRedemtionView" bundle:bundle] instantiateWithOwner:self options:nil] firstObject];
    self.redemtionView.reward = self.reward;
    LTUserAccount *userAccount = [LTManager sharedManager].data.user;
    self.redemtionView.state = userAccount.confirmed ? LTRedemtionViewLoading : LTRedemtionViewNonRegistered;
    self.redemtionView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;


    self.redemtionView.frame =  self.contentView.bounds;

    //self.contentView = [[UIView alloc] initWithFrame:  self.view.frame];


    [self.contentView addSubview: self.redemtionView];

    //self.contentView.backgroundColor = [UIColor redColor];

    __weak UIView *weakView = self.redemtionView;
    __weak LTRewardDetailViewController *weakSelf = self;
    self.redemtionView.didChangeState = ^(LTRedemtionViewState newState) {
        if (newState == LTRedemtionViewSuccess) {
 //           [weakSelf.contentView insertSubview:weakSelf.closeButton belowSubview:weakView];
        }
    };

    self.redemtionView.didFinish = ^() {
        [weakSelf hideContentView];

        weakSelf.redeemViewShown = NO;
        [weakSelf onCloseButtonClick: nil];

    };
    


    //
    //[self.contentView bringSubviewToFront:self.closeButton];
    //[self.contentView bringSubviewToFront:self.redeemThisButton];

    self.redeemViewShown = YES;
}

#pragma mark - refresh video methods
/*! @brief Grab screenshot from current view.
 *  @return Image data.*/
- (UIImage*)imageFromContentView {
    CGSize imageSize = self.contentView.bounds.size;
    UIGraphicsBeginImageContextWithOptions(imageSize, NO, [UIScreen mainScreen].scale);
//    [self.contentView drawViewHierarchyInRect:self.contentView.bounds afterScreenUpdates:YES];
    [self.contentView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - LTUIAnimationDelegate

- (void)animateFromStartPoint:(CGPoint)startPoint completion:(LTUIAnimationCompleteBlock)block {
    self.backgroundView.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorModalBackground];
    self.view.transform = CGAffineTransformMakeTranslation(0, self.view.frame.size.height);
    [UIView animateWithDuration: 0.3f
                     animations: ^{
                         self.view.transform = CGAffineTransformIdentity;
                     } completion: ^(BOOL finished) {
                         if(block) {
                             block();
                         }
                     }];
}

#pragma mark - UIScrollViewDelegate
/*! @brief Called when component did scroll.
 *  @param scrollView Scroll view object.*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    CGFloat scrollViewYOffset = scrollView.contentOffset.y;
    CGFloat scrollViewHeight = self.scrollView.bounds.size.height;
    CGFloat scrollableHeight = self.scrollableView.bounds.size.height;
    
    CGFloat heightDifference = scrollableHeight - scrollViewHeight;
    
    CGFloat rotateRadians =  (scrollViewYOffset / heightDifference) * M_PI;
    
    self.scrollDirection = self.lastContentOffsetY - scrollViewYOffset;
    
    self.lastContentOffsetY = scrollViewYOffset;


    CGFloat alpha = (scrollViewYOffset / heightDifference);
    if (alpha < 0) {
        alpha = 0;
    }
    else if (alpha > 1) {
        alpha = 1;
    }

    CGAffineTransform transform = CGAffineTransformMakeRotation(rotateRadians);
    self.detailsArrow.transform = transform;
    
    self.descriptionLabel.alpha = alpha;

}

- (void)resetAnimation {
    [self.scrollView setScrollEnabled:true];

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.10];
    
    [self.redeemThisButton setAlpha:1.0];
    [self.rewardImage setAlpha:1.0];
    [self.favoriteButton setAlpha:1.0];
    [self.closeButton setAlpha:1.0];

    [UIView commitAnimations];
}

- (CGSize)calculateBoundingSizeForText: (NSString*) text withFont: (UIFont*) font {
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text];
    //Add LineBreakMode
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
    [attributedString setAttributes:@{NSParagraphStyleAttributeName:paragraphStyle} range:NSMakeRange(0, attributedString.length)];
    // Add Font
    [attributedString setAttributes:@{NSFontAttributeName:font} range:NSMakeRange(0, attributedString.length)];

    //Now let's make the Bounding Rect
    return  [attributedString boundingRectWithSize: CGSizeMake(380, 50)
                                                      options: NSStringDrawingUsesLineFragmentOrigin
                                                      context:nil].size;

}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    CGFloat scrollViewHeight = self.scrollView.bounds.size.height;
    CGFloat scrollableHeight = self.scrollableView.bounds.size.height;
    
    CGFloat heightDifference = scrollableHeight - scrollViewHeight;
    
    if(self.scrollDirection < 0) {
        if(velocity.y > 0) {
            targetContentOffset->y = heightDifference;
        }else if (scrollView.contentOffset.y >=  (heightDifference / 3)) {
            targetContentOffset->y = heightDifference;
        } else {
            targetContentOffset->y = 0;
        }
    } else {
        if(velocity.y < 0) {
            targetContentOffset->y = 0;
        } else if (scrollView.contentOffset.y >=  ((heightDifference / 3) * 2)) {
            targetContentOffset->y = heightDifference;
        } else {
            targetContentOffset->y = 0;
        }
    }

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGFloat scrollViewHeight = self.scrollView.bounds.size.height;
    CGFloat scrollableHeight = self.scrollableView.bounds.size.height;
    
    CGFloat heightDifference = scrollableHeight - scrollViewHeight;

    if(self.scrollDirection < 0) {
        if (scrollView.contentOffset.y >=  (heightDifference / 3)) {
            [scrollView setContentOffset:CGPointMake(0, heightDifference) animated:YES];
        }  else {
            [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    } else {
         if (scrollView.contentOffset.y >=  ((heightDifference / 3) * 2)) {
            [scrollView setContentOffset:CGPointMake(0, heightDifference) animated:YES];
        }  else {
            [scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    }
}

- (void)closeButtonPressed {
    NSLog(@"closeButtonPressed");
    [[LTManager sharedManager].eventManager sendEvents];
}

- (void)hideContentView {
    self.contentView.hidden = YES;
    // remove subviews.
}

@end
