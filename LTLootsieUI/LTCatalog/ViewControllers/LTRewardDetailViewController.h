//
//  LTRewardDetailViewController.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/19/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTBaseAnimateViewController.h"

/*! @typedef LTRewardDetailViewControllerDidClose
 *  @brief Callback is called when user did close controller.*/
typedef void(^LTRewardDetailViewControllerDidClose)();


@class LTCatalogReward;
/*! @class LTRewardDetailViewController
 *  @brief Controller for show detail information about reward.*/
@interface LTRewardDetailViewController : LTBaseAnimateViewController

/*! Reward info object.*/
@property (nonatomic, strong)   LTCatalogReward* reward;

/*! Callback for close controller. @see LTRewardDetailViewControllerDidClose*/
@property (nonatomic, copy)     LTRewardDetailViewControllerDidClose onClose;

@end
