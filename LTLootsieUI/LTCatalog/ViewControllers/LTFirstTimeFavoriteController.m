//  LTFirstTimeFavoriteController.ь
//  Created by Dovbnya Alexander on 6/16/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTFirstTimeFavoriteController.h"
#import "LTPrimaryButton.h"

#import "LTUIManager.h"
#import "LTTools.h"
#import "LTTheme.h"

@interface LTFirstTimeFavoriteController ()

/*! Background view.*/
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

/*! Title label for view.*/
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! Favorite icon for view.*/
@property (weak, nonatomic) IBOutlet UIImageView *favoritesIcon;

/*! Description lable.*/
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

/*! Gradient view for show on the top of content.*/
@property (weak, nonatomic) IBOutlet UIImageView *gradientBackground;

/*! Close button for close controller.*/
@property (weak, nonatomic) IBOutlet LTPrimaryButton *closeButton;

@end


@implementation LTFirstTimeFavoriteController

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentView.layer.cornerRadius = 5.f;
    self.contentView.layer.masksToBounds = YES;
    UIImage *image = [UIImage imageNamed:@"tileCongratsBackgroud"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeTile];
    self.backgroundImage.image = image;
    self.favoritesIcon.image = [self.favoritesIcon.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.favoritesIcon.tintColor = [[LTTheme sharedTheme] colorWithType:LTColorAltBackground5];
    self.contentView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    UIColor *backgroundColor = self.contentView.backgroundColor;
    self.gradientBackground.image = [LTTools gradientBgImageWithHeight: self.gradientBackground.bounds.size.height
                                                              topColor: [backgroundColor colorWithAlphaComponent:0.2]
                                                           bottomColor: [backgroundColor colorWithAlphaComponent:1]];
    self.titleLabel.font = [[LTTheme sharedTheme] fontWithFamily:LTPrimaryFontFamily weight:LTBlackFontWeight style:LTRegularFontStyle size:LTLargeFontSize];
    self.descriptionLabel.font = [[LTTheme sharedTheme] fontWithFamily:LTPrimaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:LTMediumBFontSize];
    [self.titleLabel setTextColor:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryText]];
    [self.descriptionLabel setTextColor:[[LTTheme sharedTheme] colorWithType:LTColorPrimaryText]];
}

/*! @brief Called when user click to Ok button.
 *  @param sender Button object which called action.*/
- (IBAction)onOkButtonPressed:(id)sender {
    [[LTUIManager sharedManager] hideViewController: self];
}

@end
