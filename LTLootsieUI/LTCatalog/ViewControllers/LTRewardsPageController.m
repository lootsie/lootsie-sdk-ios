//  LTRewardsPageController.m
//  Created by Fabio Teles on 5/18/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTRewardsPageController.h"
#import "LTRewardDetailViewController.h"

#import "LTUIManager.h"
#import "LTAlertOperation.h"
#import "LTTheme.h"

#import "LTManager+Catalog.h"
#import "LTEventManager.h"
#import "LTEventManager+Private.h"
#import "LTUIManager+Catalog.h"

#import "LTCatalogReward.h"
#import "LTCatalogRewardsList.h"
#import "LTCatalogRewardCell.h"
#import "LTFeaturedRewadCell.h"

#import "LTIANGenericItem.h"
#import "LTIANCatalogItem.h"
#import "LTIANRewardItem.h"
#import "LTIANVideoItem.h"

#import "LTMWLogging.h"
#import "LTFeaturedSectionHeaderView.h"


static NSString* const CellIdentifier                   =   @"rewardCell";
static NSString* const CellRewardsHeaderSection         =   @"rewardsHeaderSection";
static NSString* const CellFeaturedHeaderSection        =   @"featuredHeaderSection";
static NSString* const CellFeaturedRewardsIdentifier    =   @"featuredRewardsCell";
NSString * LTDetailRewardSegueIdentifier        = @"DetailSegue";

static NSString* const DetailRewardPage         =   @"detailRewardPage";


@interface LTRewardsPageController () <UICollectionViewDelegate, UICollectionViewDataSource, LTCatalogRewardCellDelegate>

/*! List of rewards.*/
@property (strong, nonatomic)   LTCatalogRewardsList* rewardsList;

/*! Selected reward.*/
@property (nonatomic, strong)   LTCatalogReward* selectedReward;

@end


@implementation LTRewardsPageController

/*! @brief Enable/Disable interaction for collection view.
 *  @param flag YES if need to enable interaction.*/
- (void)interactionForRewardsCollectionViewEnabled:(BOOL)flag {
    self.rewardsCollectionView.userInteractionEnabled = flag;
}

/*! @brief Create a new object.*/
- (instancetype)init {
    if (self == [super init]) {
        self.title = NSLocalizedStringFromTable(@"Catalog", @"LTLootsie", nil);
    }
    return self;
}

/*! @brief Called when component was created from xib or storyboard.*/
- (void)awakeFromNib {
    [super awakeFromNib];
    // Do any additional setup after loading the view.
    self.title = NSLocalizedStringFromTable(@"Catalog", @"LTLootsie", nil);
}

/*! @brief Show detail controller for selected reward.
 *  @param reward Reward info object.
 *  @param cell Cell object.*/
- (void)showDetailPageWithReward:(LTCatalogReward*)reward fromCell:(UICollectionViewCell *)cell {
    CGRect frameInView = [self.view convertRect:cell.bounds fromView:cell];
    CGPoint startPoint = CGPointMake(CGRectGetMidX(frameInView), CGRectGetMidY(frameInView));
    __weak typeof(self) tmp = self;
    [[LTUIManager sharedManager] showDetailRewardPage:reward onViewController:self fromStartPoint:startPoint onClose: ^{
        [tmp interactionForRewardsCollectionViewEnabled: YES];
    }];
}

/*! @brief Generate test data.*/
- (void)generateTestData {
    [[LTManager sharedManager] rewardsWithSuccess:^(LTCatalogRewardsList *rewards) {
        self.rewardsList = rewards;
        [self.rewardsCollectionView reloadData];
    } failure:nil];
}

/*! @brief Called when object was released.*/
- (void)dealloc {
	// clean notifications observer
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorPrimaryBackground];
    
    [self.rewardsCollectionView registerNib: [UINib nibWithNibName: @"LTCatalogRewardCell" bundle: nil]
                 forCellWithReuseIdentifier: CellIdentifier];
    [self.rewardsCollectionView registerNib: [UINib nibWithNibName: @"LTFeaturedRewadCell" bundle: nil]
                 forCellWithReuseIdentifier: CellFeaturedRewardsIdentifier];
    [self.rewardsCollectionView registerNib: [UINib nibWithNibName: @"LTRewardsSectionHeaderView" bundle: nil]
                 forSupplementaryViewOfKind: UICollectionElementKindSectionHeader
                        withReuseIdentifier: CellRewardsHeaderSection];
    [self.rewardsCollectionView registerNib: [UINib nibWithNibName: @"LTFeaturedSectionHeaderView" bundle: nil]
                 forSupplementaryViewOfKind: UICollectionElementKindSectionHeader
                        withReuseIdentifier: CellFeaturedHeaderSection];
}

/*! @brief Called when user start rotate device and need to refresh UI.
 *  @param toInterfaceOrientation New interface orientation.
 *  @param duration Animation duration.*/
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self.rewardsCollectionView.collectionViewLayout invalidateLayout];
}

/*! @brief Called when controller will appear on screen.
 *  @param animated YES if controllwe was appear with animation.*/
- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];

	[self generateTestData];
}

/*! @brief Called when controller did appear on screen.
 *  @param animated YES if controllwe was appear with animation.*/
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.childViewControllers.count == 0) {
        [[LTManager sharedManager].eventManager addBaseEvent:LTCatalogViewEventID additionalNumber:nil];
    }

}

/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*! @brief Prepare for use segue.
 *  @param segue Segue object.
 *  @param sender Sender object.*/
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:LTDetailRewardSegueIdentifier]) {
        LTRewardDetailViewController *controller = (LTRewardDetailViewController *)segue.destinationViewController;
        controller.reward = self.selectedReward;
    }
}

#pragma mark - Catalog
/*! @brief Get list of rewards and reload collection view.*/
- (void)updateRewards {
    [[LTManager sharedManager] rewardsWithSuccess:^(LTCatalogRewardsList *catalog) {
//        self.rewards = rewards;
        [self.rewardsCollectionView reloadData];
    } failure:nil];
}

#pragma mark - Collection view delegates
/*! @brief Get number of section for collection view
 *  @param collectionView Collection view object.
 *  @return Number of section.*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 2;
}

/*! @brief Get number of items for show in section.
 *  @param collectionView Collection view object.
 *  @param section Index of collection view section.
 *  @return Number of items.*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(section == 0) {
        return (self.rewardsList.featuredRewards.count > 0) ? 1 : 0;
    } else {
        return self.rewardsList.rewards.count;
    }
}

/*! @brief Get reference size for header in section.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param section Index of section.
 *  @return Size of header.*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if(section == 0) {
        return CGSizeMake(collectionView.bounds.size.width, 30.0);
    } else {
        return CGSizeMake(collectionView.bounds.size.width, 1.0);
    }
}

/*! @brief Get size of item for index path.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param indexPath Index path for item.
 *  @return Size of item*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.section == 0) {
        CGFloat height = 229.f;
        return CGSizeMake(collectionView.bounds.size.width, height);
    } else {
        CGFloat sectionLeftOffset = ((UICollectionViewFlowLayout*)collectionViewLayout).sectionInset.left;
        CGFloat sectionRightOffset = ((UICollectionViewFlowLayout*)collectionViewLayout).sectionInset.right;
        CGFloat minimumLineSpacing = ((UICollectionViewFlowLayout*)collectionViewLayout).minimumLineSpacing;
        CGFloat ww = (collectionView.bounds.size.width - sectionLeftOffset - sectionRightOffset - minimumLineSpacing) / 2.0;
        CGFloat scale = ww / ((UICollectionViewFlowLayout*)collectionViewLayout).itemSize.width;
        return CGSizeMake(ww, ((UICollectionViewFlowLayout*)collectionViewLayout).itemSize.height * scale);
    }
}

/*! @brief Get header or footer view for section.
 *  @param collectionView Collection view object.
 *  @param kind Type of view.
 *  @param indexPath Index path of view.
 *  @return Reusable view object.*/
- (UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView* reusableView = nil;
    if(kind == UICollectionElementKindSectionHeader) {
        if(indexPath.section == 0) {
            reusableView = [collectionView dequeueReusableSupplementaryViewOfKind: UICollectionElementKindSectionHeader
                                                              withReuseIdentifier: CellFeaturedHeaderSection
                                                                     forIndexPath: indexPath];
        } else {
            reusableView = [collectionView dequeueReusableSupplementaryViewOfKind: UICollectionElementKindSectionHeader
                                                              withReuseIdentifier: CellRewardsHeaderSection
                                                                     forIndexPath: indexPath];
        }
    }
    return reusableView;
}

/*! @brief Get cell for collection view with index path.
 *  @param collectionView Collection view object.
 *  @param indexPath Index path for item.
 *  @return Cell object.*/
- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell* cell = nil;
    
    if(indexPath.section == 1) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier: CellIdentifier forIndexPath: indexPath];
        if([cell isKindOfClass: [LTCatalogRewardCell class]]) {
            LTCatalogReward* reward = [self.rewardsList.rewards objectAtIndex: indexPath.item];
//            reward.refreshEnabled = YES;
//            reward.state = LTNewRewardState;
//            reward.markerString = (indexPath.item == 0) ? NSLocalizedStringFromTable(@"Only 2 left!", @"LTLootsie", nil) : nil;
            ((LTCatalogRewardCell*)cell).reward = reward;
            ((LTCatalogRewardCell*)cell).index = indexPath.item;
            ((LTCatalogRewardCell*)cell).delegate = self;
        }
    } else {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier: CellFeaturedRewardsIdentifier forIndexPath: indexPath];
        ((LTFeaturedRewadCell*)cell).rewards = self.rewardsList.featuredRewards;
        ((LTFeaturedRewadCell*)cell).delegate = self;
    }
    return cell;
}

/*! @brief Get section insets for section.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param section Index of section.
 *  @return Cell object.*/
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if(section == 0) {
        return UIEdgeInsetsMake(0, 0, 10, 0);
    } else {
        return ((UICollectionViewFlowLayout*)collectionViewLayout).sectionInset;
    }
}

/*! @brief Called when user start dragging view.
 *  @param scrollView Scroll view object.*/
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [scrollView endEditing:YES];
}

#pragma mark - Base overwrite methods

- (void)needUpdateData {
    [super needUpdateData];
    
    [self interactionForRewardsCollectionViewEnabled: YES];
    [self generateTestData];
}

#pragma mark - LTCatalogRewardCellDelegate

- (void)rewardCell:(LTBaseCatalogRewardCell *)cell didSelect:(LTCatalogReward *)reward {
    if(self.rewardsCollectionView.userInteractionEnabled) {
        [self interactionForRewardsCollectionViewEnabled: NO];
        self.selectedReward = (LTCatalogReward*)reward;
        [self showDetailPageWithReward: self.selectedReward fromCell:cell];
    }
}

- (void)rewardCellDidFavoriteClick:(LTBaseCatalogRewardCell*)cell {
    LTCatalogReward *reward = cell.reward;
    if(reward) {
        BOOL curentValue = reward.isFavorite;
        NSIndexPath *indexPath = [self.rewardsCollectionView indexPathForCell:cell];
        [[LTManager sharedManager] changeFavoriteStateForReward: cell.reward.rewardId success: ^{
            [self.rewardsList updateData];
        } failure:^(NSError *error) {
            reward.isFavorite = curentValue;
            [self.rewardsCollectionView reloadItemsAtIndexPaths:@[indexPath]];
        }];
        reward.isFavorite = !curentValue;
        if (reward.isFavorite) {
            CGSize size = self.view.bounds.size;
            [[LTUIManager sharedManager] showFirtsTimeFavoriteOnViewController: self fromStartPoint: CGPointMake(size.width / 2.f, size.height / 2.f)];
        }
    }
}

- (BOOL)isCanFavoriteCell:(UICollectionViewCell *)cell {
    return CGRectContainsRect(self.rewardsCollectionView.bounds, cell.frame);
}

- (void)rewardCellDidDoubleTap:(LTBaseCatalogRewardCell *)cell {
}

@end
