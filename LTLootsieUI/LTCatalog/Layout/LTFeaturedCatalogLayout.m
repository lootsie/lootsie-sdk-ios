//  LTFeaturedCatalogLayout.m
//  Created by Dovbnya Alexander on 6/3/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTFeaturedCatalogLayout.h"

const CGFloat offsetForVisibility = 1.f;

@implementation LTFeaturedCatalogLayout

/*! @brief Return YES to cause the collection view to requery the layout for geometry information.
 *  @param newBounds New bounds value.*/
- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

/*! @brief Return layout attributes for for supplementary or decoration views, or to perform layout in an as-needed-on-screen fashion.
 *  @param rect Frame for elements.*/
- (nullable NSArray<__kindof UICollectionViewLayoutAttributes *> *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray * original = [super layoutAttributesForElementsInRect:rect];
    NSArray *array = [[NSArray alloc] initWithArray:original copyItems:YES];
    for (UICollectionViewLayoutAttributes *attribute in array) {
        [self calculateVisibilityParameter:attribute];
    }
    NSArray *visibleIndexs = self.collectionView.indexPathsForVisibleItems;
    NSInteger rightRow = -1;
    for (NSIndexPath *indexPath in visibleIndexs) {
        if (rightRow < indexPath.row) {
            rightRow = indexPath.row;
        }
    }
    if (rightRow > 0 && rightRow + 1 < [self.collectionView numberOfItemsInSection:0]) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:rightRow + 1 inSection:0];
        UICollectionViewLayoutAttributes *attribute = [self layoutAttributesForItemAtIndexPath:indexPath];
        [self calculateVisibilityParameter:attribute];
        NSMutableArray *copyArray = [NSMutableArray arrayWithArray:array];
        [copyArray addObject:attribute];
        array = copyArray;
    }
    return array;
}

/*! @brief Calculate visibility parameter.
 *  @param attribute Layout attributes.*/
- (void)calculateVisibilityParameter:(UICollectionViewLayoutAttributes *)attribute {
    CGFloat visibility = 1;
    attribute.zIndex = 10;
    CGFloat sign = 0;
    CGPoint contentOffset = self.collectionView.contentOffset;
    CGFloat width = self.collectionView.bounds.size.width;
    CGFloat offsetPersent = 0.3f;
    if (attribute.frame.origin.x > contentOffset.x + width * (1 - offsetPersent)) {
        visibility = attribute.frame.origin.x - (contentOffset.x + width * (1 - offsetPersent));
        visibility /= (width * offsetPersent);
        visibility = 1 - visibility;
        attribute.zIndex = 0;
        sign = -1;
    }
    else if (contentOffset.x > attribute.frame.origin.x  + width * (1 - offsetPersent)) {
        visibility = contentOffset.x - (attribute.frame.origin.x  + width * (1 - offsetPersent));
        visibility /= (width * offsetPersent);
        visibility = 1 - visibility;
        attribute.zIndex = 0;
        sign = 1;
    }
    attribute.alpha = 0.2 + 0.8 * visibility;
    CGFloat scale = 0.8 + 0.2 * visibility;
    CGAffineTransform transform = CGAffineTransformMakeScale(scale, scale);
    CGRect newBounds = CGRectApplyAffineTransform(attribute.bounds, transform);
    CGFloat offset = (attribute.bounds.size.width - newBounds.size.width) / 2.f;
    transform.tx = offset * sign - offsetForVisibility;
    attribute.transform = transform;
}

@end
