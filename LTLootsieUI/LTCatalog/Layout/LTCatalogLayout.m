//  LTCatalogLayout.h
//  Created by Dovbnya Alexander on 5/31/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTCatalogLayout.h"

@implementation LTCatalogLayout

/*! @brief The collection view calls -prepareLayout once at its first layout as the first message to the layout instance.
 *  The collection view calls -prepareLayout again after layout is invalidated and before requerying the layout information.*/
- (void)prepareLayout {
    [super prepareLayout];
    CGPoint contentOffset = self.collectionView.contentOffset;
    CGFloat heightCollection = self.collectionView.bounds.size.height;
    CGFloat widthCollection = self.collectionView.bounds.size.width;
    CGRect corectBounds = CGRectMake(0, 0, widthCollection, heightCollection);
    NSArray<NSIndexPath *> *indexPaths = [self.collectionView indexPathsForVisibleItems];
    for (NSIndexPath *indexPath in indexPaths) {
        UICollectionViewLayoutAttributes *attribute = [self layoutAttributesForItemAtIndexPath:indexPath];
        CGFloat offset = attribute.frame.origin.y - contentOffset.y;
        CGFloat alpha = (heightCollection - offset - 20) / (attribute.frame.size.height * 0.75f);
        if (attribute.frame.origin.x > widthCollection / 2.f) {
            alpha -= 0.6f;
        }
        alpha = alpha < 0 ? 0 : alpha;
        alpha = alpha > 1 ? 1 : alpha;
        attribute.alpha = CGRectIntersectsRect(corectBounds, attribute.frame) ? 1 : alpha;
    }
}

/*! @brief Return YES to cause the collection view to requery the layout for geometry information.
 *  @param newBounds New bounds value.*/
- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

@end
