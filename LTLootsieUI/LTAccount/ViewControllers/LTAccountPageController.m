//  LTAccountPageController.m
//  Created by Fabio Teles on 5/19/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTAccountPageController.h"
#import "LTUIManager.h"
#import "LTManager+Account.h"
#import "LTManager+Private.h"
#import "LTUIManager+Account.h"

#import "LTUserAccount.h"

#import "LTData.h"

#import "LTAlertOperation.h"
#import "LTMWLogging.h"
#import "LTTools.h"

#import "LTTheme.h"
#import "LTEditButton.h"
#import "LTSecondaryButton.h"
#import "LTPrimaryButton.h"
#import "LTContactSupportButton.h"

#import <MessageUI/MessageUI.h>

typedef NS_ENUM(NSInteger, ScrollDirection) {
    ScrollDirectionNone,
    ScrollDirectionRight,
    ScrollDirectionLeft,
    ScrollDirectionUp,
    ScrollDirectionDown,
    ScrollDirectionCrazy,
};


@interface LTAccountPageController () <UITextFieldDelegate, MFMailComposeViewControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate>
/*! User account data.*/
@property (strong, nonatomic) LTUserAccount *displayUserAccount;

/*! Field for set/udpate user email.*/
@property (nonatomic, weak) IBOutlet UITextField* emailField;

@property (weak, nonatomic) IBOutlet UIView *topContainer;
@property (weak, nonatomic) IBOutlet UIView *bottomContainer;

@property (weak, nonatomic) IBOutlet UIScrollView *deactivateScrollContainer;
@property (weak, nonatomic) IBOutlet UIView *deactivateContentContainer;

@property (weak, nonatomic) IBOutlet UIView *emailFormContainer;
@property (weak, nonatomic) IBOutlet UIImageView *emailIcon;

/*! Button for enable/disable edit mode.*/
@property (weak, nonatomic) IBOutlet LTEditButton *editButton;

/*! Button for show terms infromation.*/
@property (weak, nonatomic) IBOutlet UIButton *termsButton;

/*! Button for show about infromation.*/
@property (weak, nonatomic) IBOutlet UIButton *aboutButton;

/*! Button for deactivate/logout account.*/
@property (weak, nonatomic) IBOutlet LTPrimaryButton *logoutButton;

/*! Button for show contact support.*/
@property (weak, nonatomic) IBOutlet LTContactSupportButton* contactSupportButton;

/*! Edit line view.*/
@property (weak, nonatomic) IBOutlet UIView *editLineView;

@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

/*! Edit line height constrain.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *editLineHeightConstrain;
@property (weak, nonatomic) IBOutlet UIView *contactContentView;
@property (weak, nonatomic) IBOutlet UIView *deactivateContentView;
@property (weak, nonatomic) IBOutlet UILabel *needHelpLabel;

/*! Button deactivate bottom offset constrain.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoutButtonBottomOffset;

/*! Button deactivate width constrain.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *logoutButtonWidth;

/*! Button contact support width constrain.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contactSupportButtonWidth;


@property (weak, nonatomic) IBOutlet UIImageView *directionArrow;

@property (nonatomic, assign) CGFloat lastContentOffset;
@property (nonatomic, assign) ScrollDirection scrollDirection;;

@end

@implementation LTAccountPageController

/*! @brief Create a new object.*/
- (instancetype)init {
    if (self = [super init]) {
        self.title = NSLocalizedStringFromTable(@"Account", @"LTLootsie", nil);
    }
    return self;
}

/*! @brief Clear memory when object was released*/
- (void)dealloc {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    self.deactivateScrollContainer.contentSize = self.deactivateContentContainer.frame.size;
}

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
	[super viewDidLoad];
    
    self.topContainer.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    self.bottomContainer.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    
    self.emailFormContainer.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];

    self.emailIcon.image = [self.emailIcon.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    self.emailIcon.tintColor = [[LTTheme sharedTheme] colorWithType:LTColorAltBackground4];
    
    self.editing = NO;
    self.contactSupportButtonWidth.constant = ([UIScreen mainScreen].bounds.size.height < 736.0f) ? 178.0f : 200.0f;
    self.logoutButtonWidth.constant = ([UIScreen mainScreen].bounds.size.height < 736.0f) ? 178.0f : 200.0f;
    
    self.view.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    
    self.contactContentView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    
    self.contactContentView.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorAltButton1];
    
    self.deactivateContentView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    
    self.deactivateContentView.backgroundColor = [UIColor clearColor];
    self.deactivateContentView.layer.borderColor = [[LTTheme sharedTheme] colorWithType: LTColorAltButton1].CGColor;
    self.deactivateContentView.layer.borderWidth = 1.0;

    self.needHelpLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];
    
    // User update
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleUserUpdate:) name:LTUserAccountUpdatedNotification object:nil];
    self.editLineHeightConstrain.constant = 1.0 / [UIScreen mainScreen].scale;
    
    NSDictionary* attributes = @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily:LTPrimaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:LTMediumBFontSize],
                                 NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText],
                                 NSUnderlineStyleAttributeName: @1};
    
    NSAttributedString* termsString = [[NSAttributedString alloc] initWithString:self.termsButton.currentTitle attributes: attributes];
    NSAttributedString* aboutString = [[NSAttributedString alloc] initWithString:self.aboutButton.currentTitle attributes: attributes];
    
    [self.termsButton setAttributedTitle: termsString forState: UIControlStateNormal];
    [self.aboutButton setAttributedTitle: aboutString forState: UIControlStateNormal];
    
    self.logoutButtonBottomOffset.constant = ([UIScreen mainScreen].bounds.size.height >= 667.0f) ? 42.0f : 28.0f;
    self.logoutButton.alpha = 1.0f; // Changed this to permanently visible
    
    [self updatePlaceholder];
    self.emailField.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];
    self.emailField.tag = 2;
    
    self.emailLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorAltText1];
    self.editLineView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryDiv];
    
    self.deactivateScrollContainer.delegate = self;
    
    self.contactContentView.layer.cornerRadius = 4;
    self.contactContentView.layer.masksToBounds = true;
    
    self.emailFormContainer.layer.cornerRadius = 4;
    self.emailFormContainer.layer.masksToBounds = true;
    
    //
    
//    CGRect bounds = self.deactivateScrollContainer.bounds;
//    
//    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"bounds"];
//    animation.duration = 1.0;
//    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    
//    animation.fromValue = [NSValue valueWithCGRect:bounds];
//    
//    bounds.origin.x += 200;
//    
//    animation.toValue = [NSValue valueWithCGRect:bounds];
//    
//    [self.deactivateScrollContainer.layer addAnimation:animation forKey:@"bounds"];
//    
//    self.deactivateScrollContainer.bounds = bounds;
}

/*! @brief Called when controller will appear on screen.
 *  @param animated YES if controller will appear with animation.*/
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
}

/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*! @brief Called when user account was updated.
 *  @param notification Object with new data.*/
- (void)handleUserUpdate:(NSNotification *)notification {
    LTUserAccount *account = (LTUserAccount *)[notification.userInfo objectForKeyNilSafe: LTUserUserInfoKey];
    [self updateDataWithUserAccount: account];
}

/*! @brief Udpate placeholder UI for email field.*/
- (void)updatePlaceholder {
    NSDictionary* placeholderAttribute = @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                                                                 weight: LTMediumFontWeight
                                                                                                  style: LTRegularFontStyle
                                                                                                   size: LTMediumAFontSize],
                                           NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorAltText4]};
    
    self.emailField.attributedPlaceholder = [[NSAttributedString alloc] initWithString: self.emailField.placeholder
                                                                            attributes: placeholderAttribute];
}

#pragma mark - UITextFieldDelegate
/*! @brief Called when user click to return (done) button on the keyboard.
 *  @param textField Text field object.*/
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    [self handleEditEmail: self.editButton];
    return YES;
}

#pragma mark - Base overwrite methods

- (void)needUpdateData {
    [super needUpdateData];
    
    [[LTManager sharedManager] userAccountWithSuccess:^(LTUserAccount *userAccount) {
        [self updateDataWithUserAccount:userAccount];
    } failure:nil];
}

#pragma mark - Data
/*! @brief Update UI with account info.
 *  @param userAccount User's info object.*/
- (void)updateDataWithUserAccount:(LTUserAccount *)userAccount {
    self.displayUserAccount = userAccount;
    if (!userAccount) return;
    
    self.emailField.text = userAccount.email;

    // @TODO - Potential bug - exact float comparison

    /*

    if(self.emailField.text.length > 0 && self.logoutButton.alpha == 0.0) {
        [UIView animateWithDuration:0.3 animations:^{
            self.logoutButton.alpha = 1.0;
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        }];
    } else if(self.emailField.text.length == 0 && self.logoutButton.alpha == 1.0) {
        [UIView animateWithDuration:0.3 animations:^{
            self.logoutButton.alpha = 0.0;
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        }];
    }
     */
}

#pragma mark - MFMailComposeViewControllerDelegate
/*! @brief Called when need to close standard controller for send email.
 *  @param controller Controller for send email.
 *  @param result Result state.
 *  @param error Error object.*/
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(nullable NSError *)error {
    [controller dismissViewControllerAnimated: YES completion: nil];
}

#pragma mark Actions
/*! @brief Called when user click to merge button.
 *  @note Doesn't work for now.
 *  @param sender Button object.*/
- (IBAction)handleMergeOrLogout:(id)sender {
    if (!self.displayUserAccount) return;
    
    NSString *emailStr = self.emailField.text;
    if ([LTTools validateEmailWithString:emailStr]) {
        LTManager *manager = [LTManager sharedManager] ;
        [manager userAccountWithSuccess:^(LTUserAccount *userAccount) {
            userAccount.email = emailStr;
            [manager updateUserAccountWithAccount:userAccount success: nil failure:nil];
        } failure:nil];
    } else {
        LTLogDebug(@"Invalid e-mail address");
        [[LTUIManager sharedManager] showPopupWithTitle: NSLocalizedStringFromTable(@"Uh oh!", @"LTLootsie", nil)
                                            description: NSLocalizedStringFromTable(@"Invalid email address.", @"LTLootsie", nil)
                                            withSadFace: YES
                                             buttonText: NSLocalizedStringFromTable(@"OK", @"LTLootsie", nil)
                                         didCloseAction: nil];
    }
    
    [self.view endEditing:YES];
}

/*! @brief Called when user click to edit button.
 *  @param sender Button object.*/
- (IBAction)handleEditEmail:(id)sender {
    self.editing = !self.editing;
    self.editButton.selected = !self.editButton.selected;
    self.editLineView.hidden = (self.editing) ? NO : YES;
    self.emailField.enabled = (self.editing);

    if(!self.editing) {
        [self handleUpdateMyInfo];
    }
}

/*! @brief Called when user click to contact support button.
 *  @param sender Button object.*/
- (IBAction)handleContantSupport:(id)sender {
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController* mailController = [[MFMailComposeViewController alloc] init];
        mailController.delegate = self;
        mailController.mailComposeDelegate = self;
        [mailController setSubject: NSLocalizedStringFromTable(@"", @"LTLootsie", nil)];
        [mailController setToRecipients: @[@"support@lootsie.com"]];
        [self presentViewController: mailController animated: YES completion: nil];
    }
}

/*! @brief Called when user click to deactivate/logout button.
 *  @param sender Button object.*/
- (IBAction)handleLogoutClick:(UIButton*)sender {
    if(self.editing) {
        [self handleUpdateMyInfo];

    }
    
    [[LTUIManager sharedManager] showPopupWithTitle:NSLocalizedStringFromTable(@"CONFIRM", @"LTLootsie", nil) description:NSLocalizedStringFromTable(@"Are you sure you want to deactivate your Lootsie account? You will lose any points and will no longer have access to your reward catalog. This action cannot be undone!", @"LTLootsie", nil) leftButtonText:@"CANCEL" rightButtonText:@"DEACTIVATE" didCloseAction:nil];
    

}

/*! @brief Called when user click to about text button.
 *  @param sender Button object.*/
- (IBAction)handleAboutPage:(UIButton*)sender {
    CGRect frameInView = [self.view convertRect: sender.bounds fromView: sender];
    [[LTUIManager sharedManager] showAboutPageOnViewController:self fromStartPoint:CGPointMake(CGRectGetMidX(frameInView), CGRectGetMidY(frameInView))];
}

/*! @brief Called when user click to terms text button.
 *  @param sender Button object.*/
- (IBAction)handleTermsPage:(UIButton*)sender {
    CGRect frameInView = [self.view convertRect: sender.bounds fromView: sender];
    [[LTUIManager sharedManager] showTermsPageOnViewController:self fromStartPoint:CGPointMake(CGRectGetMidX(frameInView), CGRectGetMidY(frameInView))];
}

/*! @brief Update user's account.*/
- (void)handleUpdateMyInfo {
    LTLogDebug(@"Updating user account");
    
    NSLog(@"%@", self.emailField.text);
    NSLog(@"%@", self.emailField.placeholder);
    NSLog(@"%@", self.displayUserAccount.email);
    
    NSString *emailStringBuffer = [[NSUserDefaults standardUserDefaults]
                            stringForKey:@"emailStringBuffer"];
    
    if ([self.emailField.text isEqual: @""] || [self.emailField.text isEqual: emailStringBuffer]) {
        self.emailField.text = emailStringBuffer;
        self.emailField.placeholder = emailStringBuffer;
    } else if (![LTManager sharedManager].data.user.isGuest) {
        self.displayUserAccount.email = self.emailField.text;
        
        if ([LTTools validateEmailWithString: self.emailField.text]) {
            __weak typeof(self) tmp = self;
            [[LTManager sharedManager] updateUserAccountWithAccount: self.displayUserAccount success:^{
                if(tmp.displayUserAccount.confirmed == NO) {
                    [[LTUIManager sharedManager] showSuccessPopupWithTitle: NSLocalizedStringFromTable(@"Success!", @"LTLootsie", nil)
                                                        description: NSLocalizedStringFromTable(@"Thanks for signing up! Please check your email to confirm and activate your account.", @"LTLootsie", nil)
                                                         buttonText: NSLocalizedStringFromTable(@"OK", @"LTLootsie", nil)
                                                     didCloseAction: nil];
                } else {
                    [[LTUIManager sharedManager] showSuccessPopupWithTitle: NSLocalizedStringFromTable(@"Success!", @"LTLootsie", nil)
                                                        description: NSLocalizedStringFromTable(@"Account Updated!", @"LTLootsie", nil)
                                                         buttonText: NSLocalizedStringFromTable(@"OK", @"LTLootsie", nil)
                                                     didCloseAction: nil];
                }
            } failure:nil];
        } else {
            LTLogDebug(@"Invalid e-mail address");
            [[LTUIManager sharedManager] showPopupWithTitle: NSLocalizedStringFromTable(@"Uh oh!", @"LTLootsie", nil)
                                                description: NSLocalizedStringFromTable(@"Email address which you are entered is incorrect. Please check it and try again.", @"LTLootsie", nil)
                                                withSadFace: YES
                                                 buttonText: NSLocalizedStringFromTable(@"OK", @"LTLootsie", nil)
                                             didCloseAction: nil];
        }
    }

    [self.view endEditing:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 2) {
        
        NSLog(@"text: %@", self.emailField.text);
        NSLog(@"placeholder: %@", self.emailField.placeholder);
        
        if (![self.emailField.text isEqual: @""] || ![self.emailField.placeholder isEqual: @""]) {
            [[NSUserDefaults standardUserDefaults] setObject:self.emailField.placeholder forKey:@"emailStringBuffer"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            self.emailField.text = @"";
            self.emailField.placeholder = @"";
        }
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 2) {
        
        NSLog(@"tesxt%@", self.emailField.text);
        NSLog(@"%@", self.emailField.placeholder);
    }
}

#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)

#pragma mark Scroll
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat maxOffsetY = scrollView.contentSize.height - scrollView.frame.size.height;
    
    self.aboutButton.alpha = 1 - scrollView.contentOffset.y/maxOffsetY * 2;
    self.termsButton.alpha = 1 - scrollView.contentOffset.y/maxOffsetY * 2;
    
    if (self.lastContentOffset > scrollView.contentOffset.y) {
        self.scrollDirection = ScrollDirectionDown;
        
        self.directionArrow.transform = CGAffineTransformMakeRotation(-DEGREES_TO_RADIANS(scrollView.contentOffset.y/maxOffsetY * 180));
    } else if (self.lastContentOffset < scrollView.contentOffset.y) {
        self.scrollDirection = ScrollDirectionUp;
        
        self.directionArrow.transform = CGAffineTransformMakeRotation(-DEGREES_TO_RADIANS(scrollView.contentOffset.y/maxOffsetY * 180));
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset NS_AVAILABLE_IOS(5_0) {
    
}
// called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    CGFloat maxOffsetY = scrollView.contentSize.height - scrollView.frame.size.height;

    if (self.scrollDirection == ScrollDirectionUp) {
        [UIView animateWithDuration:0.1
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [scrollView setContentOffset:CGPointMake(0, maxOffsetY) animated:NO];
                         } completion:^(BOOL finished){}];
    } else {

        [UIView animateWithDuration:0.1
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [scrollView setContentOffset:CGPointMake(0, 0) animated:NO];
                         } completion:^(BOOL finished){}];
    }

}

@end
