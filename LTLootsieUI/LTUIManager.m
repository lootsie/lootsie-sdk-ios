//  LTUIManager.m
//  Created by Fabio Teles on 5/19/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTUIManager.h"
#import "LTManager+Private.h"

#import "LTRootViewController.h"
#import "LTViewController.h"

#import "LTAchievementPopupViewController.h"
#import "LTPopupViewController.h"
#import "LTPopupTwoButtonViewController.h"
#import "LTPopupRegistrationSuccessViewController.h"

#import "LTOperationQueue.h"
#import "LTAlertOperation.h"
#import "LTBlockOperation.h"

#import "LTMutuallyExclusiveCondition.h"

#import "LTInitSequenceOperation.h"
#import "LTRedemptionSequenceOperation.h"
#import "LTFavoritesPageController.h"
#import "LTAchievement.h"
#import "LTUIManager+Achievement.h"
#import "LTUIManager+Interstitial.h"

#import "LTNetworkManager.h"

#import "LTApp.h"
#import "LTData.h"

#import "LTConstants.h"
#import "LTMWLogging.h"
#import "LTTools.h"

#import "LTTermsPageController.h"
#import "LTAboutPageController.h"

#import "LTLootsieWindow.h"
#import "LTUIAnimationDelegate.h"

#import "LTIANItem.h"
#import "LTIANViewController.h"
#import "LTFTContainerController.h"

NSString* LTUIStoryboardName               = @"LTInterface";

NSString* const LTDismissedAlertKey        = @"com.lootsie.ui.manager.dismissedAlert";

@interface LTUIManager () <LTOperationQueueDelegate>

/*! Special controller for show all UI elements from Lootsie SDK*/
@property (nonatomic, strong) UIViewController *wrapRootViewController;

/*! Storyboard for show Lootsie UI main page.*/
@property (nonatomic, strong) UIStoryboard *resourcesStoryboard;

/*! Core manager for get access to Lootise Core methods.*/
@property (nonatomic, weak) LTManager *coreManager;

/*! Lootsie SDK main window. Using for show all UI component. */
@property (nonatomic, strong) LTLootsieWindow* window;

/*! Main controller with main UI elements.*/
@property (nonatomic, strong) LTViewController* mainController;

/*! Controller for show IAN item(s).*/
@property (nonatomic, weak) LTIANViewController *ianController;

/*! @brief Add constrain for view using another view.
 *  @param view View which will use constrain.
 *  @param parent Parent view.
 *  @param attr Constrain attributes.*/
- (void)addConstraint:(UIView *)view toView:(UIView *)parent attribute:(NSLayoutAttribute)attr;

/*! @brief Get supported interface orientaion mask.*/
- (UIInterfaceOrientationMask)supportedInterfaceOrientations;

/*! @brief Show controller with animation.
 *  @param controller Controller which need to show.
 *  @param parentController Parent controller where will show controller.
 *  @param startPoint Start point for start position of animation.*/
- (void)showController:(UIViewController*)controller onController:(UIViewController*)parentController withStartPoint:(CGPoint)startPoint;

/*! @brief Get parent controller for source controller.
 *  @param controller Source controller.
 *  @return Parent controller.*/
- (UIViewController *)parentController:(UIViewController *)controller;

/*! @brief Add operation to main queue.
 *  @param opeartion New operation object.*/
- (void)addOperationToQueue:(NSOperation *)opeartion;

/*! @brief Show first time wizard on a special controller.
 *  @param parentController Parent controller where need to show wizard.
 *  @param startPoint Start point where will start play animation for show.*/
- (void)showFirstTimeWizardOnViewController:(UIViewController *)parentController fromStartPoint:(CGPoint)startPoint;
@end

@implementation LTUIManager

+ (instancetype)sharedManager {
    static LTUIManager *_engine;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _engine = [self new];
    });
    return _engine;
}

/*! @brief Create a new object.*/
- (instancetype)init {
    if (self = [super init]) {
        _displayAlertsOverAppUserInterface = YES;
        _mainQueue = [NSOperationQueue mainQueue];
        self.coreManager = [LTManager sharedManager];
        [self.coreManager setOperationDelegate:self];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showIan:) name:LTIANNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(disableLootsie:)
                                                     name:LTDisableLootsieNotification
                                                   object:nil];
    }
    return self;
}

/*! @brief Called when object was released.*/
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
- (void)applyTheme:(LTThemeType)type {
    [[LTTheme sharedTheme] configureColorWithThemeType:type];
}
- (void)showIan:(NSNotification *)notif {
    NSArray *ians = [notif userInfo][LTIANKey];
    [self showIANWithItems:ians bannerTitle:@"NEW NOTIFICATIONS" handlerAction:nil handlerClose:nil];
}

#pragma mark - private
- (void)addConstraint:(UIView *)view toView:(UIView *)parent attribute:(NSLayoutAttribute)attr {
    [parent addConstraint:[NSLayoutConstraint constraintWithItem:view
                                                       attribute:attr
                                                       relatedBy:NSLayoutRelationEqual
                                                          toItem:parent
                                                       attribute:attr
                                                      multiplier:1
                                                        constant:0]];
    
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    UIViewController *controller = [self topViewControllerWithRootViewController:self.wrapRootViewController];
    return [controller supportedInterfaceOrientations];
}
- (void)showController:(UIViewController*)controller onController:(UIViewController*)parentController withStartPoint:(CGPoint)startPoint {
    parentController = [self parentController:parentController];
    [parentController addChildViewController:controller];
    
    // Set the frame of the next view controller to equal the outgoing (current) view controller
    controller.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    controller.view.frame = parentController.view.bounds;
    controller.view.translatesAutoresizingMaskIntoConstraints = YES;
    [parentController.view addSubview:controller.view];
    [controller.view layoutIfNeeded];
    
    if([controller conformsToProtocol:@protocol(LTUIAnimationDelegate)]) {
        if([controller respondsToSelector:@selector(animateFromStartPoint:completion:)]) {
            [(id<LTUIAnimationDelegate>)controller animateFromStartPoint: startPoint completion: ^{
                [controller didMoveToParentViewController:parentController];
            }];
        }
    } else {
        controller.view.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorModalBackground];
        controller.view.transform = CGAffineTransformMakeTranslation(0, parentController.view.frame.size.height);
        [UIView animateWithDuration:0.3f
                         animations:^{
                             controller.view.transform = CGAffineTransformIdentity;
                         } completion:^(BOOL finished) {
                             [controller didMoveToParentViewController:parentController];
                         }];
    }
}
- (UIViewController *)parentController:(UIViewController *)controller {
    if(!controller) {
        controller = [self topViewControllerWithRootViewController: self.wrapRootViewController];
        UIPageViewController* pagerController = nil;
        for(UIViewController* vController in controller.childViewControllers) {
            if([vController isKindOfClass: [UIPageViewController class]]) {
                pagerController = (UIPageViewController*)vController;
                break;
            }
        }
        if(pagerController) {
            controller = [pagerController.viewControllers lastObject];
        }
    }
    return controller;
}
- (void)addOperationToQueue:(NSOperation *)opeartion {
    if (!self.coreManager.data.app.enabled) {
        return;
    }
    [self->_mainQueue addOperation:opeartion];
}

#pragma mark - Properties
/*! @brief Getter for resource storyboard property.
 *  @return Storyboard object.*/
- (UIStoryboard *)resourcesStoryboard {
    if (!_resourcesStoryboard) {
        NSBundle *bundle = [NSBundle bundleForClass:[self class]];
        _resourcesStoryboard = [UIStoryboard storyboardWithName:LTUIStoryboardName bundle:bundle];
    }
    return _resourcesStoryboard;
}

/*! @brief Getter for wrap controller property.
 *  @return Wrap controller.*/
- (UIViewController *)wrapRootViewController {
    if (!_wrapRootViewController) {
        _wrapRootViewController = [[LTRootViewController alloc] init];
    }
    return _wrapRootViewController;
}

/*! @brief Getter for lootise SDK window property.
 *  @return Lootise SDK window.*/
- (LTLootsieWindow*)window {
    if(!_window) {
        _window = [LTLootsieWindow createWindow];
        _window.rootViewController = self.wrapRootViewController;
        _window.hidden = NO;
    }
    
    return _window;
}

/*! @brief Getter for main controller property.
 *  @return Main controller.*/
- (LTViewController *)mainController {
    if (_mainController == nil) {
        _mainController = [self.resourcesStoryboard instantiateViewControllerWithIdentifier:@"MainScene"];
    }
    return _mainController;
}

#pragma mark - Pages UI
- (void)showPageWithSegueIdentifier:(Class)classController {
    if ([LTManager sharedManager].status == LTSDKStatusFailed) {
        LTLogCritical(@"Lootsie's SDK has failed to initialize");
        return;
    }
    
    if (self.mainController.presentingViewController == nil) {
        // wraps showing the UI in a operation so we can make it mutually exclusive
        NSBlockOperation *showWindow = [NSBlockOperation blockOperationWithBlock:^{
            [self willShowLootsieUI:nil];
        }];
        [self addOperationToQueue:showWindow];
        NSBlockOperation *showUI = [NSBlockOperation blockOperationWithBlock:^{
            [self.wrapRootViewController presentViewController:self.mainController animated:YES completion:nil];
        }];
        [self addOperationToQueue:showUI];
    }
    
    NSBlockOperation *displayBlock = [NSBlockOperation blockOperationWithBlock:^{
        [self.mainController showViewControllerWithClass:classController];
    }];
    [self addOperationToQueue:displayBlock];
}
- (void)showTermsPageOnViewController:(nonnull UIViewController *)parentController fromStartPoint:(CGPoint)startPoint {
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        LTTermsPageController *controller = [[LTTermsPageController alloc] initWithNibName:@"LTTermsPageController" bundle:nil];
        [self showController: controller onController: parentController withStartPoint: startPoint];
    }];
    [self addOperationToQueue:operation];
}
- (void)showAboutPageOnViewController:(nonnull UIViewController *)parentController fromStartPoint:(CGPoint)startPoint {
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        LTAboutPageController *controller = [[LTAboutPageController alloc] initWithNibName:@"LTAboutPageController" bundle:nil];
        [self showController: controller onController: parentController withStartPoint: startPoint];
    }];
    [self addOperationToQueue:operation];
}
- (void)showFirstTimeWizardOnViewController:(nonnull UIViewController *)parentController fromStartPoint:(CGPoint)startPoint {
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        LTFTContainerController *controller = [[LTFTContainerController alloc] initWithNibName: @"LTFTContainerController" bundle:nil];
        [self showController:controller onController:parentController withStartPoint:startPoint];
    }];
    [self->_mainQueue addOperation:operation];
}
- (BOOL)isShowingUI {
    return (nil != self.wrapRootViewController.presentedViewController) || self.wrapRootViewController.childViewControllers.count > 0 ;
}
- (void)hideViewController:(nonnull UIViewController *)controller {
    [self hideViewController:controller withComplition:nil];
}
- (void)hideViewController:(nonnull UIViewController *)controller withComplition:(void(^__nullable)(void))callBack {
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        if (controller.parentViewController != nil) {
            UIViewController *sourceController = controller;
            [sourceController willMoveToParentViewController:nil];
            [UIView animateWithDuration:0.3f
                             animations:^{
                                 sourceController.view.alpha = 0;
                             } completion:^(BOOL finished) {
                                 [sourceController.view removeFromSuperview];
                                 [sourceController removeFromParentViewController];
                                 if (callBack != nil) {
                                     callBack();
                                 }
                                 if (![self isShowingUI]) {
                                     [self notifyToClose];
                                 }
                             }];
        }
        else {
            [controller dismissViewControllerAnimated:YES completion:callBack];
        }
    }];
    [self addOperationToQueue:operation];
}
- (void)closeUI {
    if ([self isShowingUI]) {
        // wraps showing the UI in a operation so we can make it mutually exclusive
        __weak __typeof(self) weakSelf = self;
        NSBlockOperation *closeUIOP = [NSBlockOperation blockOperationWithBlock:^{
            __strong __typeof(weakSelf) strongSelf = weakSelf;
            BOOL isAnimated = weakSelf.wrapRootViewController.presentedViewController != nil;
            [strongSelf.wrapRootViewController dismissViewControllerAnimated:isAnimated completion:^{
                [strongSelf notifyToClose];
            }];
        }];
        [self->_mainQueue addOperation:closeUIOP];
    }
}
- (void)notifyToClose {
    // notify delegate of closing UI
    if ([self.delegate respondsToSelector:@selector(lootsieUIManagerDidCloseUI)]) {
        [self.delegate lootsieUIManagerDidCloseUI];
    }
    [UIViewController attemptRotationToDeviceOrientation];
    if(self.wrapRootViewController.presentingViewController) {
        [self.wrapRootViewController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
    }
}

#pragma mark - Alerts
- (void)showAlertWithError:(nonnull NSError *)error {
    [self showAlertWithError: error closeAction: nil];
}

/*! @brief Easy way to show an alert dialog by providing a NSError object.
 *  @param error Error object with error info.
 *  @param closeAction Callback is called when alert was closed.*/
- (void)showAlertWithError:(NSError *)error closeAction:(void (^ __nullable)(void))closeAction {
    if (self.displayAlertsOverAppUserInterface || (!self.displayAlertsOverAppUserInterface && [self isShowingUI])) {
        [[LTUIManager sharedManager] showPopupWithTitle: error.localizedDescription
                                            description: error.userInfo[NSLocalizedRecoverySuggestionErrorKey]
                                            withSadFace: YES
                                             buttonText: NSLocalizedStringFromTable(@"OK", @"LTLootsie", nil)
                                         didCloseAction: closeAction];
    }
}

#pragma mark - Popup
- (void)showPopupWithTitle:(nonnull NSString *)title description:(nonnull NSString *)description withSadFace:(BOOL)isShowIcon buttonText:(nonnull NSString *)buttonText didCloseAction:(void (^ __nullable)(void))closeAction {
    
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSString *controllerNib = isShowIcon ? @"LTPopupViewController" : @"LTPopupViewControllerWithoutSadFace";
        LTPopupViewController *controller = [[LTPopupViewController alloc] initWithNibName:controllerNib bundle:nil];
        
        [controller setTitleText:title];
        [controller setDescriptionText:description];
        [controller setButtonText:buttonText];
        controller.modalPresentationStyle = UIModalPresentationCustom;
        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        if (![self isShowingUI]) {
            [self willShowLootsieUI:nil];
            controller.closeAction = ^{
                if (closeAction != nil) {
                    closeAction();
                }
                [self closeUI];
            };
        }
        else {
            controller.closeAction = closeAction;
        }
        UIViewController *topController = [self topViewControllerWithRootViewController:self.wrapRootViewController];
        [topController presentViewController:controller animated:YES completion:nil];
    }];
    [self addOperationToQueue:operation];
}

- (void)showSuccessPopupWithTitle:(nonnull NSString *)title description:(nonnull NSString *)description buttonText:(nonnull NSString *)buttonText didCloseAction:(void (^ __nullable)(void))closeAction {
    
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSString *controllerNib = @"LTPopupRegistrationSuccessViewController";
        LTPopupRegistrationSuccessViewController *controller = [[LTPopupRegistrationSuccessViewController alloc] initWithNibName:controllerNib bundle:nil];
        
        [controller setTitleText:title];
        [controller setDescriptionText:description];
        [controller setButtonText:buttonText];
        controller.modalPresentationStyle = UIModalPresentationCustom;
        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        if (![self isShowingUI]) {
            [self willShowLootsieUI:nil];
            controller.closeAction = ^{
                if (closeAction != nil) {
                    closeAction();
                }
                [self closeUI];
            };
        }
        else {
            controller.closeAction = closeAction;
        }
        UIViewController *topController = [self topViewControllerWithRootViewController:self.wrapRootViewController];
        [topController presentViewController:controller animated:YES completion:nil];
    }];
    [self addOperationToQueue:operation];
}

// Shows popup with 2 buttons
- (void)showPopupWithTitle:(nonnull NSString *)title description:(nonnull NSString *)description leftButtonText:(nonnull NSString *)leftButtonText rightButtonText:(nonnull NSString *)rightButtonText didCloseAction:(void (^ __nullable)(void))closeAction {
    
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        
        NSString *popUpViewController = @"LTPopupTwoButtonViewController";
        
        LTPopupTwoButtonViewController *controller = [[LTPopupTwoButtonViewController alloc] initWithNibName:popUpViewController bundle:nil];
        
        [controller setTitleText:title];
        [controller setDescriptionText:description];
        [controller setLeftButtonText:leftButtonText];
        [controller setRightButtonText:rightButtonText];
        controller.modalPresentationStyle = UIModalPresentationCustom;
        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        if (![self isShowingUI]) {
            [self willShowLootsieUI:nil];
            controller.closeAction = ^{
                if (closeAction != nil) {
                    closeAction();
                }
                [self closeUI];
            };
        }
        else {
            controller.closeAction = closeAction;
        }
        UIViewController *topController = [self topViewControllerWithRootViewController:self.wrapRootViewController];
        [topController presentViewController:controller animated:YES completion:nil];
    }];
    [self addOperationToQueue:operation];
}

- (void)showAchievementPopupWithTitle:(nonnull NSString *)title description:(nonnull NSString *)description difficulty:(LTAchievementDifficulty) difficulty wasAchieved:(BOOL)wasAchieved buttonText:(nonnull NSString *)buttonText didCloseAction:(void (^ __nullable)(void))closeAction {
    NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
        NSString *controllerNib = @"LTAchievementPopupViewController";
        
        LTAchievementPopupViewController *controller = [[LTAchievementPopupViewController alloc] initWithNibName:controllerNib bundle:nil];
        [controller setTitleText:title];
        [controller setDescriptionText:description];
        [controller setButtonText:buttonText];
        [controller setDifficulty:difficulty];
        controller.wasAchieved = wasAchieved;
        
        controller.modalPresentationStyle = UIModalPresentationCustom;
        controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        
        if (![self isShowingUI]) {
            [self willShowLootsieUI:nil];
            controller.closeAction = ^{
                if (closeAction != nil) {
                    closeAction();
                }
                [self closeUI];
            };
        }
        else {
            controller.closeAction = closeAction;
        }
        UIViewController *topController = [self topViewControllerWithRootViewController:self.wrapRootViewController];
        [topController presentViewController:controller animated:YES completion:nil];
    }];
    [self addOperationToQueue:operation];
}

#pragma mark - IAN methods
- (void)showIANWithItem: (nonnull LTIANItem*)item bannerTitle:(nonnull NSString*)bannerTitle handlerAction:(void(^__nullable)(LTIANItem* _Nonnull  item))onAction handlerClose:(void(^__nullable)())onClose {
    [self showIANWithItems:@[item] bannerTitle:bannerTitle handlerAction:onAction handlerClose:onClose];
}
- (void)showIANWithItems: (nonnull NSArray<LTIANItem*> *)items bannerTitle:(nonnull NSString*)bannerTitle handlerAction:(void(^__nullable)(LTIANItem* _Nonnull  item))onAction handlerClose:(void(^__nullable)())onClose {
    if(items.count > 0 && self.ianController == nil) {
        NSBlockOperation *operation = [NSBlockOperation blockOperationWithBlock:^{
            LTIANViewController* ianController = [[LTIANViewController alloc] initWithNibName: @"LTIANViewController" bundle: nil];
            ianController.items = items;
            
            /*
             We use the supplied bannerTitle only if there are more than one ian's to display,
             otherwise we use the one supplied with the ian.
             */
            if(items.count == 1 && items[0].title != nil) {
                ianController.bannerTitle = items[0].title;
                
            } else {
                ianController.bannerTitle = bannerTitle;
            }
            
            ianController.modalPresentationStyle = UIModalPresentationCustom;
            ianController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
            if (![self isShowingUI]) {
                [self willShowLootsieUI:nil];
            }
            ianController.onClose = onClose;
            ianController.onAction = onAction;
            
            UIViewController *topController = [self topViewControllerWithRootViewController:self.wrapRootViewController];
            [topController addChildViewController:ianController];
            [topController.view addSubview:ianController.view];
            ianController.view.translatesAutoresizingMaskIntoConstraints = NO;
            
            
            /*
             The following code provides the constraints for the ian view. If a header view is found the
             notifications will be shown below that header view, overlaying the tab bar.
             */
            if ([topController conformsToProtocol:@protocol(LTIANHaderView)]) {
                id<LTIANHaderView> headerDelegate = (id<LTIANHaderView>)topController;
                UIView *view = [headerDelegate headerViewForIan];
                
                [topController.view addConstraint:[NSLayoutConstraint constraintWithItem:ianController.view
                                                                               attribute:NSLayoutAttributeTop
                                                                               relatedBy:NSLayoutRelationEqual
                                                                                  toItem:view
                                                                               attribute:NSLayoutAttributeBottom
                                                                              multiplier:1
                                                                                constant:0]];
                /*
                 [topController.view addConstraint:[NSLayoutConstraint constraintWithItem: ianController.view
                 attribute: NSLayoutAttributeTop
                 relatedBy: NSLayoutRelationEqual
                 toItem: topController.topLayoutGuide
                 attribute: NSLayoutAttributeTop
                 multiplier: 1
                 constant: 54]];
                 */
                
                [topController.view addConstraint:[NSLayoutConstraint constraintWithItem: ianController.bannerView.contantView
                                                                               attribute: NSLayoutAttributeTop
                                                                               relatedBy: NSLayoutRelationEqual
                                                                                  toItem: topController.topLayoutGuide
                                                                               attribute: NSLayoutAttributeBottom
                                                                              multiplier: 1
                                                                                constant: 40]];
                
            }
            else {
                [topController.view addConstraint:[NSLayoutConstraint constraintWithItem: ianController.view
                                                                               attribute: NSLayoutAttributeTop
                                                                               relatedBy: NSLayoutRelationEqual
                                                                                  toItem: topController.topLayoutGuide
                                                                               attribute: NSLayoutAttributeTop
                                                                              multiplier: 1
                                                                                constant: 0]];
                [topController.view addConstraint:[NSLayoutConstraint constraintWithItem: ianController.bannerView.contantView
                                                                               attribute: NSLayoutAttributeTop
                                                                               relatedBy: NSLayoutRelationEqual
                                                                                  toItem: topController.topLayoutGuide
                                                                               attribute: NSLayoutAttributeBottom
                                                                              multiplier: 1
                                                                                constant: 0]];
            }
            [self addConstraint:ianController.view toView:topController.view attribute:NSLayoutAttributeBottom];
            [self addConstraint:ianController.view toView:topController.view attribute:NSLayoutAttributeLeft];
            [self addConstraint:ianController.view toView:topController.view attribute:NSLayoutAttributeRight];
            [ianController didMoveToParentViewController:topController];
            self.ianController = ianController;
        }];
        [self addOperationToQueue:operation];
    }
}

#pragma mark - Presentation
/*! @brief Gettert for presentation context property.
 *  @return Controller for presentation.*/
- (UIViewController *)presentationContext {
    UIViewController *context;
    if ([self.delegate respondsToSelector:@selector(presentationContextForUIManager)]) {
        context = [self.delegate presentationContextForUIManager];
    }
    
    if (!context) {
        if(self.window.hidden) {
            self.window.hidden = NO;
        }
        context = [self.window visibleViewController];
    }
    
    return context;
}
- (UIViewController *)topViewControllerWithRootViewController:(UIViewController *)rootViewController witchChild:(BOOL)withChild {
    if (withChild && rootViewController.childViewControllers.count > 0) {
        return [self topViewControllerWithRootViewController:[rootViewController.childViewControllers lastObject] witchChild:withChild];
    }
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController* tabBarController = (UITabBarController*)rootViewController;
        return [self topViewControllerWithRootViewController:tabBarController.selectedViewController witchChild:withChild];
    } else if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController* navigationController = (UINavigationController*)rootViewController;
        return [self topViewControllerWithRootViewController:navigationController.visibleViewController witchChild:withChild];
    } else if (rootViewController.presentedViewController && !rootViewController.presentedViewController.isBeingDismissed) {
        UIViewController* presentedViewController = rootViewController.presentedViewController;
        return [self topViewControllerWithRootViewController:presentedViewController witchChild:withChild];
    } else {
        return rootViewController;
    }
}

/*! @brief Get top of controller from root controller.
 *  @param rootViewController Parent controller which wchih will need to start search.*/
- (UIViewController*)topViewControllerWithRootViewController:(UIViewController*)rootViewController {
    return [self topViewControllerWithRootViewController:rootViewController witchChild:NO];
}

/*! @brief Called Lootsie UI will show.
 *  @param completion Callback is called when lootsie UI component will show.*/
- (void)willShowLootsieUI:(void (^ __nullable)(void))completion {
    // grab presentation context
    UIViewController *presentationContext = [self presentationContext];
    
    // notify delegate of opening UI
    if ([self.delegate respondsToSelector:@selector(lootsieUIManagerWillShowUI)] && ![self isShowingUI]) {
        [self.delegate lootsieUIManagerWillShowUI];
    }
    
    // present Lootsie's UI
    if(presentationContext != self.wrapRootViewController) //if user application set the controller for show Lootsie UI we show on it.
    {
        [presentationContext presentViewController:self.wrapRootViewController animated:NO completion:completion];
    }
    else    //If user applications doesn't set controller we create window and show UI on it.
    {
        if(completion)
        {
            completion();
        }
    }
}

#pragma mark - Queue Delegate
- (void)operationQueue:(LTOperationQueue *)queue willAddOperation:(NSOperation *)operation {
    [self.coreManager operationQueue:queue willAddOperation:operation];
}
- (void)operationQueueDidFinishAllCurrentTasks:(LTOperationQueue *)queue {
    [self.coreManager operationQueueDidFinishAllCurrentTasks:queue];
}
- (void)operationQueue:(LTOperationQueue *)queue operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors {
    // Don't forget to call super
    [self.coreManager operationQueue:queue operationDidFinish:operation withErrors:errors];
    //if (![queue isEqual:[LTNetworkManager lootsieManager].networkQueue]) return;
    /*
     We only want to show the first error, since subsequent errors might
     be caused by the first.
     */
    if (errors && errors.firstObject) {
        NSError *error = errors.firstObject;
        
        // Ignore operations that erred because conditions. This avoids infinite operations
        if ([error.domain isEqualToString:LTOperationErrorDomain] && error.code == LTOperationErrorCodeCondition) return;
        
        if (operation.isCancelled || operation.isFinished) {
            BOOL needCloseUI = NO;
            NSError* tmpError = [error.userInfo objectForKey: NSUnderlyingErrorKey];
            for(NSEnumerator* key in [tmpError.userInfo keyEnumerator]) {
                NSHTTPURLResponse* response = [tmpError.userInfo objectForKey: key];
                if([response isKindOfClass:[NSHTTPURLResponse class]]) {
                    if(response.statusCode == 451) {
                        needCloseUI = YES;
                        break;
                    }
                }
            }
            
            if(needCloseUI == YES) {
                __weak typeof(self) weakSelf = self;
                [self showAlertWithError:error closeAction:^{
                    [weakSelf closeUI];
                }];
            } else {
                [self showAlertWithError: error];
                /// If the operation that fails is the init operation, we should close the UI
                if ([operation isKindOfClass:[LTInitSequenceOperation class]]) {
                    [self closeUI];
                }
            }
        }
    }
}
- (void)reactivateAccountWithUI: (BOOL) withUI {
    if(withUI) {
        
        // Adding a popup
        UIAlertController * alert=[UIAlertController
                                   
                                   alertControllerWithTitle: NSLocalizedStringFromTable(@"Activate Account", @"LTLootsie", nil) message: NSLocalizedStringFromTable(@"Activate your account to access your rewards.", @"LTLootsie", nil)preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle: NSLocalizedStringFromTable(@"Yes", @"LTLootsie", nil)
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //    LTLogDebug(@"Logging out from account: %@", self.emailAddressTextField.text);
                                        
                                        [[LTManager sharedManager] setStatusAccountActivated];
                                        
                                        if ([self.delegate respondsToSelector:@selector(lootsieUIManagerAccountActivationStatusChange)]) {
                                            [self.delegate lootsieUIManagerAccountActivationStatusChange];
                                        }
                                        
                                        
                                        
                                    }];
        UIAlertAction* noButton = [UIAlertAction
                                   actionWithTitle: NSLocalizedStringFromTable(@"No", @"LTLootsie", nil)
                                   style:UIAlertActionStyleDefault
                                   handler: nil];
        
        [alert addAction:yesButton];
        [alert addAction:noButton];
        
        UIViewController *rootViewController = [UIApplication sharedApplication].keyWindow.rootViewController;
        
        [rootViewController presentViewController:alert animated:YES completion:nil];
        
        
    } else {
        [[LTManager sharedManager] setStatusAccountActivated];
        if ([self.delegate respondsToSelector:@selector(lootsieUIManagerAccountActivationStatusChange)]) {
            [self.delegate lootsieUIManagerAccountActivationStatusChange];
        }
        
    }
}
- (void)deactivateAccount {
    [[LTManager sharedManager] setStatusAccountDeactivated];
    [self closeUI];
    if ([self.delegate respondsToSelector:@selector(lootsieUIManagerAccountActivationStatusChange)]) {
        [self.delegate lootsieUIManagerAccountActivationStatusChange];
    }
}
- (void)disableLootsie:(NSNotification *) notification {
    [self.delegate lootsieUIManagerStatusChanged:NO];
}

@end
