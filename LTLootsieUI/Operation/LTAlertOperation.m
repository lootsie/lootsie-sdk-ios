//  LTAlertOperation.m
//  Created by Fabio Teles on 8/12/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTAlertOperation.h"
#import "LTMutuallyExclusiveCondition.h"

#pragma mark - LTAlertController

/*! @class LTAlertController
 *  @brief Controller for show alert message.*/
@interface LTAlertController : UIAlertController

/*! Callback is called when controller is dismiss.*/
@property (nonatomic, strong) void(^dismissalHandler)(void);

@end


@implementation LTAlertController

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if (self.dismissalHandler) {
        self.dismissalHandler();
        self.dismissalHandler = nil;
    }
}

@end


/*! @class LTAlertOperationAction
 *  @brief Action settings for show button in alert.*/
@interface LTAlertOperationAction : NSObject

/*! Title label of alert.*/
@property (nonatomic, readonly, strong) NSString *title;

/*! Style of action button.*/
@property (nonatomic, readonly, assign) LTAlertActionStyle style;

/*! Operation callback.*/
@property (nonatomic, readonly, strong) void (^handler)(LTAlertOperation *);


/*! @brief Create a new button.
 *  @param title Button title.
 *  @param style Button style.
 *  @param handler Callback is called when user click to button.*/
- (instancetype)initWithTitle:(NSString *)title style:(LTAlertActionStyle)style handler:(void (^)(LTAlertOperation *))handler;

/*! @brief Create a new button.
 *  @param title Button title.
 *  @param style Button style.
 *  @param handler Callback is called when user click to button.*/
+ (instancetype)actionWithTitle:(NSString *)title style:(LTAlertActionStyle)style handler:(void (^)(LTAlertOperation *))handler;

@end



@implementation LTAlertOperationAction

- (instancetype)init {
    return [self initWithTitle:nil style:LTAlertActionStyleDefault handler:nil];
}

- (instancetype)initWithTitle:(NSString *)title style:(LTAlertActionStyle)style handler:(void (^)(LTAlertOperation *))handler {
    if (self = [super init]) {
        _title = title;
        _style = style;
        _handler = handler;
    }
    return self;
}

+ (instancetype)actionWithTitle:(NSString *)title style:(LTAlertActionStyle)style handler:(void (^)(LTAlertOperation *))handler {
    return [[self alloc] initWithTitle:title style:style handler:handler];
}

@end


@interface LTAlertOperation () <UIAlertViewDelegate>

/*! Controller where will be show alert controller.*/
@property (nonatomic, strong) UIViewController *presentationContext;

/*! Callback which return special controller.*/
@property (nonatomic, strong) UIViewController *(^dynamicPresentationContext)(void);

/*! List of buttons for alert controller.*/
@property (nonatomic, strong) NSMutableArray *actions;

@end


@implementation LTAlertOperation

/*! @brief Create a new object.*/
- (instancetype)init {
    return [self initWithTitle:nil message:nil presentationContext:nil];
}

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message presentationContext:(UIViewController *)presentationContext {
    if (self = [super init]) {
        _title = [title copy];
        _message = [message copy];
        _presentationContext = presentationContext;
        _actions = [NSMutableArray array];
    }
    return self;
}

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message dynamicPresentationContext:(UIViewController *(^)(void))dynamicPresentationContext {
    if (self = [self initWithTitle:title message:message presentationContext:nil]) {
        _dynamicPresentationContext = dynamicPresentationContext;
    }
    return self;
}

- (void)addActionWithTitle:(NSString *)title style:(LTAlertActionStyle)style handler:(void (^)(LTAlertOperation *action))handler {
    LTAlertOperationAction *action = [LTAlertOperationAction actionWithTitle:title style:style handler:handler];
    [self.actions addObject:action];
}

- (void)execute {
    // If no actions, add default "OK"
    if (self.actions.count == 0) {
        [self.actions addObject:[LTAlertOperationAction actionWithTitle:@"OK" style:LTAlertActionStyleDefault handler:nil]];
    }
    
    // Make sure a presentation context is available
    if (self.dynamicPresentationContext) {
        self.presentationContext = self.dynamicPresentationContext();
    }
    if (!self.presentationContext) {
        self.presentationContext = [UIApplication sharedApplication].keyWindow.rootViewController;
    }
    
    __weak __typeof(self) weakSelf = self;
    if ([UIAlertController class]) {
        LTAlertController *alertController = [LTAlertController alertControllerWithTitle:self.title message:self.message preferredStyle:UIAlertControllerStyleAlert];
        
        // add actions
        [self.actions enumerateObjectsUsingBlock:^(LTAlertOperationAction *action, NSUInteger idx, BOOL *stop) {
            
            UIAlertActionStyle actionStyle;
            switch (action.style) {
                case LTAlertActionStyleDestructive:
                    actionStyle = UIAlertActionStyleDestructive;
                    break;
                case LTAlertActionStyleCancel:
                    actionStyle = UIAlertActionStyleCancel;
                    break;
                default:
                    actionStyle = UIAlertActionStyleDefault;
                    break;
            }
            
            UIAlertAction *alertAction = [UIAlertAction actionWithTitle:action.title style:actionStyle handler:^(UIAlertAction *alertAction) {
                if (action.handler) {
                    action.handler(weakSelf);
                }
            }];
            [alertController addAction:alertAction];
        }];
        
        // handling dismissal
        // present it
        [self.presentationContext presentViewController:alertController animated:YES completion:nil];
        
    } else {
        UIAlertView *alertView = [UIAlertView new];
        alertView.title = self.title;
        alertView.message = self.message;
        alertView.delegate = self;
        
        [self.actions enumerateObjectsUsingBlock:^(LTAlertOperationAction *action, NSUInteger idx, BOOL *stop) {
            [alertView addButtonWithTitle:action.title];
            if (action.style == LTAlertActionStyleCancel) {
                alertView.cancelButtonIndex = alertView.numberOfButtons - 1;
            }
        }];
        
        [alertView show];
    }
}

/*! @brief Create action object with title and index.
 *  @param alertView Alert view object.
 *  @param buttonIndex Index of button.*/
- (LTAlertOperationAction *)actionForAlertView:(UIAlertView *)alertView buttonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    __block LTAlertOperationAction *action;
    [self.actions enumerateObjectsUsingBlock:^(LTAlertOperationAction *obj, NSUInteger idx, BOOL *stop) {
        if ([obj.title isEqualToString:title]) {
            action = obj;
            *stop = YES;
        }
    }];
    return action;
}

#pragma mark - UIAlertViewDelegate
/*! @brief Called when user tap to button in alert view.
 *  @param alertView Alert view object.
 *  @param buttonIndex Index of button.*/
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    LTAlertOperationAction *action = [self actionForAlertView:alertView buttonAtIndex:buttonIndex];
    if (action && action.handler) {
        action.handler(self);
    }
}

/*! @brief Called when user close alert.
 *  @param alertView Alert view object.*/
- (void)alertViewCancel:(UIAlertView *)alertView {
}

@end
