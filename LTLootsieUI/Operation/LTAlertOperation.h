//  LTAlertOperation.h
//  Created by Fabio Teles on 8/12/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <UIKit/UIKit.h>
#import "LTBaseUIOperation.h"

/*! Alert actions type.*/
typedef NS_ENUM(NSUInteger, LTAlertActionStyle) {
    LTAlertActionStyleDefault,          /*! Defaiult button.*/
    LTAlertActionStyleCancel,           /*! Cancel button.*/
    LTAlertActionStyleDestructive       /*! Destructive button.*/
};


/*! @class LTAlertOperation
 *  @brief Operation for show alert controller.*/
@interface LTAlertOperation : LTBaseUIOperation

/*! Title of alert.*/
@property (nonatomic, copy) NSString *title;

/*! Description of alert.*/
@property (nonatomic, copy) NSString *message;


/*! @brief Create alert with alert and message.
 *  @param title Title of alert.
 *  @param message Description message of alert.
 *  @param presentationContext Controller where will show current alert.*/
- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message presentationContext:(UIViewController *)presentationContext;

/*! @brief Create alert with alert and message.
 *  @param title Title of alert.
 *  @param message Description message of alert.
 *  @param dynamicPresentationContext Callback is called when need to show alert with dynamic controller.*/
- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message dynamicPresentationContext:(UIViewController *(^)(void))dynamicPresentationContext;

/*! @brief Create alert with alert and message.
 *  @param title Title of button.
 *  @param style Action button style.
 *  @param handler Callback is called when user click to action button.*/
- (void)addActionWithTitle:(NSString *)title style:(LTAlertActionStyle)style handler:(void (^)(LTAlertOperation *action))handler;

@end
