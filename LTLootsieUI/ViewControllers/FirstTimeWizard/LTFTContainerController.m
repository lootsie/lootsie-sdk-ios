//
//  LTFTContainerController.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/5/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFTContainerController.h"
#import "LTTools.h"
#import "LTUIManager.h"

#import "LTFTWizardView.h"
#import "LTFTWelcomePage.h"
#import "LTFTCatalogPage.h"
#import "LTFTAchievementsPage.h"
#import "LTFTFavoritesPage.h"
#import "LTFTAccountPage.h"
#import "LTFTRefreshPage.h"
#import "LTFTThatsItPage.h"

#import "LTUIManager+Account.h"


@interface LTFTContainerController ()

/*! Background view.*/
@property (nonatomic, weak)     IBOutlet UIView* backgroundView;

/*! Container view for show all UI elements.*/
@property (nonatomic, weak)     IBOutlet UIView* containerView;

/*! Wizard view. Show Steps of wizard.*/
@property (nonatomic, weak)     IBOutlet LTFTWizardView* wizardView;


/*! Welcome page.*/
@property (nonatomic, strong)   LTFTWelcomePage* welcomPage;

/*! Catalog page.*/
@property (nonatomic, strong)   LTFTCatalogPage* catalogPage;

/*! Last step of wizard.*/
@property (nonatomic, strong)   LTFTThatsItPage* thatsItPage;


/*! @brief Create a first time wizard view.*/
- (void)loadFirstTimeWizard;

/*! @brief Create a page for wizard.
 *  @param name Name of XIB with UI elements for wizard step.
 *  @return First time wizard page.*/
- (LTFirstTimeBaseView*)viewWithNibName:(NSString*)name;

@end


/*! First time wizard key name.*/
static NSString* const kLTFirstStepWizardKey      =   @"kLTFirstStepWizardKey";


@implementation LTFTContainerController

#pragma mark - private methods

- (void)loadFirstTimeWizard {
    self.thatsItPage = (LTFTThatsItPage*)[self viewWithNibName: @"LTFTThatsItPage"];
    __weak typeof (self) tmp = self;
    self.thatsItPage.onSignUpNowButtonClicked = ^{
        [[LTUIManager sharedManager] hideViewController: tmp];
        [[LTUIManager sharedManager] showAccountPage];
        
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        [userDefaults setObject:@YES forKey:kLTFirstStepWizardKey];
        if(tmp.closeAction) {
            tmp.closeAction();
        }
    };
    self.thatsItPage.onMaybeLaterButtonClicked = ^{
        [[LTUIManager sharedManager] hideViewController: tmp];
        if(tmp.closeAction) {
            tmp.closeAction();
        }
    };
    self.catalogPage = (LTFTCatalogPage*)[self viewWithNibName: @"LTFTCatalogPage"];
    
    NSArray* pages = @[];
    if ([LTManager sharedManager].data.app.refreshEnabled) {
        pages = @[self.catalogPage,
                           [self viewWithNibName: @"LTFTAchievementsPage"],
                           [self viewWithNibName: @"LTFTFavoritesPage"],
                           [self viewWithNibName: @"LTFTAccountPage"],
                           [self viewWithNibName: @"LTFTRefreshPage"],
                           self.thatsItPage];
    } else {
        pages = @[self.catalogPage,
                           [self viewWithNibName: @"LTFTAchievementsPage"],
                           [self viewWithNibName: @"LTFTFavoritesPage"],
                           [self viewWithNibName: @"LTFTAccountPage"],
                           self.thatsItPage];
    }
    
    self.wizardView.frame = self.containerView.bounds;
    [self.wizardView updateViews: pages];
}

- (LTFirstTimeBaseView*)viewWithNibName:(NSString*)name {
    if(name.length == 0) {
        return nil;
    }
    
    NSBundle* bundle = [NSBundle bundleForClass: [self class]];
    LTFirstTimeBaseView* view = [[[UINib nibWithNibName: name bundle: bundle] instantiateWithOwner: self options: nil] firstObject];
    view.frame = self.containerView.bounds;
    view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    return view;
}


#pragma mark - Animations methods
/*! @brief Play animation for show wizard view.*/
- (void)animateShowWizardView {
    self.wizardView.alpha = 1.0;
    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath: @"position"];
    anim.fromValue = [NSValue valueWithCGPoint: CGPointMake(self.wizardView.center.x + self.welcomPage.frame.size.width, self.wizardView.center.y)];
    anim.toValue = [NSValue valueWithCGPoint: self.wizardView.center];
    anim.duration = 0.6;
    anim.removedOnCompletion = YES;
    [self.wizardView.layer addAnimation:anim forKey:@"showAnimation"];
    
    [self.catalogPage performSelector:@selector(animateAllViewsToLeft) withObject:nil afterDelay:0.2];
}

/*! @brief Play animation for show steps list when user click to "LET'S GO" button.*/
- (void)showStepsViewAnimation {
    [self.welcomPage animateHideToLeft];
    [self.catalogPage needToHideAllSubviews];
    [self performSelector:@selector(animateShowWizardView) withObject: nil afterDelay:0.4];
}

/*! @brief Play animation for show welcome screen of first time wizard.*/
- (void)animateWelcomPageFromDown {
    [self.welcomPage animateShowFromDownWithSubviewsAnimation: YES];
}


#pragma mark - controller methods
/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.wizardView.alpha = 0.0;
    self.welcomPage = (LTFTWelcomePage*)[self viewWithNibName: @"LTFTWelcomePage"];
    self.welcomPage.frame = self.containerView.bounds;
    __weak typeof(self) tmp = self;
    self.welcomPage.onMaybeButtonClicked = ^{
        [[LTUIManager sharedManager] hideViewController: tmp];
        if(tmp.closeAction) {
            tmp.closeAction();
        }
    };
    self.welcomPage.onLetsGoButtonClicked = ^{
        [tmp showStepsViewAnimation];
    };
    [self.containerView addSubview: self.welcomPage];
    [self.welcomPage needToHideAllSubviews];
    [self performSelector:@selector(animateWelcomPageFromDown) withObject:nil afterDelay:0.1];
    
    [self loadFirstTimeWizard];
}

/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
