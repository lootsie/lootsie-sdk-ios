//
//  LTRootViewController.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 4/20/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTRootViewController.h"


@implementation LTRootViewController

/*! @brief Create a new object.*/
- (instancetype)init {
    self = [super init];
    if(self) {
        self.view.backgroundColor = [UIColor clearColor];
        self.modalPresentationStyle = UIModalPresentationCustom;
        self.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    }
    return self;
}

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
