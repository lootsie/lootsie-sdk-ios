//  LTPopupViewController.m
//  Created by Konstantin Yurchenko
//  Copyright (c) 2017 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "LTPopupRegistrationSuccessViewController.h"
#import "LTTheme.h"
#import "LTTools.h"

@interface LTPopupRegistrationSuccessViewController () <UIViewControllerTransitioningDelegate>

/*! Popup icon view.*/
@property (weak, nonatomic) IBOutlet UIImageView* iconView;

/*! Description label.*/
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

/*! Title label.*/
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! Close button*/
@property (weak, nonatomic) IBOutlet UIButton *button;

/*! Container view for show all UI elements.*/
@property (weak, nonatomic) IBOutlet UIView *container;

@property (weak, nonatomic) IBOutlet UIImageView *gradientBackground;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

/*! @brief Generate description attribute.
 *  @return Attributes structure.*/
- (NSDictionary*)descriptionAttributes;

@end

@implementation LTPopupRegistrationSuccessViewController

#pragma mark - Private methods

- (NSDictionary*)descriptionAttributes {
    NSMutableParagraphStyle* paragraph = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraph.alignment = NSTextAlignmentCenter;
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.lineHeightMultiple = 1.24;
    return @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:LTMediumBFontSize],
             NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText],
             NSParagraphStyleAttributeName: paragraph};
}

#pragma mark - ViewController methods
/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.container.layer.cornerRadius = 5.f;
    
    
    // Background Gradient
    UIImage *image = [UIImage imageNamed:@"tileCongratsBackgroud"];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeTile];
    
    self.backgroundImage.image = image;
    
    UIColor *backgroundColor = self.container.backgroundColor;
    
    NSLog(@"%f", self.container.bounds.size.height);
    
    self.gradientBackground.image = [LTTools gradientBgImageWithHeight: self.gradientBackground.bounds.size.height  topColor: [backgroundColor colorWithAlphaComponent:0.2] bottomColor: [backgroundColor colorWithAlphaComponent:1]];
    
    self.gradientBackground.layer.cornerRadius = 10.f;
    self.backgroundImage.layer.cornerRadius = 10.f;
    
    self.gradientBackground.layer.masksToBounds = YES;
    self.backgroundImage.layer.masksToBounds = YES;
    
    // Background Gradient
    
    self.iconView.image = [self.iconView.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    self.iconView.tintColor = [[LTTheme sharedTheme] colorWithType:LTColorAltBackground4];
    
    self.titleLabel.text = [self.titleText uppercaseString];
    NSString* str = [NSString stringWithFormat: @"%@", self.descriptionText];
    self.descriptionLabel.attributedText = [[NSAttributedString alloc] initWithString: str attributes: [self descriptionAttributes]];

    [self.button setTitle: [self.buttonText uppercaseString] forState: UIControlStateNormal];
    self.button.layer.cornerRadius = 5.f;
    
    self.container.layer.shadowColor = [UIColor blackColor].CGColor;
    self.container.layer.shadowOpacity = 0.44f;
    self.container.layer.shadowOffset = CGSizeZero;
    self.container.layer.shadowRadius = 18.f;
    self.container.layer.shadowPath = [UIBezierPath bezierPathWithRect: self.container.bounds].CGPath;
    self.container.layer.cornerRadius = 5.f;
    self.container.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    self.descriptionLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];
}

/*! @brief Called when user click to batton.
 *  @param sender Button object which called action.*/
- (IBAction)didPressButton:(UIButton *)sender {
    [self dismissViewControllerAnimated: YES completion: self.closeAction];
}

@end
