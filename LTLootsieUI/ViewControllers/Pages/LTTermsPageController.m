//  LTTermsPageController.m
//  Created by Fabio Teles on 5/20/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTTermsPageController.h"
#import "LTTermsCell.h"
#import "LTTools.h"
#import "LTTheme.h"
#import "LTUIManager.h"

#import "LTUIAnimationDelegate.h"

static NSString * TermCellIdentifier = @"TermCell";

@interface LTTermsPageController () <UITableViewDataSource, UITableViewDelegate>

/*! Information list for show.*/
@property (strong, nonatomic) NSArray *terms;

/*! Table view for show information.*/
@property (weak, nonatomic) IBOutlet UITableView *termsTableView;


/*! @brief Generate data for show.*/
- (void)populateData;

/*! @brief Set data to cell.
 *  @param cell Information cell.
 *  @param dictionary Information info structure.*/
- (void)setupCell:(LTTermsCell *)cell withTerm:(NSDictionary *)dictionary;

/*! @brief Called when user close current page.
 *  @param sender Button on which user is tapped.*/
- (IBAction)onCloseButtonClick:(id)sender;

@end



@implementation LTTermsPageController

#pragma mark - private methods

- (IBAction)onCloseButtonClick:(id)sender {
    [[LTUIManager sharedManager] hideViewController: self];
}


#pragma mark - View controller methods

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.contentView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
	self.termsTableView.rowHeight = UITableViewAutomaticDimension;
	self.termsTableView.estimatedRowHeight = 110.f;

    [self.termsTableView registerNib: [UINib nibWithNibName: @"LTTermsCell" bundle: nil] forCellReuseIdentifier: TermCellIdentifier];
    self.termsTableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    
	[self populateData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)populateData {
	self.terms = @[
				   @{
					   @"title": @"Fraud",
					   @"blurb" : @"Lootsie reserves the right to refuse, cancel or hold for review gift cards and orders, redemptions or claims for suspected fraud, for cards mistakenly issued in an incorrect denomination, or for other violations of gift card policies."
					   },
				   @{
					   @"title": @"Returns",
					   @"blurb" : @"All returns and/or exchanges or customer service issues must be dealt with through that card's retailer. Lootsie, Inc. is not responsible for any of the above mentioned. Once a gift card or special offer has been redeemed through Lootsie, Inc., any and all further engagement with the gift card/offer provider must be through that retailer."
					   },
				   @{
					   @"title": @"Resale",
					   @"blurb" : @"Gift cards/discounts cannot be resold or combined with any other offer, unless specifically stated."
					   },
				   @{
					   @"title": @"Redemption",
					   @"blurb" : @"If the order total is more than the gift card amount, the remaining balance must be paid for by cash, credit/debit card, PayPal account, product voucher or additional gift card(s). The gift card balance will immediately be reduced. Upon redemption, any unused balance on the gift card will be carried over for future purchases. Balances cannot be redeemed or exchanged for cash."
					   },
				   @{
					   @"title": @"Expiration",
					   @"blurb" : @"Gift cards and special offers may have an expiration date listed on Lootsie.com and will not be valid after that date. "
					   },
				   @{
					   @"title": @"Refunds",
					   @"blurb" : @"Gift cards may not be returned or cancelled after redemption. There are no refunds of Lootsie Points."
					   },
				   @{
					   @"title": @"Lost or stolen gift cards",
					   @"blurb" : @"Gift cards will not be replaced if lost or stolen. Treat them like gold."
					   }
				   ];
}

- (void)setupCell:(LTTermsCell *)cell withTerm:(NSDictionary *)dictionary {
    NSMutableParagraphStyle* paragraph = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraph.lineHeightMultiple = 1.27;
    NSDictionary* attributes = @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily:LTPrimaryFontFamily
                                                                                       weight:LTBoldFontWeight
                                                                                        style:LTRegularFontStyle
                                                                                         size:LTMediumCFontSize],
                                 NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText],
                                 NSParagraphStyleAttributeName: paragraph};
    
	cell.blurbLabel.attributedText = [[NSAttributedString alloc] initWithString:dictionary[@"blurb"] attributes:attributes];
}

#pragma mark - UITableView Delegate and DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return self.terms.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
	NSDictionary *terms = self.terms[section];

	UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 320.f, 20.f)];
	header.userInteractionEnabled = NO;
    header.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorSecondaryBackground];

	UILabel *title = [UILabel new];
    title.font = [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                  weight: LTMediumFontWeight
                                                   style: LTRegularFontStyle
                                                    size: LTMediumAFontSize];
    title.textColor = [[LTTheme sharedTheme] colorWithType: LTColorAltText5];
    title.textAlignment = NSTextAlignmentLeft;
	title.text = terms[@"title"];

	title.translatesAutoresizingMaskIntoConstraints = NO;
	[header addSubview:title];

    [header addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-26-[title]-26-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(title)]];
    [header addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:header attribute:NSLayoutAttributeCenterY multiplier:1.f constant:0.f]];
	header.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);

	return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
	return 20.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	LTTermsCell *cell = [tableView dequeueReusableCellWithIdentifier:TermCellIdentifier forIndexPath:indexPath];
	[self setupCell:cell withTerm:self.terms[indexPath.section]];

	[cell setNeedsLayout];
	[cell layoutIfNeeded];

	return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	LTTermsCell *sizingCell = [tableView dequeueReusableCellWithIdentifier:TermCellIdentifier];
	sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(tableView.frame), CGRectGetHeight(sizingCell.bounds));

	[self setupCell:sizingCell withTerm:self.terms[indexPath.section]];

	[sizingCell setNeedsLayout];
	[sizingCell layoutIfNeeded];

	CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
	return roundf(size.height + 1.f); // account for separator
}

@end
