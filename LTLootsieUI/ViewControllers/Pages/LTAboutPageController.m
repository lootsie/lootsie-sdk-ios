//  LTAboutPageController.m
//  Created by Fabio Teles on 5/20/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
#import <CoreLocation/CoreLocation.h>

#import "LTAboutPageController.h"
#import "LTConstants.h"
#import "LTUIManager.h"
#import "LTNetworkManager.h"

#import "LTApp.h"
#import "LTData.h"
#import "LTCloseButton.h"
#import "LTAboutCell.h"

static NSString * AboutCellIdentifier = @"AboutCell";


@interface LTAboutPageController () <UITableViewDelegate, UITableViewDataSource>

/*! List of items for show.*/
@property (nonatomic, strong) NSArray *allInfo;


/*! @brief Generate data for show.*/
- (void)populateData;

/*! @brief Called when user close current page.
 *  @param sender Button on which user is tapped.*/
- (IBAction)onCloseButtonClick:(id)sender;

@end


@implementation LTAboutPageController

#pragma mark - Private methods

- (void)populateData {
    self.allInfo = @[@{@"name": NSLocalizedStringFromTable(@"Loading", @"LTLootsie", nil),
                       @"value": NSLocalizedStringFromTable(@"Please wait", @"LTLootsie", nil)}];
    
    self.allInfo = @[
                     @{
                         @"name": NSLocalizedStringFromTable(@"Platform", @"LTLootsie", nil),
                         @"value" : LTPlatform
                         },
                     @{
                         @"name": NSLocalizedStringFromTable(@"Device", @"LTLootsie", nil),
                         @"value" : [UIDevice currentDevice].model
                         },
                     @{
                         @"name": NSLocalizedStringFromTable(@"Firmware", @"LTLootsie", nil),
                         @"value" : [[UIDevice currentDevice] systemVersion]
                         },
                     @{
                         @"name": NSLocalizedStringFromTable(@"Language", @"LTLootsie", nil),
                         @"value" : [[NSLocale currentLocale] objectForKey: NSLocaleLanguageCode]
                         },
                     @{
                         @"name": NSLocalizedStringFromTable(@"Country", @"LTLootsie", nil),
                         @"value" : [[NSLocale currentLocale] objectForKey: NSLocaleCountryCode]
                         },
                     [@{
                        @"name": NSLocalizedStringFromTable(@"Location", @"LTLootsie", nil),
                        @"value" : @"Fetching location"
                        } mutableCopy],
                     @{
                         @"name": NSLocalizedStringFromTable(@"IDFV", @"LTLootsie", nil),
                         @"value" : [[[UIDevice  currentDevice] identifierForVendor] UUIDString]
                         },
                     @{
                         @"name": NSLocalizedStringFromTable(@"SDK Version", @"LTLootsie", nil),
                         @"value" : LTSDKVersion
                         },
                     @{
                         @"name": NSLocalizedStringFromTable(@"API Server URL", @"LTLootsie", nil),
                         @"value" : [[[LTNetworkManager lootsieManager] baseURL] absoluteString]
                         }
                     ];
    
    [self.aboutTableView reloadData];
}

- (IBAction)onCloseButtonClick:(id)sender {
    [[LTUIManager sharedManager] hideViewController: self];
}


#pragma mark - View controllers methods
/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.aboutTableView registerNib: [UINib nibWithNibName: @"LTAboutCell" bundle: nil] forCellReuseIdentifier: AboutCellIdentifier];
    
    self.aboutTableView.allowsSelection = NO;
    self.aboutTableView.contentInset = UIEdgeInsetsMake(0, 0, 40, 0);
    self.contentView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];

	[self populateData];
}

/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate and DataSource
/*! @brief Get number of section in table view.
 *  @param tableView Table object.
 *  @return Number of section. Default 1*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

/*! @brief Get number of items for show in section.
 *  @param tableView Table object.
 *  @param section Index of section.
 *  @return Number of items.*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.allInfo.count;
}

/*! @brief Get cell object for tabel with index path.
 *  @param tableView Table object.
 *  @param indexPath Index path for cell.
 *  @return Cell object.*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	LTAboutCell *cell = [tableView dequeueReusableCellWithIdentifier:AboutCellIdentifier forIndexPath:indexPath];

	NSDictionary *info = [self.allInfo objectAtIndex:indexPath.row];

	cell.title = [info objectForKey:@"name"];
	cell.subtitle = [info objectForKey:@"value"];

	return cell;
}

/*! @brief Check need to show menu on cell with index path.
 *  @param tableView Table object.
 *  @param indexPath Index path for cell.
 *  @return YES if menu must be shown.*/
- (BOOL)tableView:(UITableView *)tableView shouldShowMenuForRowAtIndexPath:(NSIndexPath *)indexPath {
    return self.allInfo.count > 1;
}

/*! @brief Called when need to call action for cell with index path.
 *  @param tableView Table object.
 *  @param action Action fro call.
 *  @param indexPath Index path for cell.
 *  @param sender Sender object.*/
- (void)tableView:(UITableView*)tableView performAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender {
    UIPasteboard* pasteboard = [UIPasteboard generalPasteboard];
    NSDictionary *info = [self.allInfo objectAtIndex:indexPath.row];
    pasteboard.string = [info objectForKey:@"value"];
}

/*! @brief Check is can call action for object with index path.
 *  @param tableView Table object.
 *  @param action Action fro call.
 *  @param indexPath Index path for cell.
 *  @param sender Sender object.
 *  @return YES if action can be called.*/
- (BOOL)tableView:(UITableView*)tableView canPerformAction:(SEL)action forRowAtIndexPath:(NSIndexPath*)indexPath withSender:(id)sender {
    if (action == @selector(copy:)) {
        return YES;
    }
    return NO;
}

@end
