//  LTBasePageViewController.m
//  Created by Dovbnya Alexandr on 5/17/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTBasePageViewController.h"

@interface LTBasePageViewController ()

/*! Y offset.*/
@property (nonatomic, assign) CGFloat previousYOffset;

/*! Resistance consumed value.*/
@property (nonatomic, assign) CGFloat resistanceConsumed;


/*! YES if need to contract header view.*/
@property (nonatomic, assign) BOOL contracting;

/*! Previous contraction state*/
@property (nonatomic, assign) BOOL previousContractionState;


/*! YES if view controller is shown on screen.*/
@property (nonatomic, readonly) BOOL isViewControllerVisible;


/*! Expansion Resistance*/
@property (nonatomic) CGFloat expansionResistance;      // default 200

/*! Contraction Resistance*/
@property (nonatomic) CGFloat contractionResistance;    // default 0

@end


@implementation LTBasePageViewController

- (void)needUpdateData {
}

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    self.contracting = NO;
    self.previousContractionState = YES;
    
    self.expansionResistance = 200.f;
    self.contractionResistance = 0.f;
    
    self.previousYOffset = NAN;
}

/*! @brief Called when controller will appear on screen.
 *  @param animated YES if controller will appear with animation.*/
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear: animated];
    
    [self needUpdateData];
}

/*! @brief Called when user start dragging view.
 *  @param scrollView Scroll view object.*/
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.previousYOffset = NAN;
}

/*! @brief Scroll view is suffeciently long.
 *  @param scrollView Scroll view object.*/
- (BOOL)scrollViewIsSuffecientlyLong:(UIScrollView *)scrollView {
    CGRect scrollFrame = UIEdgeInsetsInsetRect(scrollView.bounds, scrollView.contentInset);
    CGFloat scrollableAmount = scrollView.contentSize.height - CGRectGetHeight(scrollFrame);
    return scrollableAmount > 0;
}

/*! @brief Called when component did scroll.
 *  @param scrollView Scroll view object.*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (![self scrollViewIsSuffecientlyLong:scrollView])
    {
        return;
    }
    
    if (!isnan(self.previousYOffset))
    {
        // 1 - Calculate the delta
        CGFloat deltaY = (self.previousYOffset - scrollView.contentOffset.y);
        // 2 - Ignore any scrollOffset beyond the bounds
        CGFloat start = -scrollView.contentInset.top;
        if (self.previousYOffset < start)
        {
            deltaY = MIN(0, deltaY - (self.previousYOffset - start));
        }
        
        /* rounding to resolve a dumb issue with the contentOffset value */
        CGFloat end = floorf(scrollView.contentSize.height - CGRectGetHeight(scrollView.bounds) + scrollView.contentInset.bottom - 0.5f);
        if (self.previousYOffset > end && deltaY > 0)
        {
            deltaY = MAX(0, deltaY - self.previousYOffset + end);
        }
        
        // 3 - Update contracting variable
        if (fabs(deltaY) > FLT_EPSILON)
        {
            self.contracting = deltaY < 0;
        }
        
        // 4 - Check if contracting state changed, and do stuff if so
        if (self.contracting != self.previousContractionState)
        {
            self.previousContractionState = self.contracting;
            self.resistanceConsumed = 0;
        }
        
        // GTH: Calculate the exact point to avoid expansion resistance
        // CGFloat statusBarHeight = [self.statusBarController calculateTotalHeightRecursively];
        
        // 5 - Apply resistance
        // 5.1 - Always apply resistance when contracting
        if (self.contracting)
        {
            CGFloat availableResistance = self.contractionResistance - self.resistanceConsumed;
            self.resistanceConsumed = MIN(self.contractionResistance, self.resistanceConsumed - deltaY);
            
            deltaY = MIN(0, availableResistance + deltaY);
        }
        // 5.2 - Only apply resistance if expanding above the status bar
        else if (scrollView.contentOffset.y > 0)
        {
            CGFloat availableResistance = self.expansionResistance - self.resistanceConsumed;
            self.resistanceConsumed = MIN(self.expansionResistance, self.resistanceConsumed + deltaY);
            
            deltaY = MAX(0, deltaY - availableResistance);
        }
        [self.navigationBarDelegate didChangeNavigationBar:deltaY];
    }
    
    self.previousYOffset = scrollView.contentOffset.y;
}

/*! @brief Called on finger up if the user dragged.
 *  @param scrollView Scroll view object.
 *  @param decelerate YES if it will continue moving afterwards*/
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [self.navigationBarDelegate didEndScroll];
}

/*! @brief Called when aniamtion of scrolling did end.
 *  @param scrollView Scroll view object.*/
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    [self.navigationBarDelegate didEndScroll];
}

@end
