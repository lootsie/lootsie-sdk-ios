//
//  LTBaseAnimateViewController.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/28/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTUIAnimationDelegate.h"

/*! @class LTBaseAnimateViewController
 *  @brief Base controller with animation protocol. Use this class to create page with animation for show.*/
@interface LTBaseAnimateViewController : UIViewController <LTUIAnimationDelegate>

/*! Background view.*/
@property (nonatomic, weak) IBOutlet UIView* backgroundView;

/*! Content view for show all UI elements.*/
@property (nonatomic, weak) IBOutlet UIView* contentView;

@end
