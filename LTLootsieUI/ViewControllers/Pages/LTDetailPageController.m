//
//  LTDetailPageController.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/2/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTDetailPageController.h"
#import "LTTheme.h"
#import "LTCloseButton.h"
#import "LTTools.h"


@implementation LTDetailPageController

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    
    if(self.bottomImageView) {
        self.bottomImageView.image = [LTTools gradientBgImageWithHeight: self.bottomImageView.bounds.size.height  //57 48 46
                                                               topColor: [[[LTTheme sharedTheme] colorWithType: LTColorSecondaryBackground] colorWithAlphaComponent:0]
                                                            bottomColor: [[LTTheme sharedTheme] colorWithType: LTColorSecondaryBackground]];
    }
    self.contentView.layer.cornerRadius = 5.0;
    self.contentView.clipsToBounds = YES;
    self.contentView.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    
    self.lineViewWidth.constant = ([UIScreen mainScreen].bounds.size.height >= 667.0) ? 211.0 : 170.0;
}

@end
