//
//  LTDetailPageController.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/2/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LTBaseAnimateViewController.h"

@class LTCloseButton;
/*! @class LTDetailPageController
 *  @brief Base class for show special information page.*/
@interface LTDetailPageController : LTBaseAnimateViewController

/*! Close button.*/
@property (weak, nonatomic) IBOutlet LTCloseButton *closeButton;

/*! Title of page.*/
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! Title devider view.*/
@property (weak, nonatomic) IBOutlet UIView *lineView;

/*! Bottom view container.*/
@property (weak, nonatomic) IBOutlet UIView *bottomView;

/*! Gradient view.*/
@property (weak, nonatomic) IBOutlet UIImageView *bottomImageView;


/*! Constrain for set line width.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewWidth;

@end
