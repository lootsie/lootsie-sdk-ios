//
//  LTBaseAnimateViewController.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/28/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTBaseAnimateViewController.h"
#import "LTTheme.h"

@implementation LTBaseAnimateViewController

#pragma mark - LTUIAnimationDelegate methods

- (void)animateFromStartPoint:(CGPoint)startPoint completion:(LTUIAnimationCompleteBlock)block{
    _backgroundView.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorModalBackground];
    if (_contentView != nil) {
        CGPoint contentPosition = _contentView.layer.position;
        CGAffineTransform transform = CGAffineTransformMakeScale(0.2f, 0.2f);
        transform.tx = startPoint.x - contentPosition.x;
        transform.ty = startPoint.y - contentPosition.y;
        _contentView.transform = transform;
    }
    
    _backgroundView.alpha = 0;
    [UIView animateWithDuration:0.3f
                     animations:^{
                         if(_backgroundView) {
                             _backgroundView.alpha = 1;
                         }
                         _contentView.transform = CGAffineTransformIdentity;
                     } completion:^(BOOL finished) {
                         if(block) {
                             block();
                         }
                     }];
}


#pragma mark - ViewController methods
/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


#pragma mark - Clear memory methods
/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
