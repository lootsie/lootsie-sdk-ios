//  LTPopupViewController.m
//  Created by Alexander Dovbnya on 2/17/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "LTPopupViewController.h"
#import "LTTheme.h"



@interface LTPopupViewController () <UIViewControllerTransitioningDelegate>

/*! Popup icon view.*/
@property (weak, nonatomic) IBOutlet UIImageView* iconView;

/*! Description label.*/
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

/*! Title label.*/
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

/*! Close button*/
@property (weak, nonatomic) IBOutlet UIButton *button;

/*! Container view for show all UI elements.*/
@property (weak, nonatomic) IBOutlet UIView *container;


/*! @brief Generate description attribute.
 *  @return Attributes structure.*/
- (NSDictionary*)descriptionAttributes;

@end


@implementation LTPopupViewController


#pragma mark - Private methods

- (NSDictionary*)descriptionAttributes {
    NSMutableParagraphStyle* paragraph = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    paragraph.alignment = NSTextAlignmentCenter;
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.lineHeightMultiple = 1.24;
    return @{NSFontAttributeName: [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily weight:LTBoldFontWeight style:LTRegularFontStyle size:LTMediumBFontSize],
             NSForegroundColorAttributeName: [[LTTheme sharedTheme] colorWithType: LTColorPrimaryText],
             NSParagraphStyleAttributeName: paragraph};
}


#pragma mark - ViewController methods
/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.container.layer.cornerRadius = 5.f;
    
    self.iconView.image = [self.iconView.image imageWithRenderingMode: UIImageRenderingModeAlwaysTemplate];
    self.iconView.tintColor = [[LTTheme sharedTheme] colorWithType:LTColorAltBackground2];
    
    self.titleLabel.text = [self.titleText uppercaseString];
    NSString* str = [NSString stringWithFormat: @"%@", self.descriptionText];
    self.descriptionLabel.attributedText = [[NSAttributedString alloc] initWithString: str attributes: [self descriptionAttributes]];

    [self.button setTitle: [self.buttonText uppercaseString] forState: UIControlStateNormal];
    self.button.layer.cornerRadius = 5.f;
    
    self.container.layer.shadowColor = [UIColor blackColor].CGColor;
    self.container.layer.shadowOpacity = 0.44f;
    self.container.layer.shadowOffset = CGSizeZero;
    self.container.layer.shadowRadius = 18.f;
    self.container.layer.shadowPath = [UIBezierPath bezierPathWithRect: self.container.bounds].CGPath;
    self.container.layer.cornerRadius = 5.f;
    self.container.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];
    self.descriptionLabel.textColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryText];
}

/*! @brief Called when user click to batton.
 *  @param sender Button object which called action.*/
- (IBAction)didPressButton:(UIButton *)sender {
    [self dismissViewControllerAnimated: YES completion: self.closeAction];
}

@end
