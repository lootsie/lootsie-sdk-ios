//  LTViewController.m
//  Created by Fabio Teles on 5/18/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTViewController.h"
#import "LTUIManager.h"
#import "LTTools.h"
#import "LTTheme.h"

#import "LTUserAccount.h"
#import "LTUIManager+Account.h"
#import "LTAccountPageController.h"
#import "LTManager+Catalog.h"
#import "LTUIManager+Catalog.h"
#import "LTRewardsPageController.h"
#import "LTFavoritesPageController.h"
#import "LTUIManager+Achievement.h"
//#import "LTAchievementsPageController.h"
#import "LTTimelinePageController.h"
#import "LTData.h"

#import "LTBasePageViewController.h"
#import "LTContainerScrollView.h"

#import "LTIANGenericItem.h"
#import "LTIANCatalogItem.h"
#import "LTIANRewardItem.h"
#import "LTIANVideoItem.h"
#import "LTFooterView.h"
#import "LTMainNavigationView.h"



//const CGFloat minHeightNavigationBar = 44;
//const CGFloat maxHeightNavigationBar = 79;


/*! First step wizard key name.*/
static NSString* const kLTFirstStepWizardKey      = @"kLTFirstStepWizardKey";

/*! First step wizard count key name.*/
static NSString* const kLTFirstStepWizardCountKey = @"kLTFirstStepWizardCountKey";

/*! Flag for show first time wizard for current session.*/
static BOOL needShow = YES;


/*! @category LTUIManager(LTUIManagerPrivateMethods)
 *  @brief Special private methods for LTUIManager.*/
@interface LTUIManager (LTUIManagerPrivateMethods)

/*! @brief Show first time wizard on a special controller.
 *  @param parentController Parent controller where need to show wizard.
 *  @param startPoint Start point where will start play animation for show.*/
- (void)showFirstTimeWizardOnViewController:(UIViewController *)parentController fromStartPoint:(CGPoint)startPoint;


- (void) updateNavigationBarVisibilities:(UIInterfaceOrientation)interfaceOrientation;
- (CGFloat) minHeightForOrientation: (UIInterfaceOrientation)interfaceOrientation;
- (CGFloat) maxHeightForOrientation: (UIInterfaceOrientation)interfaceOrientation;

@end

@interface LTViewController () <LTNavigationScrollDelegate, UIScrollViewDelegate, UIPageViewControllerDelegate, UIPageViewControllerDataSource,LTMainNavigationViewDelegate>


// This is a workaround to deal with landscape mode.
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeaderHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeaderNavHeightConstraint;

/*! Header view. Show information and navigation bar.*/
@property (weak, nonatomic) IBOutlet LTHeaderView *headerView;
@property (weak, nonatomic) IBOutlet LTFooterView *footerView;

/*! Constrain for set height of header view.*/
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightHeader;

/*! Page controller for show controller as a selected page.*/
@property (nonatomic, strong) UIPageViewController *pageController;

/*! List of all pages which user can see when click on button in navigation bar or swipe from left to right and back.*/
@property (nonatomic, strong) NSMutableArray *pageViewControllers;


/*! @brief Need to show first time wizard.*/
- (void)showFirstTimeWizardIfNeeded;

/*! @brief Called when user account was updated.
 *  @param notification Notification info object with new data.*/
- (void)userUpdated:(NSNotification *)notification;

/*! @brief Called when user change favorite state for reward.
 *  @param notification Notification info object with new data.*/
- (void)didChangeFavoriteState:(NSNotification *)notification;

/*! @brief Need to update data.*/
- (void)updateData;

/*! @brief Need to update user account info.
 *  @param userAccount User account object info.*/
- (void)updateDataFromAccount:(LTUserAccount *)userAccount;

/*! @brief Update header view.
 *  @param heightHeader New height of header view.*/
- (void)updateHeader:(CGFloat)heightHeader;

/*! @brief Header view for show IAN.
 *  @return Header view.*/
- (UIView *)headerViewForIan;

@end


@implementation LTViewController

/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"disableNavigationBar" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        self.headerView.navigationBar.userInteractionEnabled = NO;
        for (UIScrollView *view in self.pageController.view.subviews) {
            if ([view isKindOfClass:[UIScrollView class]]) {
                view.scrollEnabled = NO;
            }
        }
    }];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:@"enableNavigationBar" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        self.headerView.navigationBar.userInteractionEnabled = YES;
        for (UIScrollView *view in self.pageController.view.subviews) {
            if ([view isKindOfClass:[UIScrollView class]]) {
                view.scrollEnabled = YES;
            }
        }
    }];
    
    
    [self updateData];
    self.pageViewControllers = [NSMutableArray array];
    __weak LTViewController *weakSelf = self;
    
    LTRewardsPageController *rewardController = [LTRewardsPageController new];
    [self.pageViewControllers addObject:rewardController];
    
//    [self.pageViewControllers addObject:[LTAchievementsPageController new]];
    [self.pageViewControllers addObject:[LTTimelinePageController new]];
    
    LTFavoritesPageController *favoriteController = [LTFavoritesPageController new];
    favoriteController.didAppearFavoriteScreen = ^() {
        if (!weakSelf.headerView.navigationBar.favoriteIcon.hidden) {
            [UIView animateWithDuration:0.3f animations:^{
                weakSelf.headerView.navigationBar.favoriteIcon.alpha = 0;
            } completion:^(BOOL finished) {
                weakSelf.headerView.navigationBar.favoriteIcon.hidden = YES;
            }];
        }

        if (!weakSelf.footerView.mainNavigationView.favoriteIcon.hidden) {
            [UIView animateWithDuration:0.3f animations:^{
                weakSelf.footerView.mainNavigationView.favoriteIcon.alpha = 0;
            } completion:^(BOOL finished) {
                weakSelf.footerView.mainNavigationView.favoriteIcon.hidden = YES;
            }];
        }
    };
    [self.pageViewControllers addObject:favoriteController];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeFavoriteState:) name:LTChangeFavoriteRewardNotification object:nil];
    [self.pageViewControllers addObject:[LTAccountPageController new]];
    self.headerView.navigationBar.onSelectionIndexChenged = ^(LTHeaderNavigationBar* navBar, NSUInteger index) {
        LTBasePageViewController *controller = weakSelf.pageViewControllers[index];
        [weakSelf showNextViewController:controller];
    };
    self.headerView.infoBar.onCloseClicked = ^{
        [self closeButtonTapped: nil];
    };

    [self updateNavigationBarVisibilities: [UIApplication sharedApplication].statusBarOrientation];

    self.view.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorSecondaryBackground];;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userUpdated:) name:LTUserAccountUpdatedNotification object:nil];

    self.footerView.mainNavigationView.delegate = self;
}

/*! @brief Called when controller did appear on screen.
 *  @param animated YES if controller appeared with animation.*/
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    
    if(([self isBeingPresented] || [self isMovingToParentViewController])) {
        [self showFirstTimeWizardIfNeeded];
    }
}

/*! @brief Prepare for use segue.
 *  @param segue Segue object.
 *  @param sender Sender object.*/
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"pageViewControllers"]) {
        self.pageController = segue.destinationViewController;
        self.pageController.delegate = self;
        self.pageController.dataSource = self;
        self.pageController.doubleSided = YES;
        self.pageController.view.backgroundColor = [[LTTheme sharedTheme] colorWithType:LTColorPrimaryBackground];
    }
}

/*! @brief Clear memory when object was released*/
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    needShow = YES;
}

/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*! @brief Get style of status bar for current controller.
 *  @return Style of status bar.*/
- (UIStatusBarStyle)preferredStatusBarStyle {
    return [[LTTheme sharedTheme] statusBarStyleForCurrentTheme];
}


#pragma mark - Private

- (void)showFirstTimeWizardIfNeeded {
    UIViewController* currentControllerInPage = [self.pageController.viewControllers firstObject];
    if([currentControllerInPage isKindOfClass: [LTRewardsPageController class]] && needShow) {
        NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
        NSNumber* ftWizard = [userDefaults objectForKey: kLTFirstStepWizardKey];
        NSNumber* countTries = [userDefaults objectForKey: kLTFirstStepWizardCountKey];
        if((ftWizard == nil || ![ftWizard boolValue]) && (countTries == nil || [countTries integerValue] < 2)) {
            [[LTUIManager sharedManager] showFirstTimeWizardOnViewController: currentControllerInPage fromStartPoint: currentControllerInPage.view.center];
            needShow = NO;
            
            NSInteger countValue = [countTries integerValue];
            countValue += 1;
            [userDefaults setObject: @(countValue) forKey: kLTFirstStepWizardCountKey];
        }
    }
}

- (void)userUpdated:(NSNotification *)notification {
    [self updateDataFromAccount:notification.userInfo[@"user"]];
    //[self updateData];
}

- (void)didChangeFavoriteState:(NSNotification *)notification {
    BOOL isDisplayFavorite = NO;
    for (UIViewController *controller in self.pageController.viewControllers) {
        if ([controller isKindOfClass:[LTFavoritesPageController class]]) {
            isDisplayFavorite = YES;
        }
    }
    if (self.headerView.navigationBar.favoriteIcon.hidden && !isDisplayFavorite) {
        self.headerView.navigationBar.favoriteIcon.hidden = NO;
        self.headerView.navigationBar.favoriteIcon.alpha = 0;
        [UIView animateWithDuration:0.3f animations:^{
            self.headerView.navigationBar.favoriteIcon.alpha = 1;
        }];
    }

    if (self.footerView.mainNavigationView.favoriteIcon.hidden && !isDisplayFavorite) {
        self.footerView.mainNavigationView.favoriteIcon.hidden = NO;
        self.footerView.mainNavigationView.favoriteIcon.alpha = 0;
        [UIView animateWithDuration:0.3f animations:^{
            self.footerView.mainNavigationView.favoriteIcon.alpha = 1;
        }];
    }


}

- (void)updateData {
	if (!self.isViewLoaded) {
		return;
	}
    __weak typeof(self) weakSelf = self;
    [[LTManager sharedManager] userAccountWithSuccess:^(LTUserAccount *userAccount) {
        // pointBalanceLabel
        [weakSelf updateDataFromAccount:userAccount];
    } failure:nil];
}

- (void)updateDataFromAccount:(LTUserAccount *)userAccount {
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    self.headerView.infoBar.points = userAccount.points;
}

- (void)updateHeader:(CGFloat)heightHeader {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGFloat minHeightNavigationBar = [self minHeightForOrientation: orientation];
    CGFloat maxHeightNavigationBar= [self maxHeightForOrientation: orientation];

    CGFloat alpha = (heightHeader - minHeightNavigationBar) / (maxHeightNavigationBar - minHeightNavigationBar);
    self.headerView.infoBar.alpha = alpha;
    self.heightHeader.constant = heightHeader;
}

- (UIView *)headerViewForIan {
    return self.headerView.infoBar;
}


#pragma mark Segue
/*! @brief Show Controller with class name.
 *  @param classController Name of class.*/
- (void)showViewControllerWithClass:(Class)classController {
    for (UIViewController *controller in self.pageViewControllers) {
        if ([controller isKindOfClass:classController]) {
            [self showNextViewController:(LTBasePageViewController *)controller];
            return;
        }
    }
}

/*! @brief Show page controller.
 *  @param controller Page controller object.*/
- (void)showNextViewController:(LTBasePageViewController *)controller {
    LTBasePageViewController *visibleController = [self.pageController.viewControllers firstObject];
    [self prepareControllerToVisibility:controller];
    
    NSInteger visibleIndex = [self.pageViewControllers indexOfObject:visibleController];
    NSInteger index = [self.pageViewControllers indexOfObject:controller];
    
    UIPageViewControllerNavigationDirection direction = visibleIndex > index ? UIPageViewControllerNavigationDirectionReverse : UIPageViewControllerNavigationDirectionForward;
    [self.pageController setViewControllers:@[controller] direction:direction animated:YES completion:nil];
    BOOL isViewControllerPresented = (([self isBeingPresented] || [self isMovingToParentViewController]));
    if(isViewControllerPresented == NO) {
        [self showFirstTimeWizardIfNeeded];
    }
}

/*! @brief Prepare controller for visible on screen.
 *  @param controller Controller object.*/
- (void)prepareControllerToVisibility:(LTBasePageViewController *)controller {
    controller.navigationBarDelegate = self;
    self.headerView.infoBar.appName = controller.title;
    self.headerView.navigationBar.selectedIndex = [self.pageViewControllers indexOfObject:controller];

    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGFloat maxHeightNavigationBar= [self maxHeightForOrientation: orientation];

    [self updateHeader:maxHeightNavigationBar];

    self.footerView.mainNavigationView.selectedIndex = [self.pageViewControllers indexOfObject:controller];

}


#pragma mark UIPage delegate
/*! @brief Get controller object before current.
 *  @param pageViewController Page controller object.
 *  @param viewController Controller which now shown.
 *  @return Controler object.*/
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    NSInteger curentControlleIndex = [self.pageViewControllers indexOfObject:viewController];
    return [self viewControllerWithIndex:curentControlleIndex - 1];
}

/*! @brief Get controller object after current.
 *  @param pageViewController Page controller object.
 *  @param viewController Controller which now shown.
 *  @return Controler object.*/
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSInteger curentControlleIndex = [self.pageViewControllers indexOfObject:viewController];
    return [self viewControllerWithIndex:curentControlleIndex + 1];
}

/*! @brief Called when animation of move between two controllers was finished.
 *  @param pageViewController Page controller object.
 *  @param finished YES if need fifnishing animating.
 *  @param previousViewControllers List of previous controllers.
 *  @param completed Aniamtion completed.*/
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed {
    if (completed) {
        LTBasePageViewController *scrolledController = (LTBasePageViewController *)[pageViewController.viewControllers firstObject];
        [self prepareControllerToVisibility:scrolledController];
        [self showFirstTimeWizardIfNeeded];
    }
}

/*! @brief Spine location for orienation.
 *  @param pageViewController Page controller object.
 *  @param orientation Interface orientation.
 *  @return Spine location value.*/
- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation {
    return UIPageViewControllerSpineLocationNone;
}

/*! @brief Get controller by index from array.
 *  @param index Index of controller.
 *  @return Controler object.*/
- (UIViewController *)viewControllerWithIndex:(NSInteger)index {
    UIViewController *controller = nil;
    if (index >= 0 && index < self.pageViewControllers.count) {
        controller = self.pageViewControllers[index];
    }
    return controller;
}

#pragma mark Actions
/*! @brief Close button action.
 *  @param button Button object which called action.*/
- (void)closeButtonTapped:(UIButton *)button {
//    self.headerView.infoBar.isShowingReward = false;
    
    if ([LTManager sharedManager].isShowingReward) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"closeRewardCard" object:self];
    } else {
        [[LTUIManager sharedManager] closeUI];
    }
}

#pragma mark - Orientation stuff
/*! @brief Return YES if need to rotatio UI when user rotate device.*/
- (BOOL)shouldAutorotate {
    return YES;
}

/*! @brief Get interface orientation mask.
 *  @return Interface orientation mask.*/
- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown ; // UIInterfaceOrientationMaskPortrait;
}

/*! @brief Get interface orientation for presentation.
 *  @return Interface orientation*/
- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark LTNavigationScrollDelegate

- (void)didChangeNavigationBar:(CGFloat)delta {
    CGFloat curentHeight = self.heightHeader.constant;
    CGFloat newHeight = curentHeight + delta;

    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGFloat minHeightNavigationBar = [self minHeightForOrientation: orientation];
    CGFloat maxHeightNavigationBar= [self maxHeightForOrientation: orientation];


    if (newHeight < minHeightNavigationBar) {
        newHeight = minHeightNavigationBar;
    }
    else if (newHeight > maxHeightNavigationBar) {
        newHeight = maxHeightNavigationBar;
    }

    [self updateHeader:newHeight];
}

- (void)didEndScroll {
    CGFloat curentHeight = self.heightHeader.constant;

    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGFloat minHeightNavigationBar = [self minHeightForOrientation: orientation];
    CGFloat maxHeightNavigationBar= [self maxHeightForOrientation: orientation];



    CGFloat newHeight = (curentHeight > (maxHeightNavigationBar + minHeightNavigationBar) / 2) ? maxHeightNavigationBar : minHeightNavigationBar;
    __weak LTViewController *weakSelf = self;
    [UIView animateWithDuration:0.3f animations:^{
        [weakSelf updateHeader:newHeight];
        [weakSelf.view layoutIfNeeded];
    }];
}

#pragma mark LTMainNavigationViewDelegate

- (void) navigateToPage: (LTMainNavigationView *) sender andPageSelected: (NSInteger) page {
    LTBasePageViewController *controller = self.pageViewControllers[page];
    [self showNextViewController:controller];
}

#pragma mark Orientation

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {

    [self updateNavigationBarVisibilities: toInterfaceOrientation];

    //[[self navigationController] setNavigationBarHidden:UIInterfaceOrientationIsLandscape(toInterfaceOrientation) animated:YES];
    //[[UIApplication sharedApplication] setStatusBarHidden:UIInterfaceOrientationIsLandscape(toInterfaceOrientation) animated:YES];
}


- (void) updateNavigationBarVisibilities:(UIInterfaceOrientation)interfaceOrientation {
    // Package this in a helper
    if (UIInterfaceOrientationIsLandscape(interfaceOrientation) /*(interfaceOrientation == UIDeviceOrientationLandscapeLeft ||
                                                                 interfaceOrientation == UIDeviceOrientationLandscapeRight) */) {
        [self.headerView.navigationBar setHidden: YES];
        [self.footerView setHidden: NO];

        [self.topHeaderHeightConstraint setConstant: [self maxHeightForOrientation: interfaceOrientation]];
        [self.topHeaderNavHeightConstraint setConstant: 0.0];

    } else {
        [self.topHeaderHeightConstraint setConstant: [self maxHeightForOrientation: interfaceOrientation]];
        [self.topHeaderNavHeightConstraint setConstant: 44.0];

        [self.headerView.navigationBar setHidden: NO];
        [self.footerView setHidden: YES];
    }
}

- (CGFloat) maxHeightForOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (UIInterfaceOrientationIsLandscape(interfaceOrientation) /*(interfaceOrientation == UIDeviceOrientationLandscapeLeft ||
                                                                 interfaceOrientation == UIDeviceOrientationLandscapeRight) */) {
        return 79.0 - 44.0;
    } else {
        return 79.0;
    }
}

- (CGFloat) minHeightForOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (UIInterfaceOrientationIsLandscape(interfaceOrientation) /*(interfaceOrientation == UIDeviceOrientationLandscapeLeft ||
         interfaceOrientation == UIDeviceOrientationLandscapeRight) */) {
        return 0.0;
    } else {
        return 35.0;
    }
}



@end
