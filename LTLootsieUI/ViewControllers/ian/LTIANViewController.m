//
//  LTIANViewController.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANViewController.h"
#import "LTIANBannerView.h"
#import "LTIANManager.h"
#import "LTUIManager.h"
#import "LTTheme.h"

#import "LTIANGenericItem.h"
#import "LTIANCatalogItem.h"
#import "LTIANRewardItem.h"
#import "LTIANVideoItem.h"

#import "LTUIManager+Catalog.h"
#import "LTEventManager+Private.h"

#import "LTIANGenericView.h"
#import "LTIANCatalogView.h"

#import "LTIANRewardView.h"

#import "LTUIManager+Interstitial.h"
#import "LTIANVideoView.h"
#import "LTBaseInterstitialReward.h"

#import "LTRootViewController.h"

#import "LTMWLogging.h"

#import "LTIANTipHeaderView.h"


@interface LTIANViewController () <UICollectionViewDelegate, UICollectionViewDataSource, LTIANViewDelegate, LTIANVideoViewDelegate>

/*! Container view for show all UI elements.*/
@property (nonatomic, weak)     IBOutlet UIView* containerView;

/*! Collection view for show all IANs items as a list of IAN.*/
@property (nonatomic, weak)     IBOutlet UICollectionView* contentView;

/*! YES when expand animation is start to play.*/
@property (nonatomic, assign)   BOOL isShowExpandAnimation;

/*! Timer for hide banner if user don't interact with it.*/
@property (nonatomic, strong)   NSTimer* timer;


/*! @brief Sort indexes for visible IAN.
 *  @return List of indexes after sort.*/
- (NSArray*)sortIndexPathForVisibleCells;

/*! @brief If banner is in expand mode we need to show items. If not expand mode we remove all ians and close controller.
 *  @param bannerView View with content.*/
- (void)showAllItemsWithBannerView:(LTIANBannerView*)bannerView;

/*! @brief Close controller with delay.
 *  @param timer Timer to detect delay for close.*/
- (void)closeWithDelay:(NSTimer*)timer;

/*! @brief Update all visible IANs with animation.*/
- (void)updateVisibleCellsWithAnimation;

/*! @brief Play start animation for show banner view.*/
- (void)playStartAnimation;

/*! @brief Plat aniamtion for show expand action.*/
- (void)playExpandAnimation;

/*! @brief Remove all items and close controller.*/
- (void)removeAllItems;

/*! @brief Remove special item.
 *  @param item The item object for remove.*/
- (void)removeItem: (LTIANItem* )item;

/*! @brief This methods calls to correct position of all IANs view when user is removed one.*/
- (void)correctPositionAfterRemoveItem;

/*! @brief Update content of banner view. Calls when user removed IAN from list.*/
- (void)updateBennerViewContent;

/*! @brief Load all IANs from stack.*/
- (void)loadStackNotifications;

@end


/*! Identifire for create generic IAN view.*/
static NSString* const LTGenericNotificationCellIdentifier      =   @"genericNotificationCellIdentifier";

/*! Identifire for create catalog IAN view.*/
static NSString* const LTCatalogNotificationCellIdentifier      =   @"catalogNotificationCellIdentifier";

/*! Identifire for create reward IAN view.*/
static NSString* const LTRewardNotificationCellIdentifier       =   @"rewardNotificationCellIdentifier";

/*! Identifire for create video IAN view.*/
static NSString* const LTVideoNotificationCellIdentifier        =   @"videoNotificationCellIdentifier";

/*! First time tool tip key name.*/
static NSString* const LTNotificationFirstTimeKey               =   @"LTNotificationFirstTimeKey";

/*! Identifire for create tool tip view.*/
static NSString* const LTIANFirstTimeViewIdentifier             =   @"ianFirstTimeViewIdentifier";


@implementation LTIANViewController


#pragma mark - Private methods

- (NSArray*)sortIndexPathForVisibleCells {
    NSArray* indexPaths = [self.contentView indexPathsForVisibleItems];
    indexPaths = [indexPaths sortedArrayUsingComparator:^NSComparisonResult(NSIndexPath*  _Nonnull obj1, NSIndexPath*  _Nonnull obj2) {
        return [obj1 compare: obj2];
    }];

    return indexPaths;
}

- (void)showAllItemsWithBannerView:(LTIANBannerView*)bannerView {
    if(bannerView.expanded) {
        if([self.timer isValid]) {
            [self.timer invalidate];
            self.timer = nil;
        }
        [self.contentView reloadData];
        self.view.backgroundColor = [[LTTheme sharedTheme] colorWithType: LTColorModalBackground];
        [self performSelector: @selector(playExpandAnimation) withObject: nil afterDelay: 0.3];
    } else {
        [self removeAllItems];
    }
}

- (void)closeWithDelay:(NSTimer*)timer {
    [self.bannerView hideWithAnimationAndCompletion:^{
        [self removeAllItems];
    }];
}

- (void)updateVisibleCellsWithAnimation {
    NSArray<LTIANGenericView*> *cells = [self.contentView visibleCells];
    for(LTIANGenericView* cell in cells) {
        [cell showCard];
        [cell springAnimation];
    }
}

- (void)playStartAnimation {
    CASpringAnimation* anim = [CASpringAnimation animationWithKeyPath: @"position"];
    anim.fromValue = [NSValue valueWithCGPoint: CGPointMake(self.bannerView.center.x, self.bannerView.center.y - (self.bannerView.frame.size.height))];
    anim.toValue = [NSValue valueWithCGPoint: CGPointMake(self.bannerView.center.x, self.bannerView.center.y)];
    anim.removedOnCompletion = YES;
    anim.duration = 1.4;
    anim.damping = 10.f;
    anim.stiffness = 300.f;
    [self.bannerView.layer addAnimation: anim forKey: @"startAnim"];
    [self.bannerView playAnimationsWithCompletion:^{
        if (!self.bannerView.expanded) {
            self.timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target: self selector: @selector(closeWithDelay:) userInfo:nil repeats:NO];
        }
    }];
}

- (void)playExpandAnimation {
    [self.timer invalidate];
    self.timer = nil;
    NSArray<LTIANGenericView*> *cells = [self.contentView visibleCells];
    cells = [cells sortedArrayUsingComparator:^NSComparisonResult(LTIANGenericView*  _Nonnull obj1, LTIANGenericView*  _Nonnull obj2) {
        if (obj1.item.ianId > obj2.item.ianId) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if (obj1.item.ianId < obj2.item.ianId) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        return (NSComparisonResult)NSOrderedSame;
    }];
    CGFloat duration = 1;
    for(LTIANGenericView* cell in cells) {
        [cell showCardWithAnimation:(2*cell.center.y) duration:duration];
        duration += 0.2f;
    }
    self.isShowExpandAnimation = YES;
    for (LTIANItem *item in _items) {
        [[LTManager sharedManager].eventManager addBaseEvent:LTIANShownEventID additionalNumber:@(item.ianId)];
    }
}

- (void)removeAllItems {
    self.items = nil;
    [[LTUIManager sharedManager] hideViewController: self];
}

- (void)removeItem: (LTIANItem* )item {
    NSMutableArray* tmp = [NSMutableArray arrayWithArray: self.items];
    [tmp removeObject: item];
    self.items = [NSArray arrayWithArray: tmp];
    if(self.items.count == 0) {
        [[LTUIManager sharedManager] hideViewController: self];
    } else {
        [self updateBennerViewContent];
    }
}

- (void)correctPositionAfterRemoveItem {
    NSArray* indexPathesForVisibleCells = [self sortIndexPathForVisibleCells];
    NSIndexPath* indexPath = [indexPathesForVisibleCells firstObject];
    [self.contentView scrollToItemAtIndexPath: indexPath atScrollPosition: UICollectionViewScrollPositionTop animated: YES];
}

- (void)updateBennerViewContent {
    if(self.items.count == 1) {
        LTIANItem* itemObj = [self.items objectAtIndex: 0];
        self.bannerView.title = (self.bannerTitle.length > 0) ? self.bannerTitle : itemObj.title;
        
        if([itemObj isMemberOfClass: [LTIANGenericItem class]] || [itemObj isKindOfClass:[LTIANCatalogItem class]]) {
            self.bannerView.leftPoints = ((LTIANGenericItem*)itemObj).points;
            self.bannerView.leftImage = nil;
        } else {
            self.bannerView.leftPoints = 0;
            self.bannerView.leftImage = [UIImage imageNamed: @"notification_Icon"];
        }
    } else {
        self.bannerView.leftPoints = self.items.count;
        self.bannerView.title = [NSLocalizedStringFromTable(@"New Notifications", @"LTLootsie", nil) uppercaseString];
    }
}

- (void)loadStackNotifications {
    if(self.items.count > 0) {
        [self updateBennerViewContent];
        [self.contentView reloadData];
    }
}


#pragma mark - UICollectionView Delegate
/*! @brief Get number of section for collection view
 *  @param collectionView Collection view object.
 *  @return Number of section.*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

/*! @brief Get number of items for show in section.
 *  @param collectionView Collection view object.
 *  @param section Index of collection view section.
 *  @return Number of items.*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return (self.bannerView.expanded) ? self.items.count : 0;
}

/*! @brief Get header or footer view for section.
 *  @param collectionView Collection view object.
 *  @param kind Type of view.
 *  @param indexPath Index path of view.
 *  @return Reusable view object.*/
- (UICollectionReusableView*)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView* view = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:LTIANFirstTimeViewIdentifier forIndexPath:indexPath];
    return view;
}

/*! @brief Get reference size for header in section.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param section Index of section.
 *  @return Size of header.*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    CGSize size = CGSizeZero;
    if(self.bannerView.expanded) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *isDidShow = [defaults objectForKey: LTNotificationFirstTimeKey];
        if(isDidShow == nil || ![isDidShow boolValue]) {
            size = CGSizeMake(collectionView.frame.size.width, 44.0);
        }
    }
    
    return size;
}

/*! @brief Get size of item for index path.
 *  @param collectionView Collection view object.
 *  @param collectionViewLayout Layout for view.
 *  @param indexPath Index path for item.
 *  @return Size of item*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize size = CGSizeMake(collectionView.bounds.size.width, 154.0);
    UIEdgeInsets sectionInsets = ((UICollectionViewFlowLayout*)collectionViewLayout).sectionInset;
    LTIANItem* item = [self.items objectAtIndex: indexPath.item];
    NSBundle* bundle = [NSBundle bundleForClass: [self class]];
    LTIANGenericView* cell = nil;
    switch (item.type) {
        case LTIANGeneric: {
            cell = [[[UINib nibWithNibName: @"LTIANGenericView" bundle: bundle] instantiateWithOwner: self options: nil] firstObject];
            break;
        }
        case LTIANCatalog: {
            cell = [[[UINib nibWithNibName: @"LTIANCatalogView" bundle: bundle] instantiateWithOwner: self options: nil] firstObject];
            break;
        }
        case LTIANReward: {
            cell = [[[UINib nibWithNibName: @"LTIANRewardView" bundle: bundle] instantiateWithOwner: self options: nil] firstObject];
            break;
        }
        case LTIANVideo: {
            cell = [[[UINib nibWithNibName: @"LTIANVideoView" bundle: bundle] instantiateWithOwner: self options: nil] firstObject];
            break;
        }
        case LTIANUnknown:
        default: {
            break;
        }
    }
    
    cell.item = item;
    [cell layoutIfNeeded];
    
    return CGSizeMake(size.width - sectionInsets.left - sectionInsets.right, cell.contentSize.height);
}

/*! @brief Get cell for collection view with index path.
 *  @param collectionView Collection view object.
 *  @param indexPath Index path for item.
 *  @return Cell object.*/
- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LTIANGenericView* cell = nil;
    LTIANItem* item = [self.items objectAtIndex: indexPath.item];
    switch (item.type) {
        case LTIANGeneric: {
            cell = (LTIANGenericView* )[collectionView dequeueReusableCellWithReuseIdentifier: LTGenericNotificationCellIdentifier forIndexPath: indexPath];
            break;
        }
        case LTIANCatalog: {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier: LTCatalogNotificationCellIdentifier forIndexPath: indexPath];
            break;
        }
        case LTIANReward: {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier: LTRewardNotificationCellIdentifier forIndexPath: indexPath];
            break;
        }
        case LTIANVideo: {
            cell = [collectionView dequeueReusableCellWithReuseIdentifier: LTVideoNotificationCellIdentifier forIndexPath: indexPath];
            break;
        }
        case LTIANUnknown:
        default: {
            break;
        }
    }
    if (self.isShowExpandAnimation) {
        [cell showCard];
    }
    cell.zIndex =  self.items.count - (indexPath.item + 1);
    cell.item = [self.items objectAtIndex: indexPath.item];
    cell.delegate = self;
    
    return cell;
}


#pragma mark - LTIANViewDelegate methods
/*! @brief Called when IAN view did close.
 *  @param ianView IAN view object.*/
- (void)ianViewDidClose:(LTIANGenericView *)ianView {
    [UIView performSystemAnimation: UISystemAnimationDelete onViews: @[ianView] options: UIViewAnimationOptionCurveEaseInOut animations: ^{
        ianView.alpha = 0.0;
    } completion: ^(BOOL finished) {
        if(ianView.item.type == LTIANCatalog || ianView.item.type == LTIANVideo) {
            [[LTUIManager sharedManager] showRewardsPage];
        }
        else if(ianView.item.type == LTIANReward) {
            CGPoint center = [UIApplication sharedApplication].keyWindow.center;
            [[LTUIManager sharedManager] showDetailRewardPage: ((LTIANRewardItem*)ianView.item).reward onViewController: nil  fromStartPoint: center onClose: nil];
        }
        else {
            if(self.onAction) {
                self.onAction(ianView.item);
            }
        }
        [self removeAllItems];
    }];
}

/*! @brief Called when user use right swipe for remove IAN.
 *  @param ianView IAN view object.*/
- (void)ianViewDidSwipeRight:(LTIANGenericView *)ianView {
    [self removeCell:ianView toPostion:self.contentView.frame.size.width];
}

/*! @brief Called when user use left swipe for remove IAN.
 *  @param ianView IAN view object.*/
- (void)ianViewDidSwipeLeft:(LTIANGenericView *)ianView {
    [self removeCell:ianView toPostion:-self.contentView.frame.size.width];
}

/*! @brief Animate remove cell.
 *  @param ianView IAN view object.
 *  @param positionCell Position of cell*/
- (void)removeCell:(LTIANGenericView *)ianView toPostion:(CGFloat)positionCell {
    NSIndexPath* indexPath = [self.contentView indexPathForCell: ianView];
    NSInteger countItemsBeforeRemove = self.items.count;
    [UIView animateWithDuration: 0.3 animations: ^{
        CGRect frame = ianView.frame;
        frame.origin.x = positionCell;
        ianView.frame = frame;
        ianView.alpha = 0.0;
    } completion: ^(BOOL finished) {
        [self.contentView performBatchUpdates:^{
            [self removeItem: ianView.item];
            [self.contentView deleteItemsAtIndexPaths: @[indexPath]];
            [self updateVisibleCellsWithAnimation];
        } completion:^(BOOL finished) {
            if(self.items.count > 1) {
                [self updateVisibleCellsWithAnimation];
            } else if(self.items.count == 1 && indexPath.item < countItemsBeforeRemove - 1) {
                [self updateVisibleCellsWithAnimation];
            }
            [self correctPositionAfterRemoveItem];
        }];
    }];
}


#pragma mark - UIViewController standard
/*! @brief Called when controller is loaded.*/
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    
    [self.contentView registerNib: [UINib nibWithNibName: @"LTIANGenericView" bundle: nil] forCellWithReuseIdentifier: LTGenericNotificationCellIdentifier];
    [self.contentView registerNib: [UINib nibWithNibName: @"LTIANCatalogView" bundle: nil] forCellWithReuseIdentifier: LTCatalogNotificationCellIdentifier];
    [self.contentView registerNib: [UINib nibWithNibName: @"LTIANRewardView" bundle: nil] forCellWithReuseIdentifier: LTRewardNotificationCellIdentifier];
    [self.contentView registerNib: [UINib nibWithNibName: @"LTIANTipHeaderView" bundle:nil]
       forSupplementaryViewOfKind: UICollectionElementKindSectionHeader
              withReuseIdentifier: LTIANFirstTimeViewIdentifier];
    self.contentView.contentInset = UIEdgeInsetsMake(21, 0, 20, 0);

    [self loadStackNotifications];

    __weak typeof(self) tmp = self;
    self.bannerView.onViewButtonTap = ^(LTIANBannerView* view) {
        [tmp showAllItemsWithBannerView: view];
    };
    self.bannerView.onBannerTouch = ^(LTIANBannerView* view) {
        [tmp showAllItemsWithBannerView: view];
    };
    
    self.bannerView.hidden = YES;
}

/*! @brief Called when controller did appear on screen.
 *  @param animated YES if controllwe was appear with animation.*/
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear: animated];
    self.bannerView.hidden = NO;
    if ([self isBeingPresented] || [self isMovingToParentViewController]) {
        [self playStartAnimation];
    }
}

/*! @brief Called when controller did disappear from screen.
 *  @param animated YES if controllwe was disappeared with animation.*/
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    if ([self isBeingDismissed] || [self isMovingFromParentViewController]) {
        if([self.timer isValid]) {
            [self.timer invalidate];
        }
        if(self.onClose) {
            self.onClose();
        }
        [[NSUserDefaults standardUserDefaults] setObject:@YES forKey: LTNotificationFirstTimeKey];
    }
}


#pragma mark - Clear memory
/*! @brief Called when system detect memory warning and need to clear some data.*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
