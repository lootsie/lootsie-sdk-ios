//
//  LTPopupTwoButtonViewController.h
//  LTLootsie iOS Example
//
//  Created by Courtney Osborne on 5/25/17.
//  Copyright © 2017 Lootsie. All rights reserved.
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//

#import <UIKit/UIKit.h>


/*! @class LTPopupTwoButtonViewController
 *  @brief Popup controller for displaying message.*/
@interface LTPopupTwoButtonViewController : UIViewController

/*! Title of message.*/
@property (nonatomic, strong) NSString *titleText;

/*! Description of message.*/
@property (nonatomic, strong) NSString *descriptionText;

/*! First button text.*/
@property (nonatomic, strong) NSString *leftButtonText;

/*! Second button text.*/
@property (nonatomic, strong) NSString *rightButtonText;

/*! Callback is called when user close popup.*/
@property (nonatomic, copy) void (^closeAction)(void);


@end
