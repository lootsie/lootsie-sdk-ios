//  LTDatabase.m
//  Created by Fabio Teles on 7/9/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTConstants.h"
#import "LTDatabase.h"
#import "LTData.h"

#import "LTMWLogging.h"

@interface LTDatabase () <NSKeyedUnarchiverDelegate>
/*! Data object for save. @see LTData*/
@property (nonatomic, readwrite, strong) LTData *data;

@end


@implementation LTDatabase

/*! @brief Generate path to file which will save data.
 *  @return Full path to file.*/
+ (NSString *)pathForDatabaseFile
{
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dir = [libraryPath stringByAppendingPathComponent:LTDatabaseStorageFolder];
    
    return [dir stringByAppendingPathComponent:LTDatabaseStorageFilename];
}

- (LTData *)data {
    if (!_data) {
        [self loadData];
    }
    return _data;
}

/*! @brief Load data from cache.*/
- (void)loadData {
    if (_data) return;
    
        // 1. Try to load existing data
        _data = [self loadDataWithContentsOfFile:[[self class] pathForDatabaseFile] oldVesion:NO];
    if (_data) return;
    
        // 2. If not found, backward Compatibility
        _data = [self tryLoadingOlderVersion];
    if (_data) {
        if ([self save]) {
            // delete older version if we can save if successfully
            [self deleteOlderVersions];
        }
        return;
    }
    
        // 3. If not found, creates a new data object
        _data = [LTData new];
    [self save];
}

- (BOOL)save {
    if (_data) {
        LTLogDebug(@"Saving database");
        NSString *filePath = [[self class] pathForDatabaseFile];
        NSError *error;
        if (![[NSFileManager defaultManager] fileExistsAtPath:[filePath stringByDeletingLastPathComponent]]) {
            [[NSFileManager defaultManager] createDirectoryAtPath:[filePath stringByDeletingLastPathComponent] withIntermediateDirectories:YES attributes:nil error:&error];
        }
        
        if (error) {
            LTLogError(@"Failed to create directory to save database. Error: %@", error.localizedDescription);
            return NO;
        }
        
        NSMutableData *data = [NSMutableData data];
        
        NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
        archiver.requiresSecureCoding = YES;
        [archiver encodeObject:_data forKey:NSKeyedArchiveRootObjectKey];
        [archiver finishEncoding];
        
        [data writeToFile:filePath options:NSDataWritingAtomic error:&error];
        if (error) {
            LTLogError(@"Failed to save database. Error: %@", error.localizedDescription);
            return NO;
        } else {
            return YES;
        }
    }
    return NO;
}

/*! @brief Load data from cache.
 *  @param path Path to cache file.
 *  @param oldVersion If YES we need to load data from old version of cache.*/
- (LTData *)loadDataWithContentsOfFile:(NSString *)path oldVesion:(BOOL)oldVersion
{
    LTData *loadedObject = nil;
    // tries to load data
    NSData *data = [NSData dataWithContentsOfFile:path];
    // if found file tries to unarchiver
    if (data) {
        LTLogDebug(@"Loading data with contents of file: %@", path);
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        unarchiver.requiresSecureCoding = YES;
        NSString *objectKey = NSKeyedArchiveRootObjectKey;
        if (oldVersion) {
            [unarchiver setClass:[LTData class] forClassName:LTDatabaseStorageOldClassName];
            objectKey = @"Data";
        }
        loadedObject = [unarchiver decodeObjectOfClass:[LTData class] forKey:objectKey];
        [unarchiver finishDecoding];
    }
    return loadedObject;
}

/*! @brief Loading data from old version of cache.
 *  @return Data object if data was loaded correctly.*/
- (LTData *)tryLoadingOlderVersion {
    // get private docs dir
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dir = [libraryPath stringByAppendingPathComponent:LTDatabaseStorageOldFolder];
    LTLogDebug(@"Trying to load older version of database at directory: %@", dir);
    
    // get contents of directory (don't handle error because it's ok to return nil)
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dir error:NULL];
    
    // go through files and return the first found (just like the older version did)
    for (NSString *file in files) {
        if ([file.pathExtension compare:@"lootsiedata" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            NSString *foundFilePath = [[dir stringByAppendingPathComponent:file] stringByAppendingPathComponent:LTDatabaseStorageOldFilename];
            if ([[NSFileManager defaultManager] fileExistsAtPath:foundFilePath isDirectory:NULL]) {
                LTLogInfo(@"Found older version of database: %@", foundFilePath);
                return [self loadDataWithContentsOfFile:foundFilePath oldVesion:YES];
            }
        }
    }
    
    // if nothing found, return nil
    return nil;
}

/*! @brief Delete old version of data from cache.*/
- (void)deleteOlderVersions {
    NSString *libraryPath = [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *dir = [libraryPath stringByAppendingPathComponent:LTDatabaseStorageOldFolder];
    
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:dir error:NULL];
    
    // Go through files and delete all that matches lootsie old databases
    for (NSString *file in files) {
        if ([file.pathExtension compare:@"lootsiedata" options:NSCaseInsensitiveSearch] == NSOrderedSame) {
            NSString *foundFile = [dir stringByAppendingPathComponent:file];
            [[NSFileManager defaultManager] removeItemAtPath:foundFile error:NULL];
            LTLogInfo(@"Deleting older version of database at: %@", foundFile);
        }
    }
}

@end
