//  LTDatabaseProtocol.h
//  Created by Fabio Teles on 9/16/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

@import Foundation;

@class LTData;
/*! @protocol LTDatabaseProtocol
 *  @brief Additional methods for work with data from cache.*/
@protocol LTDatabaseProtocol <NSObject>

/*! Readonly property that returns a LTData object */
@property (nonatomic, readonly, strong) LTData *data;

/*! @brief Saves the database and returns a BOOL value indicating the success or failure of
 *  the operation. LTManager will call this method periodically to save the user's data
 *  and will also manage the auto saving for background mode. Do not implement saving
 *  the database within itself.*/
- (BOOL)save;

@end
