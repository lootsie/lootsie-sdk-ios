//  LTManager+Private.h
//  Created by Alexander Dovbnya on 3/23/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTManager.h"
#import "LTBlockOperation.h"

@class LTOperation;
/*! @typedef LTSuccessBlock
 *  @brief Block of code for success operation.*/
typedef dispatch_block_t LTSuccessBlock;

/*! @typedef LTSuccessBlock
 *  @brief Block of code for failure operation.*/
typedef void (^LTFailureBlock)(NSError *);


/*! @category LTManager(Private)
 *  @brief Private methods and properties for core manager.*/
@interface LTManager (Private)

/*! Operation which will call after initialized SDK.*/
@property (nonatomic, strong, readonly) LTBlockOperation *afterInitOp;


/*! @brief Add start pending network operation.
 *  @param operation Operation object.*/
- (void)addStartPendingNetworkOperation:(LTOperation *)operation;

/*! @brief Start operation.*/
- (void)addStartOperation;

/*! @brief Add network operation.
 *  @param operation Operation object.*/
- (void)addNetworkOperation:(LTOperation *)operation;

/*! @brief Start network operation.
 *  @param operation Operation object.
 *  @param startIfPossible YES if need to start this operation if this is possible.*/
- (void)addStartPendingNetworkOperation:(LTOperation *)operation startIfPossible:(BOOL)startIfPossible;

/*! @brief Add lazy operation.
 *  @param block Callback is called if operation is success finished.*/
- (void)addLazyStartSuccessBlock:(LTSuccessBlock)block;

/*! @brief Add lazy operation.
 *  @param block Callback is called if operation is failure.*/
- (void)addLazyStartFailureBlock:(LTFailureBlock)block;

/*! @brief Add general operation.
 *  @param generalOp Operation for work.
 *  @param dependencies Operation depencencies list.
 *  @param allowCancelledDependencies Allow cancel operation by dependencies.*/
- (void)addGeneralOperation:(LTOperation *)generalOp dependencies:(NSArray *)dependencies allowCancelledDependencies:(BOOL)allowCancelledDependencies;

/*! @brief Create after operation.
 *  @return Block of code.*/
- (LTBlockOperation *)createAfterOperation;

/*! @brief Enable sequesnde timer.
 *  @param enable YES enable timer, NO - disable.*/
- (void)enableSequenceExponentialBackoffTimer:(BOOL)enable;

/*! @brief Set operation delegate for use special operation methods.
 *  @param delegate Operation methods deleagate. @see LTOperationQueueDelegate*/
- (void)setOperationDelegate:(id<LTOperationQueueDelegate>)delegate;

/*! @brief Change the status to account deactivated. */
- (void)setStatusAccountDeactivated;

/*! @brief Change the status to account activated. */
- (void)setStatusAccountActivated;

@end
