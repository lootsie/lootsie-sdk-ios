//  LTSequence.m
//  Created by Fabio Teles on 8/13/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTSequence.h"
#import "LTMWLogging.h"

NSString * const LTSequenceOtherErrosKey = @"com.lootsie.sequence.otherErros";

@interface LTSequence ()

/*! List of trivial operations.*/
@property (nonatomic, strong) NSMutableSet *trivialOperations;

@end


@implementation LTSequence

+ (instancetype)groupWithOperations:(NSArray *)operations trivialOperations:(NSArray *)trivialOperations {
    LTSequence *sequence = [self groupWithOperations:operations];
    [trivialOperations enumerateObjectsUsingBlock:^(NSOperation *op, NSUInteger idx, BOOL *stop) {
        [sequence addTrivialOperation:op];
    }];
    return sequence;
}

- (instancetype)initWithOperations:(NSArray *)operations {
    if (self = [super initWithOperations:operations]) {
        [self setSafeName:[NSString stringWithFormat:@"LTSequence<%@>", NSStringFromClass([self class])]];
    }
    return self;
}

- (void)operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors {
    // cancel all subsequent operations if one fails and it's non-trivial
    if (!self.isCancelled && operation.isCancelled && ![self.trivialOperations containsObject:operation]) {
        LTLogDebug(@"Cancelling %@ because the op: %@ failed", self, operation);
        [self cancel];
    }
}

- (void)finished:(NSArray *)errors {

    NSError *returningError;
    if (errors && errors.firstObject) {
        if (errors.count == 1) returningError = errors.firstObject;
        else {
            // Combine all errors so the sequence can return only one.
            NSError *firstError = errors.firstObject;
            NSMutableDictionary *mainUserInfo = [firstError.userInfo mutableCopy];
            mainUserInfo[LTSequenceOtherErrosKey] = [errors subarrayWithRange:NSMakeRange(1, errors.count-1)];
            returningError = [NSError errorWithDomain:firstError.domain
                                                 code:firstError.code
                                             userInfo:[NSDictionary dictionaryWithDictionary:mainUserInfo]];
        }
    }
    
    __weak __typeof(self) weakSelf = self;
    dispatch_async((self.resultQueue ?: dispatch_get_main_queue()), ^{
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        LTLogDebug(@"Finishing %@ %@", self, (errors.count > 0 ? [[errors valueForKeyPath:@"localizedDescription"] arrayByAddingObjectsFromArray:[errors valueForKeyPath:@"localizedRecoverySuggestion"]] : @""));
        
        /// IMPORTANT: Consider only cancelled sequences as failures.
        if (self.isCancelled) {
            if (strongSelf.failure) strongSelf.failure(returningError);
        } else {
            if (strongSelf.success) strongSelf.success();
        }
        
        // freeing blocks
        strongSelf.success = nil;
        strongSelf.failure = nil;
    });
}

- (void)addTrivialOperation:(NSOperation *)operation {
    if (!self.trivialOperations) {
        self.trivialOperations = [NSMutableSet set];
    }
    [self.trivialOperations addObject:operation];
    [self addOperation:operation];
}

- (void)setSuccess:(void (^)(void))success failure:(void (^)(NSError *))failure {
    self.success = success;
    self.failure = failure;
}

@end
