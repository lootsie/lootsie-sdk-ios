//  LTSequence.h
//  Created by Fabio Teles on 8/13/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTGroupOperation.h"

extern NSString * const LTSequenceOtherErrosKey;

/*! @class LTSequence
 *  @brief Base class of sequeance operation.*/
@interface LTSequence : LTGroupOperation

/*! Callback is called when all operations was success finished.*/
@property (nonatomic, strong) void (^success)(void);

/*! Callback is called when all operations was failure finished.*/
@property (nonatomic, strong) void (^failure)(NSError *error);

/*! Result queue.*/
@property (nonatomic, strong) dispatch_queue_t resultQueue;


/*! @brief Create a new squeance of operation.
 *  @param operations List of operations.
 *  @param trivialOperations List of trivial operations.*/
+ (instancetype)groupWithOperations:(NSArray *)operations trivialOperations:(NSArray *)trivialOperations;

/*! @brief Set callbacks of seccuss and failure.
 *  @param success Callback is called when all operations is correct finished.
 *  @param failure Callback is called when operation is finished for error.*/
- (void)setSuccess:(void (^)(void))success failure:(void (^)(NSError *))failure;

/*! @brief Operations added as trivial are operation that won't cancel the whole sequence
 *  in case they fail. A good example is the Location operation that might not have
 *  permissions or not be available.
 *  @param operation Operation object.*/
- (void)addTrivialOperation:(NSOperation *)operation;

@end
