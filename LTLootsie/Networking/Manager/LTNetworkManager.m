//  LTNetworkManager.m
//  Created by Fabio Teles on 7/16/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTNetworkManager.h"
#import "LTNetworkObserver.h"
#import "LTOperationQueue.h"

#import "LTConstants.h"
#import "LTDatabase.h"
#import "LTData.h"

#import "LTAFHTTPSessionManager.h"

NSString * const LTAPISessionPath                           = @"0/init";
NSString * const LTAPIUserAccountPath                       = @"0/user";
NSString * const LTAPIUserRewardRedemptionsPath             = @"0/redeem";
NSString * const LTAPIUserRewardPath                        = @"0/catalog";
NSString * const LTAPIUserRewardWithIDPath                  = @"0/reward/";
NSString * const LTAPIFavoriteRewardPath                    = @"0/favorite";
NSString * const LTAPIUserActivityPath                      = @"0/events";

NSString * const LTNetworkErrorDomain                       = @"com.lootsie.networking.error";
NSString * const LTNetworkResponseErrorsJSONKey             = @"com.lootsie.networking.response.json.errors";
NSString * const LTNetworkQueueName                         = @"com.lootsie.networking.queue";


@interface LTNetworkManager ()
/*! Requests queue.*/
@property (readwrite, nonatomic, strong) LTOperationQueue *networkQueue;

/*! Session manager. Using for work with requests session.*/
@property (nonatomic, strong) LTAFHTTPSessionManager *sessionManager;

@end


@implementation LTNetworkManager

+ (instancetype)lootsieManager {
    static LTNetworkManager *_sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        _sharedInstance = [self new];
    });
    return _sharedInstance;
}

/*! @brief Create a new object.*/
- (instancetype)init {
    if (self = [super init]) {
        _sessionManager = [[LTAFHTTPSessionManager alloc] initWithBaseURL:[[self class] baseAPIURL]
                                                     sessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        _sessionManager.requestSerializer = [LTAFJSONRequestSerializer serializer];
        _networkQueue = [LTOperationQueue new];
        _networkQueue.name = LTNetworkQueueName;
    }
    return self;
}

/*! @brief Return base url for send request.*/
+ (NSURL *)baseAPIURL {
    if ([self typeServer] == LTLifeTypeServer) {
        return [NSURL URLWithString:LTBaseLiveAPIPath];
    }
    else {
        return [NSURL URLWithString:LTBaseStagingAPIPath];
    }
}

+ (LTTypeServer)typeServer {
    return LTLifeTypeServer;
}

- (void)addNetworkOperation:(LTOperation *)operation {
    // add network observer to display network activity in status bar
    [operation addObserver:[LTNetworkObserver new]];
    
    [self.networkQueue addOperation:operation];
}

- (LTOperation *)lastQueuedNonFinishedOperationOfType:(Class)operationType {
    __block LTOperation *foundOp;
    [self.networkQueue.operations enumerateObjectsWithOptions:NSEnumerationReverse
                                                   usingBlock:^(__kindof NSOperation * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:operationType] && !obj.isFinished) {
            foundOp = obj;
            *stop = YES;
        }
    }];
    return foundOp;
}

- (NSURLRequest *)requestWithHTTPMethod:(NSString *)method
                                   path:(NSString *)path
                             parameters:(id)parameters
                  additionalHTTPHeaders:(NSDictionary *)additionalHTTPHeaders
                                  error:(NSError *__autoreleasing *)error {
    NSString *urlString = [[NSURL URLWithString:path relativeToURL:self.baseURL] absoluteString];
    return [self requestWithHTTPMethod:method url:urlString parameters:parameters additionalHTTPHeaders:additionalHTTPHeaders error:error];
}

- (NSURLRequest *)requestWithHTTPMethod:(NSString *)method
                                    url:(NSString *)urlString
                             parameters:(id)parameters
                  additionalHTTPHeaders:(NSDictionary *)additionalHTTPHeaders
                                  error:(NSError *__autoreleasing *)error {
    
    NSError *serializationError = nil;
    NSMutableURLRequest *request = [self.sessionManager.requestSerializer requestWithMethod:method URLString:urlString parameters:parameters error:&serializationError];
    
    
    if (serializationError) {
        if (error) *error = serializationError;
        return nil;
    }
    
    [additionalHTTPHeaders enumerateKeysAndObjectsUsingBlock:^(id field, id value, BOOL * __unused stop) {
        if (![request valueForHTTPHeaderField:field]) {
            [request setValue:value forHTTPHeaderField:field];
        }
    }];
    
    return request;
}

- (NSURL *)baseURL {
    return self.sessionManager.baseURL;
}

- (LTAFNetworkReachabilityManager *)reachabilityManager {
    return self.sessionManager.reachabilityManager;
}

- (NSURLSessionDataTask *)dataTaskWithRequest:(NSURLRequest *)request
                            completionHandler:(void (^)(NSURLResponse *response, id responseObject, NSError *error))completionHandler {
    return [self.sessionManager dataTaskWithRequest:request completionHandler:completionHandler];
}

@end
