//  LTNetworkManager.h
//  Created by Fabio Teles on 7/16/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>

@class LTOperation, LTOperationQueue;

/*! API session path key.*/
extern NSString * const LTAPISessionPath;

/*! API account path key.*/
extern NSString * const LTAPIUserAccountPath;

/*! API catalog path key.*/
extern NSString * const LTAPIUserRewardPath;

/*! API reward with id path key.*/
extern NSString * const LTAPIUserRewardWithIDPath;

/*! API reward redemptions path key.*/
extern NSString * const LTAPIUserRewardRedemptionsPath;

/*! API favorite reward path key.*/
extern NSString * const LTAPIFavoriteRewardPath;

/*! API user event path key.*/
extern NSString * const LTAPIUserActivityPath;

/*! Network error domain.*/
extern NSString * const LTNetworkErrorDomain;

/*! JSON data key name.*/
extern NSString * const LTNetworkResponseErrorsJSONKey;


/*! Servers type.*/
typedef NS_ENUM(NSUInteger, LTTypeServer) {
    LTLifeTypeServer,       /*! Real server for get data.*/
    LTStageTypeServer       /*! Stage server with test data.*/
};


@class LTAFNetworkReachabilityManager;
/*! @class LTNetworkManager
 *  @brief Manager for work with network. Use this manager for sending request to server.*/
@interface LTNetworkManager : NSObject

/*! Base url for server.*/
@property (nonatomic, readonly) NSURL *baseURL;

/*! Reachability manager for detect connection state.*/
@property (nonatomic, readonly) LTAFNetworkReachabilityManager *reachabilityManager;

/*! Requests queue. Using for send request to server.*/
@property (nonatomic, readonly, strong) LTOperationQueue *networkQueue;


/*! @brief Create a network manager or return exists.*/
+ (instancetype)lootsieManager;

/*! @brief Get type of server. @see LTTypeServer*/
+ (LTTypeServer)typeServer;

/*! @brief Start network operation.
 *  @param operation Network operation for work.*/
- (void)addNetworkOperation:(LTOperation *)operation;

/*! @brief Last queued non finished operation by type.
 *  @param operationType Class of operation type.*/
- (LTOperation *)lastQueuedNonFinishedOperationOfType:(Class)operationType;

/*! @brief Generate HTTP request with path.
 *  @param method Request method (POST, GET, PUT, DELETE, etc)
 *  @param path Script path.
 *  @param parameters List of parameters
 *  @param additionalHTTPHeaders List of additional HTTP request headers.
 *  @param error Error object.*/
- (NSURLRequest *)requestWithHTTPMethod:(NSString *)method
                                   path:(NSString *)path
                             parameters:(id)parameters
                  additionalHTTPHeaders:(NSDictionary *)additionalHTTPHeaders
                                  error:(NSError *__autoreleasing *)error;

/*! @brief Generate HTTP request with path.
 *  @param method Request method (POST, GET, PUT, DELETE, etc)
 *  @param urlString Server url string.
 *  @param parameters List of parameters
 *  @param additionalHTTPHeaders List of additional HTTP request headers.
 *  @param error Error object.*/
- (NSURLRequest *)requestWithHTTPMethod:(NSString *)method
                                    url:(NSString *)urlString
                             parameters:(id)parameters
                  additionalHTTPHeaders:(NSDictionary *)additionalHTTPHeaders
                                  error:(NSError *__autoreleasing *)error;

/*! @brief Get data task for send request.
 *  @param request Request object.
 *  @param completionHandler Callback is called when request was send to server.*/
- (NSURLSessionDataTask *)dataTaskWithRequest:(NSURLRequest *)request
                            completionHandler:(void (^)(NSURLResponse *response, id responseObject, NSError *error))completionHandler;

@end

