// LTAFNetworkReachabilityManager.m
// Copyright (c) 2011–2015 Alamofire Software Foundation (http://alamofire.org/)
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

#import "LTAFNetworkReachabilityManager.h"
#if !TARGET_OS_WATCH

#import <netinet/in.h>
#import <netinet6/in6.h>
#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h>

/*! Notifications called whennetwork connection type was changed.*/
NSString * const LTAFNetworkingReachabilityDidChangeNotification = @"com.alamofire.networking.reachability.change";

/*! Network connection status key name.*/
NSString * const LTAFNetworkingReachabilityNotificationStatusItem = @"LTAFNetworkingReachabilityNotificationStatusItem";

/*! @typedef LTAFNetworkReachabilityStatusBlock
 *  @brief Callback is called when network status was changed.
 *  @param status New status of network connection.*/
typedef void (^LTAFNetworkReachabilityStatusBlock)(LTAFNetworkReachabilityStatus status);

/*! Network connection association type.*/
typedef NS_ENUM(NSUInteger, LTAFNetworkReachabilityAssociation) {
    LTAFNetworkReachabilityForAddress = 1,          /*! Network addess.*/
    LTAFNetworkReachabilityForAddressPair = 2,      /*! Network pair address.*/
    LTAFNetworkReachabilityForName = 3,             /*! Network name.*/
};

/*! @brief Callback is called when manager detect change network connection.
 *  @param status New network connection status.*/
NSString * LTAFStringFromNetworkReachabilityStatus(LTAFNetworkReachabilityStatus status) {
    switch (status) {
        case LTAFNetworkReachabilityStatusNotReachable:
            return NSLocalizedStringFromTable(@"Not Reachable", @"LTAFNetworking", nil);
        case LTAFNetworkReachabilityStatusReachableViaWWAN:
            return NSLocalizedStringFromTable(@"Reachable via WWAN", @"LTAFNetworking", nil);
        case LTAFNetworkReachabilityStatusReachableViaWiFi:
            return NSLocalizedStringFromTable(@"Reachable via WiFi", @"LTAFNetworking", nil);
        case LTAFNetworkReachabilityStatusUnknown:
        default:
            return NSLocalizedStringFromTable(@"Unknown", @"LTAFNetworking", nil);
    }
}

/*! @brief Manager detect connection status flags.
 *  @param flags Flags of connection.*/
static LTAFNetworkReachabilityStatus LTAFNetworkReachabilityStatusForFlags(SCNetworkReachabilityFlags flags) {
    BOOL isReachable = ((flags & kSCNetworkReachabilityFlagsReachable) != 0);
    BOOL needsConnection = ((flags & kSCNetworkReachabilityFlagsConnectionRequired) != 0);
    BOOL canConnectionAutomatically = (((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) || ((flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0));
    BOOL canConnectWithoutUserInteraction = (canConnectionAutomatically && (flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0);
    BOOL isNetworkReachable = (isReachable && (!needsConnection || canConnectWithoutUserInteraction));

    LTAFNetworkReachabilityStatus status = LTAFNetworkReachabilityStatusUnknown;
    if (isNetworkReachable == NO) {
        status = LTAFNetworkReachabilityStatusNotReachable;
    }
#if	TARGET_OS_IPHONE
    else if ((flags & kSCNetworkReachabilityFlagsIsWWAN) != 0) {
        status = LTAFNetworkReachabilityStatusReachableViaWWAN;
    }
#endif
    else {
        status = LTAFNetworkReachabilityStatusReachableViaWiFi;
    }

    return status;
}

/*! @brief Callback is called when manager need to analyze info.
 *  @param target Target of network reachability.
 *  @param flags Network flags.
 *  @param info Info object.*/
static void LTAFNetworkReachabilityCallback(SCNetworkReachabilityRef __unused target, SCNetworkReachabilityFlags flags, void *info) {
    LTAFNetworkReachabilityStatus status = LTAFNetworkReachabilityStatusForFlags(flags);
    LTAFNetworkReachabilityStatusBlock block = (__bridge LTAFNetworkReachabilityStatusBlock)info;
    if (block) {
        block(status);
    }


    dispatch_async(dispatch_get_main_queue(), ^{
        NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
        NSDictionary *userInfo = @{ LTAFNetworkingReachabilityNotificationStatusItem: @(status) };
        [notificationCenter postNotificationName:LTAFNetworkingReachabilityDidChangeNotification object:nil userInfo:userInfo];
    });

}

static const void * LTAFNetworkReachabilityRetainCallback(const void *info) {
    return Block_copy(info);
}

static void LTAFNetworkReachabilityReleaseCallback(const void *info) {
    if (info) {
        Block_release(info);
    }
}


@interface LTAFNetworkReachabilityManager ()

/*! Network reachability object.*/
@property (readwrite, nonatomic, strong) id networkReachability;

/*! Network reachability association. @see LTAFNetworkReachabilityAssociation*/
@property (readwrite, nonatomic, assign) LTAFNetworkReachabilityAssociation networkReachabilityAssociation;

/*! Network reachability status. @see LTAFNetworkReachabilityStatus*/
@property (readwrite, nonatomic, assign) LTAFNetworkReachabilityStatus networkReachabilityStatus;

/*! Network reachability status callback. @see LTAFNetworkReachabilityStatusBlock*/
@property (readwrite, nonatomic, copy) LTAFNetworkReachabilityStatusBlock networkReachabilityStatusBlock;

@end


@implementation LTAFNetworkReachabilityManager

+ (instancetype)sharedManager {
    static LTAFNetworkReachabilityManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        struct sockaddr_in address;
        bzero(&address, sizeof(address));
        address.sin_len = sizeof(address);
        address.sin_family = AF_INET;

        _sharedManager = [self managerForAddress:&address];
    });

    return _sharedManager;
}

+ (instancetype)managerForDomain:(NSString *)domain {
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(kCFAllocatorDefault, [domain UTF8String]);

    LTAFNetworkReachabilityManager *manager = [[self alloc] initWithReachability:reachability];
    manager.networkReachabilityAssociation = LTAFNetworkReachabilityForName;

    return manager;
}

+ (instancetype)managerForAddress:(const void *)address {
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr *)address);

    LTAFNetworkReachabilityManager *manager = [[self alloc] initWithReachability:reachability];
    manager.networkReachabilityAssociation = LTAFNetworkReachabilityForAddress;

    return manager;
}

- (instancetype)initWithReachability:(SCNetworkReachabilityRef)reachability {
    self = [super init];
    if (!self) {
        return nil;
    }

    self.networkReachability = CFBridgingRelease(reachability);
    self.networkReachabilityStatus = LTAFNetworkReachabilityStatusUnknown;

    return self;
}

- (instancetype)init NS_UNAVAILABLE
{
    return nil;
}

- (void)dealloc {
    [self stopMonitoring];
}

#pragma mark -

- (BOOL)isReachable {
    return [self isReachableViaWWAN] || [self isReachableViaWiFi];
}

- (BOOL)isReachableViaWWAN {
    return self.networkReachabilityStatus == LTAFNetworkReachabilityStatusReachableViaWWAN;
}

- (BOOL)isReachableViaWiFi {
    return self.networkReachabilityStatus == LTAFNetworkReachabilityStatusReachableViaWiFi;
}

#pragma mark -

- (void)startMonitoring {
    [self stopMonitoring];

    if (!self.networkReachability) {
        return;
    }

    __weak __typeof(self)weakSelf = self;
    LTAFNetworkReachabilityStatusBlock callback = ^(LTAFNetworkReachabilityStatus status) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;

        strongSelf.networkReachabilityStatus = status;
        if (strongSelf.networkReachabilityStatusBlock) {
            strongSelf.networkReachabilityStatusBlock(status);
        }

    };

    id networkReachability = self.networkReachability;
    SCNetworkReachabilityContext context = {0, (__bridge void *)callback, LTAFNetworkReachabilityRetainCallback, LTAFNetworkReachabilityReleaseCallback, NULL};
    SCNetworkReachabilitySetCallback((__bridge SCNetworkReachabilityRef)networkReachability, LTAFNetworkReachabilityCallback, &context);
    SCNetworkReachabilityScheduleWithRunLoop((__bridge SCNetworkReachabilityRef)networkReachability, CFRunLoopGetMain(), kCFRunLoopCommonModes);

    switch (self.networkReachabilityAssociation) {
        case LTAFNetworkReachabilityForName:
            break;
        case LTAFNetworkReachabilityForAddress:
        case LTAFNetworkReachabilityForAddressPair:
        default: {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
                SCNetworkReachabilityFlags flags;
                SCNetworkReachabilityGetFlags((__bridge SCNetworkReachabilityRef)networkReachability, &flags);
                LTAFNetworkReachabilityStatus status = LTAFNetworkReachabilityStatusForFlags(flags);
                dispatch_async(dispatch_get_main_queue(), ^{
                    callback(status);

                    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
                    [notificationCenter postNotificationName:LTAFNetworkingReachabilityDidChangeNotification object:nil userInfo:@{ LTAFNetworkingReachabilityNotificationStatusItem: @(status) }];


                });
            });
        }
            break;
    }
}

- (void)stopMonitoring {
    if (!self.networkReachability) {
        return;
    }

    SCNetworkReachabilityUnscheduleFromRunLoop((__bridge SCNetworkReachabilityRef)self.networkReachability, CFRunLoopGetMain(), kCFRunLoopCommonModes);
}

#pragma mark -

- (NSString *)localizedNetworkReachabilityStatusString {
    return LTAFStringFromNetworkReachabilityStatus(self.networkReachabilityStatus);
}

#pragma mark -

- (void)setReachabilityStatusChangeBlock:(void (^)(LTAFNetworkReachabilityStatus status))block {
    self.networkReachabilityStatusBlock = block;
}

#pragma mark - NSKeyValueObserving

+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
    if ([key isEqualToString:@"reachable"] || [key isEqualToString:@"reachableViaWWAN"] || [key isEqualToString:@"reachableViaWiFi"]) {
        return [NSSet setWithObject:@"networkReachabilityStatus"];
    }

    return [super keyPathsForValuesAffectingValueForKey:key];
}

@end
#endif
