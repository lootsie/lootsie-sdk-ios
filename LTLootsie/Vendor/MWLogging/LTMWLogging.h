
@import UIKit;

/* @section LTMWLogging LTMWLogging.
 *  Simple wrapper macros/functions around ASL (Apple System Log).
 *  We support a compile-time log level through
 *  LTMW_COMPILE_TIME_LOG_LEVEL. This will turn the associated log calls into NOPs.
 *  The log levels are the constants defined in asl.h:
 *  @li Emergency (level 0) - The highest priority, usually reserved for catastrophic failures and reboot notices.
 *  @li Alert (level 1)     - A serious failure in a key system.
 *  @li Critical (level 2)  - A failure in a key system.
 *  @li Error (level 3)     - Something has failed.
 *  @li Warning (level 4)   - Something is amiss and might fail if not corrected.
 *  @li Notice (level 5)    - Things of moderate interest to the user or administrator.
 *  @li Info (level 6)      - The lowest priority that you would normally log, and purely informational in nature.
 *  @li Debug (level 7)     - The lowest priority, and normally not logged except for messages from the kernel.
 *
 *  For a description of when to use each level, see here: http://developer.apple.com/library/mac/#documentation/MacOSX/Conceptual/BPSystemStartup/Chapters/LoggingErrorsAndWarnings.html#//apple_ref/doc/uid/10000172i-SW8-SW1
 *
 *  @note Note that by default the iOS syslog/console will only record items up to level ASL_LEVEL_NOTICE.
 *  @todo We want better multithread support. Default NULL client uses locking. Perhaps we can check for [NSThread mainThread] and associate an asl client
 *  object to that thread. Then we can specify ASL_OPT_STDERR and not need an extra call to add stderr.*/

/*! @def ASL_LEVEL_EMERG
 *  @brief The highest priority, usually reserved for catastrophic failures and reboot notices.*/
#define ASL_LEVEL_EMERG   0

/*! @def ASL_LEVEL_ALERT
 *  @brief A serious failure in a key system.*/
#define ASL_LEVEL_ALERT   1

/*! @def ASL_LEVEL_CRIT
 *  @brief A failure in a key system.*/
#define ASL_LEVEL_CRIT    2

/*! @def ASL_LEVEL_ERR
 *  @brief Something has failed.*/
#define ASL_LEVEL_ERR     3

/*! @def ASL_LEVEL_WARNING
 *  @brief Something is amiss and might fail if not corrected.*/
#define ASL_LEVEL_WARNING 4

/*! @def ASL_LEVEL_NOTICE
 *  @brief Things of moderate interest to the user or administrator.*/
#define ASL_LEVEL_NOTICE  5

/*! @def ASL_LEVEL_INFO
 *  @brief The lowest priority that you would normally log, and purely informational in nature.*/
#define ASL_LEVEL_INFO    6

/*! @def ASL_LEVEL_DEBUG
 *  @brief The lowest priority, and normally not logged except for messages from the kernel.*/
#define ASL_LEVEL_DEBUG   7


/*! By default, in non-debug mode we want to disable any logging statements except NOTICE and above.*/
#ifndef LTMW_COMPILE_TIME_LOG_LEVEL
	#ifdef NDEBUG
		#define LTMW_COMPILE_TIME_LOG_LEVEL ASL_LEVEL_NOTICE
	#else
		#define LTMW_COMPILE_TIME_LOG_LEVEL ASL_LEVEL_DEBUG
	#endif
#endif

#if LTMW_COMPILE_TIME_LOG_LEVEL >= ASL_LEVEL_EMERG
/*! @brief Print emergency info to console.
 *  @param format Format of message.*/
void LTLogEmergency(NSString *format, ...);
#else
#define LTLogEmergency(...)
#endif

#if LTMW_COMPILE_TIME_LOG_LEVEL >= ASL_LEVEL_ALERT
/*! @brief Print alert info to console.
 *  @param format Format of message.*/
void LTLogAlert(NSString *format, ...);
#else
#define LTLogAlert(...)
#endif

#if LTMW_COMPILE_TIME_LOG_LEVEL >= ASL_LEVEL_CRIT
/*! @brief Print critical error message to console.
 *  @param format Format of message.*/
void LTLogCritical(NSString *format, ...);
#else
#define LTLogCritical(...)
#endif

#if LTMW_COMPILE_TIME_LOG_LEVEL >= ASL_LEVEL_ERR
/*! @brief Print error message to console.
 *  @param format Format of message.*/
void LTLogError(NSString *format, ...);
#else
#define LTLogError(...)
#endif

#if LTMW_COMPILE_TIME_LOG_LEVEL >= ASL_LEVEL_WARNING
/*! @brief Print warning message to console.
 *  @param format Format of message.*/
void LTLogWarning(NSString *format, ...);
#else
#define LTLogWarning(...)
#endif

#if LTMW_COMPILE_TIME_LOG_LEVEL >= ASL_LEVEL_NOTICE
/*! @brief Print note message to console.
 *  @param format Format of message.*/
void LTLogNotice(NSString *format, ...);
#else
#define LTLogNotice(...)
#endif

#if LTMW_COMPILE_TIME_LOG_LEVEL >= ASL_LEVEL_INFO
/*! @brief Print info to console.
 *  @param format Format of message.*/
void LTLogInfo(NSString *format, ...);
#else
#define LTLogInfo(...)
#endif

#if LTMW_COMPILE_TIME_LOG_LEVEL >= ASL_LEVEL_DEBUG
/*! @brief Print debug message to console.
 *  @param format Format of message.*/
void LTLogDebug(NSString *format, ...);
#else
#define LTLogDebug(...)
#endif

extern __weak id<UIKeyInput> _ltLogUIInput;