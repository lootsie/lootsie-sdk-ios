
// We need all the log functions visible so we set this to DEBUG
#ifdef LTMW_COMPILE_TIME_LOG_LEVEL
    #undef LTMW_COMPILE_TIME_LOG_LEVEL
#endif

#define LTMW_COMPILE_TIME_LOG_LEVEL ASL_LEVEL_DEBUG

#include <asl.h>
#import "LTMWLogging.h"

static void AddStderrOnce()
{
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		asl_add_log_file(NULL, STDERR_FILENO);
	});
}

__weak id<UIKeyInput> _ltLogUIInput;
#if defined(DEBUG) || defined(LOOTSIE_EXAMPLE_ONLY)
    void _LTLogUI(int level, NSString *message) {
        static const char *levels[8] = {"EMERG", "ALERT", "CRIT", "ERR", "WARNING", "NOTICE", "INFO", "DEBUG"};
        if (_ltLogUIInput) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [_ltLogUIInput insertText:[NSString stringWithFormat:@"<%s> @see  %@\n", levels[level], message]];
            });
        }
    }

    #define LTLogUI _LTLogUI
#else
    #define LTLogUI(...)
#endif

#define __LTMW_MAKE_LOG_FUNCTION(LEVEL, NAME) \
void NAME (NSString *format, ...) \
{ \
    if (!format) format = @"(null)"; \
	AddStderrOnce(); \
	va_list args; \
	va_start(args, format); \
	NSString *message = [[NSString alloc] initWithFormat:format arguments:args]; \
	asl_log(NULL, NULL, (LEVEL), "%s", [message UTF8String]); \
    LTLogUI(LEVEL, message); \
	va_end(args); \
}

__LTMW_MAKE_LOG_FUNCTION(ASL_LEVEL_EMERG, LTLogEmergency)
__LTMW_MAKE_LOG_FUNCTION(ASL_LEVEL_ALERT, LTLogAlert)
__LTMW_MAKE_LOG_FUNCTION(ASL_LEVEL_CRIT, LTLogCritical)
__LTMW_MAKE_LOG_FUNCTION(ASL_LEVEL_ERR, LTLogError)
__LTMW_MAKE_LOG_FUNCTION(ASL_LEVEL_WARNING, LTLogWarning)
__LTMW_MAKE_LOG_FUNCTION(ASL_LEVEL_NOTICE, LTLogNotice)
__LTMW_MAKE_LOG_FUNCTION(ASL_LEVEL_INFO, LTLogInfo)
__LTMW_MAKE_LOG_FUNCTION(ASL_LEVEL_DEBUG, LTLogDebug)

#undef __LTMW_MAKE_LOG_FUNCTION
