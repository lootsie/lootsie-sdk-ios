//  LTNetworkObserver.m
//  Created by Fabio Teles on 8/17/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTNetworkObserver.h"
#import "LTTimer.h"

/*! @class NetworkIndicatorController
 *  @brief A singleton to manage a visual "reference count" on the network activity indicator.*/
@interface NetworkIndicatorController : NSObject

/*! @brief Create and returen instance of singelton.*/
+ (instancetype)sharedController;

/*! Numbers of active operations.*/
@property (nonatomic, assign) NSInteger activityCount;

/*! Timer of show and hide activity indicator.*/
@property (nonatomic, strong) LTTimer *visibilityTimer;

@end


@implementation NetworkIndicatorController

+ (instancetype)sharedController {
    static NetworkIndicatorController *_sharedController;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedController = [NetworkIndicatorController new];
    });
    return _sharedController;
}

- (void)networkActivityDidStart {
    NSAssert([NSThread isMainThread], @"Altering network activity indicator state can only be done on the main thread.");
    
    self.activityCount++;
    
    [self updateIndicatorVisibility];
}

- (void)networkActivityDidEnd {
    NSAssert([NSThread isMainThread], @"Altering network activity indicator state can only be done on the main thread.");
    
    self.activityCount--;
    
    [self updateIndicatorVisibility];
}

- (void)updateIndicatorVisibility {
    if (self.activityCount > 0) {
        [self showIndicator];
    } else {
        /*
         To prevent the indicator from flickering on and off, we delay the
         hiding of the indicator by one second. This provides the chance
         to come in and invalidate the timer before it fires.
         */
        __weak __typeof(self) weakSelf = self;
        self.visibilityTimer = [[LTTimer alloc] initWithInterval:1.0 handler:^{
            [weakSelf hideIndicator];
        }];
    }
}

- (void)showIndicator {
    [self.visibilityTimer cancel];
    self.visibilityTimer = nil;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)hideIndicator {
    [self.visibilityTimer cancel];
    self.visibilityTimer = nil;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

@end



@implementation LTNetworkObserver

- (void)operationDidStart:(LTOperation *)operation {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NetworkIndicatorController sharedController] networkActivityDidStart];
    });
}

- (void)operation:(LTOperation *)operation didProduceOperation:(NSOperation *)newOperation {
    // no op
}

- (void)operationDidFinish:(LTOperation *)operation withErros:(NSArray *)erros {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NetworkIndicatorController sharedController] networkActivityDidEnd];
    });
}

@end
