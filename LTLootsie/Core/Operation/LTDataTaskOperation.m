//  LTURLSessionTaskOperation.m
//  Created by Fabio Teles on 7/14/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTDataTaskOperation.h"
#import "LTNetworkManager.h"
#import "LTAFURLResponseSerialization.h"
#import "LTManager.h"
#import "LTManager+Private.h"
#import "LTUserAccount.h"
#import "LTCatalogRewardsList.h"
#import "LTApp.h"
#import "LTIANItem.h"
#import "LTPostNotificationOperation.h"

#import "LTMWLogging.h"
#import "LTConstants.h"

@import AdSupport;


/*! Context for task operation observer.*/
static void *URLSessionTaksOperationKVOContext = &URLSessionTaksOperationKVOContext;


/*! @brief Convert http request body to correct string.
 *  @param httpBody Body data.
 *  @return Correct string.*/
static NSString * LTStringFromHTTPBody(NSData *httpBody) {
    if (httpBody) {
        NSString *params = nil;
        params = [[NSString alloc] initWithData:httpBody encoding:NSUTF8StringEncoding];
        params = [params stringByReplacingOccurrencesOfString:@"\",\"" withString:@"\"\n\""];
        params = [params stringByReplacingOccurrencesOfString:@"{" withString:@"{\n"];
        params = [params stringByReplacingOccurrencesOfString:@"}" withString:@"\n}"];
        return params;
    }
    return nil;
}


@interface LTDataTaskOperation ()

/*! Block of code to generate request.*/
@property (nonatomic, strong) LTRequestBuilder requestBuilder;

/*! Network task object.*/
@property (nonatomic, readwrite, strong) NSURLSessionDataTask *task;

/*! Response object with server response data.*/
@property (nonatomic, readwrite, strong) NSURLResponse *response;

/*! Response object with data.*/
@property (nonatomic, readwrite, strong) id responseObject;


/*! @brief Operation task. Generate request and send it to server.*/
- (void)buildTask;

@end


@implementation LTDataTaskOperation

#pragma mark - Private methods

- (void)buildTask {
    // build request
    NSError *requestError;
    self.request = self.requestBuilder(&requestError);
    
    // if it errors out, call completion passing that error along and signal operation complete
    if (requestError) {
        LTLogError(@"Request Builer Failed with %@", requestError.localizedDescription);
        [self finishWithError:requestError];
    }
    
    // build task and return it
    LTLogDebug(@"DataTask Operation with request: %@ %@ %@ %@", self.request.HTTPMethod, self.request.URL, LTStringFromHTTPBody(self.request.HTTPBody), self.request.allHTTPHeaderFields);
    __weak __typeof(self) weakSelf = self;
    self.task = [[LTNetworkManager lootsieManager] dataTaskWithRequest:self.request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        
        strongSelf.response = response;
        strongSelf.responseObject = responseObject;
        
        if (!error) error = [strongSelf dataTaskDidFinishSuccessfully];
        
        /*! IMPORTANT
         Signal operation complete after returning data from operation for
         who's calling it. Here we cancel operations that failed and finish
         operations that succeed, that way we can differentiate between success
         and failure for data task operations.
         */
        if (error) {
            LTLogError(@"DataTask Operation Failed with: %@", [strongSelf formatDataTaskError:error].localizedDescription);
            [strongSelf processDataTaskError:error];
            [strongSelf cancelWithError:[strongSelf formatDataTaskError:error]];
        } else {
            [strongSelf finishWithError:nil];
        }
    }];
}

#pragma mark - Operation methods

- (instancetype)initWithRequestBuilder:(LTRequestBuilder)requestBuilder {
    NSParameterAssert(requestBuilder);
    
    if (self = [super init]) {
        _requestBuilder = requestBuilder;
        [self setSafeName:NSStringFromClass([self class])];
    }
    return self;
}

- (NSString *)HTTPMethod {
    return self.task.originalRequest.HTTPMethod;
}

- (NSError *)dataTaskDidFinishSuccessfully {
    NSDictionary *response = self.responseObject;
    LTLogDebug(@"response=%@", response);
    LTData *data = [LTManager sharedManager].data;
    
    NSLog(@"");
    NSLog(@"");
    NSLog(@"");
    NSLog(@"");
    NSLog(@"%lu", [data.achievements count]);
    NSLog(@"");
    NSLog(@"");
    NSLog(@"");
    NSLog(@"");
    
    if ([response objectForKey:@"user"]) {
        if (data.user != nil) {
//            [data.user populateWithDictionary:response[@"user"]];
            [data.user modifyWithDictionary:response[@"user"]];
            
            data.user = [LTUserAccount entityWithDictionary:[data.user dictionary]];
        } else {
            data.user = [LTUserAccount entityWithDictionary:response[@"user"]];
        }
    }
    if ([response objectForKey:@"app"]) {
        data.app = [LTApp entityWithDictionary:response[@"app"]];
    }
    if ([response objectForKey:@"catalog"]) {
        
        data.catalog = [LTCatalogRewardsList entityWithDictionary:response[@"catalog"]];
    }
    if ([response objectForKey:@"ians"]) {
        NSArray *ian = [LTIANItem collectionWithArray:response[@"ians"]];
        LTPostNotificationOperation *notifOp = [[LTPostNotificationOperation alloc] initWithNotificationName:LTIANNotification
                                                                                                      object:self
                                                                                                    userInfo:@{LTIANKey : ian}];
        [[LTManager sharedManager] addGeneralOperation:notifOp dependencies:nil allowCancelledDependencies:YES];
        
        
    }
    return nil; // No-op
}

- (NSError *)formatDataTaskError:(NSError *)error {
    if (error && self.responseObject) {
        LTLogError(@"DataTask Operation Failed with Lootsie Error: %@", self.responseObject);
        /*!
         SPECIAL error handling back from lootsie server. Creates and
         makes available an error with the information parsed for higher
         level code to use
         */
        NSMutableDictionary *mutableInfo = [error.userInfo mutableCopy];
        NSArray *errors = (NSArray *)self.responseObject[LTErrorErrorsKey];
        if (errors && errors.firstObject) {
            NSDictionary *errorDic = (NSDictionary *)[errors firstObject];
            if (errorDic && errorDic[LTErrorFieldKey] && errorDic[LTErrorMessageKey]) {
                NSString *recoverStr = [NSString stringWithFormat:
                                        NSLocalizedStringFromTable(@"%@ %@",
                                                                   @"LTLootsie",
                                                                   @"{missing|invalid|similar} {field name}"),
                                        errorDic[LTErrorMessageKey],
                                        [errorDic[LTErrorFieldKey] capitalizedStringWithLocale:[NSLocale currentLocale]]];
                
                mutableInfo[NSLocalizedDescriptionKey] = NSLocalizedStringFromTable(@"Uh oh!", @"LTLootsie", nil);
                mutableInfo[NSLocalizedRecoverySuggestionErrorKey] = recoverStr;
            }
        }
        if (errors) mutableInfo[LTNetworkResponseErrorsJSONKey] = errors;
        return [NSError errorWithDomain:LTNetworkErrorDomain
                                   code:error.code
                               userInfo:[NSDictionary dictionaryWithDictionary:mutableInfo]];
    }
    
    if (error) {
        LTLogDebug(@"Returning standard error. Original: %@", error.userInfo);
        NSDictionary *userInfo = @{NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"Uh Oh!", @"LTLootsie", nil),
                                   NSLocalizedRecoverySuggestionErrorKey: NSLocalizedStringFromTable(@"Looks like there's a problem with the network so our mini robots can't get the latest cost for you. Try again in a few and make sure you have a strong connection!", @"LTLootsie", nil),
                                   NSUnderlyingErrorKey: error};
        NSHTTPURLResponse *response = [error.userInfo objectForKeyNilSafe:LTAFNetworkingOperationFailingURLResponseErrorKey];
        
        if (response != nil) {
            if ((response.statusCode / 100) == 5) {
                userInfo = @{NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"Uh Oh!", @"LTLootsie", nil),
                             NSLocalizedRecoverySuggestionErrorKey: NSLocalizedStringFromTable(@"Looks like there's a problem with the network. Please try again in a few.", @"LTLootsie", nil),
                             NSUnderlyingErrorKey: error};
            }
            if (response.statusCode == 400 || response.statusCode == 410 || response.statusCode == 412) {
                userInfo = @{NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"Hmm, that's weird", @"LTLootsie", nil),
                             NSLocalizedRecoverySuggestionErrorKey: NSLocalizedStringFromTable(@"Looks like there was a bad or missing request. We're working to fix this. If this error continues to happen, please contact support", @"LTLootsie", nil),
                             NSUnderlyingErrorKey: error};
            }
            if (response.statusCode == 429) {
                userInfo = @{NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"WHOA! YOU’VE MAXED OUT!", @"LTLootsie", nil),
                             NSLocalizedRecoverySuggestionErrorKey: NSLocalizedStringFromTable(@"There are only so many points you can earn in a single day, you superstar. Check back in tomorrow and you’ll be earning points again in no time", @"LTLootsie", nil),
                             NSUnderlyingErrorKey: error};
            }
            if (response.statusCode == 403) {
                userInfo = @{NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"Illegal Use", @"LTLootsie", nil),
                             NSLocalizedRecoverySuggestionErrorKey: NSLocalizedStringFromTable(@"Your account has been suspended. Please contact support.", @"LTLootsie", nil),
                             NSUnderlyingErrorKey: error};
                [[NSNotificationCenter defaultCenter] postNotificationName:LTDisableLootsieNotification object:self];
            }
            if (response.statusCode == 451) {
                userInfo = @{NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"GEO PROBLEM", @"LTLootsie", nil),
                             NSLocalizedRecoverySuggestionErrorKey: NSLocalizedStringFromTable(@"Yikes, it looks like you’re in a location that we don’t support quite yet. Hold tight as we’re adding new locations all the time.", @"LTLootsie", nil),
                             NSUnderlyingErrorKey: error};
                [[NSNotificationCenter defaultCenter] postNotificationName:LTDisableLootsieNotification object:self];
            }
        }

        return [NSError errorWithDomain:error.domain
                                   code:error.code
                               userInfo:userInfo];
    }
    
    return  error;
}

- (void)execute {
    BOOL eventSendingEnabled = [LTManager sharedManager].eventManager.enabled;
    if (eventSendingEnabled) {
        [self buildTask];
        [self.task resume];
    } else {
        [self cancel];
    }
}

- (void)cancel {
    [self.task cancel];
    [super cancel];
}

+ (NSDictionary *)header {
    NSString *appKey = [LTManager sharedManager].appKey;
    NSString *deviceId = @"63EA4D00-3F50-422D-B27B-0E9041BE3486";

#if (TARGET_OS_SIMULATOR)
    deviceId = [LTDataTaskOperation getFallbackIdentifier];
#elif (DISABLE_IDFA)
    deviceId = [LTDataTaskOperation getVendorIdentifierWithFallback];
#else
    if ([ASIdentifierManager sharedManager].advertisingTrackingEnabled) {
        deviceId = [[ASIdentifierManager sharedManager].advertisingIdentifier UUIDString];
    } else {
        deviceId = [LTDataTaskOperation getVendorIdentifierWithFallback];
    }
#endif

    NSString *sdkVersion = @"1";
    return @{@"Lootsie-SDK-Version" : sdkVersion, @"Lootsie-App" : appKey, @"Lootsie-Device" : deviceId, @"Accept" : @"application/json"};
}

+ (NSString *) getVendorIdentifierWithFallback {
    if([[UIDevice currentDevice] respondsToSelector:@selector(identifierForVendor)]) {
        return [UIDevice currentDevice].identifierForVendor.UUIDString;
    } else {
        return [LTDataTaskOperation getFallbackIdentifier];
    }
}

+ (NSString *) getFallbackIdentifier {
    NSString *deviceIDKey = @"deviceIDKey";
    NSString *obj = [[NSUserDefaults standardUserDefaults] objectForKey:deviceIDKey];
    if (obj != nil) {
        return obj;
    } else {
        NSUUID *uuid = [NSUUID UUID];
        [[NSUserDefaults standardUserDefaults] setObject:[uuid UUIDString] forKey:deviceIDKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return [uuid UUIDString];
    }
}
- (void) processDataTaskError:(NSError *)error {

    NSInteger errorStatusCode = [[error.userInfo objectForKeyNilSafe:LTAFNetworkingOperationFailingURLResponseErrorKey] statusCode];
    LTData *data = [LTManager sharedManager].data;
    LTEventManager *eventManager = [LTManager sharedManager].eventManager;

    if (errorStatusCode == 403 || errorStatusCode == 451) {
        // completely shut down the SDK
        if(data.app) data.app.enabled = NO;
        if(eventManager) [eventManager disable];
    }
}
@end
