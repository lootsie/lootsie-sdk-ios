//  LTOperation.m
//  Created by Fabio Teles on 7/14/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTOperation.h"

#import "LTMWLogging.h"

/*! Operation states.*/
typedef NS_ENUM(NSInteger, LTOperationState) {
    LTOperationStateInitialized,            /*! Initialized operation.*/
    LTOperationStatePending,                /*! Operation is panding.*/
    LTOperationStateEvaluatingConditions,   /*! Operation is evaluating conditions.*/
    LTOperationStateReady,                  /*! Operation is ready.*/
    LTOperationStateExecuting,              /*! Operation is executing work.*/
    LTOperationStateFinishing,              /*! Operation is finishing work.*/
    LTOperationStateFinished,               /*! Operation is finished correct.*/
    LTOperationStateCancelled               /*! Operation was cancelled.*/
};


/*! Operation key name.*/
NSString * const LTOperationKey    = @"com.lootsie.operation.op";


@interface LTOperation () {
    LTOperationState _state;    /*! Operation state.*/
}
/*! List of internal conditions.*/
@property (nonatomic, strong) NSMutableArray *internalConditions;

/*! List of internal observers.*/
@property (nonatomic, strong) NSMutableArray *internalObservers;

/*! List of internal errors.*/
@property (nonatomic, strong) NSMutableArray *internalErrors;

/*! YES if curren operation already finished work.*/
@property (nonatomic, assign) BOOL hasFinishedAlready;

/*! Lock current thread where current operation is run.*/
@property (nonatomic, strong) NSLock *lock;

@end


@implementation LTOperation

@dynamic conditions;
@dynamic observers;
@dynamic generatedErrors;

/*! @brief Return a set of key paths for properties whose values affect the value of the keyed property.
 *  @param key Key name.
 *  @return List of key paths.*/
+ (NSSet *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
    NSMutableSet *keyPaths = [NSMutableSet setWithSet:[[self superclass] keyPathsForValuesAffectingValueForKey:key]];
    if ([key isEqualToString:@"isReady"] ||
        [key isEqualToString:@"isExecuting"] ||
        [key isEqualToString:@"isFinished"] ||
        [key isEqualToString:@"isCancelled"]) {
        
        [keyPaths addObjectsFromArray:@[@"state", @"hasFinishedAlready"]];
    }
    return keyPaths;
}

/*! @brief Create new object.*/
- (instancetype)init {
    if (self = [super init]) {
        _state = LTOperationStateInitialized;
        _hasFinishedAlready = NO;
        
        _internalConditions = [NSMutableArray array];
        _internalObservers = [NSMutableArray array];
        _internalErrors = [NSMutableArray array];

        _ErrorBubblesToUI = YES;
        
        _lock = [NSLock new];
        _lock.name = @"com.lootsie.operation.lock";
    }
    return self;
}

#pragma mark State Handling
/*! @brief Get stat of operation.
 *  @return Operation state.*/
- (LTOperationState)state {
    [self.lock lock];
    LTOperationState readState = _state;
    [self.lock unlock];
    return readState;
}

/*! @brief Update state of operation.
 *  @param state New state.*/
- (void)setState:(LTOperationState)state {
    // Manually fire the KVO notifications for state change, since this is "private".
    [self willChangeValueForKey:@"state"];
    
    // cannot leave the cancelled state or the finished state
    [self.lock lock];
    if (_state != LTOperationStateCancelled && _state != LTOperationStateFinished) {
        NSAssert(_state != state, @"Performing invalid cyclic state transition.");
        _state = state;
    }
    [self.lock unlock];
    
    [self didChangeValueForKey:@"state"];
}

/*! @brief Getter for user initiated property.*/
- (BOOL)userInitiated {
    return self.qualityOfService == NSQualityOfServiceUserInitiated;
}

/*! @brief Setter for user initiated property.
 *  @param userInitiated New value for property.*/
- (void)setUserInitiated:(BOOL)userInitiated {
    NSAssert(self.state < LTOperationStateExecuting, @"Cannot modify userInitiated after execution has begun.");
#if __IPHONE_OS_VERSION_MIN_REQUIRED < 80000
    if ([self respondsToSelector:@selector(setQualityOfService:)]) {
        self.qualityOfService = (userInitiated ? NSQualityOfServiceUserInitiated : NSQualityOfServiceDefault);
    } else {
        self.threadPriority = (userInitiated ? 0.75 : 0.5);
    }
#else
    self.qualityOfService = (userInitiated ? NSQualityOfServiceUserInitiated : NSQualityOfServiceDefault);
#endif
}

/*! @brief Return YES when operation is ready.*/
- (BOOL)isReady {
    switch (self.state) {
        case LTOperationStatePending:
        {
            if ([super isReady]) {
                [self evaluateConditions];
            }
            return NO;
        }
        case LTOperationStateReady:
        case LTOperationStateCancelled:
            return [super isReady];

        default:
            return NO;
    }
}

/*! @brief Return YES when operation is executing.*/
- (BOOL)isExecuting {
    return self.state == LTOperationStateExecuting;
}

/*! @brief Return YES when operation is finished.*/
- (BOOL)isFinished {
    LTOperationState readState = _state;
    return readState == LTOperationStateFinished || (self.hasFinishedAlready && readState == LTOperationStateCancelled);
}

/*! @brief Return YES when operation was cancelled.*/
- (BOOL)isCancelled {
    return self.state == LTOperationStateCancelled;
}

- (void)willEnqueue {
    self.state = LTOperationStatePending;
}


#pragma mark Features
/*! @brief Add dependency operation for current operation.
 *  @param op Operation object.*/
- (void)addDependency:(NSOperation *)op {
    NSAssert(self.state < LTOperationStateExecuting, @"Dependencies cannot be modified after execution has begun.");
    
    [super addDependency:op];
}

- (void)addCondition:(id<LTOperationCondition>)condition {
    NSAssert(self.state < LTOperationStateEvaluatingConditions, @"Cannot modify conditions after execution has begun.");
    
    [self.internalConditions addObject:condition];
}

- (void)addObserver:(id<LTOperationObserver>)observer {
    NSAssert(self.state < LTOperationStateExecuting, @"Cannot modify observers after execution has begun.");
    
    [self.internalObservers addObject:observer];
}

/*! @brief Check conditions for current operation.*/
- (void)evaluateConditions {
    NSAssert(self.state == LTOperationStatePending, @"-evaluateConditions was called out-of-order");
    
    self.state = LTOperationStateEvaluatingConditions;
    
    __weak __typeof(self) weakSelf = self;
    [LTOperationConditionEvaluator evaluate:self.conditions operation:self completion:^(NSArray *errors) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        if (!errors || errors.count == 0) {
            strongSelf.state = LTOperationStateReady;
        } else {
            if (strongSelf && !strongSelf.isCancelled) [strongSelf cancelWithErrors:errors];
        }
    }];
}


#pragma mark Actions
/*! @brief Start current operation.*/
- (void)start {
    LTOperationState readState = self.state;
    NSAssert(readState == LTOperationStateReady || readState == LTOperationStateCancelled, @"This operation must be performed on an operation queue.");
    
    if (readState == LTOperationStateCancelled) {
        [self finish:nil];
        return;
    }
    
    self.state = LTOperationStateExecuting;
    
    for (id<LTOperationObserver> observer in self.observers) {
        [observer operationDidStart:self];
    }
    
    [self execute];
}

- (void)execute {
    LTLogDebug(@"%@ must override `execute()`.", NSStringFromClass([self class]));
    
    [self finish:nil];
}

/*! @brief Cancel current operation.*/
- (void)cancel {
    [self cancelWithErrors:nil];
}

- (void)cancelWithError:(NSError *)error {
    [self cancelWithErrors:(error ? @[error] : nil)];
}

- (void)cancelWithErrors:(NSArray *)errors {
    if (self.isCancelled) return;
    
    if (errors) {
        [self.internalErrors addObjectsFromArray:errors];
    }
    
    BOOL wasExecuting = self.isExecuting;
    self.state = LTOperationStateCancelled;
    
    /*!
     If operation was executing when it got cancelled we need to manually call finish
     here because the start method was already called
     */
    if (wasExecuting) {
        [self finish:nil];
    }
}

- (void)produceOperation:(NSOperation *)operation {
    for (id<LTOperationObserver> observer in self.observers) {
        [observer operation:self didProduceOperation:operation];
    }
}

- (void)finishWithError:(NSError *)error {
    if (error) {
        [self finish:@[error]];
    } else {
        [self finish:nil];
    }
}

- (void)finish:(NSArray *)errors {
    if (!self.hasFinishedAlready) {
        self.state = LTOperationStateFinishing;
    
        [self.internalErrors addObjectsFromArray:errors];
        [self finished:self.internalErrors];
        
        for (id<LTOperationObserver> observer in self.observers) {
            [observer operationDidFinish:self withErros:self.internalErrors];
        }
        
        self.state = LTOperationStateFinished;
        self.hasFinishedAlready = YES;
    }
}

- (void)finished:(NSArray *)errors {
    // For use by subclassers.
}

/*! @brief Waiting on operations is almost NEVER the right thing to do. It is usually superior to use proper 
 *  locking constructs, such as `dispatch_semaphore_t` or `dispatch_group_notify`, or even `NSLocking` objects.
 *  Many developers use waiting when they should instead be chaining discrete operations together using dependencies.
 *  To reinforce this idea, invoking `waitUntilFinished` will crash your app, as incentive for you to find a more 
 *  appropriate way to express the behavior you're wishing to create.*/
- (void)waitUntilFinished {
    LTLogDebug(@"Waiting on operations is an anti-pattern. Remove this ONLY if you're absolutely sure there is No Other Way.");
    exit(0);
}

#pragma mark Helpers

- (void)setSafeName:(NSString *)name {
    if ([self respondsToSelector:@selector(setName:)]) {
        self.name = name;
    }
}

/*! @brief Debug information for currnet object.
 *  @return Debaut string with object info.*/
- (NSString *)description {
    if ([self respondsToSelector:@selector(name)] && self.name) {
        return self.name;
    } else {
        return [NSString stringWithFormat:@"LTOperation<%@>", NSStringFromClass([self class])];
    }
}

#pragma mark Readonly getters
- (NSArray *)conditions {
    return [NSArray arrayWithArray:self.internalConditions];
}

- (NSArray *)observers {
    return [NSArray arrayWithArray:self.internalObservers];
}

- (NSArray *)generatedErrors {
    return [NSArray arrayWithArray:self.internalErrors];
}

@end

#pragma mark -

@implementation NSOperation (LTOperation)

- (void)addCompletionBlock:(void (^)(void))block {
    if (!self.completionBlock) {
        self.completionBlock = block;
    } else {
        void (^existing)(void) = self.completionBlock;
        self.completionBlock = ^(void) {
            existing();
            block();
        };
    }
}

- (void)addDependencies:(NSArray *)dependencies {
    for (NSOperation *operation in dependencies) {
        [self addDependency:operation];
    }
}

@end

#pragma mark -

NSString * const LTOperationErrorDomain = @"com.lootsie.operation.error";

@implementation NSError (LTOperation)

+ (NSError *)executionErrorWithUserInfo:(NSDictionary *)userInfo {
    return [NSError errorWithDomain:LTOperationErrorDomain code:LTOperationErrorCodeExecution userInfo:userInfo];
}

+ (NSError *)conditionErrorWithUserInfo:(NSDictionary *)userInfo {
    return [NSError errorWithDomain:LTOperationErrorDomain code:LTOperationErrorCodeCondition userInfo:userInfo];
}

@end

#pragma mark -

#import "LTModelEntity.h"

NSString * const LTOperationConditionKey    = @"com.lootsie.operaation.condition";
NSString * const LTEvalResultKey            = @"result";
NSString * const LTEvalErrorKey             = @"error";

@implementation LTOperationConditionEvaluator

+ (void)evaluate:(NSArray *)conditions operation:(LTOperation *)operation completion:(void (^)(NSArray *))completion {
    // Check conditions.
    dispatch_group_t conditionGroup = dispatch_group_create();
    NSMutableArray *results = [NSMutableArray arrayWithCapacity:conditions.count];
    
    // Ask each condition to evaluate and store its result in the "results" array.
    [conditions enumerateObjectsUsingBlock:^(id<LTOperationCondition> condition, NSUInteger idx, BOOL *stop) {
        dispatch_group_enter(conditionGroup);
        [condition evaluateForOperation:operation completion:^(LTOperationConditionResult result, NSError *error) {
            results[idx] = @{LTEvalResultKey: @(result), LTEvalErrorKey: (error ?: [NSNull null])};
            dispatch_group_leave(conditionGroup);
        }];
    }];
    
    // After all the conditions have evaluated, this block will execute.
    dispatch_group_notify(conditionGroup, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // Aggregate the errors that occurred, in order.
        NSMutableArray *failures = [NSMutableArray array];
        [results enumerateObjectsUsingBlock:^(NSDictionary *result, NSUInteger idx, BOOL *stop) {
            NSError *error = [result objectForKeyNilSafe:LTEvalErrorKey];
            if (error) {
                [failures addObject:error];
            }
        }];
        
        // If any of the conditions caused this operation to be cancelled, check for that.
        if ([operation isCancelled]) {
            [failures addObject:[NSError conditionErrorWithUserInfo:nil]];
        }
        
        completion(failures);
    });
}

@end
