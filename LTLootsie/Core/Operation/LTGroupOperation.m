//  LTGroupOperation.m
//  Created by Fabio Teles on 7/15/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTGroupOperation.h"
#import "LTOperationQueue.h"

@interface LTGroupOperation () <LTOperationQueueDelegate>

/*! Queue for run operations.*/
@property (nonatomic, strong) LTOperationQueue *internalQueue;

/*! List of operations errors.*/
@property (nonatomic, strong) NSMutableArray *aggregatedErrors;

@end


@implementation LTGroupOperation

+ (instancetype)groupWithOperations:(NSArray *)operations maxConcurrentOperationCount:(NSInteger)count {
    return [[self alloc] initWithOperations:operations maxConcurrentOperationCount:count];
}

+ (instancetype)groupWithOperations:(NSArray *)operations {
    return [[self alloc] initWithOperations:operations];
}

/*! @brief Create new object.*/
- (instancetype)init {
    return [self initWithOperations:nil];
}

- (instancetype)initWithOperations:(NSArray *)operations {
    if (self = [super init]){
        _internalQueue = [LTOperationQueue new];
        _internalQueue.name = [NSString stringWithFormat:@"LTGroupOperationQueue<%@>", NSStringFromClass([self class])];
        _internalQueue.suspended = YES;
        _internalQueue.delegate = self;
        
        _aggregatedErrors = [NSMutableArray array];
        
        for (NSOperation * op in operations){
            [_internalQueue addOperation:op];
        }
    }
    return self;
}

/*! @brief Create new object with operations and and max concurrent operation.
 *  @param operations List of operations.
 *  @param count Max concurrent operation count. Number of operations will be run by one time.*/
- (instancetype)initWithOperations:(NSArray *)operations maxConcurrentOperationCount:(NSInteger)count {
    if ([self initWithOperations:operations]) {
        self.internalQueue.maxConcurrentOperationCount = count;
    }
    return self;
}

- (void)cancelWithErrors:(NSArray *)errors {
    if (self.isCancelled) return;
    
    [self.internalQueue cancelAllOperations];
    if (errors) [self.aggregatedErrors addObjectsFromArray:errors];
    [super cancelWithErrors:[self.aggregatedErrors copy]];
}

- (void)execute {
    self.internalQueue.suspended = NO;
}

- (void)addOperation:(NSOperation *)operation {
    [self.internalQueue addOperation:operation];
}

- (void)aggregateError:(NSError *)error {
    [self.aggregatedErrors addObject:error];
}

- (void)operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors {
    // For use by subclassers.
}


#pragma mark - LTOperationQueueDelegate

- (void)operationQueue:(LTOperationQueue *)operationQueue willAddOperation:(NSOperation *)operation {
    NSAssert(!self.isFinished, @"cannot add new operations to a group after the group has completed");
}

- (void)operationQueue:(LTOperationQueue *)operationQueue operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors {
    [self.aggregatedErrors addObjectsFromArray:errors];
    [self operationDidFinish:operation withErrors:errors];
}

- (void)operationQueueDidFinishAllCurrentTasks:(LTOperationQueue *)queue {
    self.internalQueue.suspended = YES;
    [self finish:[self.aggregatedErrors copy]];
}

@end
