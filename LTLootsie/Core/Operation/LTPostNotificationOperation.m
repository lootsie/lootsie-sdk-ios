//  LTPostNotificationOperation.m
//  Created by Fabio Teles on 8/25/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTPostNotificationOperation.h"
#import "LTMWLogging.h"

@interface LTPostNotificationOperation ()

/*! Notification name.*/
@property (nonatomic, strong) NSString *notificationName;

/*! Sender pointer object. Object whcih called this notification.*/
@property (nonatomic, strong) id notificationSender;

/*! Additional info for notification.*/
@property (nonatomic, strong) NSDictionary *userInfo;

/*! Block of code for generate user info.*/
@property (nonatomic, strong) NSDictionary *(^userInfoBlock)(void);

@end


@implementation LTPostNotificationOperation

- (instancetype)initWithNotificationName:(NSString *)notificationName object:(id)notificationSender userInfo:(NSDictionary *)userInfo {
    if (self = [super init]) {
        self.notificationName = notificationName;
        self.notificationSender = notificationSender;
        self.userInfo = userInfo;
    }
    return self;
}

- (instancetype)initWithNotificationName:(NSString *)notificationName object:(id)notificationSender userInfoBlock:(NSDictionary *(^)(void))userInfoBlock {
    if (self = [super init]) {
        self.notificationName = notificationName;
        self.notificationSender = notificationSender;
        self.userInfoBlock = userInfoBlock;
    }
    return self;
}

- (void)execute {
    if (self.userInfoBlock) {
        self.userInfo = self.userInfoBlock();
        self.userInfoBlock = nil;
    }
    
    void (^sendNotification)(void) = ^{
        LTLogDebug(@"Sending notification through op with name:%@, sender:%@, userInfo:%@", self.notificationName, NSStringFromClass([self.notificationSender class]), self.userInfo);
        [[NSNotificationCenter defaultCenter] postNotificationName:self.notificationName object:self.notificationSender userInfo:self.userInfo];
    };
    // Make sure we send the notification on the main thread
    if ([NSThread isMainThread]) {
        sendNotification();
    } else {
        dispatch_sync(dispatch_get_main_queue(), sendNotification);
    }
    
    [self finish:nil];
}

@end
