//  LTBlockOperation.h
//  Created by Fabio Teles on 7/14/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTOperation.h"

/*! @typedef LTOperationCompletionBlock
 *  @brief Will calls when operation will finished.*/
typedef void (^LTOperationCompletionBlock)(NSError *);

/*! @typedef LTOperationBlock
 *  @brief Operation code. Use this for set code which will call by operation.*/
typedef void (^LTOperationBlock)(LTOperationCompletionBlock completion);


/*! @class LTBlockOperation
 *  @brief Operation with block of code.*/
@interface LTBlockOperation : LTOperation

/*! @brief Create a new operation with block of code.*/
- (instancetype)initWithBlock:(LTOperationBlock)block NS_DESIGNATED_INITIALIZER;

/*! @brief Create a new operation with dispatch block.*/
- (instancetype)initWithMainQueueBlock:(dispatch_block_t)mainBlock;

/*! @brief Create a auto complete operation with dispatch block*/
+ (instancetype)autoSuccessOpWithBlock:(dispatch_block_t)block;

@end
