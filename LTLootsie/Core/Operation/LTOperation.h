//  LTOperation.h
//  Created by Fabio Teles on 7/14/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>

/*! Operation key.*/
extern NSString * const LTOperationKey;


@class LTOperation;
/*! @protocol LTOperationObserver
 *  @brief Methods for call as a part of operation.*/
@protocol LTOperationObserver <NSObject>
@required

/*! @brief Invoked immediately prior to the `Operation`'s `execute()` method.*/
- (void)operationDidStart:(LTOperation *)operation;

/*! @brief Invoked when `Operation.produceOperation(_:)` is executed.*/
- (void)operation:(LTOperation *)operation didProduceOperation:(NSOperation *)newOperation;

/*! @brief Invoked as an `Operation` finishes, along with any errors produced during execution (or readiness evaluation).*/
- (void)operationDidFinish:(LTOperation *)operation withErros:(NSArray *)erros;

@end


#pragma mark -
/*! Operation condition result types*/
typedef NS_ENUM(NSUInteger, LTOperationConditionResult) {
    LTOperationConditionResultSatisfied,    /*! Operation condition is satisfied.*/
    LTOperationConditionResultFailed        /*! Operation condition is failed.*/
};


/*! Operation condition key.*/
extern NSString * const LTOperationConditionKey;


@class LTOperation;
/*! @protocol LTOperationCondition
 *  @brief Base operation condition class.*/
@protocol LTOperationCondition <NSObject>

/*! @brief The name of the condition. This is used in userInfo dictionaries of `.ConditionFailed`
 *  errors as the value of the `OperationConditionKey` key.*/
- (NSString *)name;

/*! @brief Specifies whether multiple instances of the conditionalized operation may
 *  be executing simultaneously.*/
- (BOOL)isMutuallyExclusive;

/*! @brief Some conditions may have the ability to satisfy the condition if another
 *  operation is executed first. Use this method to return an operation that
 *  (for example) asks for permission to perform the operation
 *  @param operation The `Operation` to which the Condition has been added.
 *  @return An `NSOperation`, if a dependency should be automatically added. Otherwise, `nil`.
 *  @note Only a single operation may be returned as a dependency. If you
 *  find that you need to return multiple operations, then you should be
 *  expressing that as multiple conditions. Alternatively, you could return
 *  a single `GroupOperation` that executes multiple operations internally.*/
- (NSOperation *)dependencyForOperation:(LTOperation *)operation;

/*! @brief Evaluate the condition, to see if it has been satisfied or not.
 *  @param operation Operation object.
 *  @param completion Calls when operation will finished work.*/
- (void)evaluateForOperation:(LTOperation *)operation completion:(void (^)(LTOperationConditionResult result, NSError *error))completion;

@end


#pragma mark -

/*! @class LTOperation
 *  @brief base class for operation creation.*/
@interface LTOperation : NSOperation

/*! YES if curren operation was called by user.*/
@property (nonatomic, assign) BOOL userInitiated;

/* determines whether or not to bubble error to UI */
@property (nonatomic, assign) BOOL ErrorBubblesToUI;


/*! List of conditions for operation.*/
@property (nonatomic, readonly) NSArray *conditions;

/*! List of observers for operation.*/
@property (nonatomic, readonly) NSArray *observers;

/*! List of generated errors for operation.*/
@property (nonatomic, readonly) NSArray *generatedErrors;


/*! @brief Add new condition to current operation.
 *  @param condition Condition of operation.*/
- (void)addCondition:(id<LTOperationCondition>)condition;

/*! @brief Add observer to current operation.
 *  @param observer Observer object.*/
- (void)addObserver:(id<LTOperationObserver>)observer;

/*! @brief Called when operation will enqueue.*/
- (void)willEnqueue;

/*! @brief `execute` is the entry point of execution for all `LTOperation` subclasses.
 *  If you subclass `Operation` and wish to customize its execution, you would
 *  do so by overriding the `execute` method.
 *  At some point, your `LTOperation` subclass must call one of the "finish"
 *  methods defined below; this is how you indicate that your operation has
 *  finished its execution, and that operations dependent on yours can re-evaluate
 *  their readiness state.*/
- (void)execute;

/*! @brief Cancel the operation manually passing along the error.*/
- (void)cancelWithError:(NSError *)error;

/*! @brief Cancel the operation manually passing along the errors. */
- (void)cancelWithErrors:(NSArray *)errors;

/*! @brief Most operations may finish with a single error, if they have one at all.
 *  This is a convenience method to simplify calling the actual `finish: nil`
 *  method. This is also useful if you wish to finish with an error provided
 *  by the system frameworks. */
- (void)finishWithError:(NSError *)error;

/*! @brief Finish operation with list of errors.*/
- (void)finish:(NSArray *)errors;

/*! @brief Run operation*/
- (void)produceOperation:(NSOperation *)operation;

/*! @brief Subclasses may override `finished:` if they wish to react to the operation
 *  finishing with errors. For example, a `LoadModelOperation` may implement
 *  this method to potentially inform the user about an error when trying to
 *  bring up the Core Data stack.*/
- (void)finished:(NSArray *)errors;

/*! @brief A backward compatible way to set the operation's name which won't do anything
 *  on system versions prior to 8.0*/
- (void)setSafeName:(NSString *)name;

@end


#pragma mark -
/*! @class LTOperationConditionEvaluator
 *  @brief Evaluator for operation that analyzes operation's conditions.*/
@interface LTOperationConditionEvaluator : NSObject

/*! @brief Start analyze of conditions for operation.
 *  @param conditions List of conditions.
 *  @param operation Operation object for analyze.
 *  @param completion Completion block of code with result of evaluate.*/
+ (void)evaluate:(NSArray *)conditions operation:(LTOperation *)operation completion:(void (^)(NSArray *errors))completion;

@end


#pragma mark -
/*! @category NSOperation(LTOperation)
 *  @brief Additional methods for base operation class.*/
@interface NSOperation (LTOperation)

/*! Add completion block of operation.*/
- (void)addCompletionBlock:(void (^)(void))block;

/*! Add list of dependencies for operation.*/
- (void)addDependencies:(NSArray *)dependencies;

@end


#pragma mark -
/*! Error codes for operation.*/
typedef NS_ENUM(NSInteger, LTOperationErrorCode) {
    LTOperationErrorCodeExecution = -11,    /*! Error code for execution error.*/
    LTOperationErrorCodeCondition = -12     /*! Error code for condition error.*/
};


/*! Name of domain for generate errors object.*/
extern NSString * const LTOperationErrorDomain;


/*! @category NSError(LTOperation)
 *  @brief Special methods for error class that will be used in operation.*/
@interface NSError (LTOperation)

/*! @brief Generate execution error with user information object.
 *  @param userInfo Information structure for error.*/
+ (NSError *)executionErrorWithUserInfo:(NSDictionary *)userInfo;

/*! @brief Generate condition error with user information object.
 *  @param userInfo Information structure for error.*/
+ (NSError *)conditionErrorWithUserInfo:(NSDictionary *)userInfo;

@end
