//  LTOperationQueue.m
//  Created by Fabio Teles on 7/15/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTOperationQueue.h"
#import "LTOperation.h"
#import "LTBlockObserver.h"
#import "LTExclusivityController.h"

static void * LTOperationQueueFinishObserverContext = &LTOperationQueueFinishObserverContext;

@implementation LTOperationQueue

/*! @brief Add new operation to queue.
 *  @param operation Operation object.*/
- (void)addOperation:(NSOperation *)operation {
    
    if ([operation isKindOfClass:[LTOperation class]]) {
        LTOperation *op = (LTOperation *)operation;
        // Extract any dependencies needed by this operation.
        [op.conditions enumerateObjectsUsingBlock:^(NSObject <LTOperationCondition> *condition, NSUInteger idx, BOOL *stop){
            NSOperation *dependency = [condition dependencyForOperation:op];
            if (dependency) {
                [op addDependency:dependency];
                [self addOperation:dependency];
            }
        }];
        
        /*
         With condition dependencies added, we can now see if this needs
         dependencies to enforce mutual exclusivity.
         */
        
        NSMutableArray * concurrencyCategories = [NSMutableArray array];
        
        [op.conditions enumerateObjectsUsingBlock:^(NSObject <LTOperationCondition> *condition, NSUInteger idx, BOOL *stop){
            if (![condition isMutuallyExclusive]){
                return;
            }
            [concurrencyCategories addObject:[condition name]];
        }];
        
        if (concurrencyCategories.count > 0) {
            // Set up the mutual exclusivity dependencies.
            LTExclusivityController * exclusivityController = [LTExclusivityController sharedExclusivityController];
            
            [exclusivityController addOperation:op categories:concurrencyCategories];
            
            LTBlockObserver * observer = [[LTBlockObserver alloc] initWithStartHandler:nil
                                                                        produceHandler:nil
                                                                         finishHandler:^(LTOperation * oper, NSArray * error) {
                                                                             [exclusivityController removeOperation:oper categories:concurrencyCategories];
                                                                         }];
            
            [op addObserver:observer];
        }
        
        /*
         Indicate to the operation that we've finished our extra work on it
         and it's now it a state where it can proceed with evaluating conditions,
         if appropriate.
         */
        [op willEnqueue];
        
    }
    
    [self.delegate operationQueue:self willAddOperation:operation];
    [self addCompletionObserver:operation];
    
    [super addOperation:operation];
}

/*! @brief Add list of operations to queue.
 *  @param operations List of operations.
 *  @param wait YES if need to wait until operation is finished.*/
- (void)addOperations:(NSArray *)operations waitUntilFinished:(BOOL)wait {
    /*
     The base implementation of this method does not call `addOperation()`,
     so we'll call it ourselves.
     */
    for (NSOperation *operation in operations) {
        [self addOperation:operation];
    }
    
    if (wait) {
        for (NSOperation *operation in operations) {
            [operation waitUntilFinished];
        }
    }
}

/*! @brief Add completion observer operation.
 *  @param operation Operation object.*/
- (void)addCompletionObserver:(NSOperation *)operation {
    __weak __typeof(self) weakSelf = self;
    __weak __typeof(operation) weakOperation = operation;
    [operation addCompletionBlock:^{
        if ([weakOperation isKindOfClass:[LTOperation class]]) {
            LTOperation *op = (LTOperation *)weakOperation;
            [weakSelf.delegate operationQueue:weakSelf operationDidFinish:op withErrors:op.generatedErrors];
        }
        else {
            [weakSelf.delegate operationQueue:weakSelf operationDidFinish:weakOperation withErrors:@[]];
        }
        if ((weakSelf.operations.count == 1 && [[weakSelf.operations firstObject] isEqual:weakOperation]) || weakSelf.operations.count == 0) {
            [weakSelf.delegate operationQueueDidFinishAllCurrentTasks:weakSelf];
        }
    }];
}

@end
