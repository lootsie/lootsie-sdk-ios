//
//  LTEventManager+Private.m
//  LTLootsie iOS Example
//
//  Created by Macmini on 8/31/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTEventManager+Private.h"

@implementation LTEventManager (Private)

- (void)addBaseEvent:(LTBaseUserEventID)eventID additionalNumber:(NSNumber *)number {
    LTUserActivity *event = [LTUserActivity new];
    event.eventID = eventID;
    event.additionalNumber = number;
    event.applicationFlag = 0;
    [self addEvent:@[event]];
}

- (void)addRevenueEvent:(LTBaseUserEventID)eventID additionalNumber:(NSNumber *)number {
    LTUserActivity *event = [LTUserActivity new];
    event.eventID = eventID;
    event.additionalNumber = number;
    event.applicationFlag = 2;
    [self addEvent:@[event]];
}

@end
