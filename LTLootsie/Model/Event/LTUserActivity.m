//  LTUserActivity.m
//  Created by Fabio Teles on 7/10/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTConstants.h"
#import "LTUserActivity.h"
#import "LTTools.h"

NSString *LTEventIDKey                  = @"LTEventIDKey";
NSString *LTTimestampKey                = @"LTTimestampKey";
NSString *LTAdditionalNumberKey         = @"LTAdditionalNumberKey";
NSString *LTApplicationEventFlagKey     = @"LTApplicationEventFlagKey";

@interface LTUserActivity ()
/*! Event timestamp.*/
@property (nonatomic, assign) NSInteger timestamp;

@end


@implementation LTUserActivity

- (NSArray *)eventString {
    NSMutableArray *array = [NSMutableArray arrayWithObjects:@(self.applicationFlag), @(self.timestamp), @(self.eventID), nil];
    if (self.additionalNumber != nil) {
        [array addObject:self.additionalNumber];
    }
    return array;
}

/*! @brief Load data from cache.
 *  @param aCoder Cache object.*/
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeInteger:self.eventID forKey:LTEventIDKey];
    [aCoder encodeDouble:self.timestamp forKey:LTTimestampKey];
    [aCoder encodeObject:self.additionalNumber forKey:LTAdditionalNumberKey];
    [aCoder encodeInteger:self.applicationFlag forKey:LTApplicationEventFlagKey];
}

/*! @brief Create a new object from cache.
 *  @param aDecoder Cache object.*/
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        self.additionalNumber = [aDecoder decodeObjectOfClass:[NSNumber class] forKey:LTAdditionalNumberKey];
        self.eventID = [aDecoder decodeIntegerForKey:LTEventIDKey];
        self.timestamp = [aDecoder decodeDoubleForKey:LTTimestampKey];
        self.applicationFlag = [aDecoder decodeIntegerForKey:LTApplicationEventFlagKey];
    }
    return self;
}

/*! @brief Create a new object.*/
- (instancetype)init {
    if (self = [super init]) {
        self.timestamp = [NSDate date].timeIntervalSince1970;
        self.applicationFlag = 1;
    }
    return self;
}

@end
