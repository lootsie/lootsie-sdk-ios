//  LTEventManager.h
//  Created by Alexander Dovbnya on 7/7/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTEventManager.h"
#import "LTManager.h"
#import "LTData.h"
#import "LTApp.h"
#import "LTSendUserActivitySequenceOperation.h"
#import "LTConstants.h"

NSString *LTEventManagerFileName = @"/LTEventManagerFileName";

@interface LTEventManager ()
/*! List of events. This object is a cache for events.*/
@property (nonatomic, strong) NSMutableArray<LTUserActivity *> *events;

/*! Timer for send to events.*/
@property (nonatomic, strong) NSTimer *sendToServerEvents;

/*! Time sent engagement event.*/
@property (nonatomic, strong) NSDate *timeSendedEngagementEvent;

/*! Number of retry to send requests.*/
@property (nonatomic) int exponentialBackoffMaxRange;

@end

@implementation LTEventManager

/*! @brief Check packet of data whcih will send to server and if it's a large of 2048 bytes we need to get a part of it.
 *  @param sentEvents List of events for send to server.
 *  @return Part of event swith will less or equal to 2048 bytes.*/
- (NSArray*)correctEventsList:(NSArray<LTUserActivity *> *)sentEvents {
    if(sentEvents.count > 0) {
        NSMutableArray* array = [NSMutableArray arrayWithCapacity: sentEvents.count];
        for(LTUserActivity* item in sentEvents) {
            [array addObject: [item eventString]];
        }
        NSDictionary* jsonObj = @{@"events": array};
        NSData* data = [NSJSONSerialization dataWithJSONObject:jsonObj options:NSJSONWritingPrettyPrinted error:nil];
        if(data.length > LTEventsPackageSize) {
            NSInteger newCount = sentEvents.count / 2;
            if(newCount > 0) {
                NSRange range = {0, newCount};
                NSArray* tmpArray = [NSArray arrayWithArray: [sentEvents objectsAtIndexes: [NSIndexSet indexSetWithIndexesInRange: range]]];
                sentEvents = [self correctEventsList: tmpArray];
            }
        }
    }
    
    return sentEvents;
}

/*! @brief Create a new object.*/
- (instancetype)init {
    if (self = [super init]) {
        [self load];
        [self setupNewEventTimer];
         self.enabled = YES;
    }
    return self;
}

/*! @brief Setter for exponentialBackoffMaxRange property.
 *  @param exponentialBackoffMaxRange New value of property.*/
- (void)setExponentialBackoffMaxRange:(int)exponentialBackoffMaxRange {
    if (_exponentialBackoffMaxRange == exponentialBackoffMaxRange) return;
    _exponentialBackoffMaxRange = exponentialBackoffMaxRange;
    [self setupNewEventTimer];
}

/*! @brief Init a new event timer.*/
- (void)setupNewEventTimer {
    if (self.sendToServerEvents) {
        [self.sendToServerEvents invalidate];
         self.sendToServerEvents = nil;
    }
    self.sendToServerEvents = [NSTimer scheduledTimerWithTimeInterval:(arc4random_uniform(self.exponentialBackoffMaxRange)+1)*LTEventsTimerInterval target:self selector:@selector(sendEvents) userInfo:nil repeats:YES];
}

/*! @brief Save list of event to cache.*/
- (void)save {
    if(self.enabled) {
        [NSKeyedArchiver archiveRootObject: self.events toFile:[self path]];
    }
}

/*! @brief Load list of event from cache.*/
- (void)load {
    self.events = [NSKeyedUnarchiver unarchiveObjectWithFile:[self path]];
    if (self.events == nil) {
        self.events = [NSMutableArray array];
    }
}

- (void) disable {
    if (self.enabled || self.sendToServerEvents != nil) {
        [self.sendToServerEvents invalidate];
        self.sendToServerEvents = nil;
        [self.events removeAllObjects];
        [self save];
        self.enabled = NO;
    }
}

/*! @brief Generate path to file with events cache.
 *  @return Path to file.*/
- (NSString *)path {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *dataPath = [documentsDirectory stringByAppendingPathComponent:LTEventManagerFileName];
    return dataPath;
}

- (void)addEvent:(NSArray<LTUserActivity *> *)events {
    if (events != nil) {
        [self.events addObjectsFromArray:events];
    }
    [self save];
}

- (void)sendEvents {
    if (self.enabled) {
        [self removeAppEngagementEventIfNeed];

        NSArray<LTUserActivity *> *sentEvents = [NSArray arrayWithArray:self.events];
        if (sentEvents.count > 0) {
            sentEvents = [self correctEventsList: sentEvents];
            [[LTManager sharedManager] sendUserActivity:sentEvents success:^{
                self.exponentialBackoffMaxRange = 0;
                [self.events removeObjectsInArray:sentEvents];
                [self save];
            } failure:^(NSError *error) {
                if (error.code == 429) {
                    self.exponentialBackoffMaxRange+=1;
                }
                else {
                    self.exponentialBackoffMaxRange = 0;
                }
            }];
        }
    }
}

/*! @brief Remove app engagement event if this is needed.*/
- (void)removeAppEngagementEventIfNeed {
    NSInteger i = 1;
    BOOL needSave = NO;
    while (self.events.count > 2 && i < self.events.count - 1) {
        LTUserActivity *curentEvent = self.events[i];
        LTUserActivity *nextEvent = self.events[i + 1];
        LTUserActivity *preEvent = self.events[i - 1];
        if (curentEvent.eventID == LTAppEngagementEventID && (nextEvent.timestamp - preEvent.timestamp) < LTEventsEngagementInterval) {
            [self.events removeObject:curentEvent];
            needSave = YES;
        }
        else {
            i++;
        }
    }
    if (needSave) {
        [self save];
    }
}

- (void)removeAllEvents {
    [self.events removeAllObjects];
    [self save];
}

- (void)validateEvents:(NSArray<LTUserActivity *> *)events {
    
}

@end
