//  LTEventManager.h
//  Created by Alexander Dovbnya on 7/7/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>
#import "LTUserActivity.h"

/*! @class LTEventManager
 *  @brief Manager signleton that saves event to the cache, aggregates the events
    and sends them to the backend. */
@interface LTEventManager : NSObject

    /*! Property specifies if the application (or device) is currently enabled to send Events to the API.*/
    @property (nonatomic, assign) BOOL enabled;

/*! @brief Add Application event to stack.
 *  @param eventID Event unique id.
 *  @param number Additional parameter. For example reward id, cost reward.*/
// - (void)addCustomerEvent:(NSInteger)eventID additionalNumber:(NSNumber *)number;



/*! @brief Add custom events to stack.
 *  This method is needed if developer wants to create several events in some time or create custom events.
 *  For example:
 *  @code
 *  LTUserActivity event = [LTUserActivity new];
 *  event.eventID = 10;
 *  event.additionalNumber = @(5); // if need additional parametr for events
 *  [self addEvent:@[event]];
 *  @endcode
 *  @param events List of events.
 */
- (void)addEvent:(NSArray<LTUserActivity *> *)events;

/*! @brief Send all event immediately to server.*/
- (void)sendEvents;

- (void) disable;

/*! @brief Remove all events from cache.*/
- (void)removeAllEvents;

@end
