//  ModelEntity.h
//  Created by fabio teles on 10/12/12.
//  Copyright (c) 2012 pixel4. Copyright All Rights Reserved (http://pixel4.co)

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


#import "LTConstants.h"
#import <Foundation/Foundation.h>

/*! @class LTModelEntity
 *  @brief Base object for saving server object data.*/
@interface LTModelEntity : NSObject

/*! @brief Create a model instance from JSON object structure.
 *  @param dictionary JSON object strucutre.*/
+ (instancetype)entityWithDictionary:(NSDictionary *)dictionary;

/*! @brief Create a model instance from JSON array structure.
 *  @param array JSON array strucutre.*/
+ (instancetype)entityWithArray:(NSArray *)array;

/*! @brief Create a model instance from object.
 *  @param jsonObject Object with data.*/
+ (instancetype)entityWithObject:(id)jsonObject;

/*! @brief Create a model instance from JSON structure.
 *  @param array JSON strucutre.*/
+ (NSArray *)collectionWithArray:(NSArray *)array;

/*! @brief Create a model instance from JSON object structure.
 *  @param dictionary JSON object strucutre.*/
- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

/*! @brief Create a model instance from JSON array structure.
 *  @param dictionary JSON array strucutre.*/
- (instancetype)initWithArray:(NSArray *)dictionary;

/*! @brief Override method. This method will call when need to conver JSON data to Object.
 *  @param dictionary JSON structure object.*/
- (void)populateWithDictionary:(NSDictionary *)dictionary;

/*! @brief Override method. This method will call when need to conver JSON data to Object.
 *  @param dictionary JSON structure object.*/
- (void)modifyWithDictionary:(NSDictionary *)dictionary;

/*! @brief Override method. Convert object to JSON structure. This structure will send to server.*/
- (NSDictionary *)dictionary;

@end


/*! @category NSDictionary(LTModelEntity)
 *  @brief Methods for work with LTModelEntity object.*/
@interface NSDictionary (LTModelEntity)

/*! @brief Object with safe nil.
 *  @param aKey Key of object.*/
- (id)objectForKeyNilSafe:(id)aKey;

@end


/*! @category NSArray(LTModelEntity)
 *  @brief Methods for work with LTModelEntity object.*/
@interface NSArray (LTModelEntity)

/*! @brief Convert all items to NSArray.*/
- (NSArray *)collectionOfDictionaries;

@end
