//  LTData.m
//  Created by Fabio Teles on 7/8/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTData.h"
#import "LTApp.h"
#import "LTUserAccount.h"
#import "LTAchievement.h"
#import "LTConstants.h"

@implementation LTData

/*! @brief Create a new object from cache.
 *  @param aDecoder Cache object.*/
- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super init]) {
        _version = [[aDecoder decodeObjectOfClass:[NSNumber class] forKey:LTDataStorageVersionKey] integerValue];
    }
    return self;
}

/*! @brief Load data from cache.
 *  @param aCoder Cache object.*/
- (void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[NSNumber numberWithInteger:LTDataStorageVersion] forKey:LTDataStorageVersionKey];
}

/*! @brief Enable/disabel secure coding for object.
 *  @return YES if need to enable secure coding for current class.*/
+ (BOOL)supportsSecureCoding {
    return YES;
}

- (NSArray *)achievements {
    NSMutableArray *array = [NSMutableArray array];
    for (LTRecordAchievements *achievement in self.user.achievements) {
        [array addObject:[[LTAchievement alloc] initWithRecordAchievement:achievement]];
    }
    NSSortDescriptor *sortDescription = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO];
    [array sortUsingDescriptors:@[sortDescription]];
    return array;
}

@end