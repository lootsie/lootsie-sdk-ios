//
//  LTIANRewardItem.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANRewardItem.h"
#import "LTManager+Catalog.h"
#import "LTCatalogRewardsList.h"


@implementation LTIANRewardItem

#pragma mark - init methods

- (instancetype)initWithId: (NSInteger)uid
                     title: (NSString *)title
               description: (NSString *)desc
                  rewardId: (NSInteger)rid {
    self = [super initWithId: uid type: LTIANReward title: title description: desc];
    if(self) {
        _rewardId = rid;
        LTCatalogRewardsList* list = [LTManager sharedManager].data.catalog;
        _reward = [list rewardWithId: _rewardId];
    }
    return self;
}

@end
