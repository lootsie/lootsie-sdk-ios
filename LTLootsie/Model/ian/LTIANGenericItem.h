//
//  LTIANGenericItem.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LTIANItem.h"

/*! @class LTIANGenericItem
 *  @brief A generic IAN MAY include a point value. This will typically be used to display general messages
 *  to the user such as when they earn points, earn an achievement, or other generic messages which 
 *  offer no action. Applications SHOULD display only a close button on the achievement display.*/
@interface LTIANGenericItem : LTIANItem

/*! Number of points for notification.*/
@property (readonly, nonatomic, assign)     NSInteger points;


/*! @brief Create a new object.
 *  @param uid Unique id of notification.
 *  @param title Notification title.
 *  @param desc Notification description.
 *  @param points Number of points for notification.*/
- (instancetype)initWithId: (NSInteger)uid
                     title: (NSString *)title
               description: (NSString *)desc
                    points: (NSInteger)points;
@end
