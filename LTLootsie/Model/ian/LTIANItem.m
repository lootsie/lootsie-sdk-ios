//
//  LTIANItem.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANItem.h"
#import "LTIANGenericItem.h"
#import "LTIANVideoItem.h"
#import "LTIANCatalogItem.h"
#import "LTIANRewardItem.h"
#import "LTCatalogRewardsList.h"
#import "LTData.h"
#import "LTCatalogReward.h"

#import "LTManager.h"

@implementation LTIANItem


#pragma mark - init methods

/*! @brief Create a new object.*/
- (instancetype)init {
    self = [super init];
    if(self) {
        _ianId = 0;
        _type = LTIANUnknown;
        _title = nil;
        _descStr = nil;
    }
    return self;
}

- (instancetype)initWithId: (NSInteger)uid
                      type: (LTIANType)type
                     title: (NSString*)title
               description: (NSString*)desc {
    self = [super init];
    if(self) {
        _ianId = uid;
        _type = type;
        _title = title;
        _descStr = desc;
    }
    return self;
}

+ (instancetype)entityWithDictionary:(NSDictionary *)dictionary {
    NSInteger idIAN = [dictionary[@"id"] integerValue];
    LTIANType type = [dictionary[@"type"] integerValue];
    NSString *title = dictionary[@"title"];
    NSString *desc = dictionary[@"description"];
    LTIANItem *ianItem = nil;
    if (type == LTIANGeneric) {
        NSInteger points = [dictionary[@"points"] integerValue];
        ianItem = [[LTIANGenericItem alloc] initWithId: idIAN title: title description: desc points: points];
    }
    else if (type == LTIANCatalog) {
        ianItem = [[LTIANCatalogItem alloc] initWithId: idIAN title: title description:desc];
    }
    else if (type == LTIANReward) {
        NSInteger rewardID = [dictionary[@"reward"] integerValue];
        ianItem = [[LTIANRewardItem alloc] initWithId: idIAN title: title description: desc rewardId: rewardID];
    }
    else if (type == LTIANVideo) {
        ianItem = [[LTIANVideoItem alloc] initWithId: idIAN title: title description: desc];
    }
    return ianItem;
}


+ (NSArray *)getTestIANItems {
    LTIANGenericItem* item1 = [[LTIANGenericItem alloc] initWithId: 20 title: @"5 Followers!" description: @"Way to go! Check out the catalog to see what your points have earned you!" points: 10];
    
    LTCatalogRewardsList* list = [LTManager sharedManager].data.catalog;
    LTCatalogReward *reward = [list.rewards firstObject];
    LTIANRewardItem* item2 = [[LTIANRewardItem alloc] initWithId: 50 title: @"Check it Out!" description: @"There’s a new kid on the block! We just added a new reward to your catalog we think you’ll love. Why don’t you have a look? Don’t sleep on it they go fast!" rewardId: reward.rewardId];
    LTIANVideoItem* item3 = [[LTIANVideoItem alloc] initWithId: 100 title: @"You’re so close!" description: @"Looks like you’re only 100pts away from being able to redeem a reward!  "];
    LTIANCatalogItem* item4 = [[LTIANCatalogItem alloc] initWithId: 10 title: @"4 Followers!" description: @"Way to go! Check out the catalog to see what your points have earned you!"];
    LTIANCatalogItem* item5 = [[LTIANCatalogItem alloc] initWithId: 20 title: @"12 Followers!" description: @"Way to go! Check out the catalog to see what your points have earned you!"];
    LTIANGenericItem* item6 = [[LTIANGenericItem alloc] initWithId: 300 title: @"15 Followers!" description: @"Way to go! Check out the catalog to see what your points have earned you!" points: 500];
    return @[item1, item2, item3, item4, item5, item6];
}

@end
