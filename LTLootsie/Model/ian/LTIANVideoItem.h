//
//  LTIANVideoItem.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANItem.h"

@class LTBaseInterstitialReward;
/*! @class LTIANVideoItem
 *  @brief A video IAN is shown to the user to prompt them to watch a video to earn points. Applications MUST
 *  provide a button or link for the user to open a video, and SHOULD provide a button or link to cancel
 *  or close the IAN.*/
@interface LTIANVideoItem : LTIANItem

/*! Reward ad interstitial object.*/
@property (nonatomic, strong)   LTBaseInterstitialReward* reward;


/*! @brief Create a new object.
 *  @param uid Unique id of notification.
 *  @param title Notification title.
 *  @param desc Notification description.*/
- (instancetype)initWithId: (NSInteger)uid title: (NSString *)title description: (NSString *)desc;

@end
