//
//  LTIANCatalogItem.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANGenericItem.h"

/*! @class LTIANCatalogItem
 *  @brief A catalog IAN is meant to prompt the user to open the catalog view and MUST include a button
 *  or link that opens the marketplace view, and SHOULD also have a cancel or close button.*/
@interface LTIANCatalogItem : LTIANGenericItem

/*! @brief Create new item.
 *  @param uid Unique id of IAN.
 *  @param title Title of IAN.
 *  @param desc IAN description.*/
- (instancetype)initWithId:(NSInteger)uid title:(NSString *)title description:(NSString *)desc;

@end
