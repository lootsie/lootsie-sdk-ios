//
//  LTIANGenericItem.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANGenericItem.h"

@implementation LTIANGenericItem


#pragma mark - Init methods

- (instancetype)initWithId: (NSInteger)uid
                     title: (NSString *)title
               description: (NSString *)desc
                    points: (NSInteger)points {
    self = [super initWithId: uid type: LTIANGeneric title: title description: desc];
    if(self) {
        _points = points;
    }
    return self;
}

@end
