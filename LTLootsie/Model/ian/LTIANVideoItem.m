//
//  LTIANVideoItem.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANVideoItem.h"
#import "LTBaseInterstitialReward.h"

@implementation LTIANVideoItem

- (instancetype)initWithId: (NSInteger)uid
                     title: (NSString *)title
               description: (NSString *)desc {
    self = [super initWithId: uid type: LTIANVideo title: title description: desc];
    if(self) {
        self.reward = [LTBaseInterstitialReward createInterstitial];
        self.reward.points = 100;
    }
    return self;
}

@end
