//
//  LTIANCatalogItem.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANCatalogItem.h"

@implementation LTIANCatalogItem

- (instancetype)initWithId: (NSInteger)uid
                     title: (NSString *)title
               description: (NSString *)desc {
    self = [super initWithId: uid type: LTIANCatalog title: title description: desc];
    if(self) {
        //
    }
    return self;
}

@end
