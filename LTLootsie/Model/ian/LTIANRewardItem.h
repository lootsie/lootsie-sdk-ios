//
//  LTIANRewardItem.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 6/14/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTIANItem.h"

@class LTCatalogReward;
/*! @class LTIANRewardItem
 *  @brief A reward IAN is meant to alert users of price drops, rewards that users can afford currently,
 *  or any other reason Lootsie may want to drive users to a specific reward card. Applications MUST 
 *  include a button or link that goes to the reward card for the specified reward ID.*/
@interface LTIANRewardItem : LTIANItem

/*! Unique id of reward.*/
@property (readonly, nonatomic, assign)     NSInteger rewardId;

/*! Reward item object. Save information abot reward.*/
@property (readonly, nonatomic, strong)     LTCatalogReward* reward;


/*! @brief Create a new object.
 *  @param uid Unique id of notification.
 *  @param title Notification title.
 *  @param desc Notification description.
 *  @param rid Unique id of reward.*/
- (instancetype)initWithId: (NSInteger)uid
                     title: (NSString *)title
               description: (NSString *)desc
                  rewardId: (NSInteger)rid;

@end
