//  ModelEntity.n
//  Created by fabio teles on 10/12/12.
//  Copyright (c) 2012 pixel4. Copyright All Rights Reserved (http://pixel4.co)

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTConstants.h"
#import "LTModelEntity.h"

@implementation LTModelEntity

+ (instancetype)entityWithDictionary:(NSDictionary *)dictionary
{
	return [[self alloc] initWithDictionary:dictionary];
}

+ (instancetype)entityWithArray:(NSArray *)array {
    return [[self alloc] initWithArray:array];
}

+ (instancetype)entityWithObject:(id)jsonObject {
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        return [self entityWithArray:jsonObject];
    }
    if ([jsonObject isKindOfClass:[NSDictionary class]]) {
        return [self entityWithDictionary:jsonObject];
    }
    return nil;
}

+ (NSArray *)collectionWithArray:(NSArray *)array
{
	NSMutableArray *items = [NSMutableArray array];
	for (id jsonObject in array) {
        [items addObject:[self entityWithObject:jsonObject]];
	}
	if (items.count > 0) {
		return [NSArray arrayWithArray:items];
	}
	return nil;
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if (self = [super init]) {
        [self populateWithDictionary:dictionary];
    }
    return self;
}

- (instancetype)initWithArray:(NSArray *)array {
    if (self = [super init]) {
        
    }
    return self;
}

- (void)modifyWithDictionary:(NSDictionary *)dictionary
{
    // should be overriden by subclasses
}

- (void)populateWithDictionary:(NSDictionary *)dictionary
{
    // should be overriden by subclasses
}

- (NSDictionary *)dictionary {
    // should be overriden by subclasses
    return nil;
}

@end

@implementation NSDictionary (LTModelEntity)

- (id)objectForKeyNilSafe:(id)aKey
{
	id val = [self objectForKey:aKey];
	if ([val isEqual:[NSNull null]]) {
		return nil;
	} else {
		return val;
	}
}

@end

@implementation NSArray (LTModelEntity)

- (NSArray *)collectionOfDictionaries
{
    NSMutableArray *collection = [NSMutableArray array];
    for (id value in self) {
        if ([value isKindOfClass:[LTModelEntity class]]) {
            [collection addObject:[(LTModelEntity *)value dictionary]];
        } else {
            return nil;
        }
    }
    return [NSArray arrayWithArray:collection];
}

@end
