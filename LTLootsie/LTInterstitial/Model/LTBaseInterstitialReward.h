//  LTBaseInterstitialReward.h
//  Created by Alexander Dovbnya on 3/1/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTBaseReward.h"

/*! @protocol LTInterstitialSettings
 *  @brief Interstitial settings properties.*/
@protocol LTInterstitialSettings <NSObject>

/*! Intersitial unique id.*/
@property (nonatomic, strong) NSString *interstitialID;

/*! List of keywords.*/
@property (nonatomic, strong) NSArray *keywords;

@end


/*! @class LTBaseInterstitialReward
 *  @brief Base class for work with ad interstitial.*/
@interface LTBaseInterstitialReward : LTBaseReward <LTInterstitialSettings>

/*! Callback is called when reward is received.*/
@property (copy) void (^didReciveReward)(BOOL);

/*! Unique id of reward.*/
@property (nonatomic, strong) NSNumber *rewardID;

/*! Unique id of interstitial.*/
@property (nonatomic, strong) NSString *interstitialID;

/*! List of keywords.*/
@property (nonatomic, strong) NSArray *keywords;

/*! @brief Create intersitial object.*/
+ (instancetype)createInterstitial;

/*! @brief Show video controller of parent.
 *  @param rootViewController Parent controller for show video.*/
- (void)presentFromRootViewController:(UIViewController *)rootViewController;

@end
