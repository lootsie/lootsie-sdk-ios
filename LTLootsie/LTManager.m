//  LTManager.m
//  Created by Fabio Teles on 7/30/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTManager.h"
#import "LTManager+Private.h"

#import "LTNetworkManager.h"
#import "LTAllSequence.h"

#import "LTBlockOperation.h"
#import "LTPostNotificationOperation.h"
#import "LTNoCancelledDependencies.h"

#import "LTApp.h"
#import "LTBaseInterstitialReward.h"
#import "LTUserAccount.h"
#import "LTUserActivity.h"

#import "LTDatabaseProtocol.h"
#import "LTDatabase.h"
#import "LTData.h"

#import "LTTimer.h"
#import "LTMWLogging.h"
#import "LTAFNetworkReachabilityManager.h"

NSString * const LTInitializationCompleteNotification   = @"LTInitializationCompleteNotification";
NSString * const LTUserAccountUpdatedNotification       = @"LTUserAccountUpdatedNotification";
NSString * const LTIANNotification                      = @"LTIANNotification";
NSString * const LTChangeFavoriteRewardNotification     = @"LTChangeFavoriteRewardNotification";
NSString * const LTVideoInterstitialWathcedNotification = @"LTVideoInterstitialWathcedNotification";
NSString * const LTRewardRedeemedNotification           = @"LTRewardRedeemedNotification";
NSString * const LTDisableLootsieNotification           = @"LTDisableLootsieNotification";

NSString * const LTAchievementUserInfoKey               = @"achievement";
NSString * const LTSavedInfoUserInfoKey                 = @"savedInfo";
NSString * const LTRewardUserInfoKey                    = @"reward";
NSString * const LTUserUserInfoKey                      = @"user";
NSString * const LTIANKey                               = @"ian";

NSInteger const LTSDKFailedInitializingErrorCode        = -101;
NSString * const LTErrorDomain                          = @"com.lootsie.error";

NSString * const LTManagerGeneralQueueName              = @"com.lootsie.manager.general.queue";

void * LTKVOContext = &LTKVOContext;

#pragma mark -

@interface LTManager ()
/*! Database protocol for save/load data to cache.*/
@property (nonatomic, strong) id <LTDatabaseProtocol>database;

/*! Send GPS coordinate to server.*/
@property (nonatomic, assign) BOOL sendLocation;

/*! Panding operations.*/
@property (nonatomic, assign) BOOL hasPendingOperations;

/*! Number of max retries for send request to server.*/
@property (nonatomic) int exponentialBackoffMaxRange;

/*! Timer for retry to send request.*/
@property (nonatomic, strong) LTTimer *backoffTimer;

/*! Timer for save data to cache.*/
@property (nonatomic, strong) LTTimer *saveTimer;

/*! Callback is called after success start.*/
@property (nonatomic, strong) LTSuccessBlock lazyStartSuccessBlock;

/*! Callback is called after failure start.*/
@property (nonatomic, strong) LTFailureBlock lazyStartFailureBlock;

@end


@implementation LTManager {
    BOOL _lazyInitializationDisabled;
    BOOL _completeStartEngineCalled;
}

@dynamic data;

+ (instancetype)sharedManager {
    static LTManager *_engine;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _engine = [self new];
    });
    return _engine;
}

/*! @brief Create a new object.*/
- (instancetype)init {
    if (self = [super init]) {
        _status = LTSDKStatusNotInitialized;
        _completeStartEngineCalled = NO;
        // general queue
        _generalQueue = [LTOperationQueue new];
        _generalQueue.name = LTManagerGeneralQueueName;
        _generalQueue.maxConcurrentOperationCount = 1;
        // op to be triggered after init
        _lazyInitializationDisabled = YES;
        // database
        _database = [LTDatabase new];
        // KVO
        [_database.data addObserver:self
                         forKeyPath:@"user"
                            options:(NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew)
                            context:LTKVOContext];
        
        // Notifications
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
        
        [self setOperationDelegate:self];
        
        self.eventManager = [LTEventManager new];
        
        self.isShowingReward = false;
    }
    return self;
}

/*! @brief Called when object was released.*/
- (void)dealloc {
    // Notifications
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    
    // KVO
    //#if ENEBLE_ACCOUNT
    [self.data.user removeObserver:self forKeyPath:@"info" context:LTKVOContext];
    [self.data removeObserver:self forKeyPath:@"user" context:LTKVOContext];
    //#endif
}
#pragma mark - Status management
/*! @brief Observer for reset SDK when we detect connection problem.*/
- (void)observeConnectivityToReset {
    __weak LTAFNetworkReachabilityManager *reachabilityManager = [LTNetworkManager lootsieManager].reachabilityManager;
    if (![reachabilityManager isReachable]) {
        [reachabilityManager setReachabilityStatusChangeBlock:^(LTAFNetworkReachabilityStatus status) {
            if (reachabilityManager.isReachable) {
                [self resetSDKStatus];
                [reachabilityManager setReachabilityStatusChangeBlock:nil];
            }
        }];
    }
}

/*! @brief Update status of SDK.
 *  @param status New status of SDK.*/
- (void)setStatus:(LTSDKStatus)status {
    NSAssert(status != _status, @"Performing invalid cyclic status transition");
    // This does not work anymore because accounts can now be deactivated.
    // NSAssert(status > _status, @"Performing invalid out of order status change");
    
    // cannot leave ready or failed status
    if (_status != LTSDKStatusReady && _status != LTSDKStatusFailed) {
        _status = status;
    }
}

- (void)resetSDKStatus {
    NSAssert(self.status == LTSDKStatusFailed, @"SDK MUST be in state Failed in order to reset it");
    
    if (self.status != LTSDKStatusFailed) {
        LTLogWarning(@"Trying to reset the SDK when it's not is the failed state");
        return;
    }
    
    // re-create the after init op
    [self createAfterOperation];
    
    // reset status
    [self willChangeValueForKey:@"status"];
    _status = LTSDKStatusNotInitialized;
    [self didChangeValueForKey:@"status"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LTDisableLootsieNotification object:self];
}

#pragma mark -

- (void)setCustomDatabase:(id<LTDatabaseProtocol>)database {
    NSString *message = @"You can NOT change the database after the initialization process has started";
    NSAssert(self.status <= LTSDKStatusWaitingInitialization, message);
    if (self.status > LTSDKStatusWaitingInitialization) {
        LTLogWarning(@"%@", message);
    } else {
        self.database = database;
    }
}

#pragma mark - KVO
/*! @brief Given that the receiver has been registered as an observer of the value at a key path relative to an object, be notified of a change to that value.
 *  @param keyPath Name of property which was updated.
 *  @param object Object which property was udpated.
 *  @param change Change structure with update data.
 *  @param context Context pointer.*/
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    if (context == LTKVOContext) {
        if ([keyPath isEqualToString:@"user"]) {
            LTUserAccount *oldUser = (LTUserAccount *)[change objectForKeyNilSafe:NSKeyValueChangeOldKey];
            /*!
             If the entire object changed remove KVO from old one and add it on
             the new one. Since we're using `objectForKeyNilSafe:` if it's nil there
             is no harm calling methods.
             */
            LTUserAccount *newUser = (LTUserAccount *)[change objectForKeyNilSafe:NSKeyValueChangeNewKey];
            [oldUser removeObserver:self forKeyPath:@"info" context:LTKVOContext];
            [newUser addObserver:self forKeyPath:@"info" options:NSKeyValueObservingOptionInitial context:LTKVOContext];
        }
        if ([keyPath isEqualToString:@"user"] || [keyPath isEqualToString:@"info"]) {
            LTPostNotificationOperation *notifOp = [[LTPostNotificationOperation alloc] initWithNotificationName:LTUserAccountUpdatedNotification
                                                                                                          object:self
                                                                                                        userInfo:@{LTUserUserInfoKey : (self.data.user ?: [NSNull null])}];
            [_generalQueue addOperation:notifOp];
        }
    }
}

#pragma mark - Notifications
/*! @brief Called when application returns to active mode.
 *  @param notification Notification object.*/
- (void)applicationWillEnterForeground:(NSNotification *)notification {
    [self handleSessionBegin];
}

/*! @brief Called when application did enter to background mode.
 *  @param notification Notification object.*/
- (void)applicationDidEnterBackground:(NSNotification *)notification {
    [self handleSessionEnd];
}

#pragma mark - Database
/*! @brief Save data every 5 sec.*/
- (void)schedulePeriodicSaveData {
    [self scheduleSaveDataWithInterval:5.0];
}

/*! @brief Save data every 20 msec.*/
- (void)scheduleUrgentSaveData {
    [self scheduleSaveDataWithInterval:0.02];
}

/*! @brief Save data with interval.
 *  @param interval Interval second.*/
- (void)scheduleSaveDataWithInterval:(NSTimeInterval)interval {
    [self.saveTimer cancel];
    __weak __typeof(self) weakSelf = self;
    self.saveTimer = [[LTTimer alloc] initWithInterval:interval handler:^{
        [weakSelf saveData];
    }];
}

/*! @brief Sava data.*/
- (void)saveData {
    self.saveTimer = nil;
    [self.database save];
}


#pragma mark - Sessions
/*! @brief Called when session is start.*/
- (void)handleSessionBegin {
    [[LTNetworkManager lootsieManager].reachabilityManager startMonitoring];
}

/*! @brief Called when session is end.*/
- (void)handleSessionEnd {
    [self scheduleUrgentSaveData];
    [[LTNetworkManager lootsieManager].reachabilityManager stopMonitoring];
}

#pragma mark - API calls
- (void)startEngineWithAppKey:(NSString *)appKey
                      success:(void (^)(void))success
                      failure:(void (^)(NSError *error))failure {
    // if already initialized do nothing
    if (self.hasAccountBeenDeactivated) {
        self.appKey = appKey;
        [self addLazyStartSuccessBlock:success];
        [self addLazyStartFailureBlock:failure];
        LTLogDebug(@"SDK deactivated by user. invoke reactivateAccount to prompt user to reactivate.");
    }
    else if (self.status > LTSDKStatusNotInitialized) {
        LTLogInfo(@"SDK already started");
        return;
    } else {
        self.appKey = appKey;
        [self addLazyStartSuccessBlock:success];
        [self addLazyStartFailureBlock:failure];
        
        [self completeStartEngine];
    }
}

/*! @brief Start SDK if needed.
 *  @param success Callback is called when SDK is started.
 *  @param failure Callbakc is called when SDK did receive error while starting.*/
- (void)startIfNeededWithSuccess:(void (^)(void))success
                         failure:(void (^)(NSError *error))failure {
    LTBlockOperation *op = [[LTBlockOperation alloc] initWithMainQueueBlock:^{
        if (self.status == LTSDKStatusReady) {
            if (success) success();
        } else {
            if (failure) failure([NSError errorWithDomain:LTNetworkErrorDomain code:LTSDKFailedInitializingErrorCode userInfo:nil]);
        }
    }];
    op.userInitiated = YES;
    [op setSafeName:@"com.lootsie.manager.startIfNeeded"];
    [self addStartPendingNetworkOperation:op];
}

- (void)sendUserActivity:(NSArray<LTUserActivity *> *)userActivity
                 success:(void (^)(void))success
                 failure:(void (^)(NSError *error))failure {
    LTSendUserActivitySequenceOperation *activityOp = [LTSendUserActivitySequenceOperation sequenceWithUserActivity:userActivity];
    [activityOp setSuccess:success failure:failure];
    [self addNetworkOperation:activityOp];
}

- (void)queueEvent:(NSInteger) eventID
  additionalNumber: (NSNumber*) additionalNumber
           success:(void (^)(void))success
           failure:(void (^)(NSError *error))failure {
    
    LTUserActivity *activity = [LTUserActivity new];
    activity.applicationFlag = 1;
    activity.eventID = eventID;
    activity.additionalNumber = additionalNumber;
    
    NSArray *events = [NSArray arrayWithObjects: activity, nil];
    
    
    [self sendUserActivity: events
                   success: success
                   failure: failure];
}

- (void)queueEngagementEventWithSuccess:(void (^)(void))success
                                failure:(void (^)(NSError *error))failure {
    LTUserActivity *activity = [LTUserActivity new];
    activity.applicationFlag = 0;
    activity.eventID = 20;
    activity.additionalNumber = nil;
    
    NSArray *events = [NSArray arrayWithObjects: activity, nil];
    
    [self sendUserActivity: events
                   success: success
                   failure: failure];
}

- (void)registerSpend:(NSInteger)cost{
    LTUserActivity *userActivity = [LTUserActivity new];
    userActivity.eventID = LTRegisterSpend;
    userActivity.additionalNumber = @(cost);
    [self.eventManager addEvent:@[userActivity]];
}


#pragma mark - Easy access to data

- (LTData *)data {
    return self.database.data;
}


#pragma mark - Queue delegate
- (void)operationQueue:(LTOperationQueue *)queue operationDidFinish:(NSOperation *)operation withErrors:(NSArray *)errors {
    if ([operation isKindOfClass:[LTInitSequenceOperation class]]) {
        if (operation.isCancelled) {
            NSError *error = errors.firstObject;
            if (error.code == NSURLErrorNotConnectedToInternet ||
                error.code == NSURLErrorTimedOut ||
                error.code == NSURLErrorNetworkConnectionLost ||
                error.code == NSURLErrorDataNotAllowed) {
                [self observeConnectivityToReset];
            }
        } else {
            [self schedulePeriodicSaveData];
        }
    }
}
- (void)operationQueue:(LTOperationQueue *)queue willAddOperation:(NSOperation *)operation {
    // no op
}
- (void)operationQueueDidFinishAllCurrentTasks:(LTOperationQueue *)queue {
    // no op
}
- (BOOL) hasAccountBeenDeactivated {
    NSNumber *isNotAuthorized = [[NSUserDefaults standardUserDefaults] objectForKey:@"lt_notauthorized_key"];
    
    if(isNotAuthorized != nil && [isNotAuthorized boolValue]) {
        return YES;
    }
    else {
        return NO;
    }
}
- (void) completeStartEngine {
    if(_completeStartEngineCalled) {
        return;
    }
    _completeStartEngineCalled = YES;
    
    [self.eventManager sendEvents];
    // laziness
    if (_lazyInitializationDisabled || self.hasPendingOperations) {
        LTLogInfo(@"Starting SDK");
        [self addStartOperation];
    } else {
        LTLogDebug(@"Saving info for lazy initialization");
        self.status = LTSDKStatusWaitingInitialization;
    }
    // Record session begin
    [self handleSessionBegin];
}

@end
