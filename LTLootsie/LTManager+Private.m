//  LTManager+Private.m
//  Created by Alexander Dovbnya on 3/23/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTManager+Private.h"
#import "LTNetworkManager.h"

#import "LTOperation.h"
#import "LTSequence.h"
#import "LTNetworkObserver.h"

#import "LTBlockOperation.h"
#import "LTNoCancelledDependencies.h"
#import "LTInitSequenceOperation.h"
#import "LTPostNotificationOperation.h"

#import <objc/runtime.h>
#import "LTMWLogging.h"
#import "LTTimer.h"

/*! @brief Concat two success blocks.*/
static LTSuccessBlock LTConcatSuccessBlocks(LTSuccessBlock blockA, LTSuccessBlock blockB) {
    if (!blockA) return blockB;
    if (!blockB) return blockA;
    return ^{
        blockA();
        blockB();
    };
}

/*! @brief Concat two failure blocks.*/
static LTFailureBlock LTConcatFailureBlocks(LTFailureBlock blockA, LTFailureBlock blockB) {
    if (!blockA) return blockB;
    if (!blockB) return blockA;
    return ^(NSError *error){
        blockA(error);
        blockB(error);
    };
}

static char kAfterInitOpObjectKey;


/*! @category LTManager(PrivateProperty)
 *  @brief Private methods and properties.*/
@interface LTManager (PrivateProperty)

/*! SDK status.*/
@property (nonatomic, readwrite, assign) LTSDKStatus status;

/*! Application init key.*/
@property (nonatomic, copy) NSString *appKey;

/*! Registered email of user.*/
@property (nonatomic, strong) NSString *registerEmail;

/*! YES if need to send GPS coordinate.*/
@property (nonatomic, assign) BOOL sendLocation;

/*! Panding operations.*/
@property (nonatomic, assign) BOOL hasPendingOperations;

/*! Range of retry to send request if SDK receive status code 429.*/
@property (nonatomic) int exponentialBackoffMaxRange;

/*! Timer for retry send requests to server.*/
@property (nonatomic, strong) LTTimer *backoffTimer;


/*! Callback is called on success start.*/
@property (nonatomic, strong) LTSuccessBlock lazyStartSuccessBlock;

/*! Callback is called on failure start.*/
@property (nonatomic, strong) LTFailureBlock lazyStartFailureBlock;


/*! @brief Reset status of SDK.*/
- (void)resetSDKStatus;

@end


@implementation LTManager (Private)

NSString * const  LTNotAuthorizedKey               =   @"lt_notauthorized_key";

- (void)addStartPendingNetworkOperation:(LTOperation *)operation {
    [self addStartPendingNetworkOperation:operation startIfPossible:YES];
}

- (void)addStartPendingNetworkOperation:(LTOperation *)operation startIfPossible:(BOOL)startIfPossible {
    [operation addDependency:self.afterInitOp];
    [operation addCondition:[LTNoCancelledDependencies new]];
    
    /// Avoid sequences of the same type to be added
    BOOL shouldAdd = YES;
    if ([operation isKindOfClass:[LTSequence class]]) {
        LTSequence *existentOp = (LTSequence *)[[LTNetworkManager lootsieManager] lastQueuedNonFinishedOperationOfType:[operation class]];
        if (existentOp) {
            /// concat blocks so the new sequence get notified when the old completes
            LTSequence *currentOp = (LTSequence *)operation;
            [existentOp setSuccess:LTConcatSuccessBlocks(existentOp.success, currentOp.success)
                           failure:LTConcatFailureBlocks(existentOp.failure, existentOp.failure)];
            shouldAdd = NO;
        }
    }
    
    if (shouldAdd) [self addNetworkOperation:operation];
    
    if (startIfPossible) {
        if (self.status < LTSDKStatusInitializing && self.appKey) {
            [self addStartOperation];
        }
        self.hasPendingOperations = YES;
    }
}

- (void)addStartOperation {
    NSAssert(self.status < LTSDKStatusInitializing, @"Called after SDK was already started");
    if (!self.appKey) {
        LTLogInfo(@"Operation queued because start engine has NOT been called yet");
        return;
    }
    
    if (self.status >= LTSDKStatusInitializing) return;
    
    
    self.status = LTSDKStatusInitializing;
    LTLogInfo(@"Starting SDK with app secret key: %@", self.appKey);
    
    LTInitSequenceOperation *initOp = [LTInitSequenceOperation initOperation];
    // status management
    void (^lazySuccess)(void) = self.lazyStartSuccessBlock;
    void (^lazyFailure)(NSError *) = self.lazyStartFailureBlock;
    
    __weak __typeof(self) weakSelf = self;
    [initOp setSuccess:^{
        weakSelf.status = LTSDKStatusReady;
        if (lazySuccess) lazySuccess();
    } failure:^(NSError *error) {
        weakSelf.status = LTSDKStatusFailed;
        if (lazyFailure) lazyFailure(error);
    }];
    
    // notification
    LTPostNotificationOperation *noteOp = [[LTPostNotificationOperation alloc] initWithNotificationName:LTInitializationCompleteNotification object:self userInfo:nil];
    [self addGeneralOperation:noteOp dependencies:@[initOp] allowCancelledDependencies:NO];
    
    // queueing (after init)
    [self.afterInitOp addDependency:initOp];
    [self addNetworkOperation:self.afterInitOp];
    [self addNetworkOperation:initOp];
    
    // clean up lazy blocks
    self.lazyStartSuccessBlock = nil;
    self.lazyStartFailureBlock = nil;
}

- (void)addNetworkOperation:(LTOperation *)operation {
    if (self.status == LTSDKStatusFailed) {
        LTLogCritical(@"Lootsie's SDK has failed to initialize");
        return;
    }
    
    [[LTNetworkManager lootsieManager] addNetworkOperation:operation];
}

- (void)setOperationDelegate:(id<LTOperationQueueDelegate>)delegate {
    self->_generalQueue.delegate = delegate;
    [LTNetworkManager lootsieManager].networkQueue.delegate = delegate;
}

- (void)enableSequenceExponentialBackoffTimer:(BOOL)enable {
    [self.backoffTimer cancel];
    
    if (!enable) {
        self.exponentialBackoffMaxRange = 0;
        self.backoffTimer = nil;
        return;
    }
    else {
        self.exponentialBackoffMaxRange++;
    }
    __weak __typeof(self) weakSelf = self;
    self.backoffTimer = [[LTTimer alloc] initWithInterval:(arc4random_uniform(self.exponentialBackoffMaxRange)+1)*LTEventsTimerInterval handler:^{
        weakSelf.backoffTimer = nil;
        if (weakSelf.status == LTSDKStatusFailed) {
            [weakSelf resetSDKStatus];
            [weakSelf addStartOperation];
        }
    }];
}

#pragma mark - Property

- (LTBlockOperation *)afterInitOp {
    LTBlockOperation *_afterInitOp = objc_getAssociatedObject(self, &kAfterInitOpObjectKey);
    if (_afterInitOp == nil) {
        _afterInitOp = [self createAfterOperation];
    }
    return _afterInitOp;
}

/*! @brief Get general queue.*/
- (LTOperationQueue *)generalQueue {
    return _generalQueue;
}

#pragma mark - Helpers

- (LTBlockOperation *)createAfterOperation {
    LTBlockOperation *_afterInitOp = [[LTBlockOperation alloc] initWithBlock: ^(LTOperationCompletionBlock completion) { completion(nil); }];
    [_afterInitOp setSafeName:@"com.lootsie.manager.afterInit"];
    [_afterInitOp addCondition:[LTNoCancelledDependencies new]];
    objc_setAssociatedObject(self, &kAfterInitOpObjectKey, _afterInitOp, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    return _afterInitOp;
}

- (void)addGeneralOperation:(LTOperation *)generalOp dependencies:(NSArray *)dependencies allowCancelledDependencies:(BOOL)allowCancelledDependencies {
    [generalOp addDependencies:dependencies];
    if (!allowCancelledDependencies) [generalOp addCondition:[LTNoCancelledDependencies new]];
    [self.generalQueue addOperation:generalOp];
}


- (void)addLazyStartSuccessBlock:(LTSuccessBlock)block {
    NSAssert(self.status <= LTSDKStatusInitializing, @"Cannot change lazy blocks after the SDK is initialized");
    
    if (!block) return;
    self.lazyStartSuccessBlock = LTConcatSuccessBlocks(self.lazyStartSuccessBlock, block);
}

- (void)addLazyStartFailureBlock:(LTFailureBlock)block {
    NSAssert(self.status <= LTSDKStatusInitializing, @"Cannot change lazy blocks after the SDK is initialized");
    
    if (!block) return;
    self.lazyStartFailureBlock = LTConcatFailureBlocks(self.lazyStartFailureBlock, block);
}

- (void)setStatusAccountDeactivated {
    self.status = LTSDKStatusNotAuthorized;
        [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool:YES] forKey:@"lt_notauthorized_key"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSAssert(self.hasAccountBeenDeactivated, @"Account is marked deactivated now");
}


- (void)setStatusAccountActivated {
    self.status = LTSDKStatusWaitingInitialization; // Check if this is correct.

    [[NSUserDefaults standardUserDefaults] setObject: [NSNumber numberWithBool:NO] forKey:@"lt_notauthorized_key"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    NSAssert(!self.hasAccountBeenDeactivated, @"Account is marked active now");

    [self completeStartEngine]; // @TODO This probably works only the first time. Need to check.
}

@end
