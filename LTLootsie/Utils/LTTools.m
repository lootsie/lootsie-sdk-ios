//  LTTools.m
//  Created by Fabio Teles on 7/30/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTTools.h"
#import "LTManager.h"
#import "LTData.h"
#import "LTApp.h"
#import "LTTheme.h"
#import <sys/sysctl.h>

#import <Accelerate/Accelerate.h>
#import <float.h>


@implementation LTTools

+ (NSString*)deviceName {
    NSString* deviceName = nil;
    
#if TARGET_IPHONE_SIMULATOR
    deviceName = [[NSProcessInfo processInfo].environment objectForKey: @"SIMULATOR_DEVICE_NAME"];
#else
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *name = malloc(size);
    sysctlbyname("hw.machine", name, &size, NULL, 0);
    NSString* machine = [NSString stringWithUTF8String:name];
    free(name);
    
    if([machine isEqualToString:@"iPad1,1"] ||
       [machine isEqualToString:@"iPad1,2"])
    {
        deviceName = @"iPad";
    }
    else if([machine isEqualToString:@"iPad2,1"] ||
            [machine isEqualToString:@"iPad2,2"] ||
            [machine isEqualToString:@"iPad2,3"] ||
            [machine isEqualToString:@"iPad2,4"])
    {
        deviceName = @"iPad 2";
    }
    else if([machine isEqualToString:@"iPad2,5"] ||
            [machine isEqualToString:@"iPad2,6"] ||
            [machine isEqualToString:@"iPad2,7"])
    {
        deviceName = @"iPad mini";
    }
    else if([machine isEqualToString:@"iPad3,1"] ||
            [machine isEqualToString:@"iPad3,2"] ||
            [machine isEqualToString:@"iPad3,3"])
    {
        deviceName = @"iPad 3";
    }
    else if([machine isEqualToString:@"iPad3,4"] ||
            [machine isEqualToString:@"iPad3,5"] ||
            [machine isEqualToString:@"iPad3,6"])
    {
        deviceName = @"iPad 4";
    }
    else if([machine isEqualToString:@"iPad4,1"] ||
            [machine isEqualToString:@"iPad4,2"] ||
            [machine isEqualToString:@"iPad4,3"])
    {
        deviceName = @"iPad Air";
    }
    else if([machine isEqualToString:@"iPad4,4"] ||
            [machine isEqualToString:@"iPad4,5"] ||
            [machine isEqualToString:@"iPad4,6"])
    {
        deviceName = @"iPad mini 2";
    }
    else if([machine isEqualToString:@"iPad5,3"] ||
            [machine isEqualToString:@"iPad5,4"])
    {
        deviceName = @"iPad Air 2";
    }
    else if([machine isEqualToString:@"iPad4,7"] ||
            [machine isEqualToString:@"iPad4,8"])
    {
        deviceName = @"iPad mini 3";
    }
    else if([machine isEqualToString:@"iPhone1,1"])
    {
        deviceName = @"iPhone";
    }
    else if([machine isEqualToString:@"iPhone1,2"])
    {
        deviceName = @"iPhone 3G";
    }
    else if([machine isEqualToString:@"iPhone2,1"])
    {
        deviceName = @"iPhone 3GS";
    }
    else if([machine isEqualToString:@"iPhone3,1"] ||
            [machine isEqualToString:@"iPhone3,2"] ||
            [machine isEqualToString:@"iPhone3,3"])
    {
        deviceName = @"iPhone 4";
    }
    else if([machine isEqualToString:@"iPhone4,1"])
    {
        deviceName = @"iPhone 4S";
    }
    else if([machine isEqualToString:@"iPhone5,1"] ||
            [machine isEqualToString:@"iPhone5,2"])
    {
        deviceName = @"iPhone 5";
    }
    else if([machine isEqualToString:@"iPhone5,3"] ||
            [machine isEqualToString:@"iPhone5,4"])
    {
        deviceName = @"iPhone 5C";
    }
    else if([machine isEqualToString:@"iPhone6,1"] ||
            [machine isEqualToString:@"iPhone6,2"])
    {
        deviceName = @"iPhone 5S";
    }
    else if([machine isEqualToString:@"iPhone7,1"])
    {
        deviceName = @"iPhone 6";
    }
    else if([machine isEqualToString:@"iPhone7,2"])
    {
        deviceName = @"iPhone 6 Plus";
    }
    else if([machine isEqualToString:@"iPhone8,1"])
    {
        deviceName = @"iPhone 6s";
    }
    else if([machine isEqualToString:@"iPhone8,2"])
    {
        deviceName = @"iPhone 6s Plus";
    }
    else if([machine isEqualToString:@"iPod1,1"])
    {
        deviceName = @"iPod touch";
    }
    else if([machine isEqualToString:@"iPod2,1"])
    {
        deviceName = @"iPod touch 2";
    }
    else if([machine isEqualToString:@"iPod3,1"])
    {
        deviceName = @"iPod touch 3";
    }
    else if([machine isEqualToString:@"iPod4,1"])
    {
        deviceName = @"iPod touch 4";
    }
    else if([machine isEqualToString:@"iPod5,1"])
    {
        deviceName = @"iPod touch 5";
    }
#endif
    
    return deviceName;
}

+ (BOOL)isIphoneFive {
    NSString* deviceName = [LTTools deviceName];
    if([deviceName hasPrefix:@"iPhone 5"]) {
        return YES;
    }
    
    return NO;
}

+ (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (UIColor*) colorWithHex: (NSUInteger)hex {
    CGFloat red, green, blue, alpha;
    
    red = ((CGFloat)((hex >> 16) & 0xFF)) / ((CGFloat)0xFF);
    green = ((CGFloat)((hex >> 8) & 0xFF)) / ((CGFloat)0xFF);
    blue = ((CGFloat)((hex >> 0) & 0xFF)) / ((CGFloat)0xFF);
    alpha = hex > 0xFFFFFF ? ((CGFloat)((hex >> 24) & 0xFF)) / ((CGFloat)0xFF) : 1;
    
    return [UIColor colorWithRed: red green:green blue:blue alpha:alpha];
}

+ (NSDateFormatter *)dateFormatterWithDateFormat:(NSString *)dateFormat
{
    static NSDateFormatter *dateFormatter = nil;
    if (dateFormatter == nil) {
        dateFormatter = [[NSDateFormatter alloc] init];
    }
    dateFormatter.dateFormat = dateFormat;
    return dateFormatter;
}

+ (NSAttributedString *)pointsStringValue:(NSInteger)points {
    UIFont* pointFont = [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                         weight: LTRegularFontWeight
                                                          style: LTRegularFontStyle
                                                           size: LTMediumAFontSize];
    UIFont* currencyFont = [[LTTheme sharedTheme] fontWithFamily: LTPrimaryFontFamily
                                                            weight: LTRegularFontWeight
                                                             style: LTRegularFontStyle
                                                              size: LTMediumCFontSize];
    UIColor* textColor = [[LTTheme sharedTheme] colorWithType: LTColorPointText];
    NSMutableAttributedString *attributedStr = [[NSMutableAttributedString alloc] initWithAttributedString:[self pointsStringValue:points withPointFont:pointFont currencyFont:currencyFont]];
    [attributedStr addAttributes:@{NSForegroundColorAttributeName : textColor} range:NSMakeRange(0, attributedStr.string.length)];
    return attributedStr;
}

+ (NSAttributedString *)pointsStringValue:(NSInteger)points withPointFont:(UIFont *)pointFont currencyFont:(UIFont *)currencyFont {
    static NSNumberFormatter *formatter;
    static NSString *currency_abbreviation = @"pts";
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [NSNumberFormatter new];
        [formatter setNumberStyle: NSNumberFormatterDecimalStyle];
        [formatter setLocale: [NSLocale currentLocale]];
    });
#if !TARGET_INTERFACE_BUILDER
    currency_abbreviation = [LTManager sharedManager].data.app.currencyAbbreviation;
#endif
    UIColor* textColor = [[LTTheme sharedTheme] colorWithType: LTColorPointText];
    NSString *priceString = [formatter stringFromNumber:@(points)];
    NSMutableAttributedString* str = [NSMutableAttributedString new];
    [str appendAttributedString: [[NSAttributedString alloc] initWithString: priceString attributes: @{NSFontAttributeName: pointFont,
                                                                                                       NSForegroundColorAttributeName: textColor}]];
    [str appendAttributedString: [[NSAttributedString alloc] initWithString: [NSString stringWithFormat:@" %@", currency_abbreviation]
                                                                 attributes: @{NSFontAttributeName: currencyFont,
                                                                               NSForegroundColorAttributeName: textColor}]];
    
    return str;
}

// CoreGraphics
+ (void)drawLinearGradient:(CGContextRef)context rect:(CGRect)rect startColor:(CGColorRef)startColor endColor:(CGColorRef)endColor
{
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGFloat locations[] = { 0.0, 1.0 };

	NSArray *colors = @[(__bridge id) startColor, (__bridge id) endColor];

	CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (__bridge CFArrayRef) colors, locations);

	CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
	CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));

	CGContextSaveGState(context);
	CGContextAddRect(context, rect);
	CGContextClip(context);
	CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
	CGContextRestoreGState(context);

	CGGradientRelease(gradient);
	CGColorSpaceRelease(colorSpace);
}

+ (UIImage *)gradientBgImageWithHeight:(CGFloat) height topColor:(UIColor *)topColor bottomColor:(UIColor *)bottomColor {
	UIGraphicsBeginImageContextWithOptions(CGSizeMake(1.f, height), NO, 0.f);

	[LTTools drawLinearGradient:UIGraphicsGetCurrentContext()
						 rect:CGRectMake(0.f, 0.f, 1.f, height)
				   startColor:topColor.CGColor
					 endColor:bottomColor.CGColor];

	UIImage *bgImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return bgImage;
}

+ (UIImage *)applyBlurToImage:(UIImage*)image withRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage {
    // Check pre-conditions.
    if (image.size.width < 1 || image.size.height < 1) {
        return nil;
    }
    if (!image.CGImage) {
        return nil;
    }
    if (maskImage && !maskImage.CGImage) {
        return nil;
    }
    
    CGRect imageRect = { CGPointZero, image.size };
    UIImage *effectImage = image;
    
    BOOL hasBlur = blurRadius > __FLT_EPSILON__;
    BOOL hasSaturationChange = fabs(saturationDeltaFactor - 1.) > __FLT_EPSILON__;
    if (hasBlur || hasSaturationChange) {
        UIGraphicsBeginImageContextWithOptions(image.size, NO, [[UIScreen mainScreen] scale]);
        CGContextRef effectInContext = UIGraphicsGetCurrentContext();
        CGContextScaleCTM(effectInContext, 1.0, -1.0);
        CGContextTranslateCTM(effectInContext, 0, -image.size.height);
        CGContextDrawImage(effectInContext, imageRect, image.CGImage);
        
        vImage_Buffer effectInBuffer;
        effectInBuffer.data     = CGBitmapContextGetData(effectInContext);
        effectInBuffer.width    = CGBitmapContextGetWidth(effectInContext);
        effectInBuffer.height   = CGBitmapContextGetHeight(effectInContext);
        effectInBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectInContext);
        
        UIGraphicsBeginImageContextWithOptions(image.size, NO, [[UIScreen mainScreen] scale]);
        CGContextRef effectOutContext = UIGraphicsGetCurrentContext();
        vImage_Buffer effectOutBuffer;
        effectOutBuffer.data     = CGBitmapContextGetData(effectOutContext);
        effectOutBuffer.width    = CGBitmapContextGetWidth(effectOutContext);
        effectOutBuffer.height   = CGBitmapContextGetHeight(effectOutContext);
        effectOutBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectOutContext);
        
        if (hasBlur) {
            // A description of how to compute the box kernel width from the Gaussian
            // radius (aka standard deviation) appears in the SVG spec:
            // http://www.w3.org/TR/SVG/filters.html#feGaussianBlurElement
            //
            // For larger values of 's' (s >= 2.0), an approximation can be used: Three
            // successive box-blurs build a piece-wise quadratic convolution kernel, which
            // approximates the Gaussian kernel to within roughly 3%.
            //
            // let d = floor(s * 3*sqrt(2*pi)/4 + 0.5)
            //
            // ... if d is odd, use three box-blurs of size 'd', centered on the output pixel.
            //
            CGFloat inputRadius = blurRadius * [[UIScreen mainScreen] scale];
            uint32_t radius = floor(inputRadius * 3. * sqrt(2 * M_PI) / 4 + 0.5);
            if (radius % 2 != 1) {
                radius += 1; // force radius to be odd so that the three box-blur methodology works.
            }
            vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
            vImageBoxConvolve_ARGB8888(&effectOutBuffer, &effectInBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
            vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
        }
        BOOL effectImageBuffersAreSwapped = NO;
        if (hasSaturationChange) {
            CGFloat s = saturationDeltaFactor;
            CGFloat floatingPointSaturationMatrix[] = {
                0.0722 + 0.9278 * s,  0.0722 - 0.0722 * s,  0.0722 - 0.0722 * s,  0,
                0.7152 - 0.7152 * s,  0.7152 + 0.2848 * s,  0.7152 - 0.7152 * s,  0,
                0.2126 - 0.2126 * s,  0.2126 - 0.2126 * s,  0.2126 + 0.7873 * s,  0,
                0,                    0,                    0,  1,
            };
            const int32_t divisor = 256;
            NSUInteger matrixSize = sizeof(floatingPointSaturationMatrix)/sizeof(floatingPointSaturationMatrix[0]);
            int16_t saturationMatrix[matrixSize];
            for (NSUInteger i = 0; i < matrixSize; ++i) {
                saturationMatrix[i] = (int16_t)roundf(floatingPointSaturationMatrix[i] * divisor);
            }
            if (hasBlur) {
                vImageMatrixMultiply_ARGB8888(&effectOutBuffer, &effectInBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
                effectImageBuffersAreSwapped = YES;
            }
            else {
                vImageMatrixMultiply_ARGB8888(&effectInBuffer, &effectOutBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
            }
        }
        if (!effectImageBuffersAreSwapped)
            effectImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (effectImageBuffersAreSwapped)
            effectImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    // Set up output context.
    UIGraphicsBeginImageContextWithOptions(image.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -image.size.height);
    
    // Draw base image.
    CGContextDrawImage(outputContext, imageRect, image.CGImage);
    
    // Draw effect image.
    if (hasBlur) {
        CGContextSaveGState(outputContext);
        if (maskImage) {
            CGContextClipToMask(outputContext, imageRect, maskImage.CGImage);
        }
        CGContextDrawImage(outputContext, imageRect, effectImage.CGImage);
        CGContextRestoreGState(outputContext);
    }
    
    // Add in color tint.
    if (tintColor) {
        CGContextSaveGState(outputContext);
        CGContextSetFillColorWithColor(outputContext, tintColor.CGColor);
        CGContextFillRect(outputContext, imageRect);
        CGContextRestoreGState(outputContext);
    }
    
    // Output image is ready.
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outputImage;
}


@end
