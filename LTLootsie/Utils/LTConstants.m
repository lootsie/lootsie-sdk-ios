//  LTConstants.m
//  Created by Fabio Teles on 7/7/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTConstants.h"

@implementation LTConstants

///////////////////////////////////////////
// SERVER PATHS AND KEYS
///////////////////////////////////////////

NSString * const LTBaseStagingAPIPath                       = @"https://rehearsal-api.lootsie.com/";
NSString * const LTBaseLiveAPIPath                          = @"https://api.lootsie.com/";

///////////////////////////////////////////
// EVENTS STUFF
///////////////////////////////////////////

NSUInteger const LTEventsPackageSize                        = 2048;     //bytes
CGFloat const LTEventsTimerInterval                         = 60;       //seconds (1 mimnute)
CGFloat const LTEventsEngagementInterval                    = 300;      //seconds (5 minutes)

///////////////////////////////////////////
// GLOBAL STUFF
///////////////////////////////////////////

NSString * const LTPlatform                                 = @"iOS";
NSString * const LTAPIVersion                               = @"v3";
NSString * const LTSDKVersion                               = @"5.2.201608301110";

///////////////////////////////////////////
// KEY VALUE CODING
///////////////////////////////////////////

NSInteger const LTDataStorageVersion                        = 1;
NSString * const LTDataStorageVersionKey                    = @"version";

NSString * const LTDatabaseStorageFolder                    = @"com.lootsie";
NSString * const LTDatabaseStorageFilename                  = @"database.lootsie";
NSString * const LTDatabaseStorageOldFolder                 = @"Private Documents";
NSString * const LTDatabaseStorageOldFilename               = @"data.plist";
NSString * const LTDatabaseStorageOldClassName              = @"LootsieData";

///////////////////////////////////////////
// JSON API KEYS
///////////////////////////////////////////

NSString * const LTAPIDateOnlyFormat                        = @"yyyy-MM-dd";

NSString * const LTPointsKey                                = @"lp";
NSString * const LTTotalPointsKey                           = @"total_lp";

NSString * const LTAchievedPointsKey                        = @"achieved_lp";
NSString * const LTAchievementsKey							= @"achievements";

NSString * const LTRewardIdsKey                             = @"reward";
NSString * const LTRewardRedemptionIdKey                    = @"reward_redemption_id";

NSString * const LTErrorErrorsKey                           = @"errors";
NSString * const LTErrorFieldKey                            = @"field";
NSString * const LTErrorMessageKey                          = @"message";

@end
