//  LTTools.h
//  Created by Fabio Teles on 7/30/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <UIKit/UIKit.h>

/*! @class LTTools
 *  @brief Special class for add some tools methods.*/
@interface LTTools : NSObject

/*! @brief Get name of currnet device.*/
+ (NSString*)deviceName;

/*! @brief YES if current device is iPhone 5, iPhone 5S, iPhone 5C*/
+ (BOOL)isIphoneFive;

/*! @brief YES if email string is valid.
 *  @param email Email string for check.*/
+ (BOOL)validateEmailWithString:(NSString*)email;

/*! @brief Create UIColor object from hex value.
 *  @param hex Hex value of color.*/
+ (UIColor*)colorWithHex: (NSUInteger)hex;

/*! @brief Generate date formater wirh format.
 *  @param dateFormat Format of date.*/
+ (NSDateFormatter *)dateFormatterWithDateFormat:(NSString *)dateFormat;

/*! @brief Generate points string for point.
 *  @param points Number of points.*/
+ (NSAttributedString *)pointsStringValue:(NSInteger)points;

/*! @brief Generate points string for point.
 *  @param points Number of points.
 *  @param pointFont Font for points value.
 *  @param currencyFont Font for currency of points.*/
+ (NSAttributedString *)pointsStringValue:(NSInteger)points withPointFont:(UIFont *)pointFont currencyFont:(UIFont *)currencyFont;

/*! @brief Generate image with gradient colors.
 *  @param height Height of gradient.
 *  @param topColor First value of color.
 *  @param bottomColor Last value of color.*/
+ (UIImage *)gradientBgImageWithHeight:(CGFloat) height topColor:(UIColor *)topColor bottomColor:(UIColor *)bottomColor;

/*! @brief Apply blur effect for image.
 *  @param image Source image. This image will use for apply blur.
 *  @param blurRadius Radius of blur.
 *  @param tintColor Blur tint color.
 *  @param saturationDeltaFactor Saturation delta factor.
 *  @param maskImage Image for set mask Optional.*/
+ (UIImage *)applyBlurToImage:(UIImage*)image withRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;

@end