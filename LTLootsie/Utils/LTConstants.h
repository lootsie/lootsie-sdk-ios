//  LTConstants.h
//  Created by Fabio Teles on 7/7/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>
@import UIKit;

/*! @class LTConstants
 *  @brief Global Lootsie SDK constants.*/
@interface LTConstants : NSObject

///////////////////////////////////////////
// SERVER PATHS AND KEYS
///////////////////////////////////////////
/*! Link for staging server API.*/
extern NSString * const LTBaseStagingAPIPath;

/*! Link for real server API.*/
extern NSString * const LTBaseLiveAPIPath;


///////////////////////////////////////////
// EVENTS STUFF
///////////////////////////////////////////
/*! Size of package which will send to server. Default value 2048 bytes.*/
extern NSUInteger const LTEventsPackageSize;

/*! Time interval for send event to server. Default value 60 seconds*/
extern CGFloat const LTEventsTimerInterval;

/*! Time interfal for send engagement event to server. Defaul (5 * 60) 5 minutes.*/
extern CGFloat const LTEventsEngagementInterval;


///////////////////////////////////////////
// GLOBAL STUFF
///////////////////////////////////////////
/*! Platform name.*/
extern NSString * const LTPlatform;

/*! Version of API.*/
extern NSString * const LTAPIVersion;

/*! Version of SDK.*/
extern NSString * const LTSDKVersion;


///////////////////////////////////////////
// KEY VALUE CODING
///////////////////////////////////////////
/*! Version of data storage structure.*/
extern NSInteger const LTDataStorageVersion;

/*! Name of key for save version data.*/
extern NSString * const LTDataStorageVersionKey;


/*! Folder where will be save all data.*/
extern NSString * const LTDatabaseStorageFolder;

/*! Database file name.*/
extern NSString * const LTDatabaseStorageFilename;

/*! Name of old database folder.*/
extern NSString * const LTDatabaseStorageOldFolder;

/*! Name of old database file name*/
extern NSString * const LTDatabaseStorageOldFilename;

/*! Name of old database class name.*/
extern NSString * const LTDatabaseStorageOldClassName;


///////////////////////////////////////////
// JSON API KEYS
///////////////////////////////////////////
/*! Format of date from API.*/
extern NSString * const LTAPIDateOnlyFormat;

/*! Name of points key name.*/
extern NSString * const LTPointsKey;

/*! Total points key name*/
extern NSString * const LTTotalPointsKey;

/*! Achieved points ket name.*/
extern NSString * const LTAchievedPointsKey;

/*! Achievement key name.*/
extern NSString * const LTAchievementsKey;

/*! Rewards ids key name.*/
extern NSString * const LTRewardIdsKey;

/*! Reward redemption key name.*/
extern NSString * const LTRewardRedemptionIdKey;

/*! Error list key name.*/
extern NSString * const LTErrorErrorsKey;

/*! Error feild key name.*/
extern NSString * const LTErrorFieldKey;

/*! Error message key name.*/
extern NSString * const LTErrorMessageKey;

@end
