//  LTUserRewardRedemptionOperation.m
//  Created by Fabio Teles on 7/28/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTUserRewardRedemptionOperation.h"
#import "LTNetworkManager.h"
#import "LTUserRewardRedemption.h"

#import "LTAFURLResponseSerialization.h"

@interface LTUserRewardRedemptionOperation ()
/*! Redemption info object.*/
@property (nonatomic, readwrite, strong) LTUserRewardRedemption *redemption;

@end


@implementation LTUserRewardRedemptionOperation

+ (instancetype)POSTWithRewardId:(NSInteger)rewardId  cost: (NSInteger) points {
    
    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        NSDictionary *additionalHeaders = [self header];
        
        NSDictionary *parameters = @{LTRewardIdsKey: @(rewardId) , @"points": @(points)  }; // The API requires that we pass a list here.
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"POST"
                                                                   path:LTAPIUserRewardRedemptionsPath
                                                             parameters:parameters
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    };
    
    return [[self alloc] initWithRequestBuilder:requestBuilder];
}

- (NSError *)dataTaskDidFinishSuccessfully {
    return nil;
}

- (NSError *)formatDataTaskError:(NSError *)error {
    NSError *baseError = [super formatDataTaskError:error];
    
    NSMutableDictionary *mutableInfo;
    NSHTTPURLResponse *response = [baseError.userInfo objectForKeyNilSafe:LTAFNetworkingOperationFailingURLResponseErrorKey];
    if (response) {
        switch (response.statusCode) {
            case 402:
                mutableInfo = [baseError.userInfo mutableCopy];
                mutableInfo[NSLocalizedDescriptionKey] = NSLocalizedStringFromTable(@"Uh oh!", @"LTLootsie", nil);
                mutableInfo[NSLocalizedRecoverySuggestionErrorKey] = NSLocalizedStringFromTable(@"Looks like you don't have enough points for this one.", @"LTLootsie", nil);
                break;
            case 409:
            {
                NSDictionary *responseErrorDic = [[baseError.userInfo objectForKeyNilSafe:LTNetworkResponseErrorsJSONKey] firstObject];
                if (responseErrorDic && [responseErrorDic[LTErrorMessageKey] rangeOfString:@"LP"].location != NSNotFound) {
                    mutableInfo = [baseError.userInfo mutableCopy];
                    mutableInfo[NSLocalizedDescriptionKey] = NSLocalizedStringFromTable(@"You're too good!", @"LTLootsie", nil);
                    mutableInfo[NSLocalizedRecoverySuggestionErrorKey] = NSLocalizedStringFromTable(@"Looks like you've hit the daily app max. Dont worry, you'll keep earning more tomorrow!", @"LTLootsie", nil);
                } else {
                    mutableInfo = [baseError.userInfo mutableCopy];
                    mutableInfo[NSLocalizedDescriptionKey] = NSLocalizedStringFromTable(@"Uh oh!", @"LTLootsie", nil);
                    mutableInfo[NSLocalizedRecoverySuggestionErrorKey] = NSLocalizedStringFromTable(@"Redemption limit reached.", @"LTLootsie", nil);
                }
                break;
            }
        }
    }
    return (mutableInfo ? [NSError errorWithDomain:error.domain code:error.code userInfo:mutableInfo] : baseError);
}

@end
