//
//  LTCatalogRewardsList.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/25/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTCatalogRewardsList.h"
#import "LTCatalogReward.h"
#import "LTManager.h"

@interface LTCatalogRewardsList ()
/*! @brief Find reward by unique id in array.
 *  @param rewardId Unique id of reward.
 *  @param list List of rewards. This object can be like rewards list or fetured rewards list.
 *  @return Reward object of nil if reward doesn't find.*/
- (LTCatalogReward*)rewardWithId:(NSInteger)rewardId fromList:(NSArray*)list;

/*! @brief Get list of favorite list of rewards from array.
 *  @param array List of all rewards. This object can be rewards or featured list.*/
- (NSArray*)getFavoriteRewardsList:(NSArray*)array;

/*! @brief Update data without post notification.
 *  @return YES if don't need to send notification.*/
- (BOOL)updateDataWithoutNotification;

/*! @brief Update data.*/
- (void)updateData;

@end


@implementation LTCatalogRewardsList

#pragma mark - Private methods

- (LTCatalogReward*)rewardWithId:(NSInteger)rewardId fromList:(NSArray*)list {
    LTCatalogReward* reward = nil;
    if(rewardId > 0 && list.count > 0) {
        for(LTCatalogReward* item in list) {
            if(item.rewardId == rewardId) {
                reward = item;
                break;
            }
        }
    }
    
    return reward;
}

- (NSArray*)getFavoriteRewardsList:(NSArray*)array {
    NSArray* res = nil;
    if(array.count > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat: @"isFavorite == YES"];
        res = [array filteredArrayUsingPredicate: predicate];
    }
    return res;
}


#pragma mark - update data

- (BOOL)updateDataWithoutNotification {
    NSMutableArray* res = [NSMutableArray arrayWithArray: [self getFavoriteRewardsList: _rewards]];
    [res addObjectsFromArray: [self getFavoriteRewardsList: _featuredRewards]];
    NSArray *array = [NSArray arrayWithArray: res];

    if (![_favoriteRewards isEqualToArray:array] ) {
        _favoriteRewards = array;
        return YES;
    }
    return NO;
}

- (void)updateData {
    if ([self updateDataWithoutNotification]) {
        [[NSNotificationCenter defaultCenter] postNotificationName:LTChangeFavoriteRewardNotification object:nil];
    }
}

#pragma mark - 

- (void)populateWithDictionary:(NSDictionary *)dictionary {
    _featuredRewards = [LTCatalogReward collectionWithArray:dictionary[@"featured"]];
    _rewards = [LTCatalogReward collectionWithArray:dictionary[@"rewards"]];
    [self updateDataWithoutNotification];
}


#pragma mark - Getters

- (LTCatalogReward*)rewardWithId:(NSInteger)rewardId {
    LTCatalogReward* reward = nil;
    
    if(rewardId > 0) {
        reward = [self rewardWithId: rewardId fromList: _rewards];
        if(!reward) {
            reward = [self rewardWithId: rewardId fromList: _featuredRewards];
        }
    }
    
    return reward;
}

/*
 Helper to update the reward value for an existing reward.
 */
//- (void) updateRewardPointsForRewardId: (NSInteger)rewardId andPoints: (NSInteger) points {
//
//}

@end
