//
//  LTCatalogReward.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/17/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTCatalogReward.h"
#import "LTManager.h"
#import "LTData.h"
#import "LTApp.h"

@implementation LTCatalogReward

/*! @brief Create new object.*/
- (instancetype)init {
    if (self = [super init]) {
        self.type = LTUnknowRewardType;
    }
    return self;
}

- (BOOL)isEqualToReward:(LTCatalogReward *)reward {
    if (!reward) {
        return NO;
    }
    
    BOOL haveEqualIDs = (!self.rewardId && !reward.rewardId) || (self.rewardId  == reward.rewardId);
    
    return haveEqualIDs;
}

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }
    
    if (![object isKindOfClass:[LTCatalogReward class]]) {
        return NO;
    }
    
    return [self isEqualToReward:(LTCatalogReward *)object];
}

- (void)populateWithDictionary:(NSDictionary *)dictionary {
    self.isHideEarnInFast = YES;
    [super populateWithDictionary:dictionary];
    self.rewardId = [dictionary[@"id"] integerValue];
    self.name = dictionary[@"title"];
    self.desc = dictionary[@"description"];
    self.points = [dictionary[@"cost"] integerValue];
    self.isFavorite = [dictionary[@"favorite"] boolValue];
    self.instantID = [dictionary[@"instant_type"] integerValue];
    self.type = [dictionary[@"type"] integerValue];
    self.banners = [self arrayURLFromDictonary:dictionary[@"banners"]];
    self.thumbnails = [self arrayURLFromDictonary:dictionary[@"thumbnails"]];
}

/*! @brief Convert JSON structure to list of urls.
 *  @param dict JSON structure.
 *  @return List of urls.*/
- (NSArray<NSURL *> *)arrayURLFromDictonary:(NSDictionary *)dict {
    NSMutableArray<NSURL *> *array = [NSMutableArray array];
    for (NSString *urlBanners in dict) {
        [array addObject:[NSURL URLWithString:urlBanners]];
    }
    return array;
}

- (BOOL)refreshEnabled {
    return [LTManager sharedManager].data.app.refreshEnabled;
}

@end
