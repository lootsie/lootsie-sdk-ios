//
//  LTCatalogRewardsList.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 5/25/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LTModelEntity.h"

@class LTCatalogReward;
/*! @class LTCatalogRewardsList
 *  @brief Object for saving list of rewards.*/
@interface LTCatalogRewardsList : LTModelEntity

/*! List of rewards.*/
@property (nonatomic, strong, readonly)   NSArray<LTCatalogReward*> *rewards;

/*! List of featured rewards.*/
@property (nonatomic, strong, readonly)   NSArray<LTCatalogReward*> *featuredRewards;

/*! List of favorites rewards.*/
@property (nonatomic, strong, readonly)   NSArray<LTCatalogReward*> *favoriteRewards;


/*! @brief Update data in list.*/
- (void)updateData;

/*! @brief Get reward object by id.
 *  @param rewardId Unique id of reward.
 *  @return Reward object.*/
- (LTCatalogReward *)rewardWithId:(NSInteger)rewardId;


/*! @brief Updates the points in an existing reward.
 *  @param rewardId Unique id of reward.
 *  @param points Number of points for this reward
 */
// - (void) updateRewardPointsForRewardId: (NSInteger)rewardId andPoints: (NSInteger) points;

@end
