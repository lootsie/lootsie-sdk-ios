//  LTManager+Catalog.h
//  Created by Alexander Dovbnya on 3/29/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTManager.h"
#import "LTCatalogReward.h"
@class LTCatalogRewardsList;
/*! @protocol LTCatalogManagerProtocol
 *  @brief Methods for work with rewards object. Get list of rewards and like/unlike reward.*/
@protocol LTCatalogManagerProtocol <NSObject>


/*
 - (void)updateRewardWithId:(NSInteger)rewardId
 success:(void (^)(LTCatalogReward *reward))success
 failure:(void (^)(NSError *))failure;
 */

/*! @brief Method that is called when user wants to get reward.
 *  @param rewardId  - ID of reward user wants to get.
 *  @param points     the cost of the reward
 *  @param success  Callback about successful reward getting.
 *  @param failure  Callback about unsuccessful reward getting with failure reason.*/
- (void)redeemRewardWithId:(NSInteger)rewardId
                      cost: (NSInteger) points
                   success:(void (^)(void))success
                   failure:(void (^)(NSError *))failure;

/*! @brief Method is called to get rewards list - uses the cached version.
 *  @param success Callback that returns rewards list.
 *  @param failure Callback about failure to get rewards list.*/
- (void)rewardsWithSuccess:(void (^)(LTCatalogRewardsList *rewards))success
                   failure:(void (^)(NSError *error))failure;

/*! @brief Method is called to get rewards list.
 *  @param success Callback that returns rewards list.
 *  @param failure Callback about failure to get rewards list.*/
- (void)forceRewardsWithSuccess:(void (^)(LTCatalogRewardsList *rewards))success
                        failure:(void (^)(NSError *error))failure;


/*! @brief Method is called to get the reward. This updates the catalog as well.
 *  @param rewardID Unique id of reward for update state.
 *  @param success Callback that returns rewards list.
 *  @param failure Callback about failure to get rewards list.*/
- (void)rewardWithForReward:(NSInteger)rewardID
                    success:(void (^)(LTCatalogRewardsList *rewards))success
                    failure:(void (^)(NSError *error))failure;


/*! @brief Method is called to change favorite state of rewards.
 *  @param rewardID Unique id of reward for update state.
 *  @param success Callback that calls after reward will update state.
 *  @param failure Callback about failure to get rewards list.*/
- (void)changeFavoriteStateForReward:(NSInteger)rewardID
                             success:(void (^)(void))success
                             failure:(void (^)(NSError *error))failure;
@end


/*! @category LTManager(Catalog)
 *  @brief Methods and properties for work with rewards.*/
@interface LTManager (Catalog) <LTCatalogManagerProtocol>

@end
