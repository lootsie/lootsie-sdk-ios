//  LTManager+Catalog.m
//  Created by Alexander Dovbnya on 3/29/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTManager.h"
#import "LTManager+Catalog.h"
#import "LTManager+Private.h"

#import "LTPostNotificationOperation.h"
#import "LTRedemptionSequenceOperation.h"
#import "LTBlockOperation.h"
#import "LTRefreshRewardsSequenceOperation.h"
#import "LTRefreshRewardSequenceOperation.h"

#import "LTFavoriteSequenceOperation.h"

#import "LTCatalogRewardsList.h"
#import "LTCatalogReward.h"
#import "LTData.h"


@implementation LTManager (Catalog)


- (void)redeemRewardWithId:(NSInteger)rewardId
                      cost: (NSInteger) points
                   success:(void (^)(void))success
                   failure:(void (^)(NSError *))failure {
    // Reward sequence
    LTRedemptionSequenceOperation *redemptionOp = [LTRedemptionSequenceOperation sequenceWithRewardId:rewardId
                                                                                                 cost: points];
    [redemptionOp setSuccess:success failure:failure];
    
    // After redemptiom sequence operation
    __block NSDictionary *blockUserInfo;
    LTBlockOperation *afterRedemption = [[LTBlockOperation alloc] initWithBlock:^(LTOperationCompletionBlock completion) {
        LTCatalogReward *reward = [[LTManager sharedManager].data.catalog rewardWithId:rewardId];
        blockUserInfo = @{LTRewardUserInfoKey: reward};
        if (completion != nil) {
            completion(nil);
        }
    }];
    [afterRedemption setSafeName:@"com.lootsie.manager.afterRedemption"];
    
    // Post notification operation afterwards
    LTPostNotificationOperation *noteOp = [[LTPostNotificationOperation alloc] initWithNotificationName:LTRewardRedeemedNotification object:self userInfo:nil];
    
    // Queueing
    [self addGeneralOperation:noteOp dependencies:@[afterRedemption] allowCancelledDependencies:NO];
    [self addGeneralOperation:afterRedemption dependencies:@[redemptionOp] allowCancelledDependencies:NO];
    [self addStartPendingNetworkOperation:redemptionOp];
    
}

- (void)rewardsWithSuccess:(void (^)(LTCatalogRewardsList *))success
                   failure:(void (^)(NSError *))failure {
    
    if (self.data.catalog) {
        if (success) success(self.data.catalog);
    } else {
        
        // define the success handler that will be used in both scenarions;
        void (^successHandler)(void) = nil;
        if (success) {
            __weak __typeof(self) weakSelf = self;
            successHandler = ^{
                success(weakSelf.data.catalog);
            };
        }
        
        LTRefreshRewardsSequenceOperation *refreshOp = [LTRefreshRewardsSequenceOperation sequence];
        [refreshOp setSuccess:successHandler failure:failure];
        [self addStartPendingNetworkOperation:refreshOp];
    }
}



- (void)forceRewardsWithSuccess:(void (^)(LTCatalogRewardsList *))success
                        failure:(void (^)(NSError *))failure {
    
    // define the success handler that will be used in both scenarions;
    void (^successHandler)(void) = nil;
    if (success) {
        __weak __typeof(self) weakSelf = self;
        successHandler = ^{
            success(weakSelf.data.catalog);
        };
    }
    
    LTRefreshRewardsSequenceOperation *refreshOp = [LTRefreshRewardsSequenceOperation sequence];
    [refreshOp setSuccess:successHandler failure:failure];
    [self addStartPendingNetworkOperation:refreshOp];
}



- (void)changeFavoriteStateForReward:(NSInteger)rewardID
                             success:(void (^)(void))success
                             failure:(void (^)(NSError *error))failure {
    LTFavoriteSequenceOperation *op = [LTFavoriteSequenceOperation sequenceWithRewardId:rewardID];
    [op setSuccess:success failure:failure];
    [self addStartPendingNetworkOperation:op];
}

- (void)rewardWithForReward: (NSInteger)rewardID
                    success: (void (^)(LTCatalogRewardsList *))success
                    failure: (void (^)(NSError *))failure {
    
    
    // define the success handler that will be used in both scenarions;
    void (^successHandler)(void) = nil;
    if (success) {
        __weak __typeof(self) weakSelf = self;
        successHandler = ^{
            success(weakSelf.data.catalog);
        };
    }
    
    LTRefreshRewardSequenceOperation *refreshOp = [LTRefreshRewardSequenceOperation sequenceWithRewardId: rewardID];
    [refreshOp setSuccess:successHandler failure:failure];
    [self addStartPendingNetworkOperation:refreshOp];
}


@end
