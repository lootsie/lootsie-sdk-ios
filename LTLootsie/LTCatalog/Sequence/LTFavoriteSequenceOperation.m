//
//  LTFavoriteSequenceOperation.m
//  LTLootsie iOS Example
//
//  Created by Macmini on 7/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTFavoriteSequenceOperation.h"
#import "LTFavoriteRewardOperation.h"
#import "LTBlockOperation.h"

#import "LTCatalogReward.h"
#import "LTCatalogRewardsList.h"
#import "LTData.h"
#import "LTManager.h"


@implementation LTFavoriteSequenceOperation

+ (instancetype)sequenceWithRewardId:(NSInteger)rewardId {
    LTCatalogReward *reward = [[LTManager sharedManager].data.catalog rewardWithId:rewardId];
    
    LTFavoriteRewardOperation *favoriteOp = nil;
    BOOL favoriteValue;
    if (reward.isFavorite) {
        favoriteOp = [LTFavoriteRewardOperation unFavoriteRewardWithID:rewardId];
        favoriteValue = NO;
    }
    else {
        favoriteOp = [LTFavoriteRewardOperation favoriteRewardWithID:rewardId];
        favoriteValue = YES;
    }
    
    LTBlockOperation *updateOp = [[LTBlockOperation alloc] initWithBlock:^(LTOperationCompletionBlock completion) {
        reward.isFavorite = favoriteValue;
        if (completion != nil) {
            completion(nil);
        }
    }];
    
    return [super groupWithOperations:@[favoriteOp, updateOp] maxConcurrentOperationCount:1];
}

@end
