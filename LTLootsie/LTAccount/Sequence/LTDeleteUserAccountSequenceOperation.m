//
//  LTDeleteUserAccountSequenceOperation.m
//  LTLootsie iOS Example
//
//  Created by Courtney Osborne on 6/2/17.
//  Copyright © 2017 Lootsie. All rights reserved.

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.


#import "LTDeleteUserAccountSequenceOperation.h"

#import "LTUserAccountOperation.h"
#import "LTBlockOperation.h"

#import "LTUserAccount.h"
#import "LTManager.h"
#import "LTData.h"

@implementation LTDeleteUserAccountSequenceOperation

+ (instancetype)sequenceDeleteWithUserAccount:(LTUserAccount *)userAccount {
    
        
        LTUserAccountOperation *updateUserAccountOp = [LTUserAccountOperation DELETEWithUserAccount:userAccount];
        
        LTBlockOperation *updateLocalOp = [LTBlockOperation autoSuccessOpWithBlock:^{
//            [LTManager sharedManager].data.user = userAccount;
        }];
        
        [updateLocalOp setSafeName:@"com.lootsie.sequence.updateUserAccount.updateData"];
        [updateLocalOp addDependency:updateUserAccountOp];
        
        return [self groupWithOperations:@[updateUserAccountOp, updateLocalOp]];
}

@end
