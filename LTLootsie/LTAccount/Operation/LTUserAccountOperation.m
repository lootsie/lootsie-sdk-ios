//  LTUserAccountOperation.m
//  Created by Fabio Teles on 7/27/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTUserAccountOperation.h"
#import "LTNetworkManager.h"
#import "LTUserAccount.h"

@interface LTUserAccountOperation ()
/*! User's account inforamtion object.*/
@property (nonatomic, readwrite, strong) LTUserAccount *userAccount;

@end


@implementation LTUserAccountOperation

+ (instancetype)GET {
    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        NSDictionary *additionalHeaders = [self header];
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"GET"
                                                                   path:LTAPIUserAccountPath
                                                             parameters:nil
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    };
    
    return [[self alloc] initWithRequestBuilder:requestBuilder];
}

+ (instancetype)PUTWithUserAccount:(LTUserAccount *)userAccount {
    NSParameterAssert(userAccount);
    
    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        NSDictionary *additionalHeaders = [self header];
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"PUT"
                                                                   path:LTAPIUserAccountPath
                                                             parameters:[userAccount putDictionary]
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    };
    
    return [[self alloc] initWithRequestBuilder:requestBuilder];
}

+ (instancetype)PUTWithEmail:(NSString *)email {
    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        NSDictionary *additionalHeaders = [self header];
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"PUT"
                                                                   path:LTAPIUserAccountPath
                                                             parameters:@{@"email" : email}
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    };
    
    return [[self alloc] initWithRequestBuilder:requestBuilder];
}


+ (instancetype)DELETEWithUserAccount:(LTUserAccount *)userAccount {
    
    LTRequestBuilder requestBuilder = ^NSURLRequest *(NSError *__autoreleasing *error) {
        
        NSDictionary *additionalHeaders = [self header];
        
        return [[LTNetworkManager lootsieManager] requestWithHTTPMethod:@"DELETE"
                                                                   path:LTAPIUserAccountPath
                                                             parameters:nil
                                                  additionalHTTPHeaders:additionalHeaders
                                                                  error:error];
    
    };


    return [[self alloc] initWithRequestBuilder:requestBuilder];
    
}


- (NSError *)dataTaskDidFinishSuccessfully {
    NSError *error = [super dataTaskDidFinishSuccessfully];
    self.userAccount = [LTUserAccount entityWithDictionary:self.responseObject[@"user"]];
    return error;
}


@end
