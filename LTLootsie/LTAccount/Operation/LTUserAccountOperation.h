//  LTUserAccountOperation.h
//  Created by Fabio Teles on 7/27/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTDataTaskOperation.h"

@class LTUserAccount;
/*! @class LTUserAccountOperation
 *  @brief This operation is used for getting info about user and sending updated data to server.*/
@interface LTUserAccountOperation : LTDataTaskOperation

/*! User's account inforamtion object.*/
@property (nonatomic, readonly, strong) LTUserAccount *userAccount;


/*! @brief Send request for get informationa about user.*/
+ (instancetype)GET;

/*! @brief Update user account with user's info object.
 *  @param userAccount New user's account info object. This object has a new data of user account.*/
+ (instancetype)PUTWithUserAccount:(LTUserAccount *)userAccount;

/*! @brief Update email for user account.
 *  @param email New user's email.*/
+ (instancetype)PUTWithEmail:(NSString *)email;

/*! @brief Delete user account.
 * @param userAccount will be blacklisted  */
+ (instancetype)DELETEWithUserAccount:(LTUserAccount *)userAccount;



@end
