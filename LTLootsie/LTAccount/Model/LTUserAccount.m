//  LTUserAccount.m
//  Created by Fabio Teles on 7/7/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTConstants.h"
#import "LTUserAccount.h"
#import "LTRecordAchievements.h"
#import "LTTools.h"

//JSON keys name.
NSString * const LTEmailKey									= @"email";
NSString * const LTConfirmedlKey							= @"confirmed";
NSString * const LTUserPointsKey							= @"points";
NSString * const LTUserAchievementsKey						= @"achievements";



@implementation LTUserAccount

- (void)modifyWithDictionary:(NSDictionary *)dictionary {
    if ([dictionary objectForKey:LTEmailKey]) {
        self.email = dictionary[LTEmailKey];
    }
    if (dictionary[LTConfirmedlKey]) {
        self.confirmed = [dictionary[LTConfirmedlKey] boolValue];
    }
    if (dictionary[LTUserPointsKey]) {
        self.points = [dictionary[LTUserPointsKey] integerValue];
    }
    if (dictionary[LTUserAchievementsKey]) {
        
        if ([self.achievements count] == 0) {
            self.achievements = [LTRecordAchievements collectionWithArray:dictionary[LTUserAchievementsKey]];
        } else {
            NSMutableArray *buffer = [[NSMutableArray alloc]init];
            
            for (LTRecordAchievements* newAchievement in [LTRecordAchievements collectionWithArray:dictionary[LTUserAchievementsKey]]) {
                
                NSIndexSet *indices = [self.achievements indexesOfObjectsPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
                    return [((LTRecordAchievements* )obj).date isEqualToDate:newAchievement.date];
                }];
                if (indices.count == 0) {
                    [buffer addObject:newAchievement];
                }
            }
            
            [buffer addObjectsFromArray:self.achievements];
            
            self.achievements = buffer;
        }
    }
}
- (void)populateWithDictionary:(NSDictionary *)dictionary
{
    if ([dictionary objectForKey:LTEmailKey]) {
        self.email = dictionary[LTEmailKey];
    }
    if (dictionary[LTConfirmedlKey]) {
        self.confirmed = [dictionary[LTConfirmedlKey] boolValue];
        
    }
    if (dictionary[LTUserPointsKey]) {
        self.points = [dictionary[LTUserPointsKey] integerValue];
    }
    if (dictionary[LTUserAchievementsKey]) {
        
        self.achievements = [LTRecordAchievements collectionWithArray:dictionary[LTUserAchievementsKey]];
    }
}

- (NSDictionary *)dictionary
{
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    dictionary[LTEmailKey] = self.email;
    dictionary[LTConfirmedlKey] = @(self.confirmed);
    dictionary[LTUserPointsKey] = @(self.points);
    NSMutableArray *array = [NSMutableArray array];
    for (LTRecordAchievements *achievement in self.achievements) {
        [array addObject:[achievement dictionary]];
    }
    dictionary[LTUserAchievementsKey] = array;
    return dictionary;
}


- (NSDictionary *)putDictionary {
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    if (self.email) dictionary[LTEmailKey]                  = self.email;
    return dictionary;
}



/*! @brief Copy object.
 *  @param zone Zone object.*/
- (id)copyWithZone:(nullable NSZone *)zone {
    return [LTUserAccount entityWithDictionary:[self dictionary]];
}

@end
