//  LTManager+Account.h
//  Created by Alexander Dovbnya on 4/6/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTManager.h"

/*! @protocol LTAccountManagerProtocol
 *  @brief Used for work with User Account data.*/
@protocol LTAccountManagerProtocol <NSObject>

/*! @brief Method is called to update user data.
 *  @param userAccount user data that will be sent to server.
 *  @param success Callback that returns success about user data update.
 *  @param failure Callback is called when user data update is failed.*/
- (void)updateUserAccountWithAccount:(LTUserAccount *)userAccount
                             success:(void (^)(void))success
                             failure:(void (^)(NSError *error))failure;

/*! @brief Method is called to get user data.
 *  @param success Callback that returns user data.
 *  @param failure Callback is called when system failed to get user data.*/
- (void)userAccountWithSuccess:(void (^)(LTUserAccount *userAccount))success
                       failure:(void (^)(NSError *error))failure;

/*! @brief Method is called to synchronize user data.
 *  @param success Callback that returns user data.
 *  @param failure Callback is called when system failed to get user data.*/
- (void)synchronizeUserAccount:(void (^)(LTUserAccount *))success
                       failure:(void (^)(NSError *error))failure;

/*! @brief Method is called to delete the user. This will blacklist user*/

-(void)deleteUserAccountWithSuccess:(LTUserAccount *)userAccount
                            success:(void (^)(void))success
                            failure:(void (^)(NSError *error))failure;

@end

/*! @category LTManager(Account)
 *  @brief Methods and properties for work with account.*/
@interface LTManager (Account) <LTAccountManagerProtocol>

@end
