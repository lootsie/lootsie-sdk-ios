//
//  LTAchievementsGroup.m
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/8/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import "LTAchievementsGroup.h"
#import "LTAchievement.h"
#import "LTTools.h"


@interface LTAchievementsGroup ()

/*! @brief Check date is today or not.
 *  @param aDate Date for checking.*/
- (BOOL)isDateToday:(NSDate*)aDate;

/*! @brief Get Begin date for achievement.
 *  @param achievement Achievement object info.
 *  @return Start date for achievement.*/
- (NSDate*)dateAtBeginningOfDayForAchievement:(LTAchievement*)achievement;

@end


@implementation LTAchievementsGroup

#pragma mark - Private methods

- (BOOL)isDateToday:(NSDate*)aDate {
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: aDate];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    return [today isEqualToDate:otherDate];
}

- (NSDate*)dateAtBeginningOfDayForAchievement:(LTAchievement*)achievement {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    NSDateComponents *dateComps = [calendar components: (NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate: achievement.date];
    
    // Set the time components manually
    [dateComps setHour: 0];
    [dateComps setMinute: 0];
    [dateComps setSecond: 0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

- (BOOL) contains: (NSInteger) achievementId {
    for(LTAchievement* item in _achievements) {
        if(item.achievementId == achievementId) {
            return YES;
        }
    }
    return NO;
}


#pragma mark - Init mathods
/*! @brief Create new object.*/
- (instancetype)init {
    self = [super init];
    if(self) {
        _achievements = nil;
        _title = nil;
        _date = nil;
    }
    return self;
}

- (instancetype)initWithItems:(NSArray<LTAchievement*> *)items title:(NSString*)title {
    self = [super init];
    if(self) {
        _title = title;
        _achievements = [NSArray arrayWithArray: items];
        if(items.count > 0) {
            _date = [self dateAtBeginningOfDayForAchievement: items[0]];
            _isTodayGroup = [self isDateToday: _date];
        }
    }
    return self;
}


#pragma mark - setters

- (void)addAchievement:(LTAchievement*)item {
    NSMutableArray* array = [NSMutableArray arrayWithArray: _achievements];
    [array addObject: item];
    _achievements = [NSArray arrayWithArray: array];
    if(_achievements.count > 0) {
        LTAchievement* achievement = _achievements[0];
        _date = [self dateAtBeginningOfDayForAchievement: achievement];
        _isTodayGroup = [self isDateToday: _date];
        if(_isTodayGroup) {
            _title = [@"Today" uppercaseString];
        } else {
            _title = [[[LTTools dateFormatterWithDateFormat: @"eee MM/dd"] stringFromDate: _date] uppercaseString];
        }
    }
}

@end



