//
//  LTAchievementsGroup.h
//  LTLootsie iOS Example
//
//  Created by Andrew Shapovalov on 7/8/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LTAchievement;
/*! @class LTAchievementsGroup
 *  @brief Achievement group object. achievements for certain date.*/
@interface LTAchievementsGroup : NSObject

/*! Title of group.*/
@property (nonatomic, strong, readonly)     NSString *title;

/*! Date of achievements.*/
@property (nonatomic, strong, readonly)     NSDate *date;

/*! List of achievements.*/
@property (nonatomic, strong, readonly)     NSArray<LTAchievement*> *achievements;

/*! YES if current group is saves achievements for today.*/
@property (nonatomic, assign, readonly)     BOOL isTodayGroup;


/*! @brief Create a new group with achievement list and title.
 *  @param items Achievements list.
 *  @param title Group title.*/
- (instancetype)initWithItems:(NSArray<LTAchievement*> *)items title:(NSString*)title;

/*! @brief Add new achievement to group.
 *  @param item New achievement object.*/
- (void)addAchievement:(LTAchievement*)item;

/*! @brief Returns true if this group contains that particular id.
 */
- (BOOL) contains: (NSInteger) achievementId;

@end
