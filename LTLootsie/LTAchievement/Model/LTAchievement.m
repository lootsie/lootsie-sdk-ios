//  LootsieAchievement.m
//  Created by Fabio Teles on 7/7/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTConstants.h"
#import "LTAchievement.h"

#import "LTManager.h"
#import "LTData.h"
#import "LTApp.h"


@implementation LTAchievement

- (instancetype)initWithRecordAchievement:(LTRecordAchievements *)achievement {
    self = [super init];
    if (self != nil) {
        self.date = achievement.date;
        self.achievementId = achievement.achievementID;
        self.points = achievement.points;
    }
    return self;
}

/*! @brief Get offer object for achievement.
 *  @return Achievement object.*/
- (LTOfferAchievements *)offerObject {
    NSArray *arrayAchievement = [LTManager sharedManager].data.app.allAchievements;
    for (LTOfferAchievements *achievement in arrayAchievement) {
        if (achievement.idAchievement == self.achievementId) {
            return achievement;
        }
    }
    return nil;
}

- (NSString *)name {
    if (self.achievementId != 0) {
        return [self offerObject].title;
    }
    return _name;
}

- (NSString *)desc {
    if (self.achievementId != 0) {
        return [self offerObject].desc;
    }
    return _desc;
}

-(LTAchievementDifficulty)difficulty {
    if (self.achievementId != 0) {
        return [self offerObject].difficulty;
    }
    return _difficulty;
}

@end